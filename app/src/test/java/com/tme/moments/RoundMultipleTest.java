package com.tme.moments;

import com.tme.moments.math.MathUtils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author zoopolitic.
 */
public class RoundMultipleTest {

    @Test
    public void previousMultipleTest() throws Exception {
        assertEquals(1264, MathUtils.roundToPreviousNumberByMultiple(1279, 16));
        assertEquals(1280, MathUtils.roundToPreviousNumberByMultiple(1280, 16));
        assertEquals(0, MathUtils.roundToPreviousNumberByMultiple(0, 16));
        assertEquals(16, MathUtils.roundToPreviousNumberByMultiple(16, 16));
        assertEquals(640, MathUtils.roundToPreviousNumberByMultiple(655, 16));
        assertEquals(32, MathUtils.roundToPreviousNumberByMultiple(33, 16));
        assertEquals(32, MathUtils.roundToPreviousNumberByMultiple(34, 16));
        assertEquals(32, MathUtils.roundToPreviousNumberByMultiple(35, 16));
        assertEquals(32, MathUtils.roundToPreviousNumberByMultiple(36, 16));
    }
}
