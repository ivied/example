uniform mat4 uTransformM;
uniform mat4 uOrientationM;
attribute vec2 aPosition;
attribute vec2 aTexCoords;

varying vec2 vTextureCoord;

void main(){
	gl_Position =  vec4(aPosition, 0.0, 1.0);
   //camera texcoord needs to be manipulated by the transform given back from the system
    vTextureCoord = (uTransformM  * vec4(aTexCoords, 0.0, 1.0)).xy;
}