precision highp float;
//precision highp int;

varying vec2 v_texCoords;
uniform vec2 u_heightMapSize;
uniform float u_steadyRippleHeight;

uniform sampler2D u_texture;
uniform sampler2D u_heightMap;

void main() {
    float offsetX = 1.0 / u_heightMapSize.x;
    float offsetY = 1.0 / u_heightMapSize.y;

    vec4 height1 = texture2D(u_heightMap, v_texCoords + vec2(offsetX, offsetY));
    vec4 height2 = texture2D(u_heightMap, v_texCoords + vec2(0.0, -offsetY));
    vec4 height3 = texture2D(u_heightMap, v_texCoords + vec2(offsetX, -offsetY));
    vec4 height4 = texture2D(u_heightMap, v_texCoords + vec2(-offsetX, 0.0));
    vec4 height5 = texture2D(u_heightMap, v_texCoords);
    vec4 height6 = texture2D(u_heightMap, v_texCoords + vec2(offsetX, 0.0));
    vec4 height7 = texture2D(u_heightMap, v_texCoords + vec2(-offsetX, -offsetY));
    vec4 height8 = texture2D(u_heightMap, v_texCoords + vec2(0.0, offsetY));
    vec4 height9 = texture2D(u_heightMap, v_texCoords + vec2(-offsetX, offsetY));

    vec4 height = height1 + height2 + height3 + height4 + height5 + height6 + height7 + height8 + height9;

    height /= 9.0;

    float a = height.a;
	float r = height.r * 255.0;
	float g = height.g * 255.0;
	float b = height.b * 255.0;

	float iPixHeight = g * 256.0 + b - 10.0;

    float pixHeight = u_steadyRippleHeight - iPixHeight;

    float centerX = 0.0;
    float centerY = 0.0;

    // 0.0009765 just adjusted value, no logic
    float xOffset = (v_texCoords.x - centerX) * pixHeight * 0.0009765 + centerX;
    float yOffset = (v_texCoords.y - centerY) * pixHeight * 0.0009765 + centerY;

    xOffset = clamp(xOffset, 0.0, 1.0);
    yOffset = clamp(yOffset, 0.0, 1.0);

    gl_FragColor = texture2D(u_texture, vec2(xOffset, yOffset));
}