package com.tme.moments.ffmpeg;

/**
 * @author zoopolitic.
 */
public class FFmpeg {

    static {
        System.loadLibrary("moments");
    }

    public native int convert(String input, String output) throws Exception;
}
