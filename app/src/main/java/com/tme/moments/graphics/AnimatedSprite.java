package com.tme.moments.graphics;

import android.util.Log;

import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.Const;

import java.util.List;

/**
 * @author zoopolitic.
 */
public abstract class AnimatedSprite extends Sprite {

    public static class FrameInfo {

        public final int index;
        public final int regionX;
        public final int regionY;

        public FrameInfo(int index, int regionX, int regionY) {
            this.index = index;
            this.regionX = regionX;
            this.regionY = regionY;
        }
    }

    protected List<FrameInfo> frames;
    protected final long updateIntervalNanos;
    protected long updateTimeCounterNanos;
    protected int currentFrameIndex;

    public AnimatedSprite(TextureRegion region, List<FrameInfo> frames, float width, float height) {
        super(region, width, height);
        this.frames = frames;
        updateIntervalNanos = 1000 / getDesiredFPS() * Const.NANOS_IN_MILLISECOND;
        updateTimeCounterNanos = updateIntervalNanos;
    }

    protected void setCurrentFrameIndex(int currentFrameIndex) {
        this.currentFrameIndex = currentFrameIndex;
    }

    protected void onAnimationReset() {
    }

    protected int getDesiredFPS() {
        return 25;
    }

    public void step(long deltaNanos) {
        // last frame of animation was drawn - reset
        if (currentFrameIndex == frames.size() - 1) {
            changeRegion(currentFrameIndex);
            updateTimeCounterNanos -= deltaNanos;
            if (updateTimeCounterNanos <= 0) {
                currentFrameIndex = 0;
                changeRegion(currentFrameIndex = 0);
                updateTimeCounterNanos = updateIntervalNanos;
                onAnimationReset();
            }
        } else {
            updateTimeCounterNanos -= deltaNanos;
            if (updateTimeCounterNanos <= 0) {
                updateTimeCounterNanos = updateIntervalNanos;
                changeRegion(currentFrameIndex++);
            }
        }
        update();
    }

    protected void changeRegion(int index) {
        TextureRegion region = getTextureRegion();
        FrameInfo info = frames.get(index);
        region.setX(info.regionX);
        region.setY(info.regionY);
        setTextureRegion(region);
    }
}
