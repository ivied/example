package com.tme.moments.graphics.g3d;

import com.tme.moments.graphics.TextureRegion;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.GL_UNSIGNED_BYTE;
import static android.opengl.GLES20.GL_UNSIGNED_SHORT;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glDrawElements;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glVertexAttribPointer;

/**
 * @author zoopolitic.
 */
public class Sprite extends Decal {

    private FloatBuffer vertexBuffer;
    private ShortBuffer indexBuffer;

    public Sprite(TextureRegion textureRegion, float width, float height) {
        setDimensions(width, height);
        setTextureRegion(textureRegion);
        setColor(1, 1, 1, 1);

        vertexBuffer = ByteBuffer
                .allocateDirect(Decal.SIZE * 4) // 4 bytes per float
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexBuffer.position(0);

        short[] indices = new short[]{0, 2, 1, 1, 2, 3};
        indexBuffer = ByteBuffer
                .allocateDirect(indices.length * Short.SIZE / 8) // index size in bytes
                .order(ByteOrder.nativeOrder())
                .asShortBuffer();
        indexBuffer.put(indices, 0, 6).position(0);
    }

    public void draw(int uProjectionMatrixLoc, float[] projectionMatrix, int aPositionHandle,
                     int aTexCoordHandle, int aColorHandle) {
        glUniformMatrix4fv(uProjectionMatrixLoc, 1, false, projectionMatrix, 0);
        glBindTexture(GL_TEXTURE_2D, textureRegion.getTextureHandle());
        update();
        vertexBuffer.clear();
        vertexBuffer.put(vertices, 0, vertices.length);
        vertexBuffer.flip();
        int stride = Decal.VERTEX_SIZE * DecalBatch.SIZE_OF_FLOAT;

        vertexBuffer.position(0);
        glVertexAttribPointer(aPositionHandle, 3, GL_FLOAT, false, stride, vertexBuffer);

        if (aColorHandle > -1) {
            vertexBuffer.position(3);
            glVertexAttribPointer(aColorHandle, 4, GL_UNSIGNED_BYTE, true, stride, vertexBuffer);
        }

        vertexBuffer.position(4);
        glVertexAttribPointer(aTexCoordHandle, 2, GL_FLOAT, false, stride, vertexBuffer);

        int count = 6;
        glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_SHORT, indexBuffer);
    }
}
