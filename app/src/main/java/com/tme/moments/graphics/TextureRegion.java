package com.tme.moments.graphics;

public class TextureRegion {

    public float u1, v1; // top, left
    public float u2, v2; // bottom, right

    private final int textureHandle;
    private final int textureWidth;
    private final int textureHeight;
    private final int regionWidthPixels;
    private final int regionHeightPixels;

    /**
     * calculate U,V coordinates from specified texture coordinates
     *
     * @param textureHandle      texture handle for shader
     * @param texWidthPixels     the width of the texture the region is for (in pixels)
     * @param texHeightPixels    the height of the texture the region is for (in pixels)
     * @param x                  left of the region on the texture (in pixels)
     * @param y                  top of the region on the texture (in pixels)
     * @param regionWidthPixels  width of the region on the texture (in pixels)
     * @param regionHeightPixels height of the region on the texture (in pixels)
     */
    public TextureRegion(int textureHandle, int texWidthPixels, int texHeightPixels, float x,
                         float y, int regionWidthPixels, int regionHeightPixels) {
        this.textureHandle = textureHandle;
        this.textureWidth = texWidthPixels;
        this.textureHeight = texHeightPixels;
        this.regionWidthPixels = regionWidthPixels;
        this.regionHeightPixels = regionHeightPixels;
        setX(x);
        setY(y);
    }

    public void setX(float x) {
        u1 = (x + 0.5f) / textureWidth;
        u2 = u1 + ((regionWidthPixels - 0.5f) / textureWidth);
    }

    public void setY(float y) {
        v1 = (y + 0.5f) / textureHeight;
        v2 = v1 + ((regionHeightPixels - 0.5f) / textureHeight);
    }

    public int getTextureHandle() {
        return textureHandle;
    }

    public int getRegionWidthPixels() {
        return regionWidthPixels;
    }

    public int getRegionHeightPixels() {
        return regionHeightPixels;
    }
}
