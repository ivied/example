/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.tme.moments.graphics.g3d;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.GL_UNSIGNED_BYTE;
import static android.opengl.GLES20.GL_UNSIGNED_SHORT;
import static android.opengl.GLES20.glDrawElements;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glVertexAttribPointer;

/**
 * <p>
 * Renderer for {@link Decal} objects.
 * </p>
 * <p>
 * New objects are added using {@link DecalBatch#add(Decal)}, there is no limit on how many decals can be added.<br/>
 * Once all the decals have been submitted a call to {@link DecalBatch#flush()} will batch them together and send big chunks of
 * geometry to the GL.
 * </p>
 * <p>
 * The size of the batch specifies the maximum number of decals that can be batched together before they have to be submitted to
 * the graphics pipeline. The default size is {@link DecalBatch#DEFAULT_SIZE}. If it is known before hand that not as many will be
 * needed on average the batch can be downsized to save memory. If the game is basically 3d based and decals will only be needed
 * for an orthogonal HUD it makes sense to tune the size down.
 * </p>
 * <p>
 * The way the batch handles things depends on the {@link GroupStrategy}. Different strategies can be used to customize shaders,
 * states, culling etc. for more details see the {@link GroupStrategy} java doc.<br/>
 * While it shouldn't be necessary to change strategies, if you have to do so, do it before calling {@link #add(Decal)}, and if
 * you already did, call {@link #flush()} first.
 * </p>
 */
public class DecalBatch {

    public static final int INDICES_PER_SPRITE = 6;
    public static final int INDEX_SIZE = Short.SIZE / 8;
    public static final int SIZE_OF_FLOAT = Float.SIZE / 8;

    private float[] vertices;

    List<Decal> decals = new ArrayList<>();

    private FloatBuffer vertexBuffer;
    private ShortBuffer indexBuffer;

    private int aPositionLocation;
    private int aColorLocation;
    private int aTexCoordLocation;

    public DecalBatch(int aPositionLocation, int aTexCoordLocation, int aColorLocation) {
        this.aPositionLocation = aPositionLocation;
        this.aColorLocation = aColorLocation;
        this.aTexCoordLocation = aTexCoordLocation;
    }

    /**
     * Initializes the batch with the given amount of decal objects the buffer is able to hold when full.
     *
     * @param size Maximum size of decal objects to hold in memory
     */
    public void initialize(int size) {
        vertices = new float[size * Decal.SIZE];

        vertexBuffer = ByteBuffer
                .allocateDirect(vertices.length * SIZE_OF_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();

        short[] indices = new short[size * INDICES_PER_SPRITE];
        int v = 0;
        int len = indices.length;
        for (int i = 0; i < len; i += INDICES_PER_SPRITE, v += 4) {
            //noinspection PointlessArithmeticExpression
            indices[i + 0] = (short) (v + 0);
            indices[i + 1] = (short) (v + 2);
            indices[i + 2] = (short) (v + 1);
            indices[i + 3] = (short) (v + 1);
            indices[i + 4] = (short) (v + 2);
            indices[i + 5] = (short) (v + 3);
        }

        indexBuffer = ByteBuffer
                .allocateDirect(size * INDICES_PER_SPRITE * INDEX_SIZE)
                .order(ByteOrder.nativeOrder())
                .asShortBuffer();
        indexBuffer.put(indices, 0, len);
        indexBuffer.position(0);
    }

    /**
     * @return maximum amount of decal objects this buffer can hold in memory
     */
    public int getSize() {
        return vertices.length / Decal.SIZE;
    }

    public int getDecalCount() {
        return decals == null ? 0 : decals.size();
    }

    /**
     * Add a decal to the batch, marking it for later rendering
     *
     * @param decal Decal to add for rendering
     */
    public void add(Decal decal) {
        decals.add(decal);
    }

    /**
     * Remove a decal from the batch
     *
     * @param decal Decal to remove for rendering
     */
    public void remove(Decal decal) {
        decals.remove(decal);
    }

    /**
     * Remove list of decals from the batch
     *
     * @param decalsToRemove list of decals to remove
     */
    public void removeAll(List<? extends Decal> decalsToRemove) {
        decals.removeAll(decalsToRemove);
    }

    /**
     * Add a decals list to the batch, marking them for later rendering
     *
     * @param decals List of decals to add for rendering
     */
    public void addAll(List<? extends Decal> decals) {
        this.decals.addAll(decals);
    }

    /**
     * Renders a group of vertices to the buffer, flushing them to GL when done/full
     */
    public void render(int uProjectionMatrixLoc, float[] projectionMatrix) {
        int idx = 0;
        glUniformMatrix4fv(uProjectionMatrixLoc, 1, false, projectionMatrix, 0);
        for (Decal decal : decals) {
            decal.update();
            System.arraycopy(decal.vertices, 0, vertices, idx, decal.vertices.length);
            idx += decal.vertices.length;
            // if our batch is full we have to flush it
            if (idx == vertices.length) {
                flush(idx);
                idx = 0;
            }
        }
        // at the end if there is stuff left in the batch we render that
        if (idx > 0) {
            flush(idx);
        }
    }

    /**
     * Flushes vertices[0,verticesPosition[ to GL verticesPosition % Decal.SIZE must equal 0
     *
     * @param idx Amount of elements from the vertices array to flush
     */
    private void flush(int idx) {
        int spritesInBatch = idx / 24;
        int count = spritesInBatch * 6;

        vertexBuffer.clear();
        vertexBuffer.put(vertices, 0, idx);
        vertexBuffer.flip();
        int stride = Decal.VERTEX_SIZE * SIZE_OF_FLOAT;

        vertexBuffer.position(0);
        glVertexAttribPointer(aPositionLocation, 3, GL_FLOAT, false, stride, vertexBuffer);

        vertexBuffer.position(3);
        glVertexAttribPointer(aColorLocation, 4, GL_UNSIGNED_BYTE, true, stride, vertexBuffer);

        vertexBuffer.position(4);
        glVertexAttribPointer(aTexCoordLocation, 2, GL_FLOAT, false, stride, vertexBuffer);

        glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_SHORT, indexBuffer);
    }

    /**
     * Remove all decals from batch
     */
    protected void clear() {
        decals.clear();
    }

    /**
     * Frees up memory by dropping the buffer and underlying resources. If the batch is needed again after disposing it can be
     * {@link #initialize(int) initialized} again.
     */
    public void dispose() {
        clear();
        vertices = null;
    }
}
