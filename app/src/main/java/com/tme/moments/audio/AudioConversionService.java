package com.tme.moments.audio;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.tme.moments.ffmpeg.FFmpeg;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

/**
 * @author zoopolitic.
 */
public class AudioConversionService extends IntentService {

    private static final String TAG = AudioConversionService.class.getSimpleName();

    private static final String EXTRA_INPUT_PATH = "EXTRA_INPUT_PATH";
    private static final String EXTRA_OUTPUT_PATH = "EXTRA_OUTPUT_PATH";

    public static void start(Context context, @NonNull String inputPath,
                             @NonNull String outputPath) {
        Intent intent = new Intent(context, AudioConversionService.class);
        intent.putExtra(EXTRA_INPUT_PATH, inputPath);
        intent.putExtra(EXTRA_OUTPUT_PATH, outputPath);
        context.startService(intent);
    }

    public AudioConversionService() {
        super(AudioConversionService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String inputPath = intent.getStringExtra(EXTRA_INPUT_PATH);
        if (TextUtils.isEmpty(inputPath)) {
            String errorMessage = "Input file not specified. Abort";
            Timber.e(errorMessage);
            return;
        }
        String outputPath = intent.getStringExtra(EXTRA_OUTPUT_PATH);
        if (TextUtils.isEmpty(outputPath)) {
            String errorMessage = "Output file not specified. Abort";
            Timber.e(errorMessage);
            return;
        }
        FFmpeg fFmpeg = new FFmpeg();
        try {
            EventBus.getDefault().post(new ConversionStartedEvent());
            long startTime = System.currentTimeMillis();
            Timber.w( "Starting conversion");
            int result = fFmpeg.convert(inputPath, outputPath);
            long conversionTime = System.currentTimeMillis() - startTime;
            long minutes = TimeUnit.MILLISECONDS.toMinutes(conversionTime);
            long seconds = TimeUnit.MILLISECONDS.toSeconds(conversionTime) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(conversionTime));
            Locale locale = Locale.getDefault();
            String conversionTimeStr = String.format(locale, "%02d min, %02d sec", minutes, seconds);
            EventBus.getDefault().post(ConversionFinishedEvent.create(outputPath));
            Timber.w(String.format(locale, "Conversion finished in %s with result = %d", conversionTimeStr, result));
        } catch (Exception e) {
            e.printStackTrace();
            Timber.e(e, e.getLocalizedMessage());
        }
    }

}