package com.tme.moments.audio;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

/**
 * @author zoopolitic.
 */
@AutoValue
public abstract class ConversionFinishedEvent {

    public static ConversionFinishedEvent create(@NonNull String convertedFilePath){
        return new AutoValue_ConversionFinishedEvent(convertedFilePath);
    }

    public abstract String convertedFilePath();
}
