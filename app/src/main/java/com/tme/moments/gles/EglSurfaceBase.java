/*
 * Copyright 2013 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tme.moments.gles;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.opengl.EGL14;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.os.Build;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import timber.log.Timber;

/**
 * Common base class for EGL surfaces.
 * <p>
 * There can be multiple surfaces associated with a single context.
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class EglSurfaceBase {

    protected static final String TAG = EglSurfaceBase.class.getSimpleName();

    private final Object lock = new Object();

    // EglCore object we're associated with.  It may be associated with multiple surfaces.
    protected EglCore eglCore;

    private EGLSurface EGLSurface = EGL14.EGL_NO_SURFACE;
    private int width = -1;
    private int height = -1;

    protected EglSurfaceBase(EglCore eglCore) {
        this.eglCore = eglCore;
    }

    /**
     * Creates a window surface.
     * <p>
     *
     * @param surface May be a Surface or SurfaceTexture.
     */
    public void createWindowSurface(Object surface) {
        if (EGLSurface != EGL14.EGL_NO_SURFACE) {
            throw new IllegalStateException("surface already created");
        }
        EGLSurface = eglCore.createWindowSurface(surface);
    }

    /**
     * Creates an off-screen surface.
     */
    public void createOffscreenSurface(int width, int height) {
        if (EGLSurface != EGL14.EGL_NO_SURFACE) {
            throw new IllegalStateException("surface already created");
        }
        EGLSurface = eglCore.createOffscreenSurface(width, height);
        this.width = width;
        this.height = height;
    }

    public boolean isCurrent() {
        return eglCore.isCurrent(EGLSurface);
    }

    /**
     * Returns the surface's width, in pixels.
     * <p>
     * If this is called on a window surface, and the underlying surface is in the process
     * of changing size, we may not see the new size right away (e.g. in the "setup"
     * callback).  The size should match after the next buffer swap.
     */
    public int getWidth() {
        if (width < 0) {
            return eglCore.querySurface(EGLSurface, EGL14.EGL_WIDTH);
        } else {
            return width;
        }
    }

    /**
     * Returns the surface's height, in pixels.
     */
    public int getHeight() {
        if (height < 0) {
            return eglCore.querySurface(EGLSurface, EGL14.EGL_HEIGHT);
        } else {
            return height;
        }
    }

    /**
     * Release the EGL surface.
     */
    public void releaseEglSurface() {
        Log.i(TAG, " release EGL surface");
        synchronized (lock) {
            eglCore.releaseSurface(EGLSurface);
            EGLSurface = EGL14.EGL_NO_SURFACE;
            width = height = -1;
        }
    }

    /**
     * Makes our EGL context and surface current.
     */
    public void makeCurrent() {
        eglCore.makeCurrent(EGLSurface);
    }

    /**
     * Makes our EGL context and surface current for drawing, using the supplied surface
     * for reading.
     */
    public void makeCurrentReadFrom(EglSurfaceBase readSurface) {
        eglCore.makeCurrent(EGLSurface, readSurface.EGLSurface);
    }

    /**
     * Calls eglSwapBuffers.  Use this to "publish" the current frame.
     *
     * @return false on failure
     */
    public boolean swapBuffers() {
        boolean result = eglCore.swapBuffers(EGLSurface);
        if (!result) {
            Timber.e("WARNING: swapBuffers() failed");
        }
        return result;
    }

    /**
     * Sends the presentation time stamp to EGL.
     *
     * @param nsecs Timestamp, in nanoseconds.
     */
    public void setPresentationTime(long nsecs) {
        eglCore.setPresentationTime(EGLSurface, nsecs);
    }

    /**
     * Saves the EGL surface to a file.
     * <p>
     * Expects that this object's EGL surface is current.
     */
    public void saveFrame(File file) throws IOException {
        if (!eglCore.isCurrent(EGLSurface)) {
            throw new RuntimeException("Expected EGL context/surface is not current");
        }

        // glReadPixels fills in a "direct" ByteBuffer with what is essentially big-endian RGBA
        // data (i.e. a byte of red, followed by a byte of green...).  While the Bitmap
        // constructor that takes an int[] wants little-endian ARGB (blue/red swapped), the
        // Bitmap "copy pixels" method wants the same format GL provides.
        //
        // Ideally we'd have some way to re-use the ByteBuffer, especially if we're calling
        // here often.
        //
        // Making this even more interesting is the upside-down nature of GL, which means
        // our output will look upside down relative to what appears on screen if the
        // typical GL conventions are used.

        String filename = file.toString();

        int width = getWidth();
        int height = getHeight();
        ByteBuffer buf = ByteBuffer.allocateDirect(width * height * 4);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        GLES20.glReadPixels(0, 0, width, height, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buf);
        GlUtil.checkGlError("glReadPixels");
        buf.rewind();

        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(filename));
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bmp.copyPixelsFromBuffer(buf);

            // rotate and preScale (because source is mirrored) bitmap before save
            Matrix matrix = new Matrix();
            matrix.preScale(-1, 1);
            matrix.postRotate(180);
            bmp = Bitmap.createBitmap(bmp, 0, 0, width, height, matrix, true);

            bmp.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bmp.recycle();
        } finally {
            if (bos != null) bos.close();
        }
        Log.d(TAG, "Saved " + width + "x" + height + " frame as '" + filename + "'");
    }
}
