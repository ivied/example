package com.tme.moments.recording;

/**
 * @author zoopolitic.
 */
public enum Quality {

    LOW(2_500_000), MEDIUM(5_000_000), HIGH(8_000_000);

    public final int bitRate;

    Quality(int bitRate) {
        this.bitRate = bitRate;
    }
}
