package com.tme.moments.recording;

import android.annotation.TargetApi;
import android.graphics.Rect;
import android.media.MediaMuxer;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.tme.moments.gles.EglCore;
import com.tme.moments.gles.EglSurfaceBase;
import com.tme.moments.gles.WindowSurface;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Sprite;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import timber.log.Timber;

/**
 * @author zoopolitic.
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public abstract class AbsRecorder implements Recorder, AndroidMuxer.MuxerListener {

    private static final boolean VERBOSE = false;

    protected static final String TAG = AbsRecorder.class.getSimpleName();

    private enum AudioRecordMode {
        MICROPHONE, AUDIO, NONE
    }

    @Nullable
    private RecordConfig config;

    private volatile boolean recordingEnabled;
    private VideoRecord markedVideoRecord;
    private VideoRecord unmarkedVideoRecord;
    private Rect videoRect;

    private int watermarkHandle;
    private float watermarkMargin;
    private float watermarkWidth;
    private float watermarkHeight;
    protected Sprite watermark;

    private int windowWidth;
    private int windowHeight;

    private EglCore eglCore;

    private RecordListener recordListener;

    AbsRecorder() {
        this.videoRect = new Rect();
    }

    /**
     * Setup watermark handle and margin for further creation. We should create separate
     * watermark for video since video width and height may be different from the surface
     * width and height. For example video width/height should be multiple of 16 to be sure
     * that encoder will work correctly on every device, but surface width and height can
     * be arbitrary. Since we adding watermark both to video and captured screenshot - we
     * need to use different watermarks to scale it appropriately for each case
     *
     * @param wmHandle openGL handle for watermark
     * @param wmWidth  watermark width
     * @param wmHeight watermark height
     * @param wmMargin watermark margin from borders
     */
    @Override
    public void setupWatermark(int wmHandle, float wmWidth, float wmHeight, float wmMargin) {
        this.watermarkHandle = wmHandle;
        this.watermarkWidth = wmWidth;
        this.watermarkHeight = wmHeight;
        this.watermarkMargin = wmMargin;
    }

    /**
     * Initializes recorder
     *
     * @param eglCore      EGL core
     * @param windowWidth  window width
     * @param windowHeight window height
     */
    public void init(EglCore eglCore, int windowWidth, int windowHeight) {
        this.eglCore = eglCore;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
    }

    @Override
    public void attachListener(RecordListener recordListener) {
        this.recordListener = recordListener;
    }

    /**
     * Creates the video encoder object and starts the encoder thread.  Creates an EGL
     * surface for encoder input.
     */
    private boolean startRecording() {
        if (config == null) {
            throw new IllegalStateException("Record config is null");
        }
        AudioRecordMode mode = identifyAudioRecordMode(config);
        if (VERBOSE) Log.d(TAG, "Start recording. Output file: " + config.markedOutputPath());
        final int bitRate = config.bitRate();
        final int frameRate = config.frameRate();
        final int iFrameInterval = config.iFrameInterval();
        final int videoWidth = config.videoWidth();
        final int videoHeight = config.videoHeight();
        float windowAspect = (float) windowHeight / (float) windowWidth;
        int outWidth, outHeight;
        if (videoHeight > videoWidth * windowAspect) {
            // limited by narrow width; reduce height
            outWidth = videoWidth;
            outHeight = (int) (videoWidth * windowAspect);
        } else {
            // limited by short height; restrict width
            outHeight = videoHeight;
            outWidth = (int) (videoHeight / windowAspect);
        }
        int offX = (videoWidth - outWidth) / 2;
        int offY = (videoHeight - outHeight) / 2;
        videoRect.set(offX, offY, offX + outWidth, offY + outHeight);

        if (watermarkHandle > -1) {
            watermark = prepareWatermark(watermarkHandle, watermarkMargin);
        }

        try {
            AndroidMuxer markedMuxer = new AndroidMuxer(
                    config.markedOutputPath(),
                    MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4
            );
            markedMuxer.setListener(this);
            markedMuxer.setVideoEncoder(new VideoEncoder(
                    Collections.singletonList(markedMuxer),
                    videoWidth, videoHeight,
                    frameRate, bitRate, iFrameInterval)
            );
            AndroidMuxer unmarkeMuxer = new AndroidMuxer(
                    config.unmarkedOutputPath(),
                    MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4
            );
            unmarkeMuxer.setVideoEncoder(new VideoEncoder(
                    Collections.singletonList(unmarkeMuxer),
                    videoWidth, videoHeight,
                    frameRate, bitRate, iFrameInterval)
            );

            switch (mode) {
                case MICROPHONE:
                    MicEncoder micEncoder = new MicEncoder(Arrays.asList(markedMuxer, unmarkeMuxer));
                    markedMuxer.setAudioEncoder(micEncoder);
                    unmarkeMuxer.setAudioEncoder(micEncoder);
                    break;
                case AUDIO:
                    MusicEncoder musicEncoder = new MusicEncoder(Arrays.asList(markedMuxer, unmarkeMuxer), config);
                    musicEncoder.setPlayListener(audioTimeMillis -> {
                        if (recordListener != null) {
                            recordListener.onAudioPositionChanged(audioTimeMillis);
                        }
                    });
                    markedMuxer.setAudioEncoder(musicEncoder);
                    unmarkeMuxer.setAudioEncoder(musicEncoder);
                    break;
                case NONE:
                    // not recording sound
                    break;
            }

            if (!markedMuxer.prepare()) {
                return false;
            }
            if (!unmarkeMuxer.prepare()) {
                return false;
            }

            markedVideoRecord = new VideoRecord(markedMuxer, eglCore);
            unmarkedVideoRecord = new VideoRecord(unmarkeMuxer, eglCore);
            markedMuxer.startRecording();
            unmarkeMuxer.startRecording();
        } catch (IOException e) {
            e.printStackTrace();
            Timber.e(e, null);
            return false;
        }
        return true;
    }

    private Sprite prepareWatermark(int textureHandle, float watermarkMargin) {
        int texWidth = 357;
        int texHeight = 81;
        TextureRegion region = new TextureRegion(textureHandle, texWidth, texHeight, 0, 0, texWidth, texHeight);
        Sprite watermark = new Sprite(region, watermarkWidth, watermarkHeight);
        float x = 1 - watermarkWidth / 2 - watermarkMargin; // 1 - is a right of the screen
        float y = -1 + watermarkHeight / 2 + watermarkMargin; // -1 is a bottom of the screen
        float z = -1;
        watermark.setPosition(x, y, z);
        return watermark;
    }

    private AudioRecordMode identifyAudioRecordMode(@NonNull RecordConfig configuration) {
        boolean audioEnabled = configuration.soundEnable();
        String audioFilePath = configuration.audioFilePath();
        return audioFilePath != null ? AudioRecordMode.AUDIO : audioEnabled
                ? AudioRecordMode.MICROPHONE
                : AudioRecordMode.NONE;
    }

    @Override
    public void onMuxerStarted() {
        if (recordListener != null) {
            recordListener.onRecordStarted();
        }
    }

    @Override
    public void onMuxerStopped() {
        if (recordListener != null) {
            recordListener.onRecordFinished();
        }
    }

    @Override
    public boolean isRecordingEnabled() {
        return recordingEnabled;
    }

    /**
     * Updates the recording state.  Stops or starts recording as needed.
     */
    @Override
    public void setRecordingEnabled(boolean enabled) {
        if (enabled == recordingEnabled) {
            return;
        }
        if (enabled) {
            if (startRecording()) {
                recordingEnabled = true;
            }
        } else {
            stopRecording();
            recordingEnabled = false;
        }
    }

    /**
     * Stops the video encoder if it's running.
     */
    private synchronized void stopRecording() {
        if (markedVideoRecord != null) {
            markedVideoRecord.getAndroidMuxer().stopRecording();
            markedVideoRecord.getRecordSurface().release();
            markedVideoRecord = null;
        }
        if (unmarkedVideoRecord != null) {
            unmarkedVideoRecord.getAndroidMuxer().stopRecording();
            unmarkedVideoRecord.getRecordSurface().release();
            unmarkedVideoRecord = null;
        }
    }

    @Override
    public void setConfig(@Nullable RecordConfig config) {
        this.config = config;
    }

    @Override
    public void record(EglSurfaceBase windowSurface, long timeStampNanos, int uProjectionMatrixLoc, int aPositionHandle, int aTexCoordHandle) {
        if (!recordingEnabled) {
            return;
        }
        if (unmarkedVideoRecord != null) {
            prepareToRecord(
                    unmarkedVideoRecord.getAndroidMuxer().getVideoEncoder(),
                    unmarkedVideoRecord.getRecordSurface()
            );
            record(
                    unmarkedVideoRecord.getRecordSurface(),
                    videoRect,
                    uProjectionMatrixLoc, aPositionHandle, aTexCoordHandle
            );
            finishRecord(
                    unmarkedVideoRecord.getRecordSurface(),
                    timeStampNanos
            );
        }
        if (markedVideoRecord != null) {
            prepareToRecord(
                    markedVideoRecord.getAndroidMuxer().getVideoEncoder(),
                    markedVideoRecord.getRecordSurface()
            );
            record(
                    markedVideoRecord.getRecordSurface(),
                    videoRect,
                    uProjectionMatrixLoc, aPositionHandle, aTexCoordHandle
            );
            recordWatermark(
                    watermark,
                    markedVideoRecord.getRecordSurface(),
                    videoRect,
                    uProjectionMatrixLoc, aPositionHandle, aTexCoordHandle
            );
            finishRecord(
                    markedVideoRecord.getRecordSurface(),
                    timeStampNanos
            );
        }
    }

    public abstract void prepareToRecord(VideoEncoder videoEncoder, WindowSurface recordSurface);

    public abstract void record(WindowSurface recordSurface, Rect videoRect,
                                int uProjectionMatrixLoc, int aPositionHandle,
                                int aTexCoordHandle);

    public abstract void recordWatermark(Sprite watermark,
                                         WindowSurface recordSurface, Rect videoRect,
                                         int uProjectionMatrixLoc, int aPositionHandle,
                                         int aTexCoordHandle
    );

    public abstract void finishRecord(WindowSurface recordSurface, long timeStampNanos);
}
