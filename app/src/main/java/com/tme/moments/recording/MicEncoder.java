package com.tme.moments.recording;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaRecorder;
import android.util.Log;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import timber.log.Timber;

/**
 * @author zoopolitic.
 */
public class MicEncoder extends MediaEncoder {

    private static final boolean VERBOSE = false;

    private static final String TAG = MicEncoder.class.getSimpleName();

    private static final String MIME_TYPE = "audio/mp4a-latm";
    // 44.1[KHz] is only setting guaranteed to be available on all devices.
    private static final int SAMPLE_RATE = 44100;
    private static final int BIT_RATE = 64000;
    private static final int SAMPLES_PER_FRAME = 1024;

    private AudioRecord audioRecord;
    private boolean prepared = false;

    MicEncoder(final List<AndroidMuxer> muxers) {
        super(muxers);
    }

    @Override
    boolean prepare() {
        if (prepared) return true;
        try {
            if (VERBOSE) Log.v(TAG, "prepare:");
            muxersStarted = isEOS = false;

            final MediaFormat format = MediaFormat.createAudioFormat(MIME_TYPE, SAMPLE_RATE, 1);
            format.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);
            format.setInteger(MediaFormat.KEY_CHANNEL_MASK, AudioFormat.CHANNEL_IN_MONO);
            format.setInteger(MediaFormat.KEY_BIT_RATE, BIT_RATE);
            format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, 1);
            if (VERBOSE) Log.i(TAG, "format: " + format);
            encoder = MediaCodec.createEncoderByType(MIME_TYPE);
            Timber.d("Create encoder %s by %s", encoder.getName(), MIME_TYPE);
            encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            encoder.start();
            //Save encoder in temporary variable to avoid drain() before codec started
            this.encoder = encoder;
            if (VERBOSE) Log.i(TAG, "prepare finishing");
            prepared = true;
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * encoding loop on private thread
     */
    @Override
    public void run() {
        synchronized (lock) {
            requestStop = false;
            lock.notify();
        }
        final int minBufferSize = AudioRecord.getMinBufferSize(
                SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

        audioRecord = new AudioRecord(
                MediaRecorder.AudioSource.CAMCORDER, // source
                SAMPLE_RATE,                         // sample rate, hz
                AudioFormat.CHANNEL_IN_MONO,         // channels
                AudioFormat.ENCODING_PCM_16BIT,      // audio format
                minBufferSize * 4);                  // buffer size (bytes)

        boolean localRequestStop;
        audioRecord.startRecording();
        while (true) {
            synchronized (lock) {
                localRequestStop = requestStop;
            }
            if (localRequestStop) {
                drain();
                // request stop recording
                encode(true);
                // process output data again for EOS signal
                drain();
                // release all related objects
                release();
                break;
            } else {
                encode(false);
                synchronized (lock) {
                    if (!running || requestStop) {
                        continue;
                    }
                    drain();
                    lock.notifyAll();
                }
            }
        } // end of while
        if (VERBOSE) Log.d(TAG, "Encoder thread exiting");
        synchronized (lock) {
            requestStop = true;
            running = false;
        }
    }

    @Override
    protected void release() {
        super.release();
        audioRecord.stop();
        audioRecord.release();
    }

    private void encode(boolean endOfStream) {
        if (!running) return;
        final ByteBuffer[] inputBuffers = encoder.getInputBuffers();
        final int inputBufferIndex = encoder.dequeueInputBuffer(-1);
        if (inputBufferIndex >= 0) {
            final ByteBuffer inputBuffer = inputBuffers[inputBufferIndex];
            inputBuffer.clear();
            int readBytes = audioRecord.read(inputBuffer, SAMPLES_PER_FRAME * 2);
            long presentationTimeUs = getPTSUs();
            prevOutputPTSUs = presentationTimeUs;
            if (readBytes == AudioRecord.ERROR_INVALID_OPERATION) {
                if (VERBOSE) Timber.e("Audio read error: invalid operation");
            }
            if (readBytes == AudioRecord.ERROR_BAD_VALUE) {
                if (VERBOSE) Timber.e("Audio read error: bad value");
            }
            if (endOfStream) {
                // send EOS
                isEOS = true;
                encoder.queueInputBuffer(inputBufferIndex, 0, readBytes,
                        presentationTimeUs, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
            } else {
                encoder.queueInputBuffer(inputBufferIndex, 0, readBytes,
                        presentationTimeUs, 0);
            }
        }
    }
}
