package com.tme.moments.recording;

import com.tme.moments.gles.EglCore;
import com.tme.moments.gles.EglSurfaceBase;

/**
 * @author zoopolitic.
 */
public interface Recorder {

    void init(EglCore eglCore, int windowWidth, int windowHeight);

    void record(EglSurfaceBase windowSurface, long timeStampNanos,
                int uProjectionMatrixLoc, int aPositionHandle, int aTexCoordHandle);

    void setRecordingEnabled(boolean enabled);

    boolean isRecordingEnabled();

    void attachListener(RecordListener listener);

    void setupWatermark(int watermarkHandle, float wmWidth, float wmHeight, float watermarkMargin);

    void setConfig(RecordConfig recordConfig);
}
