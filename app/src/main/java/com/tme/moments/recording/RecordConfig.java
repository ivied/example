package com.tme.moments.recording;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;

/**
 * @author zoopolitic.
 */
@AutoValue
public abstract class RecordConfig {

    public static RecordConfig create(@NonNull String markedOutputPath, @NonNull String unmarkedOutputPath,
                                      int bitRate, int frameRate,
                                      int iFrameInterval,
                                      int videoWidth, int videoHeight,
                                      boolean soundEnable, @Nullable String audioFilePath, long audioTimeMillis) {
        return new AutoValue_RecordConfig(markedOutputPath,unmarkedOutputPath,
                bitRate, frameRate,
                iFrameInterval,
                videoWidth, videoHeight,
                soundEnable, audioFilePath, audioTimeMillis);
    }

    public abstract String markedOutputPath();
    public abstract String unmarkedOutputPath();
    public abstract int bitRate();   // Mbps
    public abstract int frameRate();
    public abstract int iFrameInterval();
    public abstract int videoWidth();
    public abstract int videoHeight();
    public abstract boolean soundEnable();
    @Nullable
    public abstract String audioFilePath();
    public abstract long audioTimeMillis();
}
