package com.tme.moments.recording;

import android.support.annotation.NonNull;

import com.tme.moments.gles.EglCore;
import com.tme.moments.gles.WindowSurface;

final class VideoRecord {

    @NonNull
    private AndroidMuxer androidMuxer;
    @NonNull
    private WindowSurface recordSurface;

    VideoRecord(@NonNull AndroidMuxer androidMuxer, @NonNull EglCore eglCore) {
        this.androidMuxer = androidMuxer;
        VideoEncoder videoEncoder = androidMuxer.getVideoEncoder();
        if (videoEncoder == null) {
            throw new IllegalStateException("can't create VideoRecord without VideoEncoder");
        }
        recordSurface = new WindowSurface(eglCore, videoEncoder.getInputSurface(), true);
    }

    @NonNull
    AndroidMuxer getAndroidMuxer() {
        return androidMuxer;
    }

    @NonNull
    WindowSurface getRecordSurface() {
        return recordSurface;
    }
}
