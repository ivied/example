package com.tme.moments.recording;

import android.graphics.Rect;
import android.opengl.GLES30;

import com.tme.moments.gles.GlUtil;
import com.tme.moments.gles.WindowSurface;
import com.tme.moments.graphics.g3d.Sprite;

import static android.opengl.GLES20.glViewport;

/**
 * @author zoopolitic.
 */
public class Recorder3 extends AbsRecorder {

    @Override
    public void prepareToRecord(VideoEncoder videoEncoder, WindowSurface recordSurface) {
        videoEncoder.frameAvailableSoon();
        recordSurface.makeCurrentReadFrom(recordSurface);
    }

    @Override
    public void record(WindowSurface recordSurface,
                       Rect videoRect,
                       int uProjectionMatrixLoc,
                       int aPositionHandle,
                       int aTexCoordHandle
    ) {
        GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT);
        GLES30.glBlitFramebuffer(
                0, 0, recordSurface.getWidth(), recordSurface.getHeight(),
                videoRect.left, videoRect.top, videoRect.right, videoRect.bottom,
                GLES30.GL_COLOR_BUFFER_BIT, GLES30.GL_NEAREST);
    }

    @Override
    public void recordWatermark(Sprite watermark,
                                WindowSurface recordSurface,
                                Rect videoRect,
                                int uProjectionMatrixLoc,
                                int aPositionHandle,
                                int aTexCoordHandle
    ) {
        if (watermark != null) {
            glViewport(videoRect.left, videoRect.top, videoRect.width(), videoRect.height());
            watermark.draw(uProjectionMatrixLoc, GlUtil.IDENTITY_MATRIX, aPositionHandle, aTexCoordHandle, -1);
            glViewport(0, 0, recordSurface.getWidth(), recordSurface.getHeight());
        }
    }

    @Override
    public void finishRecord(WindowSurface recordSurface, long timeStampNanos) {
        recordSurface.setPresentationTime(timeStampNanos);
        recordSurface.swapBuffers();
    }
}
