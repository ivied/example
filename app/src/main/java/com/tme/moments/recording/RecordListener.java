package com.tme.moments.recording;

/**
 * @author zoopolitic.
 */
public interface RecordListener {
    void onRecordStarted();
    void onAudioPositionChanged(long audioTimeMillis);
    void onRecordFinished();
}
