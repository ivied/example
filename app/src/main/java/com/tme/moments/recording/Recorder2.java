package com.tme.moments.recording;

import android.graphics.Rect;
import android.opengl.GLES20;

import com.tme.moments.gles.GlUtil;
import com.tme.moments.gles.WindowSurface;
import com.tme.moments.graphics.g3d.Sprite;

import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glViewport;

/**
 * @author zoopolitic.
 */
public class Recorder2 extends AbsRecorder {


    @Override
    public void prepareToRecord(VideoEncoder videoEncoder, WindowSurface recordSurface) {
        videoEncoder.frameAvailableSoon();
        recordSurface.makeCurrent();
    }

    @Override
    public void record(WindowSurface recordSurface,
                       Rect videoRect,
                       int uProjectionMatrixLoc,
                       int aPositionHandle,
                       int aTexCoordHandle
    ) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
        glViewport(videoRect.left, videoRect.top, videoRect.width(), videoRect.height());
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }

    @Override
    public void recordWatermark(Sprite watermark,
                                WindowSurface recordSurface,
                                Rect videoRect,
                                int uProjectionMatrixLoc,
                                int aPositionHandle,
                                int aTexCoordHandle
    ) {
        if (watermark != null) {
            watermark.draw(uProjectionMatrixLoc, GlUtil.IDENTITY_MATRIX, aPositionHandle, aTexCoordHandle, -1);
        }
    }

    @Override
    public void finishRecord(WindowSurface recordSurface, long timeStampNanos) {
        recordSurface.setPresentationTime(timeStampNanos);
        recordSurface.swapBuffers();
    }
}
