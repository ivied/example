/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tme.moments.recording;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.os.Build;
import android.view.Surface;

import java.util.List;

import timber.log.Timber;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
class VideoEncoder extends MediaEncoder {

    private static final String MIME_TYPE = "video/avc";    // H.264 Advanced Video Coding

    private Surface inputSurface;
    private final int videoWidth;
    private final int videoHeight;
    private final int frameRate;
    private final int bitRate;
    private final int iFrameInterval;
    private boolean prepared = false;

    /**
     * Tells the video recorder to start recording.  (Call from non-encoder thread.)
     * <p>
     * Creates a new thread, which will own the provided VideoEncoderCore.  When the
     * thread exits, the VideoEncoderCore will be released.
     * <p>
     * Returns after the recorder thread has started and is ready to accept Messages.
     */
    VideoEncoder(List<AndroidMuxer> muxers, int videoWidth, int videoHeight, int frameRate,
                 int bitRate, int iFrameInterval) {
        super(muxers);
        synchronized (lock) {
            this.videoWidth = videoWidth;
            this.videoHeight = videoHeight;
            this.frameRate = frameRate;
            this.bitRate = bitRate;
            this.iFrameInterval = iFrameInterval;
        }
    }

    @Override
    boolean prepare() {
        if (prepared) return true;
        try {
            Timber.d("Prepare");
            muxersStarted = false;
            MediaFormat format = MediaFormat.createVideoFormat(MIME_TYPE, videoWidth, videoHeight);

            // Set some properties.  Failing to specify some of these can cause the MediaCodec
            // configure() call to throw an unhelpful exception.
            format.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                    MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
            format.setInteger(MediaFormat.KEY_BIT_RATE, bitRate);
            format.setInteger(MediaFormat.KEY_FRAME_RATE, frameRate);
            format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, iFrameInterval);
            Timber.d("Format: %s", format);

            // Create a MediaCodec encoder, and configure it with our format.  Get a Surface
            // we can use for input and wrap it with a class that handles the EGL work.
            encoder = MediaCodec.createEncoderByType(MIME_TYPE);
            Timber.d("Created encoder %s by %s", encoder.getName(), MIME_TYPE);
            encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            inputSurface = encoder.createInputSurface();
            encoder.start();
            //Save encoder in temporary variable to avoid drain() before codec started
            Timber.d("Prepared");
            prepared = true;
            return true;
        } catch (Exception e) {
            Timber.e(e);
            return false;
        }
    }

    /**
     * encoding loop on private thread
     */
    @Override
    public void run() {
        try {
            synchronized (lock) {
                requestStop = false;
                requestDrain = 0;
                lock.notify();
            }
            boolean localRequestStop;
            boolean localRequestDrain;
            while (true) {
                Timber.d("loop");
                synchronized (lock) {
                    localRequestStop = requestStop;
                    localRequestDrain = (requestDrain > 0);
                    if (localRequestDrain) {
                        requestDrain--;
                    }
                }
                if (localRequestStop) {
                    drain();
                    // request stop recording
                    signalEndOfInputStream();
                    // process output data again for EOS signal
                    drain();
                    // release all related objects
                    release();
                    break;
                }
                if (localRequestDrain) {
                    drain();
                } else {
                    synchronized (lock) {
                        try {
                            lock.wait();
                        } catch (final InterruptedException e) {
                            break;
                        }
                    }
                }
            } // end of while
            Timber.d("Exiting");
            synchronized (lock) {
                requestStop = true;
                running = false;
            }
        } catch (Throwable e) {
            Timber.e(e);
        }
    }

    private void signalEndOfInputStream() {
        Timber.d("sending EOS to encoder");
        encoder.signalEndOfInputStream();
        isEOS = true;
    }

    @Override
    protected void preparePresentationTime(MediaCodec.BufferInfo bufferInfo) {
        if (bufferInfo.presentationTimeUs <= prevOutputPTSUs) {
            bufferInfo.presentationTimeUs = prevOutputPTSUs + 1;
        }
        prevOutputPTSUs = bufferInfo.presentationTimeUs;
    }

    /**
     * the method to indicate frame data is soon available or already available
     *
     * @return return true if encoder is ready to encode.
     */
    boolean frameAvailableSoon() {
        synchronized (lock) {
            if (!running || requestStop) {
                return false;
            }
            requestDrain++;
            lock.notifyAll();
            return true;
        }
    }

    @Override
    protected void release() {
        Timber.d("release:");
        if (inputSurface != null) {
            inputSurface.release();
            inputSurface = null;
        }
        super.release();
    }

    /**
     * Returns the encoder's input surface.
     */
    Surface getInputSurface() {
        return inputSurface;
    }
}
