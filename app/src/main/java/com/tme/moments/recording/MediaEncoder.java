/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tme.moments.recording;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Build;
import android.util.Log;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
abstract class MediaEncoder implements Runnable {

    private final String TAG;

    final int TIMEOUT_US = 10_000;    // 10 milliseconds
    private static final boolean VERBOSE = true;

    final Object lock = new Object();      // guards ready/running

    private final List<MuxerAndTrackIndex> muxersAndTrackIndexes;
    private MediaCodec.BufferInfo bufferInfo;

    volatile MediaCodec encoder;
    boolean running;
    boolean requestStop;
    boolean isEOS;
    int requestDrain;
    boolean muxersStarted;

    MediaEncoder(List<AndroidMuxer> muxers) {
        if (muxers == null) {
            throw new NullPointerException("MediaMuxerWrapper is null");
        }
        TAG = MediaEncoder.class.getSimpleName() + ": " + getClass().getSimpleName();
        this.muxersAndTrackIndexes = new ArrayList<>(muxers.size());
        for (AndroidMuxer muxer : muxers) {
            muxersAndTrackIndexes.add(new MuxerAndTrackIndex(muxer, -1));
        }
        synchronized (lock) {
            bufferInfo = new MediaCodec.BufferInfo();
            // wait for starting thread
            new Thread(this, getClass().getSimpleName()).start();
            try {
                lock.wait();
            } catch (final InterruptedException ignore) {
            }
        }
    }

    abstract boolean prepare();

    void startRecording() {
        if (VERBOSE) Log.v(TAG, "startRecording");
        synchronized (lock) {
            running = true;
            requestStop = false;
            lock.notifyAll();
        }
    }

    /**
     * the method to request stop encoding
     */
    void stopRecording() {
        if (VERBOSE) Log.v(TAG, "stopRecording");
        synchronized (lock) {
            if (!running || requestStop) {
                return;
            }
            requestStop = true;    // for rejecting newer frame
            lock.notifyAll();
            // We can not know when the encoding and writing finish.
            // so we return immediately after request to avoid delay of caller thread
        }
    }

    /**
     * Releases encoder resources.
     */
    protected void release() {
        if (VERBOSE) Log.d(TAG, "releasing encoder objects");
        running = false;
        if (encoder != null) {
            try {
                MediaCodec encoder = this.encoder;
                this.encoder = null;
                encoder.stop();
                encoder.release();
            } catch (final Exception e) {
                String errorMessage = "failed releasing MediaCodec";
                Timber.e(e, errorMessage);
            }
        }
        if (muxersStarted) {
            try {
                for (MuxerAndTrackIndex muxerAndTrackIndex : muxersAndTrackIndexes) {
                    muxerAndTrackIndex.androidMuxer.stop();
                }
            } catch (final Exception e) {
                String errorMessage = "failed stopping muxer";
                Timber.e(e, errorMessage);
            }
        }
        bufferInfo = null;
    }

    /**
     * drain encoded data and write them to muxer
     */
    void drain() {
        try {
            if (encoder == null) return;
            ByteBuffer[] encoderOutputBuffers = encoder.getOutputBuffers();
            int encoderStatus;
            LOOP:
            while (running) {
                // get encoded data with maximum timeout duration of TIMEOUT_US (10 ms)
                encoderStatus = encoder.dequeueOutputBuffer(bufferInfo, TIMEOUT_US);
                if (encoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    if (!isEOS) {
                        break;
                    } else {
                        if (VERBOSE) Log.d(TAG, "no output available, spinning to await EOS");
                    }
                } else if (encoderStatus == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                    if (VERBOSE) Log.v(TAG, "INFO_OUTPUT_BUFFERS_CHANGED");
                    // this should not come when encoding
                    encoderOutputBuffers = encoder.getOutputBuffers();
                } else if (encoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    if (VERBOSE) Log.v(TAG, "INFO_OUTPUT_FORMAT_CHANGED");
                    // this status indicate the output format of codec is changed
                    // this should come only once before actual encoded data
                    // but this status never come on Android 4.3 or less
                    // and in that case, you should treat when MediaCodec.BUFFER_FLAG_CODEC_CONFIG come.
                    if (muxersStarted) {    // second time request is error
                        throw new RuntimeException("format changed twice");
                    }
                    // get output format from codec and pass them to muxer
                    // getOutputFormat should be called after INFO_OUTPUT_FORMAT_CHANGED otherwise crash.
                    final MediaFormat format = encoder.getOutputFormat(); // API >= 16
                    for (MuxerAndTrackIndex muxerAndTrackIndex : muxersAndTrackIndexes) {
                        AndroidMuxer muxer = muxerAndTrackIndex.androidMuxer;
                        muxerAndTrackIndex.trackIndex = muxer.addTrack(format);
                        if (!muxer.start()) {
                            // we should wait until muxer is ready
                            synchronized (muxer) {
                                while (!muxer.isStarted())
                                    try {
                                        muxer.wait(10);
                                    } catch (final InterruptedException e) {
                                        break LOOP;
                                    }
                            }
                        }
                    }
                    muxersStarted = true;
                } else if (encoderStatus < 0) {
                    // unexpected status
                    if (VERBOSE)
                        Log.w(TAG, "drain:unexpected result from encoder#dequeueOutputBuffer: " + encoderStatus);
                } else {
                    final ByteBuffer encodedData = encoderOutputBuffers[encoderStatus];
                    if (encodedData == null) {
                        // this never should come...may be a MediaCodec internal error
                        throw new RuntimeException("encoderOutputBuffer " + encoderStatus + " was null");
                    }
                    if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                        // You should set output format to muxer here when you target Android4.3 or less
                        // but MediaCodec#getOutputFormat can not call here(because INFO_OUTPUT_FORMAT_CHANGED don't come yet)
                        // therefore we should expand and prepare output format from buffer data.
                        // We use API>=18(>=Android 4.3) so just ignore this flag here
                        if (VERBOSE) Log.d(TAG, "drain:BUFFER_FLAG_CODEC_CONFIG");
                        bufferInfo.size = 0;
                    }

                    if (bufferInfo.size != 0) {
                        if (!muxersStarted) {
                            // muxer is not ready...this is programing failure.
                            throw new RuntimeException("drain: muxer hasn't started");
                        }
                        encodedData.position(bufferInfo.offset);
                        encodedData.limit(bufferInfo.offset + bufferInfo.size);
                        preparePresentationTime(bufferInfo);
                        for (MuxerAndTrackIndex muxerAndTrackIndex : muxersAndTrackIndexes) {
                            muxerAndTrackIndex.androidMuxer.writeSampleData(muxerAndTrackIndex.trackIndex, encodedData, bufferInfo);
                        }
                    }
                    // return buffer to encoder
                    encoder.releaseOutputBuffer(encoderStatus, false);
                    if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                        // when EOS come.
                        running = false;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    protected void preparePresentationTime(MediaCodec.BufferInfo bufferInfo) {

    }

    /**
     * previous presentationTimeUs for writing
     */
    protected long prevOutputPTSUs = 0;

    /**
     * get next encoding presentationTimeUs
     *
     * @return next encoding presentationTimeUs next encoding presentationTimeUs
     */
    protected long getPTSUs() {
        long result = System.nanoTime() / 1000L;
        // presentationTimeUs should be monotonic
        // otherwise muxer fail to write
        if (result < prevOutputPTSUs) result = (prevOutputPTSUs - result) + result;
        return result;
    }

    private class MuxerAndTrackIndex {
        AndroidMuxer androidMuxer;
        int trackIndex;

        MuxerAndTrackIndex(AndroidMuxer androidMuxer, int trackIndex) {
            this.androidMuxer = androidMuxer;
            this.trackIndex = trackIndex;
        }
    }
}
