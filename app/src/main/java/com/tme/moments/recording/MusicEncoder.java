package com.tme.moments.recording;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;

import com.tme.moments.presentation.utils.MediaUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import javax.annotation.Nullable;

import timber.log.Timber;

/**
 * @author zoopolitic.
 */
public class MusicEncoder extends MediaEncoder {

    public interface PlayListener {
        void onPositionChanged(long millis);
    }

    private static final boolean IS_LOOP_AUDIO = true;

    private RecordConfig config;
    private MediaExtractor extractor;
    private AudioTrack audioTrack;
    private MediaCodec decoder;
    private boolean prepared;
    private MediaCodec.BufferInfo decoderInfo;
    private @Nullable
    PlayListener playListener;

    MusicEncoder(final List<AndroidMuxer> muxers, RecordConfig config) {
        super(muxers);
        this.config = config;
    }

    void setPlayListener(@Nullable PlayListener playListener) {
        this.playListener = playListener;
    }

    @Override
    boolean prepare() {
        if (prepared) return true;
        synchronized (lock) {
            try {
                Timber.d("Prepare");
                prepared = false;
                muxersStarted = isEOS = false;
                String sourceFile = config.audioFilePath();
                if (sourceFile == null) {
                    Timber.e("Couldn't prepare. Source File path is null");
                    return false;
                }
                Timber.d("Prepare Extractor");
                MediaFormat format = prepareExtractor(sourceFile);
                if (format == null) {
                    Timber.e("Couldn't prepare MediaExtractor");
                    return false;
                }
                Timber.d("Format = %s", format.toString());
                int maxInputSize = MediaUtils.extractInt(format, MediaFormat.KEY_MAX_INPUT_SIZE, -1);
                // on some devices MediaFormat returns 0 instead of throwing NPE in case
                // when there is no such field
                if (maxInputSize <= 0) {
                    maxInputSize = 16384;
                }
                Timber.d("MAX_INPUT_SIZE = %s", maxInputSize);
                format.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, maxInputSize);
                Timber.d("Prepare Decoder");
                if (!prepareDecoder(format)) {
                    Timber.e("Can't prepare decoder");
                    return false;
                }
                int channelCount = format.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
                int sampleRate = format.getInteger(MediaFormat.KEY_SAMPLE_RATE);
                long startTimeUs = config.audioTimeMillis() * 1000;
                extractor.seekTo(startTimeUs, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
                Timber.d("Prepare Encoder");
                if (!prepareEncoder(sampleRate, channelCount)) {
                    Timber.e("Can't prepare encoder");
                    return false;
                }
                Timber.d("Prepare AudioTrack");
                if (!prepareAudioTrack(sampleRate, channelCount)) {
                    Timber.e("Can't prepare AudioTrack");
                    return false;
                }
                prepared = true;
                lock.notify();
                Timber.d("Prepared");
                return true;
            } catch (Exception e) {
                Timber.e(e);
                return false;
            }
        }
    }

    @Nullable
    private MediaFormat prepareExtractor(String sourceFile) {
        extractor = new MediaExtractor();
        try {
            extractor.setDataSource(sourceFile);
        } catch (final IOException e) {
            Timber.e(e);
            return null;
        }
        int trackIndex = MediaUtils.selectTrack(extractor, "audio/");
        if (trackIndex < 0) {
            Timber.e("Can't select track from extractor");
            return null;
        }
        extractor.selectTrack(trackIndex);
        return extractor.getTrackFormat(trackIndex);
    }

    private boolean prepareDecoder(MediaFormat format) {
        try {
            String mime = format.getString(MediaFormat.KEY_MIME);
            decoder = MediaCodec.createByCodecName("OMX.google.mp3.decoder");
            Timber.d("Create decoder %s by %s", decoder.getName(), mime);
            decoder.configure(format, null, null, 0);
            decoder.start();
            return true;
        } catch (IOException e) {
            Timber.e(e, "Couldn't prepare Decoder");
            return false;
        }
    }

    private boolean prepareEncoder(int sampleRate, int channelsCount) {
        try {
            final String mimeType = "audio/mp4a-latm";
            final MediaFormat format = new MediaFormat();
            format.setString(MediaFormat.KEY_MIME, mimeType);
            format.setInteger(MediaFormat.KEY_SAMPLE_RATE, sampleRate);
            format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, channelsCount);
            format.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);
            format.setInteger(MediaFormat.KEY_BIT_RATE, 128_000);
            format.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, 16384);
            encoder = MediaCodec.createEncoderByType(mimeType);
            Timber.d("Create encoder %s by %s", encoder.getName(), mimeType);
            encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            encoder.start();
            return true;
        } catch (IOException e) {
            Timber.e(e, "Couldn't prepare Encoder");
            return false;
        }
    }

    private boolean prepareAudioTrack(int sampleRate, int channelCount) {
        int bufSize = AudioTrack.getMinBufferSize(sampleRate,
                (channelCount == 1 ? AudioFormat.CHANNEL_OUT_MONO : AudioFormat.CHANNEL_OUT_STEREO),
                AudioFormat.ENCODING_PCM_16BIT);
        audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                sampleRate,
                (channelCount == 1 ? AudioFormat.CHANNEL_OUT_MONO : AudioFormat.CHANNEL_OUT_STEREO),
                AudioFormat.ENCODING_PCM_16BIT,
                bufSize,
                AudioTrack.MODE_STREAM);
        audioTrack.setPlaybackRate(sampleRate);
        try {
            audioTrack.play();
            return true;
        } catch (final Exception e) {
            Timber.e(e, "Failed to start audio track playing");
            audioTrack.release();
            audioTrack = null;
            return false;
        }
    }

    @Override
    public void run() {
        try {
            synchronized (lock) {
                requestStop = false;
                lock.notify();
                while (!prepared) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        break;
                    }
                }
            }

            boolean localRequestStop;
            while (true) {
                Timber.d("loop");
                synchronized (lock) {
                    localRequestStop = requestStop;
                }

                if (localRequestStop) {
                    drain();
                    decodePlayEncode(true);
                    drain();
                    release();
                    break;
                } else {
                    decodePlayEncode(false);
                    drain();
                }
            } // end of while
            Timber.d("Exiting");
            synchronized (lock) {
                requestStop = true;
                running = false;
            }
        } catch (Throwable e) {
            Timber.e(e);
        }
    }

    /**
     * Decodes one frame from input audio file, plays it and encodes to output audio file
     *
     * @param endOfStream true if should signal end stream
     */
    private void decodePlayEncode(boolean endOfStream) {
        Timber.d("start decodePlayEncode");
        ByteBuffer[] decoderInputBuffers = decoder.getInputBuffers();
        ByteBuffer[] decoderOutputBuffers = decoder.getOutputBuffers();
        ByteBuffer[] encoderInputBuffers = encoder.getInputBuffers();
        Timber.d("received all buffers");
        int encoderInputIndex = encoder.dequeueInputBuffer(TIMEOUT_US);
        if (encoderInputIndex == -1) {
            return;
        }
        Timber.d("dequeued encoders buffer");
        try {
            int decoderInputIndex = decoder.dequeueInputBuffer(TIMEOUT_US);
            if (decoderInputIndex >= 0) {
                ByteBuffer decoderInputBuffer = decoderInputBuffers[decoderInputIndex];
                int sampleSize = extractor.readSampleData(decoderInputBuffer, 0);
                long sampleTime = extractor.getSampleTime();
                if (sampleSize < 0) {
                    // decoderInfo was not initialized but we already reached end
                    // of the file - it can be only if provided startTime for initial
                    // position was equal endTime (audio was rewound to the end).
                    // instead of signaling endOfStream we rewind it to the start
                    if (decoderInfo == null && IS_LOOP_AUDIO && sampleTime < 0) {
                        Timber.d("Goes in to some fix");
                        extractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
                        sampleSize = extractor.readSampleData(decoderInputBuffer, 0);
                        decoder.queueInputBuffer(decoderInputIndex, 0, sampleSize, sampleTime, 0);
                        extractor.advance();
                    } else {
                        Timber.d("Goes in to another fix");
                        decoder.queueInputBuffer(decoderInputIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                    }
                } else {
                    Timber.d("queue sampleSize = " + sampleSize + " sampleTime=" + sampleTime);
                    decoder.queueInputBuffer(decoderInputIndex, 0, sampleSize, sampleTime, 0);
                    extractor.advance();
                    if (IS_LOOP_AUDIO) {
                        sampleTime = extractor.getSampleTime();
                        if (sampleTime == -1) {
                            extractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
                        }
                    }
                }
                if (decoderInfo == null) {
                    decoderInfo = new MediaCodec.BufferInfo();
                }
                int decoderOutputIndex = decoder.dequeueOutputBuffer(decoderInfo, TIMEOUT_US);
                if (decoderOutputIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    Timber.d("dequeueOutputBuffer timed out!");
                } else if (decoderOutputIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                    Timber.d("INFO_OUTPUT_BUFFERS_CHANGED");
                } else if (decoderOutputIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    Timber.d("INFO_OUTPUT_FORMAT_CHANGED");
                    MediaFormat format = decoder.getOutputFormat();
                    Timber.d("New format: %s", format.toString());
                    audioTrack.setPlaybackRate(format.getInteger(MediaFormat.KEY_SAMPLE_RATE));
                } else {
                    Timber.d("receive decoded buffers");
                    ByteBuffer decoderOutput = decoderOutputBuffers[decoderOutputIndex];

                    // play
                    ByteBuffer duplicate = decoderOutput.duplicate();
                    duplicate.position(decoderInfo.offset);
                    duplicate.limit(decoderInfo.offset + decoderInfo.size);
                    final byte[] chunk = new byte[decoderInfo.size];
                    duplicate.get(chunk); // Read the buffer all at once
                    Timber.d("play music");
                    audioTrack.write(chunk, decoderInfo.offset, decoderInfo.offset + decoderInfo.size);
                    if (playListener != null) {
                        playListener.onPositionChanged(sampleTime / 1000);
                    }

                    // encode
                    Timber.d("encode");
                    ByteBuffer encodedInput = encoderInputBuffers[encoderInputIndex];
                    encodedInput.position(0);
                    encodedInput.put(decoderOutput);
                    decoderOutput.clear();
                    duplicate.clear();
                    long presentationTimeUs = getPTSUs();
                    prevOutputPTSUs = presentationTimeUs;
                    if (endOfStream) {
                        isEOS = true;
                        encoder.queueInputBuffer(encoderInputIndex, 0, decoderInfo.size,
                                presentationTimeUs, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                    } else {
                        encoder.queueInputBuffer(encoderInputIndex, 0, decoderInfo.size,
                                presentationTimeUs, decoderInfo.flags);
                    }

                    decoder.releaseOutputBuffer(decoderOutputIndex, false);
                    if ((decoderInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                        Timber.d("OutputBuffer BUFFER_FLAG_END_OF_STREAM");
                    }
                }
            } else {
                Timber.e("Couldn't obtain decoder input buffer in " + TIMEOUT_US + " nanoseconds");
            }
        } catch (Throwable th) {
            Timber.e(th, "Music Encoder Crashed!");
        }
    }

    @Override
    protected void preparePresentationTime(MediaCodec.BufferInfo bufferInfo) {
        bufferInfo.presentationTimeUs = getPTSUs();
        prevOutputPTSUs = bufferInfo.presentationTimeUs;
    }

    @Override
    protected void release() {
        super.release();
        decoder.stop();
        decoder.release();
        audioTrack.stop();
        audioTrack.release();
        extractor.release();
    }
}