package com.tme.moments.recording;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.nio.ByteBuffer;

import timber.log.Timber;

/**
 * @author zoopolitic.
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class AndroidMuxer {

    public interface MuxerListener {
        void onMuxerStarted();

        void onMuxerStopped();
    }

    private final MediaMuxer muxer;
    private int encoderCount, startedCount;
    private boolean isStarted;
    @Nullable
    private MediaEncoder audioEncoder;
    @Nullable
    private VideoEncoder videoEncoder;
    private MuxerListener listener;

    AndroidMuxer(String outputPath, int outputFormat) throws IOException {
        muxer = new MediaMuxer(outputPath, outputFormat);
        encoderCount = startedCount = 0;
        isStarted = false;
    }

    boolean prepare() {
        if (videoEncoder != null) {
            if (!videoEncoder.prepare()) {
                return false;
            }
        }
        if (audioEncoder != null) {
            if (!audioEncoder.prepare()) {
                return false;
            }
        }
        return true;
    }

    void startRecording() {
        if (videoEncoder != null) {
            if (!videoEncoder.running) {
                videoEncoder.startRecording();
            }
        }
        if (audioEncoder != null) {
            if (!audioEncoder.running) {
                audioEncoder.startRecording();
            }
        }
    }

    void stopRecording() {
        if (videoEncoder != null) {
            if (videoEncoder.running) {
                videoEncoder.stopRecording();
            }
        }
        videoEncoder = null;
        if (audioEncoder != null) {
            if (audioEncoder.running) {
                audioEncoder.stopRecording();
            }
        }
        audioEncoder = null;
    }

    public void setListener(MuxerListener listener) {
        this.listener = listener;
    }

    public synchronized boolean isStarted() {
        return isStarted;
    }

    public void setAudioEncoder(@NonNull MediaEncoder audioEncoder) {
        this.audioEncoder = audioEncoder;
        updateEncodersCount();
    }

    public void setVideoEncoder(@NonNull VideoEncoder videoEncoder) {
        this.videoEncoder = videoEncoder;
        updateEncodersCount();
    }

    @Nullable
    public MediaEncoder getAudioEncoder() {
        return audioEncoder;
    }

    @Nullable
    public VideoEncoder getVideoEncoder() {
        return videoEncoder;
    }

    private void updateEncodersCount() {
        encoderCount = (videoEncoder != null ? 1 : 0) + (audioEncoder != null ? 1 : 0);
    }

    /**
     * request start recording from encoder
     *
     * @return true when muxer is ready to write
     */
    /*package*/
    synchronized boolean start() {
        startedCount++;
        Timber.d("start: %s", startedCount);
        if (encoderCount > 0 && startedCount == encoderCount) {
            muxer.start();
            isStarted = true;
            notifyAll();
            if (listener != null) {
                listener.onMuxerStarted();
            }
            Timber.d("AndroidMuxer started");
        }
        return isStarted;
    }

    /**
     * request stop recording from encoder when encoder received EOS
     */
    /*package*/
    synchronized void stop() {
        startedCount--;
        Timber.d("stop: %s", startedCount);
        if ((encoderCount > 0) && (startedCount <= 0)) {
            muxer.stop();
            muxer.release();
            isStarted = false;
            if (listener != null) {
                listener.onMuxerStopped();
            }
            Timber.d("AndroidMuxer stopped");
        }
    }

    /**
     * assign encoder to muxer
     *
     * @param format
     * @return minus value indicate error
     */
    /*package*/
    synchronized int addTrack(final MediaFormat format) {
        if (isStarted) {
            throw new IllegalStateException("muxer already started");
        }
        final int trackIx = muxer.addTrack(format);
        Timber.d("addTrack:trackNum=" + encoderCount + ",trackIx=" + trackIx + ",format=" + format);
        return trackIx;
    }

    /**
     * @see MediaMuxer#writeSampleData(int, ByteBuffer, MediaCodec.BufferInfo)
     */
    synchronized void writeSampleData(final int trackIndex, final ByteBuffer byteBuf,
                                      final MediaCodec.BufferInfo bufferInfo) {
        try {
            if (startedCount > 0) {
                muxer.writeSampleData(trackIndex, byteBuf, bufferInfo);
            }
        } catch (Throwable e) {
            Timber.e(e);
        }
    }
}
