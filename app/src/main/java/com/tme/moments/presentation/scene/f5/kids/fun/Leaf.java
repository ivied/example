package com.tme.moments.presentation.scene.f5.kids.fun;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Leaf extends Decal {

    private long waitTimeInGrass;

    private enum Direction {
        LEFT, RIGHT, BOTTOM
    }

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point 1
    private float p2x, p2y; // control point 2
    private float p3x, p3y; // end point

    private final boolean rotateAllAxis;
    private final float minXSpeed;
    private final float maxXSpeed;
    private float x;
    private float y;
    private float z;
    private float speed;
    private float rotationSpeed;
    private float t;
    private long startDelayNanos;
    private Direction direction;

    private float alphaSpeed;
    private float throwXSpeed;
    private float throwYSpeed;
    private float throwYVelocity;
    private boolean thrown;
    private boolean finished;

    private float disappearAlphaSpeed;
    private float endY;

    Leaf(TextureRegion region, float width, float height, float x, float y,
         float minXSpeed, float maxXSpeed, boolean rotateAllAxis) {
        this.minXSpeed = minXSpeed;
        this.maxXSpeed = maxXSpeed;
        this.rotateAllAxis = rotateAllAxis;
        this.x = x;
        this.y = y;
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        float minZ = -1.0f;
        float maxZ = -1.0f;

        z = random(minZ, maxZ);
        // reduce maxZ by half of biggest side of the object
        // to not being clipped during X and Y rotation
        z = Math.min(z, -1.0f - Math.max(getWidth(), getHeight()) / 2);
        boolean rotateLeft = random();
        direction = Direction.BOTTOM;
        boolean useSideDirection = random(5) == 1;
        if (useSideDirection) {
            boolean left = random();
            direction = left ? Direction.LEFT : Direction.RIGHT;
        }
        rotationSpeed = rotateLeft
                ? random(-175f, -140f) / Const.NANOS_IN_SECOND
                : random(140f, 175f) / Const.NANOS_IN_SECOND;
        init();
    }

    public boolean isFinished() {
        return finished;
    }

    void step(long deltaNanos) {
        if (startDelayNanos >= 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        if (getY() <= endY) {
            if (waitTimeInGrass > 0) {
                waitTimeInGrass -= deltaNanos;
                return;
            }
            float alpha = color.a;
            float deltaAlpha = disappearAlphaSpeed * deltaNanos;
            float newAlpha = Math.max(0, alpha - deltaAlpha);
            setAlpha(newAlpha);
            if (newAlpha == 0) {
                finished = true;
            }
            return;
        }
        if (!thrown) {
            float deltaX = throwXSpeed * deltaNanos;
            float deltaY = throwYSpeed * deltaNanos;
            float deltaAlpha = alphaSpeed * deltaNanos;
            float alpha = color.a;
            if (alpha < 1) {
                setAlpha(Math.min(1.0f, alpha + deltaAlpha));
            }
            translate(deltaX, deltaY, 0);
            float deltaVelocity = throwYVelocity * deltaNanos;
            throwYSpeed += deltaVelocity;
            if (throwYSpeed <= 0) {
                thrown = true;
                init();
            }
            return;
        }
        float delta = speed * deltaNanos;
        t += delta;
        float single = (1 - t);
        float quadratic = (1 - t) * (1 - t);
        float cube = (1 - t) * (1 - t) * (1 - t);
        float x = cube * p0x + 3 * t * quadratic * p1x + 3 * t * t * single * p2x + t * t * t * p3x;
        float y = cube * p0y + 3 * t * quadratic * p1y + 3 * t * t * single * p2y + t * t * t * p3y;

        float deltaRotation = rotationSpeed * deltaNanos;
        rotateZ(deltaRotation);
        if (rotateAllAxis) {
            rotateX(deltaRotation);
            rotateY(deltaRotation);
        }
        setX(x);
        setY(y);

        if (t >= 1) {
            init();
        }
    }

    private void init() {
        if (!thrown) {
            setAlpha(0);
            speed = random(0.75f, 1.0f) / Const.NANOS_IN_SECOND;
            alphaSpeed = 2.0f / Const.NANOS_IN_SECOND;
            throwXSpeed = random(minXSpeed, maxXSpeed) / Const.NANOS_IN_SECOND;
            throwYSpeed = random(0.9f, 1.25f) / Const.NANOS_IN_SECOND;
            throwYVelocity = -(speed * 0.5f) / Const.NANOS_IN_SECOND; // lose 60% of speed per second
            setPosition(x, y, z);
            return;
        }
        float currX = getX();
        float currY = getY();

        float deltaZ = -1.0f - z; // -1 is a maxZ
        p0x = currX;
        p0y = currY;
        float xMin = 0;
        float xMax = 0;
        float yMin = -0.3f;
        float yMax = -0.2f;
        switch (direction) {
            case LEFT:
                xMin = -0.1f;
                xMax = -0.03f;
                break;
            case RIGHT:
                xMin = 0.03f;
                xMax = 0.1f;
                break;
            case BOTTOM:
                boolean moveLeft = random();
                xMin = moveLeft ? -0.1f : 0.03f;
                xMax = moveLeft ? -0.03f : 0.1f;
                break;
        }
        p1x = p0x + random(xMin, xMax);
        p1y = p0y + random(yMin, yMax);

        p2x = p1x + random(xMin, xMax);
        p2y = p1y + random(yMin, yMax);

        p3x = p2x + random(xMin, xMax);
        p3y = p2y + random(yMin, yMax);
        endY = random(-0.95f - deltaZ, -0.78f - deltaZ);
        float disappearTime = 0.3f;
        waitTimeInGrass = TimeUnit.MILLISECONDS.toNanos(random(2000, 3500));
        disappearAlphaSpeed = 1 / disappearTime / Const.NANOS_IN_SECOND;
        t = 0;
        setPosition(p0x, p0y, z);
    }
}
