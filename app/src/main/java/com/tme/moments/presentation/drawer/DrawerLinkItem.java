package com.tme.moments.presentation.drawer;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.tme.moments.R;

@AutoValue
public abstract class DrawerLinkItem implements DrawerItem {

    static DrawerLinkItem create(@NonNull String title, @NonNull String link) {
        return new AutoValue_DrawerLinkItem(title, link);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.drawer_link_item;
    }

    public abstract String title();
    public abstract String link();
}
