package com.tme.moments.presentation.scene.f19.purple.fantasy;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.Const;
import com.tme.moments.presentation.scene.Glow;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class RotationGlow extends Glow {

    private float rotationSpeed;

    RotationGlow(TextureRegion region, float width, float height) {
        super(region, width, height);
        setStartDelay(7000); // initial delay
    }

    @Override protected void init() {
        super.init();
        rotationSpeed = random(-180f, 180f) / Const.NANOS_IN_SECOND;
    }

    @Override public void step(long deltaNanos) {
        super.step(deltaNanos);
        float deltaRotation = rotationSpeed * deltaNanos;
        rotateZ(deltaRotation);
    }
}
