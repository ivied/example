package com.tme.moments.presentation.scene.f18.paper.speech;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Circle extends Decal {

    private boolean goOutOfScreen;

    private enum Direction {
        LEFT, TOP, RIGHT, BOTTOM
    }

    private float scaleSpeed;
    private float startDelayNanos;
    private float maxScale;
    private float speedX;
    private float speedY;
    private boolean frozen;

    Circle(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        init();
    }

    void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }

    void step(long deltaNanos) {
        if (frozen) {
            return;
        }
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float deltaX = speedX * deltaNanos;
        float deltaY = speedY * deltaNanos;
        translate(deltaX, deltaY, 0);

        float deltaScale = scaleSpeed * deltaNanos;
        float newScale = Math.min(maxScale, getScaleX() + deltaScale);
        setScale(newScale);

        if (goOutOfScreen) {
            float x = getX();
            float y = getY();
            if (x < -1 - getWidth() / 2 || x > 1 + getWidth() / 2
                    || y > 1 + getHeight() / 2 || y < -1 - getHeight() / 2) {
                init();
            }
        } else {
            float fadeOutBorder = 1.0f;
            if (newScale >= fadeOutBorder) {
                float fraction = (newScale - fadeOutBorder) / (maxScale - fadeOutBorder);
                float alpha = Math.max(0, 1 - fraction);
                setAlpha(alpha);
                if (alpha == 0) {
                    init();
                }
            }
        }
    }

    private void init() {
        goOutOfScreen = random(25) == 1;
        Direction[] directions = Direction.values();
        Direction direction = directions[random(0, directions.length)];
        float x = 0;
        float y = 0;
        float z = -1;
        float minSpeed = 0.1f;
        float maxSpeed = 0.2f;
        switch (direction) {
            case LEFT:
                x = random(-0.6f, -0.45f);
                y = random(-0.5f, 0.5f);
                speedX = random(-maxSpeed, -minSpeed) / Const.NANOS_IN_SECOND;
                speedY = random(-maxSpeed, maxSpeed) / Const.NANOS_IN_SECOND;
                break;
            case TOP:
                x = random(-0.5f, 0.5f);
                y = random(0.45f, 0.6f);
                speedX = random(-maxSpeed, maxSpeed) / Const.NANOS_IN_SECOND;
                speedY = random(minSpeed, maxSpeed) / Const.NANOS_IN_SECOND;
                break;
            case RIGHT:
                x = random(0.45f, 0.6f);
                y = random(-0.5f, 0.5f);
                speedX = random(minSpeed, maxSpeed) / Const.NANOS_IN_SECOND;
                speedY = random(-maxSpeed, maxSpeed) / Const.NANOS_IN_SECOND;
                break;
            case BOTTOM:
                x = random(-0.5f, 0.5f);
                y = random(-0.6f, -0.45f);
                speedX = random(-maxSpeed, maxSpeed) / Const.NANOS_IN_SECOND;
                speedY = random(-maxSpeed, -minSpeed) / Const.NANOS_IN_SECOND;
                break;
        }
        scaleSpeed = 1.0f / Const.NANOS_IN_SECOND;
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 10000));
        maxScale = goOutOfScreen ? 1 : 1.25f;
        setAlpha(1);
        setScale(0);
        setPosition(x, y, z);
    }
}
