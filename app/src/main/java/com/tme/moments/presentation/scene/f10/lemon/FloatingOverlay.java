package com.tme.moments.presentation.scene.f10.lemon;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.Const;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class FloatingOverlay extends Sprite {

    static final float OFFSET = 0.04f;

    private float speedX;
    private float speedY;

    FloatingOverlay(TextureRegion textureRegion, float width, float height) {
        super(textureRegion, width, height);
        init();
    }

    void step(long deltaNanos) {
        float deltaX = speedX * deltaNanos;
        float deltaY = speedY * deltaNanos;

        float minX = -OFFSET;
        float maxX = OFFSET;
        float minY = -OFFSET;
        float maxY = OFFSET;

        float newX = Math.min(maxX, Math.max(minX, getX() + deltaX));
        float newY = Math.min(maxY, Math.max(minY, getY() + deltaY));

        setX(newX);
        setY(newY);

        if (newX == minX || newX == maxX || newY == minY || newY == maxY) {
            init();
        }
    }

    private void init() {
        speedX = random(0.0125f, 0.03125f) / Const.NANOS_IN_SECOND;
        speedX *= random() ? -1 : 1;
        speedY = random(0.0125f, 0.03125f) / Const.NANOS_IN_SECOND;
        speedY *= random() ? -1 : 1;
    }
}
