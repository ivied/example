package com.tme.moments.presentation.scene.f5.kids.fun;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.math.Vector2;
import com.tme.moments.presentation.Const;

/**
 * @author zoopolitic.
 */
class Grass extends Decal {

    private float moveSpeedNanos;
    private float rotationSpeedNanos;

    private final float minX;
    private final float maxX;

    Grass(TextureRegion region, float width, float height, float x, float y,
          float moveSpeedPerSecond, float rotationSpeedDegreesPerSecond,
          float moveIncline) {
        this.moveSpeedNanos = moveSpeedPerSecond / Const.NANOS_IN_SECOND;
        this.rotationSpeedNanos = rotationSpeedDegreesPerSecond / Const.NANOS_IN_SECOND;
        this.minX = x - moveIncline;
        this.maxX = x + moveIncline;
        transformationOffset = new Vector2(0, -height / 2);
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        float z = -1.0f;
        setPosition(x, y, z);
    }

    void step(long deltaNanos) {
        float rotateDelta = rotationSpeedNanos * deltaNanos;
        rotateZ(rotateDelta);

        float moveDelta = moveSpeedNanos * deltaNanos;
        float newX = getX() + moveDelta;
        newX = Math.max(minX, Math.min(maxX, newX));
        setX(newX);
        if (newX == maxX || newX == minX) {
            moveSpeedNanos = -moveSpeedNanos;
            rotationSpeedNanos = -rotationSpeedNanos;
        }
    }
}
