package com.tme.moments.presentation.capture;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.lang.ref.WeakReference;

/**
 * Handles messages sent from the render thread to the UI thread.
 * <p>
 * The object is created on the UI thread, and all handlers run there.
 */
public class CaptureActivityHandler extends Handler {

    private static final String TAG = CaptureActivityHandler.class.getSimpleName();

    private static final int MSG_UPDATE_FPS = 0;
    private static final int MSG_SCREENSHOT_CAPTURED = 1;
    private static final int MSG_IMAGE_DECODED = 2;

    // Weak reference to the Activity; only access this from the UI thread.
    private WeakReference<CaptureActivity> weakActivity;

    CaptureActivityHandler(CaptureActivity activity) {
        weakActivity = new WeakReference<>(activity);
    }

    public void sendFpsUpdate(int fps) {
        sendMessage(obtainMessage(MSG_UPDATE_FPS, fps));
    }

    public void sendScreenshotCaptured() {
        sendMessage(obtainMessage(MSG_SCREENSHOT_CAPTURED));
    }

    public void onImageDecoded() {
        sendMessage(obtainMessage(MSG_IMAGE_DECODED));
    }

    @Override  // runs on UI thread
    public void handleMessage(Message msg) {
        int what = msg.what;

        CaptureActivity activity = weakActivity.get();
        if (activity == null) {
            Log.w(TAG, "ActivityHandler.handleMessage: weak ref is null");
            return;
        }

        switch (what) {
            case MSG_UPDATE_FPS:
                int fps = (int) msg.obj;
                activity.onFpsUpdated(fps);
                break;
            case MSG_SCREENSHOT_CAPTURED:
                activity.onScreenshotCaptured();
                break;
            case MSG_IMAGE_DECODED:
                activity.onImageDecoded();
                break;
        }
    }
}
