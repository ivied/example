package com.tme.moments.presentation.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;

import timber.log.Timber;

/**
 * Creates and returns a Bitmap for a given Uri(String url).
 * inSampleSize is calculated based on requiredWidth property. However can be adjusted if OOM occurs.
 * If any EXIF config is found - bitmap is transformed properly.
 */
public class BitmapLoadTask extends AsyncTask<Void, Void, BitmapLoadTask.BitmapWorkerResult> {

    private static final String TAG = "BitmapWorkerTask";

    private final Context context;
    private Uri inputUri;
    private final int requiredWidth;
    private final int requiredHeight;

    private final BitmapLoadCallback bitmapLoadCallback;

    public static class BitmapWorkerResult {

        Bitmap bitmapResult;
        ExifInfo exifInfo;
        Exception bitmapWorkerException;

        public BitmapWorkerResult(@NonNull Bitmap bitmapResult, @NonNull ExifInfo exifInfo) {
            this.bitmapResult = bitmapResult;
            this.exifInfo = exifInfo;
        }

        public BitmapWorkerResult(@NonNull Exception bitmapWorkerException) {
            this.bitmapWorkerException = bitmapWorkerException;
        }

    }

    public BitmapLoadTask(@NonNull Context context, @NonNull Uri inputUri, int requiredWidth,
                          int requiredHeight, BitmapLoadCallback loadCallback) {
        this.context = context;
        this.inputUri = inputUri;
        this.requiredWidth = requiredWidth;
        this.requiredHeight = requiredHeight;
        bitmapLoadCallback = loadCallback;
    }

    @Override
    @NonNull
    protected BitmapLoadTask.BitmapWorkerResult doInBackground(Void... params) {
        if (inputUri == null) {
            return new BitmapLoadTask.BitmapWorkerResult(new NullPointerException("Input Uri cannot be null"));
        }

        try {
            processInputUri();
        } catch (NullPointerException | IOException e) {
            return new BitmapLoadTask.BitmapWorkerResult(e);
        }

        final ParcelFileDescriptor parcelFileDescriptor;
        try {
            parcelFileDescriptor = context.getContentResolver().openFileDescriptor(inputUri, "r");
        } catch (FileNotFoundException e) {
            return new BitmapLoadTask.BitmapWorkerResult(e);
        }

        final FileDescriptor fileDescriptor;
        if (parcelFileDescriptor != null) {
            fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        } else {
            return new BitmapLoadTask.BitmapWorkerResult(new NullPointerException("ParcelFileDescriptor was null for given Uri: [" + inputUri + "]"));
        }

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        if (options.outWidth == -1 || options.outHeight == -1) {
            return new BitmapLoadTask.BitmapWorkerResult(new IllegalArgumentException("Bounds for bitmap could not be retrieved from the Uri: [" + inputUri + "]"));
        }

        options.inSampleSize = BitmapLoadUtils.calculateInSampleSize(options, requiredWidth, requiredHeight);
        options.inJustDecodeBounds = false;

        Bitmap decodeSampledBitmap = null;

        boolean decodeAttemptSuccess = false;
        while (!decodeAttemptSuccess) {
            try {
                decodeSampledBitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
                decodeAttemptSuccess = true;
            } catch (OutOfMemoryError error) {
                Timber.e(error, "doInBackground: BitmapFactory.decodeFileDescriptor: " +
                        error.getLocalizedMessage());
                options.inSampleSize *= 2;
            }
        }

        if (decodeSampledBitmap == null) {
            return new BitmapLoadTask.BitmapWorkerResult(new IllegalArgumentException("Bitmap could not be decoded from the Uri: [" + inputUri + "]"));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            BitmapLoadUtils.close(parcelFileDescriptor);
        }

        int exifOrientation = BitmapLoadUtils.getExifOrientation(context, inputUri);
        int exifDegrees = BitmapLoadUtils.exifToDegrees(exifOrientation);
        int exifTranslation = BitmapLoadUtils.exifToTranslation(exifOrientation);

        ExifInfo exifInfo = new ExifInfo(exifOrientation, exifDegrees, exifTranslation);

        Matrix matrix = new Matrix();
        if (exifDegrees != 0) {
            matrix.preRotate(exifDegrees);
        }
        if (exifTranslation != 1) {
            matrix.postScale(exifTranslation, 1);
        }
        if (!matrix.isIdentity()) {
            return new BitmapLoadTask.BitmapWorkerResult(BitmapLoadUtils.transformBitmap(decodeSampledBitmap, matrix), exifInfo);
        }

        return new BitmapLoadTask.BitmapWorkerResult(decodeSampledBitmap, exifInfo);
    }

    private void processInputUri() throws NullPointerException, IOException {
        String inputUriScheme = inputUri.getScheme();
        Timber.w( "Uri scheme: " + inputUriScheme);
        if ("content".equals(inputUriScheme)) {
            String path = FileUtils.getPath(context, inputUri);
            if (!TextUtils.isEmpty(path) && new File(path).exists()) {
                inputUri = Uri.fromFile(new File(path));
            }
        } else if (!"file".equals(inputUriScheme)) {
            Timber.e("Invalid Uri scheme " + inputUriScheme);
            throw new IllegalArgumentException("Invalid Uri scheme" + inputUriScheme);
        }
    }

    @Override
    protected void onPostExecute(@NonNull BitmapLoadTask.BitmapWorkerResult result) {
        if (result.bitmapWorkerException == null) {
            bitmapLoadCallback.onBitmapLoaded(result.bitmapResult, result.exifInfo, inputUri.getPath());
        } else {
            bitmapLoadCallback.onFailure(result.bitmapWorkerException);
        }
    }
}
