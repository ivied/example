package com.tme.moments.presentation.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.tme.moments.R;

/**
 * @author zoopolitic.
 */
public class MaxHeightRecyclerView extends RecyclerView {

    private int maxHeight = 0;

    public MaxHeightRecyclerView(Context context) {
        this(context, null);
    }

    public MaxHeightRecyclerView(Context context,
                                 @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MaxHeightRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MaxHeightRecyclerView);
        maxHeight = a.getDimensionPixelSize(R.styleable.MaxHeightRecyclerView_mhrv_maxHeight, 0);
        a.recycle();
    }

    @Override protected void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(widthSpec, heightSpec);
        if (maxHeight > 0) {
            int height = getMeasuredHeight();
            int width = getMeasuredWidth();
            height = Math.min(height, maxHeight);
            setMeasuredDimension(width, height);
        }
    }
}
