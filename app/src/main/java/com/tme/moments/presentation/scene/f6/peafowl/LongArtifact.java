package com.tme.moments.presentation.scene.f6.peafowl;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.math.MathUtils;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
final class LongArtifact extends Decal {

    private float speed;

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float t;
    private float startDelayNanos;
    private float alphaSpeed;
    private float scaleDecreaseSpeed;

    LongArtifact(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        init();
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float deltaT = speed * deltaNanos;
        t += deltaT;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        float t1 = t + 0.3f;
        float x1 = (1 - t1) * (1 - t1) * p0x + 2 * (1 - t1) * t1 * p1x + t1 * t1 * p2x;
        float y1 = (1 - t1) * (1 - t1) * p0y + 2 * (1 - t1) * t1 * p1y + t1 * t1 * p2y;
        float angle = MathUtils.angleBetweenLines(x, y, x, y1, x, y, x1, y1) - 30;
        setRotationZ(-angle);

        setX(x);
        setY(y);

        float deltaAlpha = alphaSpeed * deltaNanos;
        float alpha = color.a;
        if (alpha < 1.0f) { // fade in
            alpha = Math.min(1.0f, alpha + deltaAlpha);
            setAlpha(alpha);
        }

        float deltaScale = scaleDecreaseSpeed * deltaNanos;
        setScale(Math.max(0.25f, getScaleX() - deltaScale));

        if (t >= 1) {
            init();
        }
    }

    protected void init() {
        p0x = random(0.68f, 0.75f);
        p0y = random(0.05f, 0.1f);
        p1x = random(0.72f, 1.0f);
        p1y = random(-0.7f, -0.4f);
        p2x = random(-1f, 1.25f);
        p2y = -1 - getHeight() / 2;

        alphaSpeed = 3.33f / Const.NANOS_IN_SECOND;
        scaleDecreaseSpeed = 0.1f / Const.NANOS_IN_SECOND;
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 8_000));
        float distance = MathUtils.dist(p0x, p0y, p1x, p1y);
        speed = distance / 2 / Const.NANOS_IN_SECOND;
        setColor(1, 1, 1, 1);
        setPosition(p0x, p0y, -1.0f);
        setAlpha(0);
        setScale(1);
        t = 0;
    }

    protected void setAlpha(float alpha) {
        float r = 1;
        float g = 1;
        float b = 1;
        setColor(r * alpha, g * alpha, b * alpha, alpha);
    }
}
