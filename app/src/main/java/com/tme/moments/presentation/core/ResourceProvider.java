package com.tme.moments.presentation.core;

import android.graphics.Bitmap;

/**
 * @author zoopolitic.
 */
public interface ResourceProvider {

    String getString(int stringResId);

    String getString(int stringResId, Object... formatArgs);

    int getColor(int colorResId);

    int dpToPx(int dp);

    Bitmap decodeBitmap(int drawableResId);
}
