package com.tme.moments.presentation.scene.f1.love;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.math.MathUtils;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class CometHeadHeart extends Sprite {

    private enum Direction {
        FROM_LEFT, FROM_RIGHT, FROM_BOTTOM
    }

    private static final float MIN_SCALE = 1.25f;
    private static final float MAX_SCALE = 1.5f;

    // 1 / interval = number of hearts generated during path
    private static final float HEART_GENERATION_INTERVAL_T = 0.005f;

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float t;
    private float speed;
    private float startDelayNanos;
    private float previousGeneratedHeartT;
    private float nextTailHeartX;
    private float nextTailHeartY;
    private boolean scaleUp = true;
    private float scaleUpSpeed;
    private float scaleDownSpeed;
    private float scaleWaitTime;
    private boolean finished;

    CometHeadHeart(TextureRegion region, float width, float height) {
        super(region, width, height);
        init();
    }

    private void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    int generatedHeartsCount() {
        int count = (int) ((t - previousGeneratedHeartT) / HEART_GENERATION_INTERVAL_T);
        if (count > 0) {
            previousGeneratedHeartT = t;
            float t0 = t - 0.04f;
            nextTailHeartX = (1 - t0) * (1 - t0) * p0x + 2 * (1 - t0) * t0 * p1x + t0 * t0 * p2x;
            nextTailHeartY = (1 - t0) * (1 - t0) * p0y + 2 * (1 - t0) * t0 * p1y + t0 * t0 * p2y;
        }
        return count;
    }

    boolean isFinished() {
        return finished;
    }

    void reset() {
        finished = false;
        init();
    }

    float getNextTailHeartX() {
        return nextTailHeartX;
    }

    float getNextTailHeartY() {
        return nextTailHeartY;
    }

    void step(long deltaNanos) {
        if (finished) {
            return;
        }
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float deltaT = speed * deltaNanos;
        t += deltaT;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        float t1 = t + 0.3f;
        float x1 = (1 - t1) * (1 - t1) * p0x + 2 * (1 - t1) * t1 * p1x + t1 * t1 * p2x;
        float y1 = (1 - t1) * (1 - t1) * p0y + 2 * (1 - t1) * t1 * p1y + t1 * t1 * p2y;
        float angle = MathUtils.angleBetweenLines(x, y, x, y1, x, y, x1, y1);
        if (angle != 0) {
            setRotationZ(y1 >= y ? -angle : -angle + 180);
        }

        if (scaleWaitTime > 0) {
            scaleWaitTime -= deltaNanos;
        } else {
            if (scaleUp) {
                float deltaScale = scaleUpSpeed * deltaNanos;
                float newScale = Math.min(MAX_SCALE, getScaleX() + deltaScale);
                setScale(newScale);
                if (newScale == MAX_SCALE) {
                    scaleUp = false;
                }
            } else {
                float deltaScale = scaleDownSpeed * deltaNanos;
                float newScale = Math.max(MIN_SCALE, getScaleX() - deltaScale);
                setScale(newScale);
                if (newScale == MIN_SCALE) {
                    scaleUp = true;
                    scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(700);
                }
            }
        }

        setX(x);
        setY(y);

        if (t >= 1) {
            finished = true;
        }
    }

    private void init() {
        t = 0;
        previousGeneratedHeartT = 0;
        float w = getWidth() * getScaleX();
        float h = getHeight() * getScaleX();
        // pick max side since heart rotating during journey
        float maxSide = Math.max(w, h);
        Direction direction = Direction.values()[random(Direction.values().length)];
        switch (direction) {
            case FROM_LEFT:
                p0x = -1 - maxSide / 2;
                p0y = random(-0.85f, 0.6f);
                p1x = random(-0.75f, 0.75f);
                p1y = random(-1.0f, 1.0f);
                p2x = 1 + maxSide / 2;
                p2y = p0y > 1.0f ? random(0.4f, 0.8f) : random(1.0f, 1.5f);
                break;
            case FROM_RIGHT:
                p0x = 1 + maxSide / 2;
                p0y = random(-0.85f, 0.6f);
                p1x = random(-0.75f, 0.75f);
                p1y = random(-1.0f, 1.0f);
                p2x = -1 - maxSide / 2;
                p2y = p0y > 1.0f ? random(0.4f, 0.8f) : random(1.0f, 1.5f);
                break;
            case FROM_BOTTOM:
                p0x = random(-1.1f, 1.1f);
                p0y = -1.0f - maxSide / 2;
                p1x = random(-0.75f, 0.75f);
                p1y = random(-0.75f, 0.75f);
                p2x = random(-1.1f, 1.1f);
                p2y = 1.0f + maxSide / 2;
                break;
        }
        scaleUpSpeed = 1.5f / Const.NANOS_IN_SECOND;
        scaleDownSpeed = 1.5f / Const.NANOS_IN_SECOND;
        speed = random(0.2666f, 0.2352f) / Const.NANOS_IN_SECOND;
        setRotationZ(0);
        setColor(1, 1, 1, 1);
        setPosition(p0x, p0y, -1.0f);
        setStartDelay(random(4000, 6000));
    }
}
