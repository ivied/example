package com.tme.moments.presentation.scene.f20.comic;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.math.MathUtils;
import com.tme.moments.presentation.Const;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Rocket extends AnimatedSprite {

    private boolean moveLeft;

    private enum Direction {
        LEFT, RIGHT, TOP
    }

    private float speed;

    private float p0x, p0y;
    private float p1x, p1y;
    private float p2x, p2y;

    private float t;
    private float startDelayNanos;

    Rocket(TextureRegion region, List<FrameInfo> frames, float width, float height) {
        super(region, frames, width, height);
        setTextureRegion(region);
        setDimensions(width, height);
        init();
    }

    @Override protected int getDesiredFPS() {
        return 30;
    }

    @Override public void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float delta = speed * deltaNanos;
        t += delta;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        float t1 = t + 0.3f;
        float x1 = (1 - t1) * (1 - t1) * p0x + 2 * (1 - t1) * t1 * p1x + t1 * t1 * p2x;
        float y1 = (1 - t1) * (1 - t1) * p0y + 2 * (1 - t1) * t1 * p1y + t1 * t1 * p2y;
        float angle = MathUtils.angleBetweenLines(x, y, x, y1, x, y, x1, y1);
        if (angle != 0) {
            angle += moveLeft ? 30 : -30;
            setRotationZ(y1 >= y ? -angle : -angle + 180);
        }

        setX(x);
        setY(y);
        if (t >= 1) {
            init();
        }
        super.step(deltaNanos);
    }

    protected void init() {
        Direction direction = Direction.values()[random(Direction.values().length)];
        switch (direction) {
            case LEFT:
                moveLeft = true;
                p0x = 1 + getWidth() / 2;
                p0y = random(-1f, 1f);
                p1x = random(-0.5f, 0.5f);
                p1y = random(-0.75f, 0.75f);
                p2x = random(-1 - getWidth() / 2, -1 - getWidth());
                p2y = random(-1f, 1f);
                break;
            case RIGHT:
                moveLeft = false;
                p0x = -1 - getWidth() / 2;
                p0y = random(-1f, 1f);
                p1x = random(-0.5f, 0.5f);
                p1y = random(-0.75f, 0.75f);
                p2x = random(1 + getWidth() / 2, 1 + getWidth());
                p2y = random(-1f, 1f);
                break;
            case TOP:
                p0x = random(-1.0f, 1.0f);
                p0y = -1 - getHeight() / 2;
                p1x = random(-0.75f, 0.75f);
                p1y = random(-0.5f, 0.5f);
                p2x = random(random(-1.0f, 1.0f));
                p2y = random(1 + getHeight() / 2, 1 + getHeight());
                moveLeft = p0x > p2x;
                break;
        }

        setScaleX(moveLeft ? 1 : -1);
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(5000, 7000));
        speed = random(0.375f, 0.6f) / Const.NANOS_IN_SECOND;
        setColor(1, 1, 1, 1);
        setRotationZ(0);
        setPosition(p0x, p0y, -1.0f);
        t = 0;
    }
}
