package com.tme.moments.presentation.scene.f14.beach;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Circle extends Decal {

    private static final float INITIAL_SCALE = 0.35f;
    private static final float DEFLATION_SPEED = 0.2f / Const.NANOS_IN_SECOND;
    private static final float INITIAL_INFLATION_SPEED = 1f / Const.NANOS_IN_SECOND;
    private static final float INFLATION_SPEED_INCREMENT = 2.5f / Const.NANOS_IN_SECOND;
    private static final float HANG_TIME = 500 * Const.NANOS_IN_MILLISECOND;

    private float speedNanos;

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point 1
    private float p2x, p2y; // control point 2
    private float p3x, p3y; // end point

    private float t;
    private float startDelayNanos;
    private float hangTimeNanos = HANG_TIME;
    private boolean inflated;
    private float inflationSpeed = INITIAL_INFLATION_SPEED;
    private float currentSpeedNanos;

    Circle(TextureRegion textureRegion, float width, float height) {
        setTextureRegion(textureRegion);
        dimensions.x = width;
        dimensions.y = height;
        setColor(1, 1, 1, 1);
        init();
    }

    /**
     * Sets start delay in seconds
     *
     * @param startDelayInSeconds start delay in seconds
     */
    void setStartDelay(float startDelayInSeconds) {
        startDelayNanos = startDelayInSeconds * Const.NANOS_IN_SECOND;
    }

    void step(long deltaNanos) {
        if (startDelayNanos >= 0) {
            startDelayNanos -= deltaNanos;
            return;
        }


        if (t >= 0.4) {
            if (!inflated) { // inflation faze
                float inflationDelta = inflationSpeed * deltaNanos;
                inflationSpeed += INFLATION_SPEED_INCREMENT;
                float newScale = Math.min(1.0f, getScaleX() + inflationDelta);
                setScale(newScale);
                if (newScale == 1.0f) {
                    inflated = true;
                    currentSpeedNanos = 0;
                }
                return;
            } else if (hangTimeNanos >= 0) {
                hangTimeNanos -= deltaNanos;
                return;
            } else {
                float deflateDelta = DEFLATION_SPEED * deltaNanos;
                float newScale = Math.max(0.5f, getScaleX() - deflateDelta);
                setScale(newScale);
            }
        }

        if (currentSpeedNanos < speedNanos) {
            float velocity = 0.01f / Const.NANOS_IN_SECOND;
            currentSpeedNanos += velocity;
            currentSpeedNanos = Math.min(speedNanos, currentSpeedNanos);
        }

        float speedDelta = currentSpeedNanos * deltaNanos;
        t += speedDelta;

        float single = (1 - t);
        float quadratic = (1 - t) * (1 - t);
        float cube = (1 - t) * (1 - t) * (1 - t);
        float x = cube * p0x + 3 * t * quadratic * p1x + 3 * t * t * single * p2x + t * t * t * p3x;
        float y = cube * p0y + 3 * t * quadratic * p1y + 3 * t * t * single * p2y + t * t * t * p3y;

        setX(x);
        setY(y);

        if (t >= 1) {
            init();
        }
    }

    private void init() {
        final float minP0X = 0.0f;
        final float maxP0X = 0.8f;

        final float minP1XInterval = 0.75f;
        final float maxP1XInterval = 1.25f;
        final float minP1Y = -0.15f;
        final float maxP1Y = 0.45f;

        final float minP2X = -0.75f;
        final float maxP2X = -0.45f;
        final float minP2Y = -1.25f;
        final float maxP2Y = -0.5f;

        final float minP3X = -1.35f;
        final float maxP3X = -1.15f;
        final float minP3Y = -0.9f;
        final float maxP3Y = -0.2f;

        final float minSpeed = 0.4f;
        final float maxSpeed = 0.7f;

        float speedPerSecond = random(minSpeed, maxSpeed);

        p0x = random(minP0X, maxP0X);
        p0y = -1 - getHeight() / 2;

        p1x = p0x + random(minP1XInterval, maxP1XInterval);
        p1y = random(minP1Y, maxP1Y);

        p2x = random(minP2X, maxP2X);
        p2y = random(minP2Y, maxP2Y);

        p3x = random(minP3X, maxP3X);
        p3y = random(minP3Y, maxP3Y);

        speedNanos = speedPerSecond / Const.NANOS_IN_SECOND;
        currentSpeedNanos = speedNanos;
        t = 0;
        hangTimeNanos = HANG_TIME;
        inflated = false;
        inflationSpeed = INITIAL_INFLATION_SPEED;
        setPosition(p0x, p0y, -1.0f);
        setScale(INITIAL_SCALE);
    }
}
