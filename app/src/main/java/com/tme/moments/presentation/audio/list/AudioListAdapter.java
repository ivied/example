package com.tme.moments.presentation.audio.list;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.tme.moments.presentation.recyclerview.DataBoundAdapter;
import com.tme.moments.presentation.recyclerview.DataBoundViewHolder;
import com.tme.moments.presentation.scene.list.SceneListItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AudioListAdapter extends DataBoundAdapter<AudioListItem> {

    public interface OnSongClickListener {
        void onFrameClicked(AudioListItem item);
    }

    private boolean clickable = true;

    private final OnSongClickListener onSongClickListener;

    @SuppressWarnings("ConstantConditions") public AudioListAdapter(List<AudioListItem> items, @NonNull
            OnSongClickListener onSongClickListener) {
        super(items);
        if (onSongClickListener == null) {
            throw new NullPointerException("OnFrameClickListener cannot be null");
        }
        this.onSongClickListener = onSongClickListener;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

    @Override protected int getLayoutResId(int position) {
        return getItem(position).getLayoutResId();
    }

    @Override public DataBoundViewHolder onCreateViewHolder(ViewGroup parent, int layoutId) {
        DataBoundViewHolder holder = super.onCreateViewHolder(parent, layoutId);
        holder.itemView.setOnClickListener(v -> {
            if (!clickable) {
                return;
            }
            int position = holder.getAdapterPosition();
            AudioListItem item = getItem(position);
            onSongClickListener.onFrameClicked(item);
        });
        return holder;
    }

    @Override public void addAll(List<? extends AudioListItem> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();

    }

    public void sort() {
        List<AudioListItem> sortItems = new ArrayList<>(items);
        Collections.sort(sortItems, (o1, o2) -> o1.name.compareTo(o2.name));
        items = sortItems;
        notifyDataSetChanged();
    }

    @Override public void add(AudioListItem item) {
        items.add(item);
        notifyItemChanged(items.size() - 1);
    }
}
