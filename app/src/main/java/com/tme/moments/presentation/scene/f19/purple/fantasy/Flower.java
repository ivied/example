package com.tme.moments.presentation.scene.f19.purple.fantasy;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Flower extends Decal {

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float speed;
    private float rotationSpeed;
    private float t;

    private final boolean moveLeft;
    private float startDelayNanos;
    private float fadeOutStartBorder;
    private float fadeOutEndBorder;

    Flower(TextureRegion region, float width, float height, boolean moveLeft) {
        this.moveLeft = moveLeft;
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        init();
    }

    public void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float deltaT = speed * deltaNanos;
        t += deltaT;

        float deltaRotation = rotationSpeed * deltaNanos;
        rotateZ(deltaRotation);

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        setX(x);
        setY(y);

        if (t >= fadeOutStartBorder) {
            float fraction = (t - fadeOutStartBorder) / (fadeOutEndBorder - fadeOutStartBorder);
            float alpha = Math.max(0, 1 - fraction);
            setAlpha(alpha);
        }

        if (t >= 1) {
            init();
        }
    }

    private void init() {
        if (moveLeft) {
            p0x = random(1 + getWidth() / 2, 2);
            p0y = random(-2.0f, 0.75f);
            p1x = random(-1.5f, 1.0f);
            p1y = random(-1.5f, 1.25f);
            p2x = random(-1.5f, -1 - getWidth() / 2);
            p2y = random(-0.5f, 2.0f);
        } else {
            p0x = random(-2, -1 - getWidth() / 2);
            p0y = random(-2.0f, 0.75f);
            p1x = random(-1.5f, 1.0f);
            p1y = random(-1.5f, 1.25f);
            p2x = random(1 + getWidth() / 2, 1.5f);
            p2y = random(-0.5f, 2.0f);
        }
        setPosition(p0x, p0y, -1.0f);
        speed = random(0.066f, 0.1f) / Const.NANOS_IN_SECOND;
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 10000));
        rotationSpeed = random(-135f, 135f) / Const.NANOS_IN_SECOND;
        fadeOutStartBorder = random(0.75f, 0.85f);
        fadeOutEndBorder = fadeOutStartBorder + random(0.1f, 0.25f);
        setAlpha(1);
        t = 0;
    }
}
