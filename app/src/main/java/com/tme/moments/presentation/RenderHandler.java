package com.tme.moments.presentation;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.lang.ref.WeakReference;

/**
 * Handler for RenderThread.  Used for messages sent from the UI thread to the render thread.
 * All openGL operations should be performed from the thread that owns EGL context
 * <p>
 * The object is created on the render thread, and the various "send" methods are called
 * from the UI thread.
 */
@SuppressWarnings("WeakerAccess")
public class RenderHandler extends Handler {

    private static final String TAG = RenderHandler.class.getSimpleName();

    static final int MSG_SURFACE_CREATED = 0;
    static final int MSG_SURFACE_CHANGED = 1;
    static final int MSG_DO_FRAME = 2;
    static final int MSG_SHUTDOWN = 3;
    static final int MSG_RECORD_ENABLED = 4;
    static final int MSG_ON_CAMERA_MODE_ENABLED = 5;
    static final int MSG_ON_IMAGE_MODE_ENABLED = 6;
    static final int MSG_SWITCH_SCENE = 7;
    static final int MSG_CAPTURE_SCREENSHOT = 8;
    static final int MSG_OBJECTS_STATE_CHANGED = 9;
    static final int MSG_BACKHROUND_EFFECTS_STATE_CHANGED = 10;

    // This shouldn't need to be a weak ref, since we'll go away when the Looper quits,
    // but no real harm in it.
    private WeakReference<RenderThread> weakRenderThread;

    /**
     * Call from render thread.
     */
    RenderHandler(RenderThread rt) {
        weakRenderThread = new WeakReference<>(rt);
    }

    /**
     * Sends the "surface created" message.
     * <p>
     * Call from UI thread.
     */
    public void sendSurfaceCreated() {
        sendMessage(obtainMessage(RenderHandler.MSG_SURFACE_CREATED));
    }

    /**
     * Sends the "surface changed" message, forwarding what we got from the SurfaceHolder.
     * <p>
     * Call from UI thread.
     */
    public void sendSurfaceChanged(int width, int height) {
        sendMessage(obtainMessage(RenderHandler.MSG_SURFACE_CHANGED, width, height));
    }

    public void sendRecordEnabled(boolean enabled) {
        sendMessage(obtainMessage(RenderHandler.MSG_RECORD_ENABLED, enabled));
    }

    /**
     * Sends the "do frame" message, forwarding the Choreographer or camera event.
     * <p>
     * Call from UI thread.
     */
    public void sendDoFrame(long frameTimeNanos) {
        sendMessage(obtainMessage(RenderHandler.MSG_DO_FRAME,
                (int) (frameTimeNanos >> 32), (int) frameTimeNanos));
    }

    public void sendCameraModeEnabled() {
        sendMessage(obtainMessage(MSG_ON_CAMERA_MODE_ENABLED));
    }

    public void sendSwitchScene() {
        sendMessage(obtainMessage(MSG_SWITCH_SCENE));
    }

    public void sendCaptureImage() {
        sendMessage(obtainMessage(MSG_CAPTURE_SCREENSHOT));
    }

    public void sendImageModeEnabled() {
        sendMessage(obtainMessage(MSG_ON_IMAGE_MODE_ENABLED));
    }

    /**
     * Sends the "shutdown" message, which tells the render thread to halt.
     * <p>
     * Call from UI thread.
     */
    public void sendShutdown() {
        removeCallbacksAndMessages(null);
        sendMessage(obtainMessage(RenderHandler.MSG_SHUTDOWN));
    }

    public void sendObjectsStateChanged() {
        sendMessage(obtainMessage(RenderHandler.MSG_OBJECTS_STATE_CHANGED));
    }

    public void sendBackgroundEffectsStateChanged() {
        sendMessage(obtainMessage(RenderHandler.MSG_BACKHROUND_EFFECTS_STATE_CHANGED));
    }

    @Override  // runs on RenderThread
    public void handleMessage(Message msg) {
        int what = msg.what;

        RenderThread renderThread = weakRenderThread.get();
        if (renderThread == null) {
            Log.w(TAG, "RenderHandler.handleMessage: weak ref is null");
            return;
        }

        switch (what) {
            case MSG_SURFACE_CREATED:
                renderThread.surfaceCreated();
                break;
            case MSG_SURFACE_CHANGED:
                renderThread.surfaceChanged(msg.arg1, msg.arg2);
                break;
            case MSG_DO_FRAME:
                long timestamp = (((long) msg.arg1) << 32) |
                        (((long) msg.arg2) & 0xffffffffL);
                renderThread.doFrame(timestamp);
                break;
            case MSG_SHUTDOWN:
                renderThread.shutdown();
                break;
            case MSG_RECORD_ENABLED:
                boolean enabled = (boolean) msg.obj;
                renderThread.setRecordEnabled(enabled);
                break;
            case MSG_ON_CAMERA_MODE_ENABLED:
                renderThread.onCameraModeEnabled();
                break;
            case MSG_ON_IMAGE_MODE_ENABLED:
                renderThread.onImageModeEnabled();
                break;
            case MSG_SWITCH_SCENE:
                renderThread.switchScene();
                break;
            case MSG_CAPTURE_SCREENSHOT:
                renderThread.captureScreenshot();
                break;
            case MSG_OBJECTS_STATE_CHANGED:
                renderThread.objectsStateChanged();
                break;
            case MSG_BACKHROUND_EFFECTS_STATE_CHANGED:
                renderThread.backgroundEffectsStateChanged();
                break;
            default:
                throw new RuntimeException("unknown message " + what);
        }
    }
}
