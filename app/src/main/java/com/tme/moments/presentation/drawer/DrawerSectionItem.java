package com.tme.moments.presentation.drawer;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.tme.moments.R;

/**
 * @author zoopolitic.
 */
@AutoValue
public abstract class DrawerSectionItem implements DrawerItem {

    @Override public int getLayoutResId() {
        return R.layout.drawer_section_item;
    }

    static DrawerSectionItem create(@NonNull String title) {
        return new AutoValue_DrawerSectionItem(title);
    }

    public abstract String title();
}
