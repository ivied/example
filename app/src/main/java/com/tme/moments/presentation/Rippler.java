package com.tme.moments.presentation;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.math.MathUtils;
import com.tme.moments.presentation.core.App;
import com.tme.moments.presentation.core.OpenGLLoader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TEXTURE0;
import static android.opengl.GLES20.GL_TEXTURE1;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.glActiveTexture;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniform1f;
import static android.opengl.GLES20.glUniform1i;
import static android.opengl.GLES20.glUniform2f;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class Rippler {

    static {
        System.loadLibrary("rippler");
    }

    private static final String TAG = Rippler.class.getSimpleName();

    private static final int MAX_HEIGHT_MAP_X = 180;
    private static final int MAX_HEIGHT_MAP_Y = 320;

    private static final int MAX_QUALITY = 75;
    private static final int MIN_QUALITY = 15;
    private static final int QUALITY = 45;

    private static final int RIPPLE_RADIUS = 1;

    private int programHandle = -1;
    private int uProjectionMatrixLocation = -1;
    private int aPositionLocation = -1;
    private int aTexCoordLocation = -1;
    private int uSteadyRippleHeightLocation = -1;
    private int uHeightMapSizeLocation = -1;
    private int uTextureLocation = -1;
    private int uHeightMapLocation = -1;

    private long dropInterval = 2000;

    private int[] heightMap;
    private int[] heightMapForTexture;
    private int[] heightMapOld;

    private Bitmap heightMapImage;

    private int heightMapWidth;
    private int heightMapHeight;
    private int heightTexture;
    private int imageTexture;

    private long lastDrop;
    private float qualityRate;
    private boolean rainEnabled = true;
    private int rippleHeight;
    protected float[] projectionMatrix = new float[16];

    private FloatBuffer floatBuffer;
    private int surfaceWidth;
    private int surfaceHeight;
    private int imageResId;

    private OpenGLLoader openGLLoader;

    public void init(OpenGLLoader openGLLoader) {
        this.openGLLoader = openGLLoader;
        prepareProgram();
    }

    public void setup(int surfaceWidth, int surfaceHeight, int imageResId) {
        this.surfaceWidth = surfaceWidth;
        this.surfaceHeight = surfaceHeight;
        this.imageResId = imageResId;
        initParams();
        initBitmaps();
        float z = -1.0f;
        reset();
        float[] vertices = new float[]{
                -1.0f, -1.0f, z, 0.0f, 1.0f,
                1.0f, -1.0f, z, 1.0f, 1.0f,
                -1.0f, 1.0f, z, 0.0f, 0.0f,
                1.0f, 1.0f, z, 1.0f, 0.0f
        };
        floatBuffer = ByteBuffer
                .allocateDirect(vertices.length * 4) // 4 bytes per float
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer().put(vertices);
        floatBuffer.position(0);
        Matrix.setIdentityM(projectionMatrix, 0);
    }

    public void reset() {
        lastDrop = System.currentTimeMillis();
        int sizeMap = heightMapWidth * (heightMapHeight + 2);
        heightMap = new int[sizeMap];
        heightMapOld = new int[sizeMap];
        heightMapForTexture = new int[sizeMap];
    }

    public void setRainEnabled(boolean rainEnabled) {
        this.rainEnabled = rainEnabled;
    }

    private void initParams() {
        int width = surfaceWidth;
        int height = surfaceHeight;
        int quality = Math.max(MIN_QUALITY, Math.min(MAX_QUALITY, QUALITY));
        qualityRate = ((float) quality) / 100.0f;
        rippleHeight = (int) (1000.0f * this.qualityRate);
        float windowWidthHeightRatio = ((float) Math.min(width, height)) / ((float) Math.max(width, height));
        heightMapWidth = (int) (((float) MAX_HEIGHT_MAP_X) * qualityRate);
        heightMapHeight = (int) (((float) heightMapWidth) / windowWidthHeightRatio);
        imageTexture = openGLLoader.loadTexture(imageResId).textureHandle;
        heightTexture = openGLLoader.loadTexture(imageResId, heightMapWidth, heightMapHeight).textureHandle;
    }

    private void initBitmaps() {
        Bitmap outputImage = BitmapFactory.decodeResource(App.getInstance().getResources(), imageResId);
        heightMapImage = Bitmap.createBitmap(heightMapWidth, heightMapHeight, Bitmap.Config.ARGB_8888);
        assignBitmap(imageTexture, outputImage);
    }

    private void prepareProgram() {
        programHandle =
                openGLLoader.createProgram(R.raw.water_vertex_shader, R.raw.water_fragment_shader);

        aPositionLocation = glGetAttribLocation(programHandle, "a_position");
        GlUtil.checkLocation(aPositionLocation, "a_position");
        glEnableVertexAttribArray(aPositionLocation);

        aTexCoordLocation = glGetAttribLocation(programHandle, "a_texCoord");
        GlUtil.checkLocation(aTexCoordLocation, "a_texCoord");
        glEnableVertexAttribArray(aTexCoordLocation);

        uProjectionMatrixLocation = glGetUniformLocation(programHandle, "u_projTrans");
        GlUtil.checkLocation(uProjectionMatrixLocation, "u_projTrans");

        uSteadyRippleHeightLocation = glGetUniformLocation(programHandle, "u_steadyRippleHeight");
        GlUtil.checkLocation(uSteadyRippleHeightLocation, "u_steadyRippleHeight");

        uHeightMapSizeLocation = glGetUniformLocation(programHandle, "u_heightMapSize");
        GlUtil.checkLocation(uHeightMapSizeLocation, "u_heightMapSize");

        uTextureLocation = glGetUniformLocation(programHandle, "u_texture");
        GlUtil.checkLocation(uTextureLocation, "u_texture");

        uHeightMapLocation = glGetUniformLocation(programHandle, "u_heightMap");
        GlUtil.checkLocation(uHeightMapLocation, "u_heightMap");
    }

    public void release() {
        glDeleteProgram(programHandle);
        programHandle = -1;
    }

    private void assignBitmap(int textureId, Bitmap bitmap) {
        GLES20.glBindTexture(GL_TEXTURE_2D, textureId);
        GLUtils.texImage2D(GL_TEXTURE_2D, 0, bitmap, 0);
        GLES20.glBindTexture(GL_TEXTURE_2D, 0);
    }

    public void render() {
        glUseProgram(programHandle);
        prepareNextFrame();
        heightMapImage.setPixels(heightMapForTexture, 0, heightMapWidth, 0, 0, heightMapWidth, heightMapHeight);
        assignBitmap(heightTexture, heightMapImage);

        // set uniforms
        glUniformMatrix4fv(uProjectionMatrixLocation, 1, false, projectionMatrix, 0);
        glUniform1f(uSteadyRippleHeightLocation, 1024.0f);
        glUniform2f(uHeightMapSizeLocation, heightMapWidth, heightMapHeight);

        // bind image texture
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, imageTexture);
        glUniform1i(uTextureLocation, 0);

        // bind heightmap texture
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, heightTexture);
        glUniform1i(uHeightMapLocation, 1);

        int stride = 5 * DecalBatch.SIZE_OF_FLOAT;

        floatBuffer.position(0);
        glVertexAttribPointer(aPositionLocation, 3, GL_FLOAT, false, stride, floatBuffer);
        glEnableVertexAttribArray(aPositionLocation);

        floatBuffer.position(3);
        glVertexAttribPointer(aTexCoordLocation, 2, GL_FLOAT, false, stride, floatBuffer);
        glEnableVertexAttribArray(aTexCoordLocation);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glActiveTexture(GL_TEXTURE0);

        if (rainEnabled) {
            handleRain();
        }
    }

    public void disturbSurface(int xCoord, int yCoord, float mult) {
        int width = surfaceWidth;
        int height = surfaceHeight;
        xCoord = (int) ((float) xCoord / ((float) width / heightMapWidth));
        yCoord = (int) ((float) yCoord / ((float) height / heightMapHeight));
        int y = yCoord - RIPPLE_RADIUS;
        while (y < RIPPLE_RADIUS + yCoord) {
            int x = xCoord - RIPPLE_RADIUS;
            while (x < RIPPLE_RADIUS + xCoord) {
                if (y >= 0 && y < heightMapHeight && x >= 0 && x < heightMapWidth
                        && MathUtils.dist(xCoord, yCoord, x, y) <= RIPPLE_RADIUS) {
                    int[] iArr = heightMap;
                    int i = (heightMapWidth + (heightMapWidth * y)) + x;
                    iArr[i] = (int) (iArr[i] + (rippleHeight * mult * qualityRate));
                }
                x++;
            }
            y++;
        }
    }

    private void prepareNextFrame() {
        int index = heightMapWidth;
        prepareNextFrame(heightMap, heightMapWidth, heightMapHeight, index, heightMapOld, heightMapForTexture);
        int[] temp = heightMapOld;
        heightMapOld = heightMap;
        heightMap = temp;
    }

    private native int prepareNextFrame(int[] heightMap, int heightMapWidth, int heightMapHeight,
                                        int index, int[] heightMapOld, int[] heightMapForTexture);

    private void handleRain() {
        int width = surfaceWidth;
        int height = surfaceHeight;
        long currentTime = System.currentTimeMillis();
        if (currentTime - lastDrop > dropInterval) {
            dropInterval = (long) random(1000, 2500);
            lastDrop = currentTime;
            int x = random(0, width);
            int y = random(0, height);
            float multiplier = 2.0f;
            disturbSurface(x, y, multiplier);
        }
    }
}
