package com.tme.moments.presentation.scene.f11.spring;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class Bird extends AnimatedSprite {

    private long startDelayNanos;
    private int playedCount;
    private int maxPlaysCount;

    Bird(TextureRegion region,
         List<FrameInfo> frames, float width, float height) {
        super(region, frames, width, height);
        init();
    }

    private void init() {
        playedCount = 0;
        maxPlaysCount = random(2, 5);
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(2500, 4500));
    }

    @Override public void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        super.step(deltaNanos);
    }

    @Override protected int getDesiredFPS() {
        return 10;
    }

    @Override protected void onAnimationReset() {
        super.onAnimationReset();
        if (++playedCount == maxPlaysCount) {
            init();
        }
    }
}
