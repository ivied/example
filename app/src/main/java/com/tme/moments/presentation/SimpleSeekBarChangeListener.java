package com.tme.moments.presentation;

import android.widget.SeekBar;

/**
 * @author zoopolitic.
 */
public abstract class SimpleSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

    @Override public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
