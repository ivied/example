package com.tme.moments.presentation.audio.list;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.tme.moments.R;

public class DataBindingAdapter {


    @BindingAdapter("android:src")
    public static void setSrcUri(ImageView view, Uri imageUri) {
        Picasso.get()
//                .load(imageUri)
                .load(R.drawable.ic_audio_placeholder)
                .placeholder(R.drawable.ic_audio_placeholder)
                .into(view);
    }

    @BindingAdapter("android:src")
    public static void setSrcBitmap(ImageView view, Bitmap imageBitmap) {
        if (imageBitmap != null)
            view.setImageBitmap(imageBitmap);
        else
            view.setImageResource(R.drawable.ic_audio_placeholder);
    }
}
