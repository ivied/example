package com.tme.moments.presentation.audio;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.WindowManager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.jakewharton.rxbinding2.view.RxView;
import com.tme.moments.BuildConfig;
import com.tme.moments.R;
import com.tme.moments.databinding.AudioActivityBinding;
import com.tme.moments.presentation.audio.list.AudioListAdapter;
import com.tme.moments.presentation.audio.list.AudioListItem;
import com.tme.moments.presentation.base.BaseActivity;
import com.tme.moments.presentation.bitmap.FileUtils;
import com.tme.moments.presentation.core.App;
import com.tme.moments.presentation.core.Config;
import com.tme.moments.presentation.recyclerview.DisableableLayoutManager;
import com.tme.moments.presentation.utils.AndroidUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class AudioActivity extends BaseActivity {

    private static final int REQUEST_IMPORT_MUSIC = 111;
    private AudioActivityBinding binding;
    AudioListAdapter adapter;
    private CompositeDisposable disposable;
    private AdView adView;

    @Inject Config config;

    private RewardedVideoAd rewardAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        rewardAd = App.getInstance().getMusicRewardAd();

        if(config.isShowMusicAd()) {
            if (rewardAd.isLoaded()) {
                rewardAd.show();

                rewardAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
                    @Override
                    public void onRewardedVideoAdLoaded() {

                    }

                    @Override
                    public void onRewardedVideoAdOpened() {

                    }

                    @Override
                    public void onRewardedVideoStarted() {

                    }

                    @Override
                    public void onRewardedVideoAdClosed() {
                        App.getInstance().reloadRewardMusicAd();
                        if( config.isShowMusicAd()){
                            finish();
                        }
                    }

                    @Override
                    public void onRewarded(RewardItem rewardItem) {
                        config.onMusicAdd();
                    }

                    @Override
                    public void onRewardedVideoAdLeftApplication() {

                    }

                    @Override
                    public void onRewardedVideoAdFailedToLoad(int i) {

                    }

                    @Override
                    public void onRewardedVideoCompleted() {
                    }
                });
            } else {
                if (AndroidUtils.checkConnection(this)) {

                    longToast(R.string.notification_wait_loading);
                } else {
                    longToast(R.string.error_check_internet_connection);
                }
                finish();
            }
        } else {
            config.onMusicAdd();
        }
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.audio_activity);
        initiate();
    }

    private void initiate() {
        initAd();
        initToolbar();
        initClickers();
        initAudioList();
    }

    private void initAd() {
        adView = new AdView(this);
        adView.setId(View.generateViewId());
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(BuildConfig.MUSIC_BANNER_AD_UNIT);
        //adView.setPadding(0, AndroidUtils.dpToPx(this,5), 0 ,0);
        binding.bottom.addView(adView);
        ConstraintSet set = new ConstraintSet();
        set.clone(binding.bottom);
        set.centerHorizontally(adView.getId(), ConstraintSet.PARENT_ID);
        int topMargin = AndroidUtils.dpToPx(this, 5);
        set.connect(adView.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0);
        set.clear(binding.chooseFromAnotherApp.getId(), ConstraintSet.BOTTOM);
        set.connect(adView.getId(), ConstraintSet.TOP, binding.chooseFromAnotherApp.getId(), ConstraintSet.BOTTOM, topMargin);
        set.setVerticalBias(adView.getId(), 1f);
        set.applyTo(binding.bottom);
        adView.loadAd(new AdRequest.Builder().build());

    }

    private void initToolbar() {
        setSupportActionBar(binding.toolbar.mainToolbar);
        setActionBarDefaultTitleVisible(false);
        binding.toolbar.mainToolbar.setTitle("Music");
    }

    private void initClickers() {
        disposable = new CompositeDisposable();
        subscribeOtherAppAudioClick();
    }

    private void subscribeOtherAppAudioClick() {
        disposable.add(RxView.clicks(binding.chooseFromAnotherApp)
                .throttleFirst(1000, TimeUnit.MILLISECONDS)
                .subscribe(o -> {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setType("audio/mpeg");
                    startActivityForResult(intent, REQUEST_IMPORT_MUSIC);

                }));
    }

    @Override
    protected void onResume() {
        super.onResume();
        adView.resume();
        initClickers();
    }

    @Override
    protected void onPause() {
        super.onPause();
        adView.pause();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adView.destroy();
    }

    @SuppressLint("CheckResult")
    private void initAudioList() {
        adapter = new AudioListAdapter(new ArrayList<>(), item -> {

            finishOk(item.path);
        });
        binding.toolbar.toolbarProgressBar.setVisibility(View.VISIBLE);
        Observable<AudioListItem> listItemObservable = Observable.create(e ->
                getAudio(new FileSearch() {
                    @Override
                    public void newFile(AudioListItem item) {
                        e.onNext(item);
                    }

                    @Override
                    public void onComplete() {
                        e.onComplete();
                    }
                }));
        disposable.add(listItemObservable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<AudioListItem>() {
                    @Override
                    public void onNext(AudioListItem value) {
                        adapter.add(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        binding.toolbar.toolbarProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onComplete() {
                        adapter.sort();
                        if(adapter.getItemCount() == 0){
                            binding.audioListView.setVisibility(View.GONE);
                        }
                        binding.toolbar.toolbarProgressBar.setVisibility(View.GONE);
                    }
                }));

        DisableableLayoutManager sceneLayoutManager = new DisableableLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.audioListView.setHasFixedSize(true);
        binding.audioListView.setLayoutManager(sceneLayoutManager);
        binding.audioListView.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMPORT_MUSIC) {
            if (resultCode == RESULT_OK && data != null) {
                final Uri uri = data.getData();
                String path = null;
                if (uri != null)
                    path = FileUtils.getPath(this, uri);
                finishOk(path);
            }
        }
    }

    private void getAudio(FileSearch callback) {
        Cursor cur = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null, null);

        if (cur != null && cur.moveToFirst()) {
            do {
                int albumIndex = cur.getColumnIndex(MediaStore.Audio.Media.ARTIST_ID);
                int titleIndex = cur.getColumnIndex(MediaStore.Audio.Media.TITLE);
                int dataIndex = cur.getColumnIndex(MediaStore.Audio.Media.DATA);
                int ID = cur.getInt(albumIndex);
                String path = cur.getString(dataIndex);
                String name = cur.getString(titleIndex);
                if (name == null) {
                    name = "unknown title";
                } else {
                    name = name.trim();
                }
                if (path != null) {
                    Uri sArtworkUri = Uri
                            .parse("content://media/external/audio/albumart");
                    Uri uri = ContentUris.withAppendedId(sArtworkUri,
                            ID);
                    if (path.contains("mp3") && !isFileCorrupted(path)) {
                        callback.newFile(new AudioListItem(path, name, uri));
                    }
                }
            } while (cur.moveToNext());
            cur.close();
        }
        callback.onComplete();
    }

    private boolean isFileCorrupted(String path) {
        try {
            MediaMetadataRetriever metaRetriever;
            metaRetriever = new MediaMetadataRetriever();
            metaRetriever.setDataSource(path);
            metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        } catch (Exception e) {
            return true;
        }
        return false;
    }

    private void finishOk(String path) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("path", path);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

//    void getPlayList(String rootPath, boolean firstDeep, FileSearch callback) {
//
//
//        try {
//            File rootFolder = new File(rootPath);
//            File[] files = rootFolder.listFiles(); //here you will get NPE if directory doesn't contains  any file,handle it like this.
//            for (File file : files) {
//                if (file.isDirectory()) {
//                    getPlayList(file.getAbsolutePath(), false, callback);
//                } else if (file.getName().endsWith(".mp3")) {
//                    AudioListItem item = new AudioListItem(file.getAbsolutePath(), file.getName());
//                    callback.newFile(item);
//                }
//            }
//        } catch (Exception e) {
//            Timber.e(e);
//        }
//        if (firstDeep) {
//
//            callback.onComplete();
//            //write your code here to be executed after 1 second
//        }
//    }

//    ArrayList<AudioListItem> getPlayList(String rootPath) {
//        ArrayList<AudioListItem> fileList = new ArrayList<>();
//
//        try {
//            File rootFolder = new File(rootPath);
//            File[] files = rootFolder.listFiles(); //here you will get NPE if directory doesn't contains  any file,handle it like this.
//            for (File file : files) {
//                if (file.isDirectory()) {
//                    if (getPlayList(file.getAbsolutePath()) != null) {
//                        fileList.addAll(getPlayList(file.getAbsolutePath()));
//                    } else {
//                        break;
//                    }
//                } else if (file.getName().endsWith(".mp3")) {
//                    AudioListItem item = new AudioListItem(file.getAbsolutePath(), file.getName());
//                    fileList.add(item);
//                }
//            }
//        } catch (Exception e) {
//            Timber.e(e);
//        }
//       return fileList;
//    }

    interface FileSearch {
        void newFile(AudioListItem item);

        void onComplete();
    }
}
