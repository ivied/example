package com.tme.moments.presentation.scene.f10.lemon;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class FallingCircle extends Decal {

    private float speed;
    private float rotationSpeed;

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float startDelayNanos;
    private float t;

    FallingCircle(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        init();
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(4000, 14000));
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float deltaRotation = rotationSpeed * deltaNanos;
        rotateZ(deltaRotation);

        float delta = speed * deltaNanos;
        t += delta;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        setX(x);
        setY(y);

        if (t >= 1 || color.a == 0) {
            init();
        }
    }

    protected void init() {
        float speedPerSecond = random(0.1111f, 0.2f);
        speed = speedPerSecond / Const.NANOS_IN_SECOND;
        // rotation speed proportional to speed
        rotationSpeed = (speedPerSecond * 900) / Const.NANOS_IN_SECOND;
        boolean rotateRight = random();
        if (rotateRight) {
            rotationSpeed *= -1f;
        }
        boolean leftSide = random();
        p0x = leftSide
                ? random(-1.0f + getWidth() / 2, -0.4f - getWidth() / 2)
                : random(0.4f + getWidth() / 2, 1.0f - getWidth() / 2);
        p0y = random(1.0f + getHeight() / 2, 1.5f);
        p1x = leftSide
                ? random(-1.25f, 0)
                : random(0, 1.25f);
        p1y = p0y - random(0.1f, 0.4f);
        p2x = leftSide
                ? random(-1.25f, -0.75f - getWidth() / 2)
                : random(0.75f + getWidth() / 2, 1.25f);
        p2y = random(-1.0f - getWidth(), -1.0f - getWidth() / 2);
        setColor(1, 1, 1, 1);
        float z = -1.0f;
        setPosition(p0x, p0y, z);
        setAlpha(1);
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 10000));
        t = 0;
    }
}
