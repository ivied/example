package com.tme.moments.presentation.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.tme.moments.R;

/**
 * @author zoopolitic.
 */
public class MaxHeightConstraintLayout extends ConstraintLayout {

    private int maxHeight = 0;

    public MaxHeightConstraintLayout(Context context) {
        this(context, null);
    }

    public MaxHeightConstraintLayout(Context context,
                                     @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MaxHeightConstraintLayout(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MaxHeightConstraintLayout);
        maxHeight = a.getDimensionPixelSize(R.styleable.MaxHeightConstraintLayout_mhcl_maxHeight, 0);
        a.recycle();
    }

    @Override protected void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(widthSpec, heightSpec);
        if (maxHeight > 0) {
            int height = getMeasuredHeight();
            int width = getMeasuredWidth();
            height = Math.min(height, maxHeight);
            super.onMeasure(
                    MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
            );
            setMeasuredDimension(width, height);
        }
    }
}
