package com.tme.moments.presentation.capture;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.MediaActionSound;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.transition.ChangeBounds;
import android.support.transition.Transition;
import android.support.transition.TransitionManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.system.ErrnoException;
import android.system.OsConstants;
import android.text.TextUtils;
import android.util.Log;
import android.view.Choreographer;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.jakewharton.rxbinding2.view.RxView;
import com.tme.moments.R;
import com.tme.moments.audio.AudioConversionService;
import com.tme.moments.audio.ConversionFinishedEvent;
import com.tme.moments.audio.ConversionStartedEvent;
import com.tme.moments.databinding.CaptureActivityBinding;
import com.tme.moments.presentation.RecordButton;
import com.tme.moments.presentation.RecordMode;
import com.tme.moments.presentation.RenderHandler;
import com.tme.moments.presentation.RenderThread;
import com.tme.moments.presentation.audio.AudioActivity;
import com.tme.moments.presentation.audio.player.AudioPlayerFragment;
import com.tme.moments.presentation.base.BaseImagePickerActivity;
import com.tme.moments.presentation.bitmap.BitmapLoadUtils;
import com.tme.moments.presentation.core.App;
import com.tme.moments.presentation.core.Config;
import com.tme.moments.presentation.core.ScreenshotConfig;
import com.tme.moments.presentation.drawer.DrawerFragment;
import com.tme.moments.presentation.preview.PreviewActivity;
import com.tme.moments.presentation.scene.Scene;
import com.tme.moments.presentation.scene.list.SceneListFragment;
import com.tme.moments.presentation.utils.AndroidUtils;
import com.tme.moments.presentation.utils.CameraUtils;
import com.tme.moments.presentation.utils.PermissionUtils;
import com.tme.moments.recording.RecordListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK;
import static android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT;

public class CaptureActivity extends BaseImagePickerActivity
        implements
        HasSupportFragmentInjector,
        CapturePresenter.View,
        SurfaceHolder.Callback,
        SurfaceTexture.OnFrameAvailableListener,
        Choreographer.FrameCallback, SceneListFragment.Listener,
        RecordButton.RecordListener, AudioPlayerFragment.OnAudioClearListener {

    private static final String TAG = CaptureActivity.class.getSimpleName();

    private static final int REQUEST_IMPORT_MUSIC = 111;
    private static final int REQUEST_PERMISSIONS = 112;

    private static final String SS_IMPORT_IN_PROGRESS = "SS_IMPORT_IN_PROGRESS";
    private static final String SS_IMAGE_URI = "SS_IMAGE_URI";

    private static String[] PERMISSIONS = {
            CAMERA,
            WRITE_EXTERNAL_STORAGE,
            RECORD_AUDIO
    };

    @Inject CapturePresenter presenter;
    @Inject Config config;
    @Inject DispatchingAndroidInjector<Fragment> fragmentInjector;

    private CaptureActivityBinding binding;
    private RenderThread renderThread;
    private CustomTouchListener touchListener;
    @Nullable private Camera camera;
    private CompositeDisposable clicksDisposable;
    private ProgressDialog importDialog;
    private MediaActionSound shutterSound;
    private ExecutorService executorService;
    private Future<?> capturePhotoFuture;
    private Transition navigationButtonTransition;
    private Boolean isDark;
    private RewardedVideoAd rewardedVideoAd;



    @Override public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentInjector;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        isDark= config.isDarkTheme();
        if (!config.isDarkTheme()) {
            setTheme(R.style.AppTheme_Light);
        }
        binding = DataBindingUtil.setContentView(this, R.layout.capture_activity);
        binding.surfaceView.getHolder().addCallback(this);
        touchListener = new CustomTouchListener(this);
        binding.surfaceView.setOnTouchListener(touchListener);
        shutterSound = new MediaActionSound();
        shutterSound.load(MediaActionSound.SHUTTER_CLICK);
        executorService = Executors.newSingleThreadExecutor();
        if (savedInstanceState != null) {
            Uri imageUri = savedInstanceState.getParcelable(SS_IMAGE_URI);
            if (imageUri != null) {
                config.setImageUri(imageUri);
            }
            boolean importInProgress = savedInstanceState.getBoolean(SS_IMPORT_IN_PROGRESS, false);
            if (importInProgress) {
                showImportDialog();
            }
        }

        String tag = SceneListFragment.class.getName();
        SceneListFragment fragment = SceneListFragment.newInstance();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragment, tag)
                .commit();

        binding.recordButton.setConfig(config);
        binding.recordButton.setRecordListener(this);

        binding.switchMode.setOnClickListener(v -> {
            if (config.getRecordMode() == RecordMode.CAPTURE_VIDEO) {
                Uri imageUri = config.getImageUri();
                if (imageUri == null) {
                    captureCameraImage();
                } else {
                    presenter.switchMode();
                }
            } else {
                presenter.switchMode();
            }
        });
        binding.switchModeArrows.setOnClickListener(v -> {
            presenter.switchMode();
        });
        binding.drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override public void onDrawerSlide(View drawerView, float slideOffset) {
                binding.drawerLayout.bringChildToFront(drawerView);
                binding.drawerLayout.requestLayout();
            }
        });
        binding.drawerLayout.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override public boolean onPreDraw() {
                binding.drawerLayout.getViewTreeObserver().removeOnPreDrawListener(this);
                //noinspection UnnecessaryLocalVariable
                int drawerWidth = (int) (AndroidUtils.getDisplayWidth(CaptureActivity.this) * 0.676f); // % of the screen
                binding.drawerContainer.getLayoutParams().width = drawerWidth;
                Intent intent = getIntent();
                if (intent.getBooleanExtra(DrawerFragment.EXTRA_THEME_CHANGE, false)) {
                    DrawerLayout drawerLayout = binding.drawerLayout;
                    drawerLayout.post(() -> drawerLayout.openDrawer(Gravity.START, false));
                    getIntent().putExtra(DrawerFragment.EXTRA_THEME_CHANGE, false);
                }
                return true;
            }
        });
        replaceFragment(DrawerFragment.newInstance(), R.id.drawerContainer, false);
        binding.settings.setOnClickListener(v -> binding.drawerLayout.openDrawer(Gravity.START));
        binding.backgroundEffect.setOnClickListener(v -> {
            presenter.switchBackgroundEffectsEnabled();
            renderThread.getHandler().sendBackgroundEffectsStateChanged();
        });
        binding.objectsButton.setOnClickListener(v -> {
            presenter.switchObjectsEnabled();
            renderThread.getHandler().sendObjectsStateChanged();
        });
        binding.navigationButton.setOnClickListener(v -> {
            AudioPlayerFragment f = getAudioPlayer();
            if (f != null && !f.isHidden()) {
                openSceneList(true);
            } else {
                openAudioPlayer(true);
            }
        });
        navigationButtonTransition = new ChangeBounds();
        navigationButtonTransition.setDuration(getResources().getInteger(R.integer.duration_scene_switch_duration));
        navigationButtonTransition.excludeTarget(R.id.surfaceView, true);
        navigationButtonTransition.setInterpolator(new FastOutSlowInInterpolator());
        rewardedVideoAd = App.getInstance().getRewardAd();

        App.getInstance().reloadRewardMusicAd();
    }

    private void setRecording(boolean recording, boolean exitApp) {
        RenderHandler rh = renderThread == null ? null : renderThread.getHandler();
        if (rh == null) {
            return;
        }
        if (recording) {
            int screenWidth = AndroidUtils.getDisplayWidth(this);
            if (!presenter.prepareRecordingConfig(this, screenWidth)) {
                longToast(R.string.error_prepare_record_config);
                return;
            }
            rh.sendRecordEnabled(true);
        } else {
            // clear audio only if it was manual action
            if (!exitApp) {
                clearAudio(false);
            }
            // first - notify record button that recording was finished
            // to not trigger onRecordTimedOut during to asynchronous nature
            // of stop recording (RenderThread is a separate thread plus
            // encoders run on separate threads each)
            binding.recordButton.setRecording(false);
            rh.removeCallbacksAndMessages(null);
            // remove frame callback because we don't need choreographer frames to draw when record is finished
            Choreographer.getInstance().removeFrameCallback(this);
            rh.sendRecordEnabled(false);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConversionStared(ConversionStartedEvent e) {
        showImportDialog();
    }

    private void showImportDialog() {
        importDialog = new ProgressDialog(new ContextThemeWrapper(this, R.style.DefaultAlertDialog));
        importDialog.setCancelable(false);
        importDialog.setMessage(getString(R.string.import_dialog_message));
        importDialog.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConversionFinished(ConversionFinishedEvent e) {
        config.setAudioFilePath(e.convertedFilePath());
        openAudioPlayer(false);
        dismissImportDialog();
    }

    private void dismissImportDialog() {
        if (importDialog != null) {
            importDialog.dismiss();
            importDialog = null;
        }
    }

    @Override protected void onStart() {
        if (isDark != null && isDark != config.isDarkTheme()) {
            recreate();
        }
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override protected void onStop() {
        super.onStop();
        stopPreviewAndReleaseCamera();
        EventBus.getDefault().unregister(this);
    }


    @Override protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (importDialog != null) {
            outState.putBoolean(SS_IMPORT_IN_PROGRESS, true);
        }
        Uri imageUri = config.getImageUri();
        if (imageUri != null) {
            outState.putParcelable(SS_IMAGE_URI, imageUri);
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        syncFragmentsState();
    }

    @Override protected void onResume() {
        super.onResume();
        presenter.attachView(this);
        if (importDialog != null) {
            importDialog.show();
        }
        touchListener.setScene(currentScene());
        clicksDisposable = new CompositeDisposable();
        subscribeToRecordsButtonClicks();
        subscribeToFastScreenshotButtonClicks();
        subscribeToActionButtonClicks();
        subscribeToPickImageClicks();
        subscribeToImportAudioClicks();
        if (!allPermissionGranted()) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_PERMISSIONS);
            return;
        }

        boolean useCamera = config.getImageUri() == null;
        if (!useCamera) {
            Choreographer.getInstance().postFrameCallback(this);
        }

        // if screen was turned off and turned on - surfaceChanged won't be called
        // but onPause was triggered - so we need to open camera again
        if (renderThread != null && useCamera && camera == null) {
            SurfaceView surfaceView = binding.surfaceView;
            int width = surfaceView.getWidth();
            int height = surfaceView.getHeight();
            int cameraType = config.getCameraType();
            if (!openCamera(cameraType, width, height)) {
                showOpenCameraError();
            }
        }
    }

    private void syncFragmentsState() {
        switch (config.getCurrentScreen()) {
            case AUDIO_PLAYER:
                openSceneList(false);
                openAudioPlayer(false);
                break;
            case SCENE_LIST:
                if (config.getAudioFilePath() != null) {
                    // if uncomment that we will have bug with Home Screen design issue ( since old builds) :
                    // start app -> home screen -> import music -> music screen ->
                    // tap on device back button & come home screen -> open settings -> change theme
                    // -> see the home screen design -> Music screen images are interrupting
                    //openAudioPlayer(false);
                } else {
                    Fragment player = getAudioPlayer();
                    if (player != null) {
                        fragmentManager
                                .beginTransaction()
                                .remove(player)
                                .commitAllowingStateLoss();
                    }
                }
                openSceneList(false);
                break;
        }
    }

    @Override protected void onPause() {
        setRecording(false, true);
        presenter.detachView();
        if (capturePhotoFuture != null) {
            capturePhotoFuture.cancel(true);
        }
        touchListener.setScene(null);
        if (importDialog != null) {
            importDialog.dismiss();
        }
        super.onPause();

        stopPreviewAndReleaseCamera();
        if (clicksDisposable != null && !clicksDisposable.isDisposed()) {
            clicksDisposable.dispose();
        }

        Choreographer.getInstance().removeFrameCallback(this);
    }


    @Override public void onBackPressed() {
        AudioPlayerFragment audioPlayer = getAudioPlayer();
        if (audioPlayer != null && !audioPlayer.isHidden()) {
            openSceneList(true);
            return;
        }
        // clear only if we are not currently recording
        Uri imageUri = config.getImageUri();
        if (imageUri != null) {
            clearImage();
        } else {
            clearAudio(false);
            super.onBackPressed();
        }
    }

    //    ============================= [START] Audio player =======================================

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMPORT_MUSIC) {
            if (resultCode == RESULT_OK && data != null) {
//                final Uri uri = data.getData();
//                if (uri == null) {
//                    longToast(R.string.error_pick_music);
//                    return;
//                }
//                String path = FileUtils.getPath(this, uri);
                String path = data.getStringExtra("path");
                if (path == null) {
                    longToast(R.string.error_pick_music);
                    return;
                }

                switch (presenter.importAudioFile(path)) {
                    case VALID:
                        config.setAudioFilePath(path);
                        break;
                    case VALID_NEED_CONVERSION:
                        File dir = getExternalCacheDir() == null ? getCacheDir() : getExternalCacheDir();
                        String output = presenter.buildConversionAudioFilePath(dir.getPath());
                        if (TextUtils.isEmpty(output)) {
                            Log.w(TAG, "Couldn't create output file for audio conversion.");
                            longToast(R.string.error_pick_music);
                        }
                        Log.w(TAG, "Picked audio file is valid. Conversion required");
                        //noinspection UnnecessaryLocalVariable
                        String input = path;
                        AudioConversionService.start(this, input, output);
                        break;
                    case INVALID:
                        longToast(R.string.error_pick_music);
                        break;
                }
            }
        }
    }

    @Override public void onAudioCleared() {
        clearAudio(true);
    }

    private void clearAudio(boolean switchToScene) {
        config.setAudioFilePath(null);
        if (switchToScene) {
            openSceneList(true);
        }
        updateNavigationButtonVisibility(true);
    }

    private void openSceneList(boolean animate) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (animate) {
            transaction.setCustomAnimations(R.anim.slide_from_left, R.anim.slide_to_right);
        }
        AudioPlayerFragment f = getAudioPlayer();
        if (f != null) {
            transaction.hide(f);
        }
        transaction.show(getSceneList());
        transaction.commitAllowingStateLoss();
        config.setCurrentScreen(Config.Screen.SCENE_LIST);
        updateNavigationButtonVisibility(true);
    }

    private void openAudioPlayer(boolean animate) {
        AudioPlayerFragment fragment = getAudioPlayer();
        boolean addToContainer = false;
        if (fragment == null) {
            if (config.getAudioFilePath() == null) {
                throw new NullPointerException("Try to create AudioPlayerFragment when audio path is null");
            }
            fragment = AudioPlayerFragment.newInstance();
            addToContainer = true;
        } else {
            fragment.changeFile(config.getAudioFilePath());
        }
        String tag = fragment.getClass().getName();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (animate) {
            transaction.setCustomAnimations(
                    R.anim.slide_from_right,
                    R.anim.slide_to_left,
                    R.anim.slide_from_left,
                    R.anim.slide_to_right);
        }
        transaction.hide(getSceneList());
        if (addToContainer) {
            transaction.add(R.id.fragmentContainer, fragment, tag);
        } else {
            transaction.show(fragment);
        }
        transaction.commitAllowingStateLoss();
        config.setCurrentScreen(Config.Screen.AUDIO_PLAYER);
        updateNavigationButtonVisibility(false);
    }


    //    ============================= [END] Audio player =======================================

    //    ============================= [START] Render state =======================================


    @Override public void render(CaptureModel model) {
        binding.objectsButton.setImageResource(model.objectsButtonIcon());
        binding.actionFastScreenshot.setImageResource(model.actionFastScreenshotIcon());
        binding.backgroundEffect.setImageResource(model.backgroundEffectsButtonIcon());
        binding.actionButton.setEnabled(model.actionButtonEnabled());
        binding.actionButton.setImageResource(model.actionButtonIcon());

        Drawable switchModeIcon = getThemedDrawable(model.switchModeButtonAttrResId());
        binding.switchMode.setImageDrawable(switchModeIcon);
        binding.switchMode.setEnabled(model.switchModeButtonEnabled());

        binding.recordButton.setEnabled(model.recordButtonEnabled());
        binding.recordButton.invalidate(); // for switchMode action
        getSceneList().setEnabled(model.sceneCarouselEnabled());

        binding.importAudio.setImageDrawable(getThemedDrawable(model.audioImportResId()));
        binding.importAudio.setEnabled(model.importAudioButtonEnabled());
        binding.settings.setEnabled(model.settingsButtonEnabled());
        binding.drawerLayout.setDrawerLockMode(model.sidebarEnabled()
                ? DrawerLayout.LOCK_MODE_UNLOCKED
                : DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        binding.switchModeArrows.setVisibility(
                model.switchModeArrowsButtonVisible() ? View.VISIBLE : View.GONE);
        binding.switchModeArrows.setEnabled(model.switchModeArrowsButtonEnabled());
        binding.pickImage.setEnabled(model.pickImageButtonEnabled());
        binding.actionFastScreenshot.setEnabled(model.fastScreenshotButtonEnabled());
        changeButtonsSound(model.soundOfButtons());
    }

    private void updateNavigationButtonVisibility(boolean isPlayerHidden) {
        ImageView navigationButton = binding.navigationButton;
        if (config.getAudioFilePath() != null) {
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) navigationButton.getLayoutParams();
            if (isPlayerHidden) {
                navigationButton.setImageDrawable(getThemedDrawable(R.attr.navigateToAudioIcon));
                params.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID;
                params.leftToLeft = ConstraintLayout.LayoutParams.UNSET;
                params.bottomToBottom = ConstraintLayout.LayoutParams.UNSET;
                params.topToBottom = R.id.fragmentContainer;
            } else {
                navigationButton.setImageDrawable(getThemedDrawable(R.attr.navigateToSceneListIcon));
                params.rightToRight = ConstraintLayout.LayoutParams.UNSET;
                params.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
                params.topToBottom = R.id.fragmentContainer;
                params.bottomToBottom = ConstraintLayout.LayoutParams.UNSET;
            }
            TransitionManager.beginDelayedTransition(binding.container, navigationButtonTransition);
            navigationButton.setLayoutParams(params);
            navigationButton.setVisibility(View.VISIBLE);
            binding.container.requestLayout();
        } else {
            navigationButton.setImageDrawable(null);
            navigationButton.setVisibility(View.GONE);
        }
    }

    //    ============================= [END] Render state =========================================


    //    ============================= [START] RenderThread requests ==============================


    private void captureCameraImage() {
        File file = createImageForTakingPhoto(this);
        if (camera == null) {
            Timber.e("Couldn't capture photo, camera does not initialized");
            longToast("Camera does not initialized");
            return;
        }
        if (file == null) {
            Timber.e("Couldn't create temp file for capture photo: File is null");
            longToast("File is null");
            return;
        }
        if (!file.exists()) {
            Timber.e( "Couldn't create temp file for capture photo: File doesn't exist");
            longToast("File does not exist");
            return;
        }
        if (!file.canWrite()) {
            Timber.e( "Couldn't create temp file for capture photo: File is not writable");
            longToast("File is not writable");
            return;
        }

        presenter.setCapturePhotoInProgress(true);
        camera.takePicture(() -> {/* empty callback will play default sound*/},
                null,
                (data, camera1) -> capturePhotoFuture = executorService.submit(() -> {
                    runOnUiThread(() -> binding.progressBar.setVisibility(View.VISIBLE));
                    FileOutputStream fos = null;
                    try {
                        Matrix matrix = new Matrix();
                        fos = new FileOutputStream(file);
                        Camera.Size size = camera.getParameters().getPictureSize();
                        int maxDimensionSize = Math.max(size.width, size.height);
                        Bitmap bmp = BitmapLoadUtils.createSampledBitmapFromBytes(data, maxDimensionSize);
                        int cameraType = config.getCameraType();
                        if (cameraType == CAMERA_FACING_FRONT) {
                            matrix.postRotate(270);
                            matrix.postScale(-1, 1);
                        } else {
                            matrix.postRotate(90);
                        }
                        bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, false);
                        Timber.w( " taken picture width=" + bmp.getWidth() + " height=" + bmp.getHeight());
                        bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        runOnUiThread(() -> binding.progressBar.setVisibility(View.GONE));
                    } catch (FileNotFoundException e) {
                        Timber.e(e, "Couldn't write captured photo to file. FileNotFound.");
                        e.printStackTrace();
                        runOnUiThread(() -> {
                            binding.progressBar.setVisibility(View.GONE);
                            presenter.setCapturePhotoInProgress(false);
                        });
                    } finally {
                        if (fos != null) {
                            try {
                                fos.close();
                                runOnUiThread(() -> {
                                    Uri uri = Uri.fromFile(file);
                                    config.setImageUri(uri);
                                    stopPreviewAndReleaseCamera();
                                    presenter.setCapturePhotoInProgress(false);
                                    Choreographer.getInstance().postFrameCallback(CaptureActivity.this);
                                    RenderHandler rh = renderThread == null ? null : renderThread.getHandler();
                                    if (rh != null) {
                                        rh.sendImageModeEnabled();
                                    } else {
                                        Timber.e(
                                                new IllegalStateException("Image taken but couldn't obtain RenderThread Handler, RenderThread=" + renderThread),null);
                                    }
                                });
                            } catch (IOException e) {
                                Timber.e(e, "Couldn't close output stream");
                                e.printStackTrace();
                                runOnUiThread(() -> {
                                    binding.progressBar.setVisibility(View.GONE);
                                    presenter.setCapturePhotoInProgress(false);
                                });
                            }
                        } else {
                            runOnUiThread(() -> {
                                binding.progressBar.setVisibility(View.GONE);
                                presenter.setCapturePhotoInProgress(false);
                            });
                        }
                    }
                }));
    }

    private void requestScreenshot() {
        if (!presenter.prepareScreenshotOutputFile(this)) {
            longToast(R.string.error_unknown);
            return;
        }
        RenderHandler rh = renderThread.getHandler();
        if (rh != null) {
            if (!config.getScreenshotConfig().isFastScreenshot()) {
                shutterSound.play(MediaActionSound.SHUTTER_CLICK);
            }
            presenter.setCapturePhotoInProgress(true);
            binding.progressBar.setVisibility(View.VISIBLE);
            rh.sendCaptureImage();
        }
    }

    void onScreenshotCaptured() {
        presenter.setCapturePhotoInProgress(false);
        binding.progressBar.setVisibility(View.GONE);
        ScreenshotConfig screenshotConfig = config.getScreenshotConfig();
        if (screenshotConfig.getThrowable() != null) {
            if (isNoSpaceException(screenshotConfig.getThrowable())) {
                longToast(R.string.error_no_space);
            } else {
                longToast(R.string.error_capture_screenshot);
            }
        } else if (screenshotConfig.getOutputPath() != null) {
            File f = new File(screenshotConfig.getOutputPath());
            File unmarkedF = new File(screenshotConfig.getUnmarkedOutputPath());
            if (f.exists() && f.canRead()) {
                Uri uri = Uri.fromFile(f);
                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
                if (screenshotConfig.isFastScreenshot()) {
                    longToast(R.string.photo_saved_successfully);
                } else {
                    resetToDefaultMode();
                    openPreview(uri, Uri.fromFile(unmarkedF), PreviewActivity.StartFor.IMAGE);
                }
            } else {
                String errorMessage = "Screenshot is captured, but file for output path doesn't exist or invalid";
                Timber.e(errorMessage);
                longToast(R.string.error_capture_screenshot);
            }
        } else {
            String errorMessage = "Screenshot is captured, but path is null";
            Timber.e(errorMessage);
            longToast(R.string.error_capture_screenshot);
        }
        screenshotConfig.clear();
    }

    private boolean isNoSpaceException(Throwable e) {
        return e instanceof IOException &&
                e.getCause() instanceof ErrnoException &&
                ((ErrnoException) e.getCause()).errno == OsConstants.ENOSPC;
    }

    private void resetToDefaultMode() {
        // clear audio if user decided to capture image
        if (config.getAudioFilePath() != null) {
            clearAudio(false);
        }
        //reset photo
        config.setImageUri(null);
        // go to the default mode
        presenter.switchMode();
    }

    private void openPreview(Uri uri, Uri unmarkedUri, PreviewActivity.StartFor image) {
        PreviewActivity.start(CaptureActivity.this, image, uri, unmarkedUri);
    }

    public void onImageDecoded() {
        binding.progressBar.setVisibility(View.GONE);
    }


    //    ============================= [END] RenderThread requests ================================


    //    ===================================== [START] Clicks =====================================

    private void subscribeToRecordsButtonClicks() {
        clicksDisposable.add(RxView.clicks(binding.recordButton)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(v -> {
                    if (config.getRecordMode() == RecordMode.CAPTURE_IMAGE) {
                        config.getScreenshotConfig().setFastScreenshot(false);
                        requestScreenshot();
                    } else {
                        setRecording(!presenter.isRecording(), false);
                    }
                }));
    }

    private void subscribeToFastScreenshotButtonClicks() {
        clicksDisposable.add(RxView.clicks(binding.actionFastScreenshot)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(v -> {
                    config.getScreenshotConfig().setFastScreenshot(true);
                    requestScreenshot();
                }));
    }


    private void subscribeToPickImageClicks() {
        clicksDisposable.add(RxView.clicks(binding.pickImage)
                .throttleFirst(1000, TimeUnit.MILLISECONDS)
                .subscribe(o -> pickImage()));
    }

    private void subscribeToActionButtonClicks() {
        clicksDisposable.add(RxView.clicks(binding.actionButton)
                .throttleFirst(1000, TimeUnit.MILLISECONDS)
                .subscribe(o -> {
                    Config conf = config;
                    int currentType = conf.getCameraType();
                    Uri imagePath = conf.getImageUri();
                    if (imagePath != null) {
                        clearImage();
                    } else {
                        switchCamera(currentType);
                        View icon = binding.actionButton;
                        icon.animate()
                                .setDuration(300)
                                .rotationBy(180)
                                .start();
                    }
                }));
    }

    private void subscribeToImportAudioClicks() {
        clicksDisposable.add(RxView.clicks(binding.importAudio)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .subscribe(o -> {
                    if(config.isShowMusicAd()){
                            AlertDialog alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.DefaultAlertDialog))
                                    .setMessage(R.string.watch_ad_to_unlock_music)
                                    .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                                    })
                                    .setPositiveButton(android.R.string.ok, (dialog, which) -> {

                                        if(rewardedVideoAd.isLoaded()) {
                                            Intent intent = new Intent(this, AudioActivity.class);
                                            startActivityForResult(intent, REQUEST_IMPORT_MUSIC);
                                        } else {
                                            if (AndroidUtils.checkConnection(this)) {
                                                longToast(R.string.notification_wait_loading);
                                            } else {
                                                longToast(R.string.error_check_internet_connection);
                                            }
                                            dialog.dismiss();
                                        }
                                    })
                                    .create();
                            alertDialog.show();

                    }else {

                        Intent intent = new Intent(this, AudioActivity.class);
                        startActivityForResult(intent, REQUEST_IMPORT_MUSIC);
                    }


                }));

    }

    private void clearImage() {
        Config conf = config;
        int currentType = conf.getCameraType();
        conf.setImageUri(null);
        SurfaceView sv = binding.surfaceView;
        int width = sv.getWidth();
        int height = sv.getHeight();
        if (!openCamera(currentType, width, height)) {
            showOpenCameraError();
        } else {
            Choreographer.getInstance().removeFrameCallback(this);
        }

        if (config.getRecordMode() == RecordMode.CAPTURE_IMAGE) {
            presenter.switchMode();
        } else {
            presenter.requestUpdate();
        }

        RenderHandler rh = renderThread == null ? null : renderThread.getHandler();
        if (rh != null) {
            rh.sendCameraModeEnabled();
        }
    }

    private void switchCamera(int currentType) {
        int nextType = currentType == CAMERA_FACING_BACK ? CAMERA_FACING_FRONT : CAMERA_FACING_BACK;

        SurfaceView sv = binding.surfaceView;
        int width = sv.getWidth();
        int height = sv.getHeight();
        if (!openCamera(nextType, width, height)) {
            showOpenCameraError();
        }
    }


    //    ===================================== [END] Clicks  ======================================


    //    ===================================== [START] RecordButton callbacks =====================

    private @Nullable AudioPlayerFragment getAudioPlayer() {
        String tag = AudioPlayerFragment.class.getName();
        AudioPlayerFragment fragment = (AudioPlayerFragment) fragmentManager.findFragmentByTag(tag);
        return fragment == null
                ? null
                : fragment.isRemoving() ? null : fragment;
    }

    private @NonNull SceneListFragment getSceneList() {
        String tag = SceneListFragment.class.getName();
        return (SceneListFragment) fragmentManager.findFragmentByTag(tag);
    }

    @Override public void onRecordTimedOut() {
        setRecording(false, false);
    }

    private void openPreviewVideo() {
        String outputPath = config.getRecordConfig().markedOutputPath();
        String unmarkedOutputPath = config.getRecordConfig().unmarkedOutputPath();
        if (outputPath != null) {
            File f = new File(outputPath);
            File unmarkedF = new File(unmarkedOutputPath);
            if (f.exists() && f.canRead()) {
                Uri uri = Uri.fromFile(f);
                // clear image info
                config.setImageUri(null);
                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
                openPreview(uri, Uri.fromFile(unmarkedF), PreviewActivity.StartFor.VIDEO);
            } else {
                String message = "Record finished, but file for output path doesn't exist or invalid";
                Timber.e(message);
            }
        } else {
            String message = "Record finished, but output path is null";
            Timber.e(message);
        }
    }


    //    ===================================== [END] RecordButton callbacks =======================


    //    ===================================== [START] Ui setup ===================================


    private Scene currentScene() {
        return getSceneList().currentScene();
    }

    @Override public void onScenePicked(Scene scene, String title) {
        if (renderThread == null) {
            Timber.e("Frame Clicked but RenderThread is null");
            return;
        }
        // enable objects when switch scene
        presenter.setObjectsEnabled(true);
        // enable background effects when switch scene
        presenter.setBackgroundEffectsEnabled(true);
        binding.backgroundEffect.setVisibility(scene.hasBackgroundEffects() ? View.VISIBLE : View.GONE);
        touchListener.setScene(scene);
        binding.frameTitle.setText(title);
        renderThread.setScene(scene);
    }

    @Override public void onSetupFinished(Scene scene, String title) {
        binding.backgroundEffect.setVisibility(scene.hasBackgroundEffects() ? View.VISIBLE : View.GONE);
        touchListener.setScene(scene);
        binding.frameTitle.setText(title);
    }

    //    ===================================== [END] Ui setup =====================================


    @Override protected void onImageUriReady(Uri imageUri) {
        config.setImageUri(imageUri);
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override protected void onErrorGettingImage() {
        longToast(R.string.error_getting_image);
    }


    //    ===================================== [START] RenderThread ===============================


    /**
     * Frame from camera
     *
     * @param surfaceTexture texture for camera
     */
    @Override public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        if (renderThread == null) {
            return;
        }
        renderThread.getHandler().sendDoFrame(System.nanoTime());
    }

    /**
     * Frame from choreographer
     *
     * @param frameTimeNanos frame time in nanoseconds
     */
    @Override public void doFrame(long frameTimeNanos) {
        if (renderThread == null) {
            return;
        }
        renderThread.getHandler().sendDoFrame(System.nanoTime());
        Choreographer.getInstance().postFrameCallback(this);
    }

    @Override public void surfaceCreated(SurfaceHolder holder) {
        if (allPermissionGranted()) {
            startRenderThread();
        } else {
            Log.w(TAG, "surfaceCreated, but not all permissions granted");
        }
    }

    @Override public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (allPermissionGranted()) {
            setup(width, height);
        } else {
            Log.w(TAG, "surfaceChanged, but not all permissions granted");
        }
    }

    @Override public void surfaceDestroyed(SurfaceHolder holder) {
        if (renderThread == null) return;
        renderThread.getHandler().sendShutdown();
        try {
            renderThread.join();
        } catch (InterruptedException ie) {
            // not expected
            throw new RuntimeException("join was interrupted", ie);
        }
        renderThread = null;
    }

    private void setup(int width, int height) {
        config.setOrientation(getResources().getConfiguration().orientation);
        if (renderThread == null) {
            throw new NullPointerException("RenderThread is null");
        }
        RenderHandler rh = renderThread.getHandler();

        int cameraType = config.getCameraType();
        boolean cameraEnabled = config.getImageUri() == null;
        if (cameraEnabled) {
            if (openCamera(cameraType, width, height)) {
                if (rh != null) {
                    rh.sendSurfaceChanged(width, height);
                } else {
                    String message = "camera mode enabled, but couldn't send surfaceChanged event since RenderThread Handler is null";
                    Timber.e(message);
                }
            } else {
                showOpenCameraError();
            }
        } else {
            if (rh != null) {
                rh.sendSurfaceChanged(width, height);
            } else {
                String message = "image mode enabled, but couldn't send surfaceChanged event since RenderThread Handler is null";
                Timber.e(message);
            }
        }
    }

    private void startRenderThread() {
        SurfaceHolder holder = binding.surfaceView.getHolder();
        renderThread = new RenderThread(holder, new CaptureActivityHandler(this));
        renderThread.setRecordListener(new RecordListener() {
            @Override public void onRecordStarted() {
                runOnUiThread(() -> {
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    binding.drawerLayout.closeDrawer(Gravity.START);
                    // set recording to presenter and record button only when
                    // actual recording was started to avoid ui/record desync
                    presenter.setRecording(true);
                    int recordTime = config.getRecordDuration();
                    // if recording with audio file - make sound enabled
                    if (config.getAudioFilePath() != null) {
                        config.setSoundEnabled(true);
                    }
                    binding.recordButton.setTotalTime(recordTime, TimeUnit.SECONDS);
                    binding.recordButton.setRecording(true);
                    AudioPlayerFragment fragment = getAudioPlayer();
                    if (fragment != null) {
                        fragment.onRecordStarted();
                    }
                });
            }

            @Override public void onAudioPositionChanged(long audioTimeMillis) {
                AudioPlayerFragment f = getAudioPlayer();
                if (f != null) {
                    f.onProgressChanged(audioTimeMillis);
                }
            }

            @Override public void onRecordFinished() {
                runOnUiThread(() -> {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    presenter.setRecording(false);
                    // we don't call RecordButton.setRecording(false) here
                    // because we already did it when we pressed stop manually
                    // because we want to notify ui that record was stopped
                    // as soon as possible to not trigger onRecordTimedOut
                    // since record stop is asynchronous action. If we'd do it
                    // here - possible situation when both onRecordFinished(this callback)
                    // and onRecordTimedOut triggered so they both will send
                    // setRecording(false) event to RenderThread through RenderHandler.
                    // This situation is safe, since there are checks in recorder itself
                    // but just logically wrong so we avoid it as described above.
                    openPreviewVideo();
                });
            }
        });
        renderThread.setName("MomentsRenderThread");
        renderThread.start();
        renderThread.waitUntilReady();
        renderThread.setScene(currentScene());
        renderThread.getHandler().sendSurfaceCreated();

        boolean useCamera = config.getImageUri() == null;
        if (!useCamera) {
            Choreographer.getInstance().postFrameCallback(this);
        }
    }

    private void changeButtonsSound(boolean enable) {
        binding.objectsButton.setSoundEffectsEnabled(enable);
        binding.actionButton.setSoundEffectsEnabled(enable);
        binding.backgroundEffect.setSoundEffectsEnabled(enable);
        binding.actionFastScreenshot.setSoundEffectsEnabled(enable);
        binding.navigationButton.setSoundEffectsEnabled(enable);
        binding.fragmentContainer.setSoundEffectsEnabled(enable);
        binding.recordButton.setSoundEffectsEnabled(enable);
    }

    public void onFpsUpdated(int fps) {
    }

    // ==================================== [END] RenderThread =====================================


    // ==================================== [START] Camera open ====================================


    private void showOpenCameraError() {
        longToast(R.string.error_opening_camera);
    }

    private void stopPreviewAndReleaseCamera() {
        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    @CheckResult
    private boolean openCamera(int cameraType, int width, int height) {
        if (renderThread == null) {
            throw new NullPointerException("Try to open camera but RenderThread is null");
        }
        int currentType = config.getCameraType();
        if (camera != null && currentType == cameraType) {
            Timber.e("Trying to reopen camera that is already opened. Abort.");
            return true;
        }
        stopPreviewAndReleaseCamera();
        //set up surfaceTexture
        SurfaceTexture cameraTexture = renderThread.createCameraTexture();
        boolean useCamera = config.getImageUri() == null;
        if (useCamera) {
            cameraTexture.setOnFrameAvailableListener(this);
        }

        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            try {
                Camera.getCameraInfo(camIdx, cameraInfo);
                // if there is only one camera - open it
                if (cameraInfo.facing == cameraType || cameraCount == 1) {

                    camera = Camera.open(camIdx);
                    // change cameraType in case when only one camera available
                    cameraType = cameraInfo.facing;

                }
            } catch (RuntimeException e) {
                Timber.e(e, "Camera failed to open: " + e.getLocalizedMessage());
                return false;
            }
        }
        if (camera == null) {
            Timber.e("Couldn't open camera type[" + cameraType + "]");
            return false;
        }
        int orientationDegrees = getCameraDisplayOrientation(cameraInfo);
        camera.setDisplayOrientation(orientationDegrees);
        try {
            camera.setPreviewTexture(cameraTexture);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            Timber.e(ioe);
            return false;
        }


        Camera.Parameters params = camera.getParameters();

        String cameraTypeLogMessage = "opened cameraType=" + cameraType;
        Timber.e(cameraTypeLogMessage);
        Camera.Size previewSize;
        Camera.Size pictureSize = null;
        CameraUtils.CameraSizes sizes = CameraUtils.getSizesWithEqualsAspectRatio(params, width, height);
        if (sizes != null) {
            previewSize = sizes.previewSize();
            pictureSize = sizes.pictureSize();
        }  else {
            previewSize = pickPreviewSize(params, width, height);
            if (previewSize != null) {
                pictureSize = pickPictureSize(params, previewSize.width, previewSize.height);
            }
        }
        if (previewSize != null) {
            params.setPreviewSize(previewSize.width, previewSize.height);
            if (orientationDegrees == 90 || orientationDegrees == 270) {
                //noinspection SuspiciousNameCombination
                config.setCameraPreviewSize(previewSize.height, previewSize.width);
            } else {
                config.setCameraPreviewSize(previewSize.width, previewSize.height);
            }
            String message = "Picked preview size: width=" + previewSize.width + ", height=" + previewSize.height;
            Crashlytics.log(message);
            Log.d(TAG, message);
            params.setPictureSize(pictureSize.width, pictureSize.height);
            message = "Picked picture size: width=" + pictureSize.width + ", height=" + pictureSize.height;
            Crashlytics.log(message);
            Log.d(TAG, message);
        }


        if (cameraType == CAMERA_FACING_BACK) {
            List<String> supportedFocusModes = params.getSupportedFocusModes();
            if (supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
            }
        }
        config.setCameraType(cameraType);
        camera.setParameters(params);

        if (Build.MODEL.equals("Nexus 5x")){
            // rotate camera 180°
            camera.setDisplayOrientation(180);
        }

        camera.startPreview();
        currentScene().updateCameraParams();
        return true;
    }

    @Nullable
    private Camera.Size pickPreviewSize(Camera.Parameters params, int targetWidth,
                                        int targetHeight) {
        List<Camera.Size> supportedSizes = params.getSupportedPreviewSizes();
        sortSizes(supportedSizes);
        if (supportedSizes.size() > 0) {
            int i;
            for (i = 0; i < supportedSizes.size(); i++) {
                int width = supportedSizes.get(i).width;
                int height = supportedSizes.get(i).height;
                String message = "Supported preview size #" + i + ": width=" + width + ", height=" + height;
                Crashlytics.log(message);
                Log.d(TAG, message);
                if (width <= targetWidth || height <= targetHeight) {
                    break;
                }
            }
            if (i > 0) {
                i--;
            }
            return supportedSizes.get(i);
        } else {
            Crashlytics.log("No supported preview sizes");
            Log.w(TAG, "No supported preview sizes");
            return null;
        }
    }

    @NonNull
    private Camera.Size pickPictureSize(Camera.Parameters params, int targetWidth,
                                        int targetHeight) {
        // Threshold between preview size ratio and picture size ratio.
        final double ASPECT_THRESHOLD = .2;
        // Threshold of difference between preview height and picture size height.
        double minDiff = Double.MAX_VALUE;
        Camera.Size optimalSize = null;
        double targetRatio = (double) targetHeight / (double) targetWidth;
        List<Camera.Size> supportedSizes = params.getSupportedPictureSizes();
        sortSizes(supportedSizes);
        // loop through all supported picture sizes to set best display ratio.
        int i = 0;
        for (Camera.Size size : supportedSizes) {
            String message = "Supported picture size #" + i++ + ": width=" + size.width + ", height=" + size.height;
            Log.d(TAG, message);
            double ratio = (double) size.height / (double) size.width;
            if (Math.abs(ratio - targetRatio) <= ASPECT_THRESHOLD) {
                if (Math.abs(targetHeight - size.height) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(targetHeight - size.height);
                }
            }
        }
        if (optimalSize == null) {
            // no optimalSize found - return best available
            Collections.sort(supportedSizes, new PictureSizeComparator());
            optimalSize = supportedSizes.get(0);
            String message = "No optimal picture size found. " +
                    "Use size: width=" + optimalSize.width + ", height=" + optimalSize.height;
            Timber.e(message);
        }
        return optimalSize;
    }

    /**
     * Sorts camera sizes in descending order
     *
     * @param sizes sizes to sort
     */
    private void sortSizes(List<Camera.Size> sizes) {
        while (sizes.remove(null));
        if (!sizes.isEmpty())
        Collections.sort(sizes, (a, b) -> b.width * b.height - a.width * a.height);
    }

    private int getCameraDisplayOrientation(Camera.CameraInfo info) {
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            return (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            return (info.orientation - degrees + 360) % 360;
        }
    }


    // ================================== [END] Camera open ========================================

    // ================================== [START] 6.0 Permissions ==================================


    public boolean allPermissionGranted() {
        return permissionGranted(CAMERA)
                && permissionGranted(WRITE_EXTERNAL_STORAGE)
                && permissionGranted(RECORD_AUDIO);
    }

    private boolean permissionGranted(String permission) {
        return ActivityCompat.checkSelfPermission(this, permission) == PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                if (!PermissionUtils.verifyPermissions(grantResults)) {
                    longToast(PermissionUtils.homManyPermissionsNotGranted(grantResults) == 1? "Permission is not granted." : "Permissions are not granted.");
                    finish();
                } else {
                    Surface surface = binding.surfaceView.getHolder().getSurface();
                    if (surface != null && surface.isValid()) {
                        startRenderThread();
                        SurfaceView surfaceView = binding.surfaceView;
                        int width = surfaceView.getWidth();
                        int height = surfaceView.getHeight();
                        setup(width, height);
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    // ================================== [END] 6.0 Permissions ====================================
}
