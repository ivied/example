package com.tme.moments.presentation.core;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ImageView;

import com.tme.moments.R;
import com.tme.moments.presentation.widget.ForegroundImageView;

/**
 * @author zoopolitic.
 */
public class CustomBindingAdapters {

    private static final String TAG = CustomBindingAdapters.class.getSimpleName();

    private CustomBindingAdapters() {
    }

    @BindingAdapter({"android:src"})
    public static void setImageViewResource(ImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }

    @BindingAdapter({"selected"})
    public static void setSceneSelected(ForegroundImageView imageView, boolean isSelected) {
        imageView.setForeground(isSelected ? getThemedDrawable(imageView.getContext(), R.attr.selectedFrameForeground) : null);
    }


    @Nullable private static Drawable getThemedDrawable(Context context, int attributeId) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(attributeId, typedValue, true);
        int drawableRes = typedValue.resourceId;
        Drawable drawable = null;
        try {
            drawable = context.getResources().getDrawable(drawableRes);
        } catch (Resources.NotFoundException e) {
            Log.w(TAG, "Not found drawable resource by id: " + drawableRes);
        }
        return drawable;
    }
}
