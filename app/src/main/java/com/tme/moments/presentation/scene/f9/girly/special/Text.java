package com.tme.moments.presentation.scene.f9.girly.special;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class Text extends Sprite {

    private long scaleWaitTime;
    private boolean scaleDown;
    private float scaleUpSpeed;
    private float scaleDownSpeed;
    private float maxScale;
    private float minScale;
    private int scalesCount;
    private int maxScalesCount;
    private float maxAngle;
    private boolean useRotation;

    public Text(TextureRegion region, float width, float height) {
        super(region, width, height);
        maxAngle = random() ? 10f : -10f;
        minScale = 0.9f;
        maxScale = 1.0f;
        float scaleDownTime = 0.1f;
        float scaleUpTime = 0.1f;
        scaleUpSpeed = ((maxScale - minScale) / scaleUpTime) / Const.NANOS_IN_SECOND;
        scaleDownSpeed = ((maxScale - minScale) / scaleDownTime) / Const.NANOS_IN_SECOND;
        maxScalesCount = random(4, 6);
        scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(random(3000, 4000));
    }

    void step(long deltaNanos) {
        if (scaleWaitTime > 0) {
            scaleWaitTime -= deltaNanos;
        } else {
            if (scaleDown) {
                float deltaScale = scaleDownSpeed * deltaNanos;
                float newScale = Math.max(minScale, getScaleX() - deltaScale);
                setScale(newScale);
                if (useRotation) {
                    float fraction = 1 - ((newScale - minScale) / (maxScale - minScale)); // 1 minus because scale is decreasing
                    float angle = maxAngle * fraction;
                    setRotationZ(angle);
                }

                if (newScale == minScale) {
                    scaleDown = false;
                }
            } else {
                float deltaScale = scaleUpSpeed * deltaNanos;
                float newScale = Math.min(maxScale, getScaleX() + deltaScale);
                setScale(newScale);
                if (useRotation) {
                    float fraction = 1 - ((newScale - minScale) / (maxScale - minScale)); // 1 minus because scale is decreasing
                    float angle = maxAngle * fraction;
                    setRotationZ(angle);
                }
                if (newScale == maxScale) {
                    scaleDown = true;
                    useRotation = random();
                    maxAngle = random() ? 10f : -10f;
                    if (++scalesCount == maxScalesCount) {
                        scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(random(4000, 6000));
                        scalesCount = 0;
                        maxScalesCount = random(3, 6);
                    }
                }
            }
        }
    }
}
