package com.tme.moments.presentation.scene.f11.spring;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.math.MathUtils;
import com.tme.moments.presentation.Const;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Meduza extends AnimatedSprite {

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float speed;
    private float t;

    private float startDelayNanos;

    Meduza(TextureRegion region, List<FrameInfo> frameInfoList,
           float width, float height) {
        super(region, frameInfoList, width, height);
        init();
        setStartDelay(5000); // initial delay
    }

    private void setStartDelay(long millis) {
        this.startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    @Override protected int getDesiredFPS() {
        return 25;
    }

    @Override public void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float deltaT = speed * deltaNanos;
        t += deltaT;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        float t1 = t + 0.3f;
        float x1 = (1 - t1) * (1 - t1) * p0x + 2 * (1 - t1) * t1 * p1x + t1 * t1 * p2x;
        float y1 = (1 - t1) * (1 - t1) * p0y + 2 * (1 - t1) * t1 * p1y + t1 * t1 * p2y;
        float angle = MathUtils.angleBetweenLines(x, y, x, y1, x, y, x1, y1);
        if (angle != 0) {
            setRotationZ(y1 >= y ? -angle : -angle + 180);
        }

        setX(x);
        setY(y);

        if (t >= 1) {
            init();
        }
        super.step(deltaNanos);
    }

    private void init() {
        boolean moveLeft = random();
        if (moveLeft) {
            p0x = 1 + getWidth() / 2;
            p0y = random(-1f, 1f);
            p1x = random(-0.5f, 0.5f);
            p1y = random(-0.75f, 0.75f);
            p2x = random(-1 - getWidth() / 2, -1 - getWidth());
            p2y = random(-1f, 1f);
        } else {
            p0x = -1 - getWidth() / 2;
            p0y = random(-1f, 1f);
            p1x = random(-0.5f, 0.5f);
            p1y = random(-0.75f, 0.75f);
            p2x = random(1 + getWidth() / 2, 1 + getWidth());
            p2y = random(-1f, 1f);
        }
        setRotationZ(0);
        setPosition(p0x, p0y, -1.0f);
        setStartDelay(random(5000, 7000));
        speed = random(0.2f, 0.3f) / Const.NANOS_IN_SECOND;
        t = 0;
    }
}
