package com.tme.moments.presentation.scene.f8.sunflower.field;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

/**
 * @author zoopolitic.
 */
class Sunflower extends Decal {

    private float moveSpeedNanos;
    private float rotationSpeedNanos;
    private float turnSpeed = 15f / Const.NANOS_IN_SECOND;

    private final float minX;
    private final float maxX;

    Sunflower(TextureRegion region, float width, float height, float x, float y,
              float moveSpeedPerSecond, float rotationSpeedDegreesPerSecond,
              float moveIncline) {
        this.moveSpeedNanos = moveSpeedPerSecond / Const.NANOS_IN_SECOND;
        this.rotationSpeedNanos = rotationSpeedDegreesPerSecond / Const.NANOS_IN_SECOND;
        this.minX = x - moveIncline;
        this.maxX = x + moveIncline;
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        // adjust Z, reducing it by half of biggest side of the leaf
        // to not being clipped during X and Y rotation
        float z = -1 - Math.max(width, height) / 2;
        setPosition(x, y, z);
    }

    void step(long deltaNanos) {
        float rotateDelta = rotationSpeedNanos * deltaNanos;
        float turnDelta = turnSpeed * deltaNanos;
        rotateZ(rotateDelta);
        rotateY(turnDelta);

        float moveDelta = moveSpeedNanos * deltaNanos;
        float newX = getX() + moveDelta;
        newX = Math.max(minX, Math.min(maxX, newX));
        setX(newX);
        if (newX == maxX || newX == minX) {
            moveSpeedNanos = -moveSpeedNanos;
            rotationSpeedNanos = -rotationSpeedNanos;
            turnSpeed = -turnSpeed;
        }
    }
}
