package com.tme.moments.presentation.core;

import com.tme.moments.presentation.audio.AudioActivity;
import com.tme.moments.presentation.capture.AudioModule;
import com.tme.moments.presentation.capture.CaptureActivity;
import com.tme.moments.presentation.capture.CaptureFragments;
import com.tme.moments.presentation.capture.CaptureModule;
import com.tme.moments.presentation.core.scope.ActivityScope;
import com.tme.moments.presentation.preview.PreviewActivity;
import com.tme.moments.presentation.preview.PreviewFragments;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * @author zoopolitic.
 */
@Module
public abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = {CaptureModule.class, CaptureFragments.class})
    abstract CaptureActivity contributeCaptureActivityInjector();


    @ActivityScope
    @ContributesAndroidInjector(modules = {AudioModule.class})
    abstract AudioActivity contributeAudioActivityInjector();


    @ActivityScope
    @ContributesAndroidInjector(modules = {PreviewFragments.class})
    abstract PreviewActivity contributePreviewActivityInjector();

}
