package com.tme.moments.presentation.scene.f20.comic;

import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.math.Vector3;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_ALWAYS;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_EQUAL;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_KEEP;
import static android.opengl.GLES20.GL_REPLACE;
import static android.opengl.GLES20.GL_STENCIL_BUFFER_BIT;
import static android.opengl.GLES20.GL_STENCIL_TEST;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glColorMask;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glStencilFunc;
import static android.opengl.GLES20.glStencilOp;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class YourComicBook extends BaseScene {

    private RotatingBackground background;
    private Sprite stencilMask;
    private Sprite overlay;

    private int stencilProgramHandle = -1;
    private int uStencilProjectionMatrixHandle = -1;
    private int aStencilPositionHandle = -1;
    private int aStencilColorHandle = -1;
    private int aStencilTexCoordHandle = -1;

    private TextureInfo atlasInfo;
    private TextureInfo wordsAtlasInfo;
    private List<Round> rounds;
    private Word word;
    private DecalBatch wordsBatch;
    private DecalBatch belowBatch;
    private List<Star> stars;
    private Rocket rocket;

    @Override protected float getCameraPositionX() {
        return -0.035f;
    }

    @Override protected float getCameraPositionY() {
        return -0.007f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.8f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.63f;
    }

    @Override public void init() {
        super.init();
        if (stencilProgramHandle == -1) {
            prepareStencilProgram();
        }
    }

    private void prepareStencilProgram() {
        stencilProgramHandle =
                openGLLoader.createProgram(R.raw.vertex_shader, R.raw.stencil_fragment_shader);

        aStencilPositionHandle = glGetAttribLocation(stencilProgramHandle, "a_position");
        GlUtil.checkLocation(aStencilPositionHandle, "a_position");
        glEnableVertexAttribArray(aStencilPositionHandle);

        aStencilColorHandle = glGetAttribLocation(stencilProgramHandle, "a_color");
        GlUtil.checkLocation(aStencilColorHandle, "a_color");
        glEnableVertexAttribArray(aStencilColorHandle);

        aStencilTexCoordHandle = glGetAttribLocation(stencilProgramHandle, "a_texCoord");
        GlUtil.checkLocation(aStencilTexCoordHandle, "a_texCoord");
        glEnableVertexAttribArray(aStencilTexCoordHandle);

        uStencilProjectionMatrixHandle = glGetUniformLocation(stencilProgramHandle, "u_projTrans");
        GlUtil.checkLocation(uStencilProjectionMatrixHandle, "u_projTrans");
    }

    @Override public boolean hasBackgroundEffects() {
        return true;
    }

    @Override protected void prepareBackgroundEffects(int surfaceWidth, int surfaceHeight) {
        prepareBackground();
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        prepareStencilMask();
        prepareOverlay();
        atlasInfo = openGLLoader.loadTexture(R.drawable.f20_atlas);
        wordsAtlasInfo = openGLLoader.loadTexture(R.drawable.f20_words_atlas);

        buildRounds(atlasInfo);
        buildRocket(atlasInfo);

        buildStars(atlasInfo);
        belowBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        belowBatch.initialize(stars.size() + rounds.size());
        belowBatch.addAll(stars);
        belowBatch.addAll(rounds);

        buildWords(wordsAtlasInfo);
        wordsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        wordsBatch.initialize(1);
        wordsBatch.add(word);
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glUseProgram(mainProgramHandle);
        if (config.isBackgroundEffectsEnabled()) {
            background.step(deltaNanos);
        }
        background.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
        glEnable(GL_STENCIL_TEST);

        glColorMask(false, false, false, false);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

        // draw mask
        glUseProgram(stencilProgramHandle);
        stencilMask.draw(uStencilProjectionMatrixHandle, projectionMatrix, aStencilPositionHandle,
                aStencilTexCoordHandle, aStencilColorHandle);

        glColorMask(true, true, true, true);
        glStencilFunc(GL_EQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

        // draw stars
        glDisable(GL_STENCIL_TEST);
        glUseProgram(mainProgramHandle);
        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        belowBatch.render(uProjectionMatrixLoc, projectionMatrix);
        if (config.isObjectsEnabled()) {
            for (Star star : stars) {
                star.step(deltaNanos);
            }
            boolean allRoundsFinished = true;
            for (Round round : rounds) {
                round.step(deltaNanos);
                if (!round.isFinished()) {
                    allRoundsFinished = false;
                }
            }
            if (allRoundsFinished) {
                long startDelay = random(2000, 4000);
                for (Round round : rounds) {
                    round.setStartDelay(startDelay);
                    round.setFinished(false);
                }
            }
        }
        glEnable(GL_STENCIL_TEST);

        if (mode == Mode.IMAGE) {
            glUseProgram(mainProgramHandle);
            drawImage();
        } else {
            drawCameraInput(cameraTexture);
        }

        glDisable(GL_STENCIL_TEST);
        glUseProgram(mainProgramHandle);
        overlay.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
        boolean objectsEnabled = config.isObjectsEnabled();
        if (objectsEnabled) {
            rocket.step(deltaNanos);
            rocket.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            glBindTexture(GL_TEXTURE_2D, wordsAtlasInfo.textureHandle);
            wordsBatch.render(uProjectionMatrixLoc, projectionMatrix);
            word.step(deltaNanos);
        }
    }

    @Override public void release() {
        super.release();
        glDeleteProgram(stencilProgramHandle);
        stencilProgramHandle = -1;
        int[] arr = new int[2];
        int texturesToDeleteSize = 0;
        if (atlasInfo != null) {
            arr[texturesToDeleteSize] = atlasInfo.textureHandle;
            texturesToDeleteSize++;
        }
        if (wordsAtlasInfo != null) {
            arr[texturesToDeleteSize] = wordsAtlasInfo.textureHandle;
            texturesToDeleteSize++;
        }
        if (texturesToDeleteSize != 0) {
            glDeleteTextures(texturesToDeleteSize, arr, 0);
            GlUtil.checkGlError("Delete textures");
        }
    }

    private void prepareBackground() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f20_background);
        int textureHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        TextureRegion region = new TextureRegion(textureHandle, w, h, 0, 0, w, h);
        float scale = calculateBackgroundScale();
        float currentRotation = 0;
        if (background != null) {
            currentRotation = background.getRotation().getAngleAround(Vector3.Z);
        }
        background = new RotatingBackground(region, getProjectionWidth(), getProjectionHeight(), scale);
        background.setRotationZ(currentRotation);
        background.setPosition(0, 0, -1.0f);
    }

    private void prepareStencilMask() {
        TextureInfo stencilInfo = openGLLoader.loadTexture(R.drawable.f20_stencil);
        int w = stencilInfo.imageWidth;
        int h = stencilInfo.imageHeight;
        TextureRegion region = new TextureRegion(stencilInfo.textureHandle, w, h, 0, 0, w, h);
        float width = getDesiredCameraWidthFraction() * getProjectionWidth();
        float height = getDesiredCameraHeightFraction() * getProjectionHeight();
        stencilMask = new Sprite(region, width, height);
        stencilMask.setPosition(getCameraPositionX(), getCameraPositionY(), -1);
    }

    private void prepareOverlay() {
        TextureInfo overlayInfo = openGLLoader.loadTexture(R.drawable.f20_overlay);
        int w = overlayInfo.imageWidth;
        int h = overlayInfo.imageHeight;
        TextureRegion region = new TextureRegion(overlayInfo.textureHandle, w, h, 0, 0, w, h);
        float width = getProjectionWidth();
        float height = getProjectionHeight();
        overlay = new Sprite(region, width, height);
        overlay.setPosition(0, 0, -1);
    }

    private void buildStars(TextureInfo info) {
        TextureRegion star1 = buildRegion(info, 1447, 0, 86, 83);
        TextureRegion star2 = buildRegion(info, 1533, 0, 86, 83);
        TextureRegion star3 = buildRegion(info, 1619, 0, 86, 83);
        float width = 0.1075f;
        float height = 0.1037f;
        stars = new ArrayList<>();
        int count = 55;
        for (int i = 0; i < count; i++) stars.add(new Star(star1, width, height));
        for (int i = 0; i < count; i++) stars.add(new Star(star2, width, height));
        for (int i = 0; i < count; i++) stars.add(new Star(star3, width, height));
    }

    private void buildRounds(TextureInfo info) {
        rounds = new ArrayList<>();
        TextureRegion round1 = buildRegion(info, 1112, 0, 105, 119);
        float width = 0.1312f;
        float height = 0.1487f;
        float startX = 0.46f;
        float endX = 1 + width / 2;
        float startY = 0.55f;
        float endY = 1.75f;
        float speed = 0.1f;
        float velocity = 0.01f;
        rounds.add(new Round(round1, width, height, startX, endX, startY, endY, speed, velocity));

        TextureRegion round2 = buildRegion(info, 1217, 0, 114, 106);
        width = 0.1425f;
        height = 0.1325f;
        startX = -0.7f;
        endX = -1 - width / 2;
        startY = 0.2f;
        endY = 0.5f;
        speed = -0.1f;
        velocity = -0.008f;
        rounds.add(new Round(round2, width, height, startX, endX, startY, endY, speed, velocity));

        TextureRegion round3 = buildRegion(info, 1331, 0, 116, 108);
        width = 0.145f;
        height = 0.135f;
        startX = 0.4f;
        endX = 1 + width / 2;
        startY = -0.45f;
        endY = -1.5f;
        speed = 0.1f;
        velocity = 0.012f;
        rounds.add(new Round(round3, width, height, startX, endX, startY, endY, speed, velocity));
    }

    private void buildRocket(TextureInfo info) {
        int inSampleSize = info.inSampleSize;
        TextureRegion region = buildRegion(info, 0, 0, 278, 432);
        float width = 0.3475f;
        float height = 0.54f;
        List<AnimatedSprite.FrameInfo> frames = new ArrayList<>();
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 278 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 556 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 834 / inSampleSize, 0 / inSampleSize));
        rocket = new Rocket(region, frames, width, height);
    }

    private void buildWords(TextureInfo info) {
        List<Pair<TextureRegion, PointF>> infos = new ArrayList<>();
        // width = width in pixels / 1600 (1600 is a target size that designer used) * 2 (2 is a projection width)
        infos.add(new Pair<>(buildRegion(info, 1339, 0, 676, 700), new PointF(0.845f, 0.9375f)));
        infos.add(new Pair<>(buildRegion(info, 0, 700, 750, 632), new PointF(0.9375f, 0.79f)));
        infos.add(new Pair<>(buildRegion(info, 0, 1332, 750, 657), new PointF(0.9375f, 0.8212f)));
        infos.add(new Pair<>(buildRegion(info, 750, 1332, 750, 709), new PointF(0.9375f, 0.8862f)));
        infos.add(new Pair<>(buildRegion(info, 750, 700, 750, 600), new PointF(0.9375f, 0.75f)));
        infos.add(new Pair<>(buildRegion(info, 0, 0, 663, 700), new PointF(0.8287f, 0.9375f)));
        infos.add(new Pair<>(buildRegion(info, 663, 0, 676, 700), new PointF(0.845f, 0.9375f)));
        word = new Word(infos);
    }
}