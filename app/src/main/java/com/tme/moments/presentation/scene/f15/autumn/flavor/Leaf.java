package com.tme.moments.presentation.scene.f15.autumn.flavor;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Leaf extends Decal {

    private final boolean flyLeft;
    private float speedY;
    private float speedX;
    private float rotationSpeed;
    private long startDelayNanos;
    private float minY;
    private float minX;
    private float maxX;

    Leaf(TextureRegion region, float width, float height, boolean flyLeft) {
        this.flyLeft = flyLeft;
        setTextureRegion(region);
        setDimensions(width, height);
        init();

    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float deltaX = speedX * deltaNanos;
        float deltaY = speedY * deltaNanos;
        translate(deltaX, deltaY, 0);

        float deltaRotation = rotationSpeed * deltaNanos;
        rotateX(deltaRotation);
        rotateY(deltaRotation);
        rotateZ(deltaRotation);

        float x = getX();
        float y = getY();
        if (y <= minY || x <= minX || x >= maxX) {
            init();
        }
    }

    private void init() {
        // adjust Z, reducing it by half of biggest side of the leaf
        // to not being clipped during X and Y rotation
        float halfWidth = getWidth() / 2;
        float halfHeight = getHeight() / 2;
        float maxZ = -1 - Math.max(halfWidth, halfHeight);
        float minZ = -2f;
        float z = random(minZ, maxZ);
        float deltaZ = -1.0f - z; // -1 is a maxZ
        speedY = -random(0.25f, 0.85f) / Const.NANOS_IN_SECOND;
        speedX = flyLeft
                ? random(-0.75f, 0.0f) / Const.NANOS_IN_SECOND
                : random(0.0f, 0.75f) / Const.NANOS_IN_SECOND;

        float x = flyLeft
                ? random(0.25f + deltaZ, 1f + halfWidth + deltaZ)
                : random(-1f - halfWidth - deltaZ, -0.25f - deltaZ);
        boolean increasedXFall = random(10) == 1;
        float y = 1.1f + halfHeight + deltaZ;
        if (increasedXFall) {
            y = 2.0f + halfHeight + deltaZ;
            x = flyLeft
                    ? random(0.35f, 0.6f) - deltaZ
                    : random(-0.6f, -0.35f) + deltaZ;
            speedX = (flyLeft ? random(-0.35f, -0.25f) : random(0.25f, 0.35f)) / Const.NANOS_IN_SECOND;
        }

        setColor(1, 1, 1, 1);
        minY = -1 - deltaZ - getHeight() / 2;
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 10000));
        minX = -1 - deltaZ - getWidth() / 2;
        maxX = 1 + deltaZ + getWidth() / 2;
        rotationSpeed = random(-150f, 150f) / Const.NANOS_IN_SECOND; // degrees per second
        setPosition(x, y, z);
    }
}
