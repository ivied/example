package com.tme.moments.presentation.scene.f17.royal;

import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;
import com.tme.moments.presentation.scene.Glow;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_ALWAYS;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_EQUAL;
import static android.opengl.GLES20.GL_KEEP;
import static android.opengl.GLES20.GL_REPLACE;
import static android.opengl.GLES20.GL_STENCIL_BUFFER_BIT;
import static android.opengl.GLES20.GL_STENCIL_TEST;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glColorMask;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glStencilFunc;
import static android.opengl.GLES20.glStencilOp;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class TheRoyal extends BaseScene {

    private int stencilProgramHandle = -1;
    private int uStencilProjectionMatrixHandle = -1;
    private int aStencilPositionHandle = -1;
    private int aStencilColorHandle = -1;
    private int aStencilTexCoordHandle = -1;

    private Sprite background;
    private Sprite overlay;
    private Sprite stencilMask;
    private TextureInfo atlasInfo;
    private List<Glow> glows;
    private List<Diamond> diamonds;
    private List<Circle> circles;
    private DecalBatch aboveBatch;
    private DecalBatch overBatch;
    private DecalBatch diamondsBatch;

    @Override protected float getCameraPositionX() {
        return 0.024f;
    }

    @Override protected float getCameraPositionY() {
        return 0.137f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.453f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.453f;
    }

    @Override public void init() {
        super.init();
        if (stencilProgramHandle == -1) {
            prepareStencilProgram();
        }
    }

    private void prepareStencilProgram() {
        stencilProgramHandle =
                openGLLoader.createProgram(R.raw.vertex_shader, R.raw.stencil_fragment_shader);

        aStencilPositionHandle = glGetAttribLocation(stencilProgramHandle, "a_position");
        GlUtil.checkLocation(aStencilPositionHandle, "a_position");
        glEnableVertexAttribArray(aStencilPositionHandle);

        aStencilColorHandle = glGetAttribLocation(stencilProgramHandle, "a_color");
        GlUtil.checkLocation(aStencilColorHandle, "a_color");
        glEnableVertexAttribArray(aStencilColorHandle);

        aStencilTexCoordHandle = glGetAttribLocation(stencilProgramHandle, "a_texCoord");
        GlUtil.checkLocation(aStencilTexCoordHandle, "a_texCoord");
        glEnableVertexAttribArray(aStencilTexCoordHandle);

        uStencilProjectionMatrixHandle = glGetUniformLocation(stencilProgramHandle, "u_projTrans");
        GlUtil.checkLocation(uStencilProjectionMatrixHandle, "u_projTrans");
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glUseProgram(mainProgramHandle);
        background.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
        glEnable(GL_STENCIL_TEST);

        glColorMask(false, false, false, false);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

        // draw mask
        glUseProgram(stencilProgramHandle);
        stencilMask.draw(uStencilProjectionMatrixHandle, projectionMatrix, aStencilPositionHandle,
                aStencilTexCoordHandle, aStencilColorHandle);

        glColorMask(true, true, true, true);
        glStencilFunc(GL_EQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

        glDisable(GL_STENCIL_TEST);
        glUseProgram(mainProgramHandle);
        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        if (config.isObjectsEnabled()) {
            aboveBatch.render(uProjectionMatrixLoc, projectionMatrix);
        }
        diamondsBatch.render(uProjectionMatrixLoc, projectionMatrix);
        for (Diamond diamond : diamonds) {
            diamond.step(deltaNanos);
            diamond.setRotationEnabled(config.isObjectsEnabled());
        }

        glUseProgram(mainProgramHandle);
        overlay.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
        if (config.isObjectsEnabled()) {
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            overBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Glow glow : glows) glow.step(deltaNanos);
            for (Circle circle : circles) circle.step(deltaNanos);
        }

        glEnable(GL_STENCIL_TEST);
        if (mode == Mode.IMAGE) {
            glUseProgram(mainProgramHandle);
            drawImage();
        } else {
            drawCameraInput(cameraTexture);
        }
        glDisable(GL_STENCIL_TEST);
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        prepareBackground();
        prepareStencilMask();
        prepareOverlay();

        atlasInfo = openGLLoader.loadTexture(R.drawable.f17_atlas);
        Pair<TextureRegion, PointF> glow1 = new Pair<>(buildRegion(atlasInfo, 0, 633, 89, 91), new PointF(0.1112f, 0.1137f));
        Pair<TextureRegion, PointF> glow2 = new Pair<>(buildRegion(atlasInfo, 89, 633, 89, 91), new PointF(0.1112f, 0.1137f));
        Pair<TextureRegion, PointF> glow3 = new Pair<>(buildRegion(atlasInfo, 178, 633, 67, 68), new PointF(0.0837f, 0.085f));
        Pair<TextureRegion, PointF> glow4 = new Pair<>(buildRegion(atlasInfo, 245, 633, 67, 67), new PointF(0.0837f, 0.0837f));

        Pair<TextureRegion, PointF> circle1 = new Pair<>(buildRegion(atlasInfo, 0, 0, 133, 133), new PointF(0.1662f, 0.1662f));
        Pair<TextureRegion, PointF> circle2 = new Pair<>(buildRegion(atlasInfo, 133, 0, 133, 133), new PointF(0.1662f, 0.1662f));
        Pair<TextureRegion, PointF> circle3 = new Pair<>(buildRegion(atlasInfo, 266, 0, 133, 133), new PointF(0.1662f, 0.1662f));

        glows = new ArrayList<>();
        glows.addAll(generateGlows(glow1, 50, 1.0f));
        glows.addAll(generateGlows(glow2, 50, 1.0f));
        glows.addAll(generateGlows(glow3, 25, 1.0f));
        glows.addAll(generateGlows(glow4, 25, 1.0f));

        circles = new ArrayList<>();
        circles.addAll(generateCircles(circle1, 30));
        circles.addAll(generateCircles(circle2, 30));
        circles.addAll(generateCircles(circle3, 30));

        diamonds = new ArrayList<>();
        TextureRegion region = buildRegion(atlasInfo, 0, 133, 500, 500);
        float width = 0.625f;
        float height = 0.625f;
        float scale = 0.75f;
        List<Diamond> allDiamonds = new ArrayList<>();

        Diamond topRight = new Diamond(region, width, height, 0.28f, 0.79f);
        topRight.setScale(scale);
        allDiamonds.add(topRight);

        Diamond topLeft = new Diamond(region, width, height, -0.24f, 0.79f);
        topLeft.setScale(scale);
        allDiamonds.add(topLeft);

        Diamond leftTop = new Diamond(region, width, height, -0.64f, 0.38f);
        leftTop.setScale(scale);
        allDiamonds.add(leftTop);

        Diamond leftBottom = new Diamond(region, width, height, -0.64f, -0.13f);
        leftBottom.setScale(scale);
        allDiamonds.add(leftBottom);

        Diamond bottomLeft = new Diamond(region, width, height, -0.23f, -0.52f);
        bottomLeft.setScale(scale);
        allDiamonds.add(bottomLeft);

        Diamond bottomRight = new Diamond(region, width, height, 0.27f, -0.52f);
        bottomRight.setScale(scale);
        allDiamonds.add(bottomRight);

        Diamond rightBottom = new Diamond(region, width, height, 0.68f, -0.13f);
        rightBottom.setScale(scale);
        allDiamonds.add(rightBottom);

        Diamond rightTop = new Diamond(region, width, height, 0.68f, 0.38f);
        rightTop.setScale(scale);
        allDiamonds.add(rightTop);

        int diamondsCount = 5;
        for (int i = 0; i < diamondsCount; i++) {
            Diamond diamond = allDiamonds.get(random(allDiamonds.size()));
            allDiamonds.remove(diamond);
            diamonds.add(diamond);
        }

        diamondsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        diamondsBatch.initialize(diamonds.size());
        diamondsBatch.addAll(diamonds);

        int overFrameGlowCount = 40;
        int overFrameCircleCount = 12;
        overBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        overBatch.initialize(overFrameGlowCount + overFrameCircleCount);

        for (int i = 0; i < overFrameGlowCount; i++) {
            overBatch.add(glows.get(i));
        }
        for (int i = 0; i < overFrameCircleCount; i++) {
            overBatch.add(circles.get(i));
        }

        int underFrameGlowCount = glows.size() - overFrameGlowCount;
        int underFrameCircleCount = circles.size() - overFrameCircleCount;
        aboveBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        aboveBatch.initialize(underFrameGlowCount + underFrameCircleCount);

        for (int i = overFrameGlowCount; i < underFrameGlowCount; i++) {
            aboveBatch.add(glows.get(i));
        }
        for (int i = overFrameCircleCount; i < underFrameCircleCount; i++) {
            aboveBatch.add(circles.get(i));
        }
    }

    private void prepareBackground() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f17);
        int textureHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        TextureRegion region = new TextureRegion(textureHandle, w, h, 0, 0, w, h);
        background = new Sprite(region, getProjectionWidth(), getProjectionHeight());
        background.setPosition(0, 0, -1);
    }

    private void prepareStencilMask() {
        TextureInfo stencilInfo = openGLLoader.loadTexture(R.drawable.f17_stencil);
        int w = stencilInfo.imageWidth;
        int h = stencilInfo.imageHeight;
        TextureRegion region = new TextureRegion(stencilInfo.textureHandle, w, h, 0, 0, w, h);
        float width = getDesiredCameraWidthFraction() * getProjectionWidth();
        float height = getDesiredCameraHeightFraction() * getProjectionHeight();
        stencilMask = new Sprite(region, width, height);
        stencilMask.setPosition(getCameraPositionX(), getCameraPositionY(), -1);
    }

    private void prepareOverlay() {
        TextureInfo overlayInfo = openGLLoader.loadTexture(R.drawable.f17_overlay);
        int w = overlayInfo.imageWidth;
        int h = overlayInfo.imageHeight;
        TextureRegion region = new TextureRegion(overlayInfo.textureHandle, w, h, 0, 0, w, h);
        float width = getProjectionWidth();
        float height = getProjectionHeight();
        overlay = new Sprite(region, width, height);
        overlay.setPosition(0, 0, -1);
    }

    private List<Glow> generateGlows(Pair<TextureRegion, PointF> infoPair, int count, float scale) {
        List<Glow> items = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            TextureRegion region = infoPair.first;
            float width = infoPair.second.x;
            float height = infoPair.second.y;
            Glow glow = new Glow(region, width, height);
            glow.setScale(scale);
            glow.setSpeed(0.05f, 0.085f);
            glow.setStartDelay(random(3000, 5000));
            items.add(glow);
        }
        return items;
    }

    private List<Circle> generateCircles(Pair<TextureRegion, PointF> infoPair, int count) {
        List<Circle> items = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            TextureRegion region = infoPair.first;
            float width = infoPair.second.x;
            float height = infoPair.second.y;
            Circle circle = new Circle(region, width, height);
            circle.setSpeed(0.05f, 0.085f);
            circle.setStartDelay(random(7000, 9000));
            items.add(circle);
        }
        return items;
    }

    @Override public void release() {
        super.release();
        glDeleteProgram(stencilProgramHandle);
        stencilProgramHandle = -1;
        if (atlasInfo != null) {
            int[] values = new int[1];
            values[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, values, 0);
            GlUtil.checkGlError("Release " + getClass().getSimpleName());
        }
    }

}