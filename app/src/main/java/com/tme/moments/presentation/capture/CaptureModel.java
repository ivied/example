package com.tme.moments.presentation.capture;

import com.google.auto.value.AutoValue;

/**
 * @author zoopolitic.
 */
@AutoValue
public abstract class CaptureModel {

    public static CaptureModel create(boolean pickImageButtonEnabled,
                                      int switchModeButtonAttrResId,
                                      boolean switchModeButtonEnabled,
                                      boolean switchModeArrowsButtonVisible,
                                      boolean switchModeArrowsButtonEnabled,
                                      boolean actionButtonEnabled, int actionButtonIcon,
                                      boolean sceneCarouselEnabled, boolean recordButtonEnabled,
                                      int objectsButtonIcon, int backgroundEffectsButtonIcon,
                                      int actionFastScreenshotIcon,
                                      boolean importAudioButtonEnabled,
                                      boolean settingsButtonEnabled, boolean sidebarEnabled,
                                      boolean fastScreenshotButtonEnabled, boolean soundOfButtons,
                                      int audioImportIcon) {
        return new AutoValue_CaptureModel(pickImageButtonEnabled,
                switchModeButtonAttrResId, switchModeButtonEnabled, switchModeArrowsButtonVisible, switchModeArrowsButtonEnabled,
                actionButtonEnabled, actionButtonIcon, sceneCarouselEnabled, recordButtonEnabled,
                objectsButtonIcon, backgroundEffectsButtonIcon,  importAudioButtonEnabled,
                settingsButtonEnabled, sidebarEnabled, fastScreenshotButtonEnabled, actionFastScreenshotIcon,
                soundOfButtons, audioImportIcon);
    }

    public abstract boolean pickImageButtonEnabled();
    public abstract int switchModeButtonAttrResId();
    public abstract boolean switchModeButtonEnabled();
    public abstract boolean switchModeArrowsButtonVisible();
    public abstract boolean switchModeArrowsButtonEnabled();
    public abstract boolean actionButtonEnabled();
    public abstract int actionButtonIcon();
    public abstract boolean sceneCarouselEnabled();
    public abstract boolean recordButtonEnabled();
    public abstract int objectsButtonIcon();
    public abstract int backgroundEffectsButtonIcon();
    public abstract boolean importAudioButtonEnabled();
    public abstract boolean settingsButtonEnabled();
    public abstract boolean sidebarEnabled();
    public abstract boolean fastScreenshotButtonEnabled();
    public abstract int actionFastScreenshotIcon();
    public abstract boolean soundOfButtons();
    public abstract int audioImportResId();


}
