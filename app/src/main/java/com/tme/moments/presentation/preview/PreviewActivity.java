package com.tme.moments.presentation.preview;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.tme.moments.BuildConfig;
import com.tme.moments.R;
import com.tme.moments.databinding.PreviewActivityBinding;
import com.tme.moments.presentation.base.BaseActivity;
import com.tme.moments.presentation.bitmap.BitmapLoadCallback;
import com.tme.moments.presentation.bitmap.BitmapLoadUtils;
import com.tme.moments.presentation.bitmap.ExifInfo;
import com.tme.moments.presentation.core.App;
import com.tme.moments.presentation.core.Config;
import com.tme.moments.presentation.drawer.DrawerFragment;
import com.tme.moments.presentation.utils.AndroidUtils;
import com.tme.moments.presentation.utils.Consumer;

import java.io.File;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import timber.log.Timber;

import static com.tme.moments.presentation.preview.PreviewActivity.StartFor.IMAGE;
import static com.tme.moments.presentation.preview.PreviewActivity.StartFor.VIDEO;

/**
 * @author zoopolitic.
 */
public class PreviewActivity extends BaseActivity implements HasSupportFragmentInjector,
        SavingContentInfoDialog.OnCloseSavingContentInfoDialogListener {

    private static final String TAG = PreviewActivity.class.getSimpleName();
    public static final String START_WITH_AD = "START_WITH_AD";
    private AdView adView;
    private RewardedVideoAd rewardAd;

    public enum StartFor {
        VIDEO,
        IMAGE
    }

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentInjector;
    @Inject
    Config config;

    private static final String SS_URI = "SS_URI";
    private static final String SS_URI_UNMARKED = "SS_URI_UNMARKED";
    private static final String SS_START_FOR = "SS_START_FOR";

    private static final String EXTRA_URI = "EXTRA_URI";
    private static final String EXTRA_URI_UNMARKED = "EXTRA_URI_UNMARKED";
    private static final String EXTRA_START_FOR = "EXTRA_START_FOR";

    private PreviewActivityBinding binding;
    private StartFor startFor;
    private Uri uri;
    private Uri unmarkedUri;
    private AlertDialog deleteConfirmationDialog;

    public static void start(Context context, StartFor startFor, Uri uri, Uri unmarkedUri) {
        Intent starter = new Intent(context, PreviewActivity.class);

        starter.putExtra(START_WITH_AD, true);
        starter.putExtra(EXTRA_START_FOR, startFor);
        starter.putExtra(EXTRA_URI, uri);
        starter.putExtra(EXTRA_URI_UNMARKED, unmarkedUri);
        context.startActivity(starter);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentInjector;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        if (!config.isDarkTheme()) {
            setTheme(R.style.AppTheme_Light);
        }
        // enable objects when we left capture screen
        config.setObjectsEnabled(true);
        config.setBackgroundEffectsEnabled(true);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                String errorMessage = "Can't start preview activity with null extras";
                Timber.e(errorMessage);
                finish();
                return;
            }
            startFor = (StartFor) extras.getSerializable(EXTRA_START_FOR);
            uri = extras.getParcelable(EXTRA_URI);
            unmarkedUri = extras.getParcelable(EXTRA_URI_UNMARKED);
        } else {
            startFor = (StartFor) savedInstanceState.getSerializable(SS_START_FOR);
            uri = savedInstanceState.getParcelable(SS_URI);
            unmarkedUri = savedInstanceState.getParcelable(SS_URI_UNMARKED);
        }
        binding = DataBindingUtil.setContentView(this, R.layout.preview_activity);

        binding.drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                binding.drawerLayout.bringChildToFront(drawerView);
                binding.drawerLayout.requestLayout();
            }
        });
        binding.drawerLayout.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                binding.drawerLayout.getViewTreeObserver().removeOnPreDrawListener(this);
                //noinspection UnnecessaryLocalVariable
                int drawerWidth = (int) (AndroidUtils.getDisplayWidth(PreviewActivity.this) * 0.676f); // % of the screen
                binding.drawerContainer.getLayoutParams().width = drawerWidth;
                Intent intent = getIntent();
                if (intent.getBooleanExtra(DrawerFragment.EXTRA_THEME_CHANGE, false)) {
                    DrawerLayout drawerLayout = binding.drawerLayout;
                    drawerLayout.post(() -> drawerLayout.openDrawer(Gravity.START, false));
                    getIntent().putExtra(DrawerFragment.EXTRA_THEME_CHANGE, false);
                }
                return true;
            }
        });
        replaceFragment(DrawerFragment.newInstance(), R.id.drawerContainer, false);
        binding.settings.setOnClickListener(v -> binding.drawerLayout.openDrawer(Gravity.START));
        binding.doneButton.setOnClickListener(v -> finish());
        binding.share.setOnClickListener(v -> share(startFor, uri));
        binding.delete.setOnClickListener(v -> askDeleteConfirmation());
        binding.videoView.setVisibility(startFor == VIDEO ? View.VISIBLE : View.INVISIBLE);
        binding.imageView.setVisibility(startFor == VIDEO ? View.INVISIBLE : View.VISIBLE);
        binding.removeWatermark.setOnClickListener(v -> onRemoveWatermarkClicked());
        ObjectAnimator animation = ObjectAnimator.ofFloat(binding.removeWatermark, "translationX", 0f);
        animation.setDuration(1000);
        animation.start();
        addAdView();
        openAd();
        rewardAd = App.getInstance().getRewardAd();
        App.getInstance().reloadRewardAd();
    }

    private void openSavingContentInfoDialog() {
        if (config.isShowSavingContentInfoDialog()) {
            binding.root.postDelayed(() -> {
                binding.videoView.pause();
                SavingContentInfoDialog contentInfoDialog = new SavingContentInfoDialog();
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(contentInfoDialog, SavingContentInfoDialog.class.getName())
                        .commitAllowingStateLoss();
            }, 1000);
        }
    }

    private void openAd() {
        runOnUiThread(() -> {

            if (getIntent().getExtras() != null &&
                    getIntent().getExtras().getBoolean(START_WITH_AD, false)) {
                getIntent().getExtras().putBoolean(START_WITH_AD, false);
                boolean showAd = config.isShowAd();
                config.onAdShown();

                InterstitialAd interstitialAd = App.getInstance().getInterstitialAd();
                if (showAd && interstitialAd.isLoaded()) {
                    interstitialAd.show();
                } else {
                    openSavingContentInfoDialog();
                }

                interstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        openSavingContentInfoDialog();
                        interstitialAd.loadAd(new AdRequest.Builder().build());
                    }

                    @Override
                    public void onAdOpened() {
                        super.onAdOpened();
                        binding.videoView.pause();
                    }
                });
            }
        });
    }

    private void askDeleteConfirmation() {
        deleteConfirmationDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.DefaultAlertDialog))
                .setMessage(getString(R.string.delete_file_confirmation, startFor == VIDEO ? "video" : "photo"))
                .setNegativeButton(R.string.delete_file_negative_button_text, (dialog, which) -> {
                })
                .setPositiveButton(R.string.delete_file_positive_button_text, (dialog, which) -> {
                    deleteOutputFile(uri.getPath());
                    sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
                    finish();
                })
                .create();
        deleteConfirmationDialog.show();
    }

    @SuppressWarnings("ConstantConditions")
    private void deleteOutputFile(@NonNull String path) {
        boolean deleted = false;
        File outputFile = null;
        if (path != null) {
            outputFile = new File(path);
        }
        if (outputFile != null && outputFile.exists()) {
            if (outputFile.delete()) {
                Log.w(TAG, outputFile.getPath() + " was successfully deleted");
                deleted = true;
            }
        }
        if (!deleted) {
            longToast(R.string.error_deleting_file);
        }
    }

    private void addAdView() {
        adView = new AdView(this);
        adView.setId(View.generateViewId());
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(BuildConfig.PREVIEW_BANNER_AD_UNIT);
        binding.root.addView(adView);
        ConstraintSet set = new ConstraintSet();
        set.clone(binding.root);
        set.centerHorizontally(adView.getId(), ConstraintSet.PARENT_ID);
        set.connect(adView.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0);
//        set.clear(binding.doneButton.getId(), ConstraintSet.BOTTOM);
//        set.connect(binding.doneButton.getId(), ConstraintSet.BOTTOM, adView.getId(), ConstraintSet.TOP);
        set.applyTo(binding.root);
        adView.loadAd(new AdRequest.Builder().build());
    }


    @Override
    public void onSubmitSavingContentInfoDialog(boolean doNotShowAgain) {
        if (doNotShowAgain) {
            config.setShowSavingContentInfoDialog(false);
        }
    }

    @Override
    public void onCloseSavingContentInfoDialog() {
        playVideo();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupUri(false);
    }

    private void setupUri(boolean playVideo) {
        switch (startFor) {
            case IMAGE:
                showImage(uri);
                break;
            case VIDEO:
                setupVideo(uri);
                break;
        }
        if (startFor == VIDEO && playVideo && !isShowingSavingContentInfoDialog()) {
            playVideo();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateWatermarkButton();
        adView.resume();
        rewardAd.resume(this);
        setupUri(true);
    }


    @Override
    protected void onPause() {
        super.onPause();
        adView.pause();
        rewardAd.pause(this);
        binding.imageLoadingProgressBar.setVisibility(View.GONE);
        binding.videoView.pause();
        if (deleteConfirmationDialog != null && deleteConfirmationDialog.isShowing()) {
            deleteConfirmationDialog.dismiss();
            deleteConfirmationDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopVideo();
        adView.destroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SS_URI, uri);
        outState.putParcelable(SS_URI_UNMARKED, unmarkedUri);
        outState.putSerializable(SS_START_FOR, startFor);
    }

    private void share(StartFor startFor, Uri fileUri) {
        File file = new File(fileUri.getPath());
        Uri shareUri = FileProvider.getUriForFile(this,
                BuildConfig.APPLICATION_ID + ".provider",
                file);
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        MimeTypeMap map = MimeTypeMap.getSingleton();
        String ext = MimeTypeMap.getFileExtensionFromUrl(file.getName());
        String type = map.getMimeTypeFromExtension(ext);
        if (type == null) {
            type = startFor == IMAGE ? "image/*" : "video/*";
        }
        sendIntent.setType(type);
        sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        sendIntent.putExtra(Intent.EXTRA_STREAM, shareUri);
        startActivity(Intent.createChooser(sendIntent, "Share:"));
    }

    private void showImage(Uri uri) {
        ImageView imageView = binding.imageView;
        imageView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                int width = imageView.getWidth();
                int height = imageView.getHeight();
                binding.imageLoadingProgressBar.setVisibility(View.VISIBLE);
                BitmapLoadUtils.decodeBitmapInBackground(PreviewActivity.this, uri, width, height, new BitmapLoadCallback() {
                    @Override
                    public void onBitmapLoaded(@NonNull Bitmap bitmap, @NonNull ExifInfo exifInfo,
                                               @NonNull String imageInputPath) {
                        binding.imageLoadingProgressBar.setVisibility(View.GONE);
                        imageView.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onFailure(@NonNull Exception bitmapWorkerException) {
                        binding.imageLoadingProgressBar.setVisibility(View.GONE);
                        Timber.e(bitmapWorkerException, null);
                        longToast("Couldn't open image");
                    }
                });
                return true;
            }
        });
    }

    private void setupVideo(Uri uri) {
        VideoView videoView = binding.videoView;
        MediaController mc = new MediaController(this);
        mc.setMediaPlayer(videoView);
        videoView.setVideoURI(uri);
        videoView.setOnErrorListener((mp, what, extra) -> {
            longToast(R.string.error_playing_video);
            Timber.e("videoView.onError - source: %s", uri.toString());
            Timber.e("videoView.onError - what: %s, extra: %s", what, extra);
            finish();
            return true;
        });
        videoView.seekTo(1);
        videoView.setOnPreparedListener(mp -> mp.setLooping(true));
    }

    private void playVideo() {
        binding.videoView.start();
    }

    private void stopVideo() {
        binding.videoView.stopPlayback();
        binding.videoView.suspend();
    }

    private boolean isShowingSavingContentInfoDialog() {
        return getSupportFragmentManager()
                .findFragmentByTag(SavingContentInfoDialog.class.getName()) != null;
    }

    private void updateWatermarkButton() {
        binding.removeWatermark.setVisibility(
                isShowRemoveWatermarkButton() ? View.VISIBLE : View.GONE
        );
    }

    private boolean isShowRemoveWatermarkButton() {
        return unmarkedUri != null;
    }

    private void onRemoveWatermarkClicked() {
        if (rewardAd.isLoaded()) {
            rewardAd.show();

            rewardAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
                @Override
                public void onRewardedVideoAdLoaded() {

                }

                @Override
                public void onRewardedVideoAdOpened() {

                }

                @Override
                public void onRewardedVideoStarted() {

                }

                @Override
                public void onRewardedVideoAdClosed() {
                    App.getInstance().reloadRewardAd();
                }

                @Override
                public void onRewarded(RewardItem rewardItem) {
                    stopVideo();
                    replaceFileToUnmarked(throwable -> {
                        if (throwable == null) {
                            setupUri(false);
                        } else {
                            longToast(R.string.error_unknown);
                        }
                    });
                }

                @Override
                public void onRewardedVideoAdLeftApplication() {

                }

                @Override
                public void onRewardedVideoAdFailedToLoad(int i) {

                }

                @Override
                public void onRewardedVideoCompleted() {

                }
            });
        } else {
            if (AndroidUtils.checkConnection(this)) {
                longToast(R.string.notification_wait_loading);
            } else {
                longToast(R.string.error_check_internet_connection);
            }
        }
    }

    private void replaceFileToUnmarked(Consumer<Throwable> callable) {
        AndroidUtils.deleteFile(uri.getPath());
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
        uri = Uri.parse(AndroidUtils.addToFileName(uri.toString(), "_unmarked"));
        AndroidUtils.replaceFile(unmarkedUri.getPath(), uri.getPath(), throwable -> {
            unmarkedUri = null;
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
            runOnUiThread(() -> callable.accept(throwable));
        });
    }
}
