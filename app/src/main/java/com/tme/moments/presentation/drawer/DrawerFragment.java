package com.tme.moments.presentation.drawer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tme.moments.R;
import com.tme.moments.databinding.DrawerFragmentBinding;
import com.tme.moments.presentation.base.BaseFragment;
import com.tme.moments.presentation.core.Config;
import com.tme.moments.presentation.preview.PreviewActivity;
import com.tme.moments.presentation.utils.AndroidUtils;
import com.tme.moments.recording.Quality;

import java.util.Arrays;

import javax.inject.Inject;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjection;

import static android.databinding.DataBindingUtil.bind;

/**
 * @author zoopolitic.
 */
public class DrawerFragment extends BaseFragment {

    private DrawerAdapter adapter;

    @Subcomponent
    public interface Component extends AndroidInjector<DrawerFragment> {
        @Subcomponent.Builder
        abstract class Builder extends AndroidInjector.Builder<DrawerFragment> {
        }
    }

    public static final String EXTRA_THEME_CHANGE = "EXTRA_THEME_CHANGE";

    private DrawerFragmentBinding binding;

    @Inject Config config;

    public static DrawerFragment newInstance() {
        Bundle args = new Bundle();
        DrawerFragment fragment = new DrawerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.drawer_fragment, container, false);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = bind(view);
    }

    @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RecyclerView list = binding.list;
        Context context = getActivity();
        list.setLayoutManager(new LinearLayoutManager(context));
        list.setHasFixedSize(false);
        list.getItemAnimator().setChangeDuration(0);
        adapter = new DrawerAdapter(config,
                item -> item.action().run(),
                link -> AndroidUtils.openUrl(context, link),
                () -> {
                    Activity activity = getActivity();
                    Intent recreateIntent = activity.getIntent();
                    recreateIntent.putExtra(EXTRA_THEME_CHANGE, true);
                    activity.startActivity(recreateIntent);
                    activity.finish();
                });
        adapter.add(DrawerSectionItem.create(getString(R.string.section_video_title)));
        adapter.add(DrawerVideoLengthItem.create(Arrays.asList(15, 30, 60)));
        adapter.add(DrawerVideoQualityItem.create(Arrays.asList(Quality.values())));
        adapter.add(DrawerVideoSoundItem.create());
        adapter.add(DrawerDividerItem.create());


        if (!context.getClass().getSimpleName().equals(PreviewActivity.class.getSimpleName())) {
            adapter.add(DrawerSectionItem.create(getString(R.string.section_theme_title)));
            adapter.add(DrawerThemeItem.create());
            adapter.add(DrawerDividerItem.create());
        }



        adapter.add(DrawerSectionItem.create(getString(R.string.section_more_title)));
        adapter.add(DrawerActionItem.create(getString(R.string.share_the_app), getThemedDrawable(R.attr.shareAppIcon),
                () -> {
                    String subject = getString(R.string.share_subject);
                    String text = getString(R.string.share_text);
                    AndroidUtils.share(context, subject, text);
                }));
        adapter.add(DrawerActionItem.create(getString(R.string.send_feedback), getThemedDrawable(R.attr.sendFeedbackIcon),
                () -> {
                    String subject = getString(R.string.feedback_subject);
                    AndroidUtils.mailTo(context, "madhuvanti.apps@gmail.com", subject, null);
                }));
        adapter.add(DrawerActionItem.create(getString(R.string.rate), getThemedDrawable(R.attr.rateAppIcon),
                () -> AndroidUtils.openPlayMarketApp(context, context.getPackageName())));
        adapter.add(DrawerActionItem.create(getString(R.string.more_apps), getThemedDrawable(R.attr.moreAppsIcon),
                () -> AndroidUtils.openPlayMarketDeveloperApps(context, "The+Madhuvanti+Express")));
        if (AndroidUtils.isScreenSmall(getContext())) {
            adapter.add(DrawerLinkItem.create(
                    getString(R.string.privacy_policy),
                    getString(R.string.privacy_policy_url))
            );
        } else {
            binding.privacyPolicy.setVisibility(View.VISIBLE);
            binding.privacyPolicy.setOnClickListener(v ->
                    AndroidUtils.openUrl(context, getString(R.string.privacy_policy_url)));
        }
        list.setAdapter(adapter);
    }

    @Override public void onResume() {
        super.onResume();
        // refresh to be always up-to-date, for example if record config params
        // was changed on preview screen - drawer on capture screen will also be updated
        adapter.notifyDataSetChanged();
    }
}
