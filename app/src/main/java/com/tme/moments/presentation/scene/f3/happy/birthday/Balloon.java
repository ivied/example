package com.tme.moments.presentation.scene.f3.happy.birthday;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Balloon extends Decal {

    private static final float ORIGINAL_SCALE = 0.9f;
    private static final float END_SCALE = 0.6f;

    private float speed;

    private float p0x, p0y;
    private float p1x, p1y;
    private float p2x, p2y;
    private float p3x, p3y;

    private float startDelayNanos;
    private float t;
    private boolean finished = true;

    void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    public boolean isFinished() {
        return finished;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    void step(long deltaNanos) {
        if (startDelayNanos >= 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float deltaT = speed * deltaNanos;
        t += deltaT;

        float single = (1 - t);
        float quadratic = (1 - t) * (1 - t);
        float cube = (1 - t) * (1 - t) * (1 - t);
        float x = cube * p0x + 3 * t * quadratic * p1x + 3 * t * t * single * p2x + t * t * t * p3x;
        float y = cube * p0y + 3 * t * quadratic * p1y + 3 * t * t * single * p2y + t * t * t * p3y;

        setX(x);
        setY(y);

        float scale = END_SCALE + (ORIGINAL_SCALE - END_SCALE) * (1 - t);
        setScale(scale);

        if (t >= 1) {
            finished = true;
        }
    }

    void init(TextureRegion region, float width, float height) {
        finished = false;
        setScale(ORIGINAL_SCALE);
        setTextureRegion(region);
        setDimensions(width, height);

        p0x = random(-1.25f, 1.25f);
        p0y = -1.0f - getHeight() / 2;

        p1x = random(-1.25f, 1.25f);
        p1y = random(-0.5f, 0.0f);

        p2x = random(-1.25f, 1.25f);
        p2y = random(0.0f, 0.5f);

        p3x = random(-1.25f, 1.25f);
        p3y = random(1 + getHeight() / 2, 2);

        speed = random(0.166f, 0.25f) / Const.NANOS_IN_SECOND;
        setColor(1, 1, 1, 1);
        setPosition(p0x, p0y, -1.0f);
        t = 0;
    }
}
