package com.tme.moments.presentation.capture;

import android.hardware.Camera;

import java.util.Comparator;

/**
 * This comparator will sort all the {@link Camera.Size} into the desxending order of the total number
 * of pixels.
 */
class PictureSizeComparator implements Comparator<Camera.Size> {
    // Used for sorting in ascending order of
    public int compare(Camera.Size a, Camera.Size b) {
        return (b.height * b.width) - (a.height * a.width);
    }
}
