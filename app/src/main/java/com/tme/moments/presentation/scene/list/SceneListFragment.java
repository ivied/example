package com.tme.moments.presentation.scene.list;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.tme.moments.R;
import com.tme.moments.databinding.SceneListFragmentBinding;
import com.tme.moments.presentation.base.BaseFragment;
import com.tme.moments.presentation.recyclerview.DisableableLayoutManager;
import com.tme.moments.presentation.scene.Scene;
import com.tme.moments.presentation.scene.f1.love.LoveIsInTheAir;
import com.tme.moments.presentation.scene.f10.lemon.TasteTheLemon;
import com.tme.moments.presentation.scene.f11.spring.SpringBlossom;
import com.tme.moments.presentation.scene.f12.music.PlayMusic;
import com.tme.moments.presentation.scene.f13.rasta.BeARasta;
import com.tme.moments.presentation.scene.f14.beach.SunnyBeach;
import com.tme.moments.presentation.scene.f15.autumn.flavor.AutumnFlavor;
import com.tme.moments.presentation.scene.f16.christmas.MerryChristmas;
import com.tme.moments.presentation.scene.f17.royal.TheRoyal;
import com.tme.moments.presentation.scene.f18.paper.speech.PaperSpeechBubble;
import com.tme.moments.presentation.scene.f19.purple.fantasy.PurpleFantasy;
import com.tme.moments.presentation.scene.f2.dreamland.Dreamland;
import com.tme.moments.presentation.scene.f20.comic.YourComicBook;
import com.tme.moments.presentation.scene.f3.happy.birthday.HappyBirthday;
import com.tme.moments.presentation.scene.f4.rose.petals.RosePetals;
import com.tme.moments.presentation.scene.f5.kids.fun.KidsFun;
import com.tme.moments.presentation.scene.f6.peafowl.LovelyPeafowl;
import com.tme.moments.presentation.scene.f7.emotional.hardcore.EmotionalHardcore;
import com.tme.moments.presentation.scene.f8.sunflower.field.WelcomeToFlowerField;
import com.tme.moments.presentation.scene.f9.girly.special.GirlySpecial;
import com.tme.moments.presentation.utils.AndroidUtils;

import java.util.ArrayList;
import java.util.List;

import static android.databinding.DataBindingUtil.bind;

/**
 * @author zoopolitic.
 */
public class SceneListFragment extends BaseFragment {

    private static final String SS_SELECTED_SCENE_INDEX = "SS_SELECTED_SCENE_INDEX";
    private RecyclerView list;

    public interface Listener {
        void onScenePicked(Scene scene, String title);
        void onSetupFinished(Scene scene, String title);
    }

    private SceneListFragmentBinding binding;
    private SceneListAdapter adapter;
    private DisableableLayoutManager sceneLayoutManager;
    private Listener listener;

    public static SceneListFragment newInstance() {
        Bundle args = new Bundle();
        SceneListFragment fragment = new SceneListFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (Listener) context;
        } catch (ClassCastException e) {
            throw new IllegalStateException(getClass().getSimpleName()
                    + " should only be attached to activity that implements "
                    + Listener.class.getSimpleName());
        }
    }

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.scene_list_fragment, container, false);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = bind(view);
    }

    @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        int selectedSceneIndex = 0;
        if (savedInstanceState != null) {
            selectedSceneIndex = savedInstanceState.getInt(SS_SELECTED_SCENE_INDEX);
        }
        setupSceneList(selectedSceneIndex);
    }

    @Override public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        int selectedSceneIndex = adapter.getSelectedSceneIndex();
        outState.putInt(SS_SELECTED_SCENE_INDEX, selectedSceneIndex);
    }

    public Scene currentScene() {
        return adapter.getSelectedFrame().scene;
    }

    public void setEnabled(boolean enabled) {
        adapter.setClickable(enabled);
        sceneLayoutManager.setScrollEnabled(enabled);
    }

    private void setupSceneList(int selectedSceneIndex) {
        // adapter
        List<SceneListItem> items = new ArrayList<>();
        items.add(new SceneListItem(R.drawable.tn1, new LoveIsInTheAir(), R.string.love_is_in_the_air));
        items.add(new SceneListItem(R.drawable.tn2, new Dreamland(), R.string.dreamland));
        items.add(new SceneListItem(R.drawable.tn3, new HappyBirthday(), R.string.happy_birthday));
        items.add(new SceneListItem(R.drawable.tn4, new RosePetals(), R.string.rose_petals));
        items.add(new SceneListItem(R.drawable.tn5, new KidsFun(), R.string.kids_fun));
        items.add(new SceneListItem(R.drawable.tn6, new LovelyPeafowl(), R.string.lovely_peafowl));
        items.add(new SceneListItem(R.drawable.tn7, new EmotionalHardcore(), R.string.emotional_hardcore));
        items.add(new SceneListItem(R.drawable.tn8, new WelcomeToFlowerField(), R.string.sunflower_field));
        items.add(new SceneListItem(R.drawable.tn9, new GirlySpecial(), R.string.hello_kitty));
        items.add(new SceneListItem(R.drawable.tn10, new TasteTheLemon(), R.string.nasty_lemons));
        items.add(new SceneListItem(R.drawable.tn11, new SpringBlossom(), R.string.spring_blossom));
        items.add(new SceneListItem(R.drawable.tn12, new PlayMusic(), R.string.musical_melody));
        items.add(new SceneListItem(R.drawable.tn13, new BeARasta(), R.string.rasta));
        items.add(new SceneListItem(R.drawable.tn14, new SunnyBeach(), R.string.sparkling_beach));
        items.add(new SceneListItem(R.drawable.tn15, new AutumnFlavor(), R.string.autumn_flavour));
        items.add(new SceneListItem(R.drawable.tn16, new MerryChristmas(), R.string.merry_christmas));
        items.add(new SceneListItem(R.drawable.tn17, new TheRoyal(), R.string.the_royal));
        items.add(new SceneListItem(R.drawable.tn18, new PaperSpeechBubble(), R.string.paper_speech_bubble));
        items.add(new SceneListItem(R.drawable.tn19, new PurpleFantasy(), R.string.purple_fantasy));
        items.add(new SceneListItem(R.drawable.tn20, new YourComicBook(), R.string.your_comic_book));
        SceneListItem selectedItem = items.get(selectedSceneIndex);
        selectedItem.setSelected(true);
        adapter = new SceneListAdapter(items, item -> listener.onScenePicked(item.scene, getString(item.frameTitleResId)));

        Context context = getActivity();
        // decorations
        DividerItemDecoration decoration =
                new DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL);
        decoration.setDrawable(getResources().getDrawable(R.drawable.frame_item_decoration));

        // list
        list = binding.sceneList;
        sceneLayoutManager = new DisableableLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        list.setHasFixedSize(true);
        list.setLayoutManager(sceneLayoutManager);
        list.addItemDecoration(decoration);
        list.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override public boolean onPreDraw() {
                list.getViewTreeObserver().removeOnPreDrawListener(this);
                int displayWidth = AndroidUtils.getDisplayWidth(context);
                int listHeight = list.getHeight(); // list height is equal item height and item width
                int paddingLeft = displayWidth / 2 - listHeight / 2; // center item on screen
                AndroidUtils.setLeftPadding(list, paddingLeft);
                list.setAdapter(adapter);
                return true;
            }
        });
        SceneListItem current = adapter.getSelectedFrame();
        listener.onSetupFinished(current.scene, getString(current.frameTitleResId));
    }


}
