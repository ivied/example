package com.tme.moments.presentation.audio.player;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;

import com.tme.moments.R;

/**
 * @author zoopolitic.
 */
public class AudioSeekBar extends AppCompatSeekBar {

    private Rect forwardLineBounds;
    private Rect backLineBounds;
    private Paint paint;
    private boolean recording;
    private long recordingStartProgress = -1;
    private int recordColor;
    private int recordProgressColor;

    public AudioSeekBar(Context context) {
        super(context);
        init(context, null, 0);
    }

    public AudioSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public AudioSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        Resources res = getResources();
        if (attrs != null) {
            TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AudioSeekBar, defStyleAttr, 0);
            recordColor = array.getColor(R.styleable.AudioSeekBar_asb_recordColor, res.getColor(R.color.colorRecordDark));
            recordProgressColor = array.getColor(R.styleable.AudioSeekBar_asb_progressRecordColor, -1);
        } else {
            recordColor = res.getColor(R.color.colorRecordDark);
        }
        forwardLineBounds = new Rect();
        backLineBounds = new Rect();
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(recordColor);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(context.getResources().getDimensionPixelSize(R.dimen.audio_line_width));
    }

    @Override protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (recording) {
            drawMixMusicTrack(canvas);
            drawThumbAgain(canvas);
        }
    }

    private void drawThumbAgain(Canvas canvas) {
        Drawable thumb = getThumb();
        if (thumb != null) {
            final int saveCount = canvas.save();
            int paddingLeft = getPaddingLeft();
            int paddingTop = getPaddingTop();
            int offset = getThumbOffset();
            // Translate the padding. For the x, we need to allow the thumb to
            // draw in its extra space
            canvas.translate(paddingLeft - offset, paddingTop);
            thumb.draw(canvas);
            canvas.restoreToCount(saveCount);
        }
    }

    @Override protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        recalculateRecordLinesBounds();
    }

    public long getRecordingStartProgress() {
        return recordingStartProgress;
    }

    public void setRecording(boolean recording) {
        this.recording = recording;
        if (recording) {
            recordingStartProgress = getProgress();
            // change drawable to no draw regular progress fill during recording
            if (recordProgressColor != -1) {
                getProgressDrawable().setColorFilter(recordProgressColor, PorterDuff.Mode.SRC_IN);
            }
            getThumb().setColorFilter(recordColor, PorterDuff.Mode.SRC_ATOP);
        } else {
            getThumb().clearColorFilter();
            // change drawable back to normal state
            if (recordProgressColor != -1) {
                getProgressDrawable().clearColorFilter();
            }
            recordingStartProgress = -1;
        }
        recalculateRecordLinesBounds();
        invalidate();
    }

    private void recalculateRecordLinesBounds() {
        int trackEnd = getWidth() - getPaddingRight();
        float barWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        float startFraction = Math.min(1.0f, (float) recordingStartProgress / getMax());
        float progressFraction = Math.min(1.0f, (float) getProgress() / getMax());
        int startX = getPaddingLeft() + (int) (startFraction * barWidth);
        int stopX = startFraction <= progressFraction
                ? getPaddingLeft() + (int) (progressFraction * barWidth)
                : trackEnd;
        int y = getHeight() / 2;

        forwardLineBounds.set(startX, y, stopX, y);

        if (startFraction > progressFraction) {
            startX = getPaddingLeft();
            stopX = Math.min(getPaddingLeft() + (int) (progressFraction * barWidth), trackEnd);
            backLineBounds.set(startX, y, stopX, y);
        } else {
            backLineBounds.set(0, 0, 0, 0);
        }
    }

    private void drawMixMusicTrack(Canvas canvas) {
        recalculateRecordLinesBounds();
        float startX, stopX, y;
        if (forwardLineBounds.width() > 0) {
            startX = forwardLineBounds.left;
            stopX = forwardLineBounds.right;
            y = forwardLineBounds.top; // can be bottom, doesn't matter since it's the same value
            canvas.drawLine(startX, y, stopX, y, paint);
        }
        if (backLineBounds.width() > 0) {
            startX = backLineBounds.left;
            stopX = backLineBounds.right;
            y = backLineBounds.top; // can be bottom, doesn't matter since it's the same value
            canvas.drawLine(startX, y, stopX, y, paint);
        }
    }
}
