package com.tme.moments.presentation.core;

import com.tme.moments.presentation.RenderThread;
import com.tme.moments.presentation.preview.PreviewActivity;
import com.tme.moments.presentation.scene.BaseScene;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * @author zoopolitic.
 */
@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AndroidInjectionModule.class,
        CoreModule.class,
        ActivityBuilder.class
})
public interface CoreComponent {
    void inject(App app);
    void inject(RenderThread renderThread);
    void inject(BaseScene baseScene);
    void inject(PreviewActivity previewActivity);
}
