package com.tme.moments.presentation.scene.f1.love;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class PulsingHeart extends Decal {

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float t;
    private float speed;

    private boolean finished;

    private boolean scaleUp;
    private float scaleUpSpeed;
    private float scaleDownSpeed;
    private float scaleWaitTime;
    private float minScale;
    private float maxScale;
    private float scaleDecreaseValue;

    PulsingHeart(TextureRegion region, float width, float height, float x, float y) {
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        setPosition(x, y, -1f);
        speed = 0.3f / Const.NANOS_IN_SECOND;
        scaleUpSpeed = 1.5f / Const.NANOS_IN_SECOND;
        scaleDownSpeed = 1.5f / Const.NANOS_IN_SECOND;
        p0x = x;
        p0y = y;
        float min = -0.2f;
        float max = 0.2f;
        p1x = p0x + random(min, max);
        p1y = p0y + random(min, max);
        p2x = p1x + random(min, max);
        p2y = p1y + random(min, max);
        scaleDecreaseValue = 0.15f;
        float initialScale = 0.75f;
        minScale = initialScale - scaleDecreaseValue;
        maxScale = initialScale + scaleDecreaseValue;
        setScale(initialScale);
        scaleUp = true;
        setRotationZ(random(0, 360f));
    }

    void step(long deltaNanos) {
        float deltaT = speed * deltaNanos;
        t += deltaT;
        t = Math.max(0, Math.min(1, t));

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        setX(x);
        setY(y);

        float fadeOutThreshold = 0.8f;
        if (t >= fadeOutThreshold) {
            float fraction = (t - fadeOutThreshold) / (1 - fadeOutThreshold);
            setAlpha(1 - fraction);
        }

        if (scaleWaitTime > 0) {
            scaleWaitTime -= deltaNanos;
        } else {
            if (scaleUp) {
                float deltaScale = scaleUpSpeed * deltaNanos;
                float newScale = Math.min(maxScale, getScaleX() + deltaScale);
                setScale(newScale);
                if (newScale == maxScale) {
                    scaleUp = false;
                }
            } else {
                float deltaScale = scaleDownSpeed * deltaNanos;
                float newScale = Math.max(minScale, getScaleX() - deltaScale);
                setScale(newScale);
                if (newScale == minScale) {
                    scaleUp = true;
                    scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(650);
                    minScale -= scaleDecreaseValue;
                    maxScale -= scaleDecreaseValue;
                }
            }
        }

        if (getScaleX() == 0 || t >= 1) {
            finished = true;
        }
    }

    public boolean isFinished() {
        return finished;
    }
}
