package com.tme.moments.presentation;

import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;

import com.tme.moments.BuildConfig;
import com.tme.moments.R;
import com.tme.moments.gles.EglCore;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.gles.WindowSurface;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.capture.CaptureActivityHandler;
import com.tme.moments.presentation.core.App;
import com.tme.moments.presentation.core.Config;
import com.tme.moments.presentation.core.OpenGLLoader;
import com.tme.moments.presentation.core.ResourceProvider;
import com.tme.moments.presentation.scene.Scene;
import com.tme.moments.recording.RecordListener;
import com.tme.moments.recording.Recorder;
import com.tme.moments.recording.Recorder2;

import java.io.File;
import java.io.IOException;
import java.nio.FloatBuffer;

import javax.inject.Inject;

import timber.log.Timber;

import static android.opengl.GLES11Ext.GL_DEPTH24_STENCIL8_OES;
import static android.opengl.GLES11Ext.GL_TEXTURE_EXTERNAL_OES;
import static android.opengl.GLES20.GL_BLEND;
import static android.opengl.GLES20.GL_CLAMP_TO_EDGE;
import static android.opengl.GLES20.GL_COLOR_ATTACHMENT0;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_CULL_FACE;
import static android.opengl.GLES20.GL_DEPTH_TEST;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_FRAMEBUFFER;
import static android.opengl.GLES20.GL_FRAMEBUFFER_COMPLETE;
import static android.opengl.GLES20.GL_LINEAR;
import static android.opengl.GLES20.GL_NEAREST;
import static android.opengl.GLES20.GL_ONE;
import static android.opengl.GLES20.GL_ONE_MINUS_SRC_ALPHA;
import static android.opengl.GLES20.GL_RENDERBUFFER;
import static android.opengl.GLES20.GL_RGB565;
import static android.opengl.GLES20.GL_RGBA;
import static android.opengl.GLES20.GL_STENCIL_ATTACHMENT;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_S;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_T;
import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.GL_UNSIGNED_BYTE;
import static android.opengl.GLES20.glBindFramebuffer;
import static android.opengl.GLES20.glBindRenderbuffer;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glBlendFunc;
import static android.opengl.GLES20.glCheckFramebufferStatus;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glDeleteFramebuffers;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteRenderbuffers;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glFramebufferRenderbuffer;
import static android.opengl.GLES20.glFramebufferTexture2D;
import static android.opengl.GLES20.glGenFramebuffers;
import static android.opengl.GLES20.glGenRenderbuffers;
import static android.opengl.GLES20.glGenTextures;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glRenderbufferStorage;
import static android.opengl.GLES20.glTexImage2D;
import static android.opengl.GLES20.glTexParameterf;
import static android.opengl.GLES20.glTexParameteri;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;
import static android.opengl.GLES20.glViewport;

/**
 * This class handles all OpenGL rendering.
 */
public class RenderThread extends Thread {

    private static final boolean VERBOSE = false;

    private static final String TAG = RenderThread.class.getSimpleName();

    private static final FloatBuffer FULL_SCREEN_BUF = GlUtil.createFloatBuffer(new float[]{
            -1.0f, -1.0f,   // 0 bottom left
            1.0f, -1.0f,    // 1 bottom right
            -1.0f, 1.0f,    // 2 top left
            1.0f, 1.0f      // 3 top right
    });

    private static final FloatBuffer FULL_SCREEN_TEX_BUF = GlUtil.createFloatBuffer(new float[]{
            0.0f, 0.0f,     // 0 bottom left
            1.0f, 0.0f,     // 1 bottom right
            0.0f, 1.0f,     // 2 top left
            1.0f, 1.0f      // 3 top right
    });

    @Inject
    OpenGLLoader openGLLoader;
    @Inject
    ResourceProvider resourceProvider;
    @Inject
    Config config;

    // Object must be created on render thread to get correct Looper, but is used from
    // UI thread, so we need to declare it volatile to ensure the UI thread sees a fully
    // constructed object.
    private volatile RenderHandler handler;

    // Handler we can send messages to if we want to update the app UI.
    private CaptureActivityHandler activityHandler;

    // Used to wait for the thread to start.
    private final Object startLock = new Object();
    private final Object switchLock = new Object();
    private boolean ready = false;

    private volatile SurfaceHolder surfaceHolder;  // may be updated by UI thread
    private EglCore eglCore;
    private WindowSurface windowSurface;

    private int surfaceWidth = -1;
    private int surfaceHeight = -1;

    // FPS counter.
    private long fpsCountStartNanos;
    private int fpsCountFrame;

    // Used for off-screen rendering for 2.0 record.
    private int offscreenRecordTextureHandle;
    private int recordFBOHandle;
    private int recordStencilBufferHandle;

    // Used for offscreen render for image capture from camera or screenshot of whole layout
    private int offscreenFBOHandle;
    private int offscreenColorBufferHandle;
    private int offscreenStencilBufferHandle;

    // blit program handles
    private int programHandle;
    private int uProjectionMatrixLoc;
    private int aPositionHandle;
    private int aTexCoordHandle;

    private SurfaceTexture cameraTexture;
    private int cameraTextureHandle = -1;
    private Recorder recorder;
    private Sprite watermark;
    private TextureInfo imageInfo;

    private long prevTimeNanos;
    private long FPSThrottlePresentationInterval;
    private Scene scene;
    private Scene newScene;
    private RecordListener recordListener;

    /**
     * Pass in the SurfaceView's SurfaceHolder.  Note the Surface may not yet exist.
     */
    public RenderThread(SurfaceHolder holder, CaptureActivityHandler activityHandler) {
        App.getInstance().coreComponent().inject(this);
        surfaceHolder = holder;
        final int FPSThrottleInterval = 2;
        FPSThrottlePresentationInterval = (long) (FPSThrottleInterval / 60.0f * 1_000_000_000);
        this.activityHandler = activityHandler;
    }

    public void setRecordListener(RecordListener listener) {
        this.recordListener = listener;
    }

    public void setScene(@NonNull Scene newScene) {
        synchronized (switchLock) {
            handler.removeMessages(RenderHandler.MSG_SWITCH_SCENE);
            handler.removeMessages(RenderHandler.MSG_DO_FRAME);
            this.newScene = newScene;
            handler.sendSwitchScene();
            try {
                switchLock.wait();
            } catch (InterruptedException ie) {
                /* not expected */
                Timber.e(ie, null);
            }
        }
    }

    void switchScene() {
        synchronized (switchLock) {
            if (scene != null) {
                scene.release();
            }
            scene = newScene;
            newScene = null;
            if (eglCore != null) {
                scene.init();
            }
            if (surfaceWidth > -1 && surfaceHeight > -1) {
                scene.setup(imageInfo, surfaceWidth, surfaceHeight);
            }
            newScene = null;
            // new frame can arrive during scene switch, in this case we should not render
            // current scene, but updateTexImage won't be called then and next frame arrive
            // won't trigger, so we update camera texture when switch finished to continue
            // render camera input
            if (cameraTexture != null && imageInfo == null) {
                cameraTexture.updateTexImage();
            }
            switchLock.notify();
        }
    }

    /**
     * Creates and returns SurfaceTexture that should be used for drawing camera input
     *
     * @return SurfaceTexture for the camera
     */
    public SurfaceTexture createCameraTexture() {
        releaseCameraTexture();

        int[] arr = new int[1];
        glGenTextures(arr.length, arr, 0);
        GlUtil.checkGlError("Generate camera texture");
        cameraTextureHandle = arr[0];
        glBindTexture(GL_TEXTURE_EXTERNAL_OES, cameraTextureHandle);
        glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        if (VERBOSE) Log.d(TAG, "Camera texture generated");

        cameraTexture = new SurfaceTexture(cameraTextureHandle);
        return cameraTexture;
    }

    private void releaseCameraTexture() {
        if (cameraTexture != null && cameraTextureHandle >= 0) {
            cameraTexture.release();
            cameraTexture = null;
            int[] arr = new int[1];
            arr[0] = cameraTextureHandle;
            glDeleteTextures(arr.length, arr, 0);
            GlUtil.checkGlError("Delete camera texture");
            if (VERBOSE) Log.d(TAG, "Camera texture released");
            cameraTextureHandle = -1;
        }
    }

    void onCameraModeEnabled() {
        deleteImage();
        scene.onCameraModeEnabled();
    }

    void onImageModeEnabled() {
        Uri uri = config.getImageUri();
        prepareImage(uri);
        if (imageInfo != null && scene != null) {
            scene.onImageModeEnabled(imageInfo);
        }
    }

    private void deleteImage() {
        if (imageInfo != null) {
            int[] values = new int[1];
            values[0] = imageInfo.textureHandle;
            glBindTexture(GL_TEXTURE_2D, 0);
            glDeleteTextures(1, values, 0);
            imageInfo = null;
            GlUtil.checkGlError("Delete image texture");
            if (VERBOSE) Log.d(TAG, "Image texture deleted");
        }
    }

    /**
     * Thread entry point.
     * <p>
     * The thread should not be started until the Surface associated with the SurfaceHolder
     * has been created.  That way we don't have to wait for a separate "surface created"
     * message to arrive.
     */
    @Override
    public void run() {
        Looper.prepare();
        handler = new RenderHandler(this);
        eglCore = new EglCore(null, EglCore.FLAG_RECORDABLE | EglCore.FLAG_TRY_GLES3);
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        synchronized (startLock) {
            ready = true;
            startLock.notify();    // signal waitUntilReady()
        }

        Looper.loop();

        if (VERBOSE) Log.d(TAG, "looper quit");
        release();
        eglCore.release();
        synchronized (startLock) {
            ready = false;
        }
    }

    /**
     * Waits until the render thread is ready to receive messages.
     * <p>
     * Call from the UI thread.
     */
    public void waitUntilReady() {
        synchronized (startLock) {
            while (!ready) {
                try {
                    startLock.wait();
                } catch (InterruptedException ie) { /* not expected */ }
            }
        }
    }

    /**
     * Shuts everything down.
     */
    @SuppressWarnings("ConstantConditions")
    void shutdown() {
        if (VERBOSE) Log.d(TAG, "shutdown");
        Looper.myLooper().quit();
    }

    /**
     * Returns the render thread's Handler.  This may be called from any thread.
     */
    public RenderHandler getHandler() {
        return handler;
    }

    /**
     * Prepares the surface.
     */
    void surfaceCreated() {
        if (VERBOSE) Log.d(TAG, "surfaceCreated");
        Surface surface = surfaceHolder.getSurface();
        prepareGl(surface);
        if (recorder == null) {
            recorder = new Recorder2();
        }
        recorder.init(eglCore, windowSurface.getWidth(), windowSurface.getHeight());
        recorder.attachListener(recordListener);
        if (scene != null) {
            scene.init();
        }
        Uri imageUri = config.getImageUri();
        if (imageUri != null) {
            prepareImage(imageUri);
        }
    }

    private void prepareImage(Uri imageUri) {
        imageInfo = openGLLoader.loadTexture(imageUri);
        if (imageInfo == null) Timber.e("Couldn't load texture for image: " + imageUri);
        activityHandler.onImageDecoded();
    }

    /**
     * Handles changes to the size of the underlying surface.  Adjusts viewport as needed.
     * Must be called before we start drawing.
     * (Called from RenderHandler.)
     */
    void surfaceChanged(int width, int height) {
        if (VERBOSE) Log.d(TAG, "surfaceChanged");
        surfaceWidth = width;
        surfaceHeight = height;
        glViewport(0, 0, width, height);
//        if (eglCore.getGlVersion() < 3) { todo glBlitFrameBuffer fails for some devices, using Recorder2 for now
        prepareRecordFBO(width, height);
//        }
        prepareOffscreenFBO(width, height);
        prepareWatermark();
        if (scene != null) {
            scene.setup(imageInfo, width, height);
        }
    }

    /**
     * Prepares window surface and GL state.
     */
    private void prepareGl(Surface surface) {
        if (VERBOSE) Log.d(TAG, "prepareGl");

        windowSurface = new WindowSurface(eglCore, surface, false);
        windowSurface.makeCurrent();

        prepareProgram();
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

        glDisable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);
    }

    private void prepareProgram() {
        programHandle = openGLLoader.createProgram(
                R.raw.blit_vertex_shader, R.raw.blit_fragment_shader);

        aPositionHandle = glGetAttribLocation(programHandle, "a_position");
        GlUtil.checkLocation(aPositionHandle, "a_position");

        aTexCoordHandle = glGetAttribLocation(programHandle, "a_texCoord");
        GlUtil.checkLocation(aTexCoordHandle, "a_texCoord");

        uProjectionMatrixLoc = glGetUniformLocation(programHandle, "u_projTrans");
        GlUtil.checkLocation(uProjectionMatrixLoc, "u_projTrans");

        glEnableVertexAttribArray(aPositionHandle);
        GlUtil.checkGlError("glEnableVertexAttribArray");

        glEnableVertexAttribArray(aTexCoordHandle);
        GlUtil.checkGlError("glEnableVertexAttribArray");

        glEnableVertexAttribArray(uProjectionMatrixLoc);
        GlUtil.checkGlError("glEnableVertexAttribArray");
    }

    private void prepareWatermark() {
        TextureInfo watermarkResult = openGLLoader.loadTexture(R.drawable.watermark);
        int watermarkHandle = watermarkResult.textureHandle;
        if (watermarkHandle == -1) {
            Timber.e("Couldn't prepare watermark");
            return;
        }
        int wmWidth = watermarkResult.imageWidth;
        int wmHeight = watermarkResult.imageHeight;
        TextureRegion region = new TextureRegion(watermarkHandle, wmWidth, wmHeight, 0, 0, wmWidth, wmHeight);
        float width = 0.435f;
        float height = 0.0725f;
        float margin = 0.05f;
        watermark = new Sprite(region, width, height);
        float x = 1 - width / 2 - margin; // 1 - is a right of the screen
        float y = -1 + height / 2 + margin; // -1 is a bottom of the screen
        float z = -1;
        watermark.setPosition(x, y, z);
        recorder.setupWatermark(watermarkHandle, width, height, margin);
    }

    /**
     * Prepares the offscreen framebuffer for recording for GL versions < 3.0
     */
    private void prepareRecordFBO(int width, int height) {
        GlUtil.checkGlError("prepareRecordFBO start");

        int[] values = new int[1];

        // Create framebuffer object and bind it.
        glGenFramebuffers(1, values, 0);
        GlUtil.checkGlError("glGenFramebuffers");
        recordFBOHandle = values[0];    // expected > 0
        glBindFramebuffer(GL_FRAMEBUFFER, recordFBOHandle);
        GlUtil.checkGlError("glBindFramebuffer " + recordFBOHandle);

        // ============ color ============

        // Create a texture object and bind it.  This will be the color buffer.
        glGenTextures(1, values, 0);
        GlUtil.checkGlError("glGenTextures");
        offscreenRecordTextureHandle = values[0];   // expected > 0
        glBindTexture(GL_TEXTURE_2D, offscreenRecordTextureHandle);
        GlUtil.checkGlError("glBindTexture " + offscreenRecordTextureHandle);
        // Create texture storage.
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, null);
        // Set parameters.  We're probably using non-power-of-two dimensions, so
        // some values may not be available for use.
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        GlUtil.checkGlError("glTexParameter");
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, offscreenRecordTextureHandle, 0);
        GlUtil.checkGlError("glFramebufferTexture2D");

        // ============ stencil ============

        // Create a stencil buffer and bind it.
        glGenRenderbuffers(1, values, 0);
        GlUtil.checkGlError("glGenRenderbuffers offscreen FBO");
        recordStencilBufferHandle = values[0];    // expected > 0
        glBindRenderbuffer(GL_RENDERBUFFER, recordStencilBufferHandle);
        GlUtil.checkGlError("glBindRenderbuffer offscreen FBO " + recordStencilBufferHandle);
        // Allocate storage for the stencil buffer.
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8_OES, width, height);
        GlUtil.checkGlError("glRenderbufferStorage offscreen FBO");
        // Attach the stencil buffer to the framebuffer object.
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, recordStencilBufferHandle);
        GlUtil.checkGlError("glFramebufferRenderbuffer offscreen FBO");

        // See if GLES is happy with all this.
        int status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE) {
            throw new RuntimeException("Framebuffer not complete, status=" + status);
        }

        // Switch back to the default framebuffer.
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        GlUtil.checkGlError("prepareRecordFBO done");
    }

    /**
     * Prepares the off-screen framebuffer for taking picture from camera.
     */
    private void prepareOffscreenFBO(int width, int height) {
        GlUtil.checkGlError("prepareOffscreenFBO start");

        int[] values = new int[1];
        // Create framebuffer object and bind it.
        glGenFramebuffers(1, values, 0);
        GlUtil.checkGlError("glGenFramebuffers offscreen FBO");
        offscreenFBOHandle = values[0];    // expected > 0
        glBindFramebuffer(GL_FRAMEBUFFER, offscreenFBOHandle);
        GlUtil.checkGlError("glBindFramebuffer offscreen FBO " + offscreenFBOHandle);

        // ============ color ============

        // Create a color buffer and bind it.
        glGenRenderbuffers(1, values, 0);
        GlUtil.checkGlError("glGenRenderbuffers offscreen FBO");
        offscreenColorBufferHandle = values[0];    // expected > 0
        glBindRenderbuffer(GL_RENDERBUFFER, offscreenColorBufferHandle);
        GlUtil.checkGlError("glBindRenderbuffer offscreen FBO " + offscreenColorBufferHandle);
        // Allocate storage for the color buffer.
        glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB565, width, height);
        GlUtil.checkGlError("glRenderbufferStorage offscreen FBO");
        // Attach the color buffer to the framebuffer object.
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, offscreenColorBufferHandle);
        GlUtil.checkGlError("glFramebufferRenderbuffer offscreen FBO");

        // ============ stencil ============

        // Create a stencil buffer and bind it.
        glGenRenderbuffers(1, values, 0);
        GlUtil.checkGlError("glGenRenderbuffers offscreen FBO");
        offscreenStencilBufferHandle = values[0];    // expected > 0
        glBindRenderbuffer(GL_RENDERBUFFER, offscreenStencilBufferHandle);
        GlUtil.checkGlError("glBindRenderbuffer offscreen FBO " + offscreenStencilBufferHandle);
        // Allocate storage for the stencil buffer.
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8_OES, width, height);
        GlUtil.checkGlError("glRenderbufferStorage offscreen FBO");
        // Attach the stencil buffer to the framebuffer object.
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, offscreenStencilBufferHandle);
        GlUtil.checkGlError("glFramebufferRenderbuffer offscreen FBO");

        // See if GLES is happy with all this.
        int status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE) {
            throw new RuntimeException("Offscreen FBO not complete, status=" + status);
        }

        // Switch back to the default framebuffer.
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        GlUtil.checkGlError("prepareOffscreenFBO done");
    }

    /**
     * Advance state and draw frame
     */
    void doFrame(long timeStampNanos) {
        if (scene == null) {
            Log.w(TAG, "No scene attached");
            return;
        }
        boolean cameraMode = config.getImageUri() == null;
        boolean swapResult;

        // can happen during camera switch or switch between camera and Choreographer
        if (prevTimeNanos > timeStampNanos) {
            prevTimeNanos = timeStampNanos;
        }
        // Compute delta from previous frame.
        long deltaNanos;
        if (prevTimeNanos == 0 || timeStampNanos == 0) {
            deltaNanos = 0;
        } else {
            deltaNanos = timeStampNanos - prevTimeNanos;

            final long ONE_SECOND_NANOS = 1_000_000_000L;
            if (deltaNanos > ONE_SECOND_NANOS) {
                // A gap this big should only happen if something paused us.  We can
                // either cap the delta at one second, or just pretend like this is
                // the first frame and not advance at all.
                deltaNanos = 0;
            }
        }
        if (!cameraMode) {
            if (!canRender(timeStampNanos, prevTimeNanos)) {
                return;
            }
        }
        prevTimeNanos = timeStampNanos;
        boolean recordingEnabled = recorder != null && recorder.isRecordingEnabled();
        if (!recordingEnabled) {
            // Render the scene, swap back to front.
            scene.render(deltaNanos, cameraTexture);
            swapResult = windowSurface.swapBuffers();
        } else {
            // todo glBlitFrameBuffer fails for some devices, using Recorder2 for now
//            if (eglCore.getGlVersion() >= 3) {
            if (false) {
                // render scene
                scene.render(deltaNanos, cameraTexture);
                // use blit program to draw watermark since we don't have access to scene handles
                glUseProgram(programHandle);
                // blit to encoder
                recorder.record(windowSurface, timeStampNanos, uProjectionMatrixLoc, aPositionHandle, aTexCoordHandle);
                // Now swap the display buffer.
                windowSurface.makeCurrent();
                swapResult = windowSurface.swapBuffers();
            } else {
                // Render offscreen.
                glBindFramebuffer(GL_FRAMEBUFFER, recordFBOHandle);
                scene.render(deltaNanos, cameraTexture);
                glBindFramebuffer(GL_FRAMEBUFFER, 0);

                // Set the texture.
                glUseProgram(programHandle);
                glBindTexture(GL_TEXTURE_2D, offscreenRecordTextureHandle);

                // use whole offscreen texture for both display and blit to recorder
                glVertexAttribPointer(aPositionHandle, 2, GL_FLOAT, false, 0, FULL_SCREEN_BUF);
                glVertexAttribPointer(aTexCoordHandle, 2, GL_FLOAT, false, 0, FULL_SCREEN_TEX_BUF);
                glUniformMatrix4fv(uProjectionMatrixLoc, 1, false, GlUtil.IDENTITY_MATRIX, 0);

                // Blit to display.
                glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                swapResult = windowSurface.swapBuffers();

                // blit to encoder
                recorder.record(windowSurface, timeStampNanos, uProjectionMatrixLoc, aPositionHandle, aTexCoordHandle);

                // Restore previous values.
                glViewport(0, 0, windowSurface.getWidth(), windowSurface.getHeight());
                windowSurface.makeCurrent();
            }
        }
        if (!swapResult) {
            // This can happen if the Activity stops without waiting for us to halt.
            Timber.e("swapBuffers failed, killing renderer thread");
            shutdown();
            return;
        }
        updateFps(System.nanoTime());
    }

    private boolean canRender(long now, long last) {
        return now - last >= FPSThrottlePresentationInterval;
    }

    /**
     * Update the FPS counter.
     *
     * @param timeStampNanos timestamp in nanoseconds
     */
    private void updateFps(long timeStampNanos) {
        if (!BuildConfig.DEBUG) {
            return;
        }
        // Ideally we'd generate something approximate quickly to make the UI look
        // reasonable, then ease into longer sampling periods.
        final int updateFrequency = 16; // update every N frames
        final long ONE_TRILLION = 1_000_000_000_000L;
        if (fpsCountStartNanos == 0) {
            fpsCountStartNanos = timeStampNanos;
            fpsCountFrame = 0;
        } else {
            fpsCountFrame++;
            if (fpsCountFrame == updateFrequency) {
                // compute thousands of frames per second
                long elapsed = timeStampNanos - fpsCountStartNanos;
                int fps = Math.round(updateFrequency * ONE_TRILLION / elapsed / 1000.0f);
                // reset
                fpsCountStartNanos = timeStampNanos;
                fpsCountFrame = 0;

                activityHandler.sendFpsUpdate(fps);
            }
        }
    }

    /**
     * Releases most of the GL resources we currently hold.
     * <p>
     * Does not release EglCore.
     */
    private void release() {
        if (VERBOSE) Log.d(TAG, "release");
        GlUtil.checkGlError("release start");

        if (recorder != null) {
            recorder.setRecordingEnabled(false);
        }

        if (windowSurface != null) {
            windowSurface.release();
            windowSurface = null;
        }

        if (scene != null) {
            scene.release();
        }

        glDeleteProgram(programHandle);
        GlUtil.checkGlError("delete program");
        programHandle = -1;

        releaseCameraTexture();
        deleteImage();

        int[] values = new int[1];
        // release offscreen record FBO
        if (offscreenRecordTextureHandle > 0) {
            values[0] = offscreenRecordTextureHandle;
            glDeleteTextures(1, values, 0);
            offscreenRecordTextureHandle = -1;
        }
        if (recordFBOHandle > 0) {
            values[0] = recordFBOHandle;
            glDeleteFramebuffers(1, values, 0);
            GlUtil.checkGlError("Delete record (OpenGL 2.0) color buffer");
            recordFBOHandle = -1;
        }
        if (recordStencilBufferHandle > 0) {
            values[0] = recordStencilBufferHandle;
            glDeleteRenderbuffers(1, values, 0);
            GlUtil.checkGlError("Delete record (OpenGL 2.0) stencil buffer");
            recordStencilBufferHandle = -1;
        }

        // release offscreen FBO
        if (offscreenFBOHandle > 0) {
            values[0] = offscreenFBOHandle;
            glDeleteFramebuffers(1, values, 0);
            offscreenFBOHandle = -1;
        }
        if (offscreenColorBufferHandle > 0) {
            values[0] = offscreenColorBufferHandle;
            glDeleteRenderbuffers(1, values, 0);
            GlUtil.checkGlError("Delete offscreen color buffer");
            offscreenColorBufferHandle = -1;
        }
        if (offscreenStencilBufferHandle > 0) {
            values[0] = offscreenStencilBufferHandle;
            glDeleteRenderbuffers(1, values, 0);
            GlUtil.checkGlError("Delete offscreen stencil buffer");
            offscreenStencilBufferHandle = -1;
        }

        GlUtil.checkGlError("release finished");

        eglCore.makeNothingCurrent();
    }

    void setRecordEnabled(boolean recordEnabled) {
        recorder.setConfig(config.getRecordConfig());
        recorder.setRecordingEnabled(recordEnabled);
    }

    void captureScreenshot() {
        String outputPath = config.getScreenshotConfig().getOutputPath();
        String unmarkedOutputPath = config.getScreenshotConfig().getUnmarkedOutputPath();
        if (outputPath == null || unmarkedOutputPath == null) {
            Timber.e("Cannot take picture. Output path is null");
            return;
        }
        try {
            glBindFramebuffer(GL_FRAMEBUFFER, offscreenFBOHandle);
            // clear current input
            glClear(GL_COLOR_BUFFER_BIT);
            // render scene
            scene.render(0, cameraTexture);
            // use blit program (since we don't have access to scene program handles)
            // to draw watermark
            glUseProgram(programHandle);
            // draw watermark
            watermark.draw(uProjectionMatrixLoc, GlUtil.IDENTITY_MATRIX, aPositionHandle, aTexCoordHandle, -1);
            // save frame to file
            windowSurface.saveFrame(new File(outputPath));

            glClear(GL_COLOR_BUFFER_BIT);
            scene.render(0, cameraTexture);
            windowSurface.saveFrame(new File(unmarkedOutputPath));
        } catch (IOException e) {
            Timber.e(e, "Couldn't take picture. Message: " + e.getMessage());
            e.printStackTrace();
            config.getScreenshotConfig().setThrowable(e);
        } finally {
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            activityHandler.sendScreenshotCaptured();
        }
        GlUtil.checkGlError("Screenshot capture");
    }

    void objectsStateChanged() {
        boolean enabled = config.isObjectsEnabled();
        if (scene != null) scene.onObjectsStateChanged(enabled);
    }

    void backgroundEffectsStateChanged() {
        boolean enabled = config.isBackgroundEffectsEnabled();
        if (scene != null) scene.onBackgroundEffectsStateChanged(enabled);
    }
}