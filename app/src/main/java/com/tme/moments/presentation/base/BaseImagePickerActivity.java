package com.tme.moments.presentation.base;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.tme.moments.R;
import com.tme.moments.presentation.bitmap.FileUtils;
import com.tme.moments.presentation.utils.PermissionUtils;

import java.io.File;
import java.io.IOException;

import timber.log.Timber;

/**
 * Activity contains pick image logic.
 */
public abstract class BaseImagePickerActivity extends BaseActivity {

    private static final String TAG = BaseImagePickerActivity.class.getSimpleName();

    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static String[] PERMISSIONS_CAMERA = {
            Manifest.permission.CAMERA
    };

    private static final int REQUEST_STORAGE_PERMISSION = 111;
    private static final int REQUEST_CAMERA_PERMISSION = 112;

    private static final int REQUEST_TAKE_PHOTO = 113;
    private static final int REQUEST_GALLERY = 114;

    protected abstract void onImageUriReady(Uri imageUri);
    protected abstract void onErrorGettingImage();

    public void pickImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !storagePermissionGranted()) {
            requestStoragePermission();
            return;
        }
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(intent, getString(R.string.choose_image)), REQUEST_GALLERY);
        }
    }

    private void takePhoto() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !cameraPermissionGranted()) {
            requestCameraPermission();
            return;

        }
        Intent intent = new Intent();
        File f = createImageForTakingPhoto(this);
        if (f != null) {
            intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, f.getPath());
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, REQUEST_TAKE_PHOTO);
            } else {
                shortToast(R.string.error_unknown);
            }
        } else {
            shortToast(R.string.error_unknown);
        }
    }


    public boolean storagePermissionGranted() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public boolean cameraPermissionGranted() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.storage_permission_explanation)
                    .setPositiveButton(android.R.string.ok, (dialog, which) ->
                            ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, REQUEST_STORAGE_PERMISSION))
                    .show();
        } else {
            ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, REQUEST_STORAGE_PERMISSION);
        }
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.camera_permission_explanation)
                    .setPositiveButton(android.R.string.ok, (dialog, which) ->
                            ActivityCompat.requestPermissions(this, PERMISSIONS_CAMERA, REQUEST_CAMERA_PERMISSION))
                    .show();
        } else {
            ActivityCompat.requestPermissions(this, PERMISSIONS_CAMERA, REQUEST_CAMERA_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_PERMISSION:
                if (!PermissionUtils.verifyPermissions(grantResults)) {
                    shortToast(R.string.storage_permission_not_granted);
                } else {
                    pickImage();
                }
                break;
            case REQUEST_CAMERA_PERMISSION:
                if (!PermissionUtils.verifyPermissions(grantResults)) {
                    shortToast(R.string.camera_permission_not_granted);
                } else {
                    takePhoto();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_GALLERY:
            case REQUEST_TAKE_PHOTO:
                Uri imageUri = data == null ? null : data.getData();
                boolean imageIsValid;
                if (imageUri == null) {
                    imageIsValid = false;
                } else {
                    String path = FileUtils.getPath(this, imageUri);
                    if (path == null) {
                        imageIsValid = false;
                    } else {
                        File file = new File(path);
                        imageIsValid = file.exists() && file.canRead() && file.length() > 0;
                        if (imageIsValid) {
                            imageUri = Uri.fromFile(file);
                        }
                    }
                }
                if (resultCode == RESULT_OK && imageIsValid) {
                    onImageUriReady(imageUri);
                } else if (resultCode != RESULT_CANCELED) {
                    longToast(R.string.error_getting_image);
                    onErrorGettingImage();
                }
                break;
        }
    }

    /**
     * Creates file for taking photo image.
     *
     * @return created file or null if something gone wrong
     */
    protected static File createImageForTakingPhoto(Context context) {
        try {
            File f = new File(context.getExternalCacheDir(), "TEMP_PHOTO_IMG.jpg");
            if (f.exists()) {
                if (!f.delete()) {
                    Log.w(TAG, "Couldn't delete existing file for photo");
                } else {
                    Log.d(TAG, "Deleted existing file for photo: " + f.getPath());
                }
            }
            try {
                if (!f.createNewFile()) {
                    Log.w(TAG, "Couldn't create file for photo");
                } else {
                    return f;
                }
            } catch (IOException e) {
                Timber.e(e, null);
                e.printStackTrace();
            }
        } catch (Exception e) {
            Timber.e(e, null);
            e.printStackTrace();
        }
        return null;
    }
}
