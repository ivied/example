package com.tme.moments.presentation.scene.f3.happy.birthday;

import android.graphics.PointF;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseBlurredScene;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class HappyBirthday extends BaseBlurredScene {

    private TextureInfo atlasInfo;
    private TextureInfo archwayAtlasInfo;
    private DecalBatch cloudsBatch;
    private DecalBatch objectsBatch;
    private DecalBatch shakingObjectsBatch;
    private Cloud cloud1;
    private List<Balloon> balloons;
    private List<FallingObject> fallingObjects;
    private List<ShakingObject> shakingObjects;
    private Archway archway;
    private List<Pair<TextureRegion, PointF>> balloonRegions;

    @Override protected int getStencilResId() {
        return R.drawable.f3_stencil;
    }

    @Override protected int getOverlayResId() {
        return R.drawable.f3_overlay;
    }

    @Override protected float getCameraPositionX() {
        return -0.0037f;
    }

    @Override protected float getCameraPositionY() {
        return -0.1759f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.8657f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.5861f;
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        super.prepareObjects(surfaceWidth, surfaceHeight);
        cloudsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        objectsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);

        atlasInfo = openGLLoader.loadTexture(R.drawable.f3_atlas);
        archwayAtlasInfo = openGLLoader.loadTexture(R.drawable.f3_archway_atlas);

        prepareHappyBirthdayLine(archwayAtlasInfo);
        prepareClouds(atlasInfo);
        prepareShakingObjects(atlasInfo);
        shakingObjectsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        shakingObjectsBatch.initialize(shakingObjects.size());
        shakingObjectsBatch.addAll(shakingObjects);

        cloudsBatch.add(cloud1);
        cloudsBatch.initialize(1);

        buildBalloons(atlasInfo, 3);
        buildFallingObjects(atlasInfo);

        objectsBatch.initialize(balloons.size() + fallingObjects.size());
        objectsBatch.addAll(balloons);
        objectsBatch.addAll(fallingObjects);
    }

    @Override protected void onBlurredImageDrawn(long deltaNanos) {
        if (config.isObjectsEnabled()) {
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            cloudsBatch.render(uProjectionMatrixLoc, projectionMatrix);
            cloud1.step(deltaNanos);
        }
    }

    @Override protected void onOverlayDrawn(long deltaNanos) {
        boolean objectsEnabled = config.isObjectsEnabled();
        glUseProgram(mainProgramHandle);
        glBindTexture(GL_TEXTURE_2D, archwayAtlasInfo.textureHandle);
        archway.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
        if (objectsEnabled) {
            archway.step(deltaNanos);
        }
        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        shakingObjectsBatch.render(uProjectionMatrixLoc, projectionMatrix);
        if (objectsEnabled) {
            for (ShakingObject obj : shakingObjects) {
                obj.step(deltaNanos);
            }
        }
        if (objectsEnabled) {
            objectsBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (FallingObject fallingObject : fallingObjects) fallingObject.step(deltaNanos);
            boolean allBalloonsFinished = true;
            for (Balloon balloon : balloons) {
                balloon.step(deltaNanos);
                if (!balloon.isFinished()) {
                    allBalloonsFinished = false;
                }
            }
            if (allBalloonsFinished) {
                int startIndex = random(0, balloonRegions.size());
                long startDelay = random(2000, 3000);
                for (int i = 0; i < balloons.size(); i++) {
                    Balloon balloon = balloons.get(i);
                    Pair<TextureRegion, PointF> info = balloonRegions.get(startIndex);
                    TextureRegion region = info.first;
                    float width = info.second.x;
                    float height = info.second.y;
                    balloon.init(region, width, height);
                    if (startIndex == balloonRegions.size() - 1) {
                        startIndex = 0;
                    } else {
                        startIndex++;
                    }
                    long interval = random(1000, 2200);
                    balloon.setStartDelay(startDelay += interval);
                }
            }
        }
    }

    @Override public void release() {
        super.release();
        int[] values = new int[2];
        int texturesToDeleteSize = 0;
        if (atlasInfo != null) {
            values[texturesToDeleteSize] = atlasInfo.textureHandle;
            texturesToDeleteSize++;
        }
        if (archwayAtlasInfo != null) {
            values[texturesToDeleteSize] = archwayAtlasInfo.textureHandle;
            texturesToDeleteSize++;
        }
        if (texturesToDeleteSize != 0) {
            glDeleteTextures(texturesToDeleteSize, values, 0);
            GlUtil.checkGlError("Delete moving object texture");
        }
        if (cloudsBatch != null) {
            cloudsBatch.dispose();
            cloudsBatch = null;
        }
    }

    private void prepareShakingObjects(TextureInfo info) {
        shakingObjects = new ArrayList<>();

        // box
        TextureRegion region = buildRegion(info, 320, 369, 165, 176);
        float width = 0.2062f;
        float height = 0.22f;
        float moveSpeed = 0.02f;
        float rotationSpeed = -35;
        float maxIncline = 0.01f;
        float x = 0.75f;
        float y = -0.8f;
        ShakingObject box = new ShakingObject(region, width, height, x, y, moveSpeed, rotationSpeed, maxIncline);
        shakingObjects.add(box);

        // yellow balloon
        region = buildRegion(info, 160, 369, 160, 215);
        width = 0.2f;
        height = 0.2687f;
        moveSpeed = 0.015f;
        rotationSpeed = -25;
        maxIncline = 0.01f;
        x = 0.47f;
        y = -0.73f;
        ShakingObject yellowBalloon = new ShakingObject(region, width, height, x, y, moveSpeed, rotationSpeed, maxIncline);
        yellowBalloon.setScale(0.9f);
        shakingObjects.add(yellowBalloon);

        // pink balloon
        region = buildRegion(info, 1664, 0, 160, 217);
        width = 0.2f;
        height = 0.2712f;
        moveSpeed = 0.01f;
        rotationSpeed = -15;
        maxIncline = 0.005f;
        x = -0.56f;
        y = 0.4f;
        ShakingObject pinkBalloon = new ShakingObject(region, width, height, x, y, moveSpeed, rotationSpeed, maxIncline);
        pinkBalloon.setScale(0.9f);
        shakingObjects.add(pinkBalloon);
    }

    private void prepareHappyBirthdayLine(TextureInfo info) {
        int inSampleSize = info.inSampleSize;
        List<AnimatedSprite.FrameInfo> frames = new ArrayList<>();
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 698 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 217 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 698 / inSampleSize, 217 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 434 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 698 / inSampleSize, 434 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 651 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 698 / inSampleSize, 651 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 868 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 698 / inSampleSize, 868 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 1085 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 698 / inSampleSize, 1085 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 1302 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 698 / inSampleSize, 1302 / inSampleSize));
        float width = 1.68f;
        float height = 0.4612f;
        TextureRegion region = buildRegion(info, 0, 0, 698, 217); // region for first frame
        archway = new Archway(region, frames, width, height);
    }

    private void prepareClouds(TextureInfo info) {
        TextureRegion region = buildRegion(info, 485, 369, 300, 185);
        float width = 0.375f;
        float height = 0.2312f;
        cloud1 = new Cloud(region, width, height);
    }

    private void buildBalloons(TextureInfo info, int count) {
        balloonRegions = new ArrayList<>();
        balloonRegions.add(new Pair<>(buildRegion(info, 1344, 0, 160, 207), new PointF(0.2f, 0.2587f)));
        balloonRegions.add(new Pair<>(buildRegion(info, 1504, 0, 160, 200), new PointF(0.2f, 0.25f)));
        balloonRegions.add(new Pair<>(buildRegion(info, 1664, 0, 160, 217), new PointF(0.2f, 0.2712f)));
//        balloonRegions.add(new Pair<>(buildRegion(info, 1824, 0, 160, 220), new PointF(0.2f, 0.275f)));
        balloonRegions.add(new Pair<>(buildRegion(info, 0, 369, 160, 220), new PointF(0.2f, 0.275f)));
        balloonRegions.add(new Pair<>(buildRegion(info, 160, 369, 160, 215), new PointF(0.2f, 0.2687f)));
        balloons = new ArrayList<>();
        for (int i = 0; i < count; i++) balloons.add(new Balloon());
    }

    private void buildFallingObjects(TextureInfo info) {
        // stars
        List<Pair<TextureRegion, PointF>> starRegions = new ArrayList<>(6);
        starRegions.add(new Pair<>(buildRegion(info, 1877, 369, 100, 97), new PointF(0.125f, 0.1212f)));
        starRegions.add(new Pair<>(buildRegion(info, 0, 597, 100, 97), new PointF(0.125f, 0.1212f)));
        starRegions.add(new Pair<>(buildRegion(info, 100, 597, 100, 97), new PointF(0.125f, 0.1212f)));
        starRegions.add(new Pair<>(buildRegion(info, 200, 597, 100, 97), new PointF(0.125f, 0.1212f)));
        starRegions.add(new Pair<>(buildRegion(info, 300, 597, 100, 97), new PointF(0.125f, 0.1212f)));
        starRegions.add(new Pair<>(buildRegion(info, 400, 597, 100, 97), new PointF(0.125f, 0.1212f)));
        starRegions.add(new Pair<>(buildRegion(info, 500, 597, 100, 97), new PointF(0.125f, 0.1212f)));

        int count = 50;
        int count3d = 13;
        int minRotationSpeed = -140;
        int maxRotationSpeed = 140;
        fallingObjects = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Pair<TextureRegion, PointF> regionInfo = starRegions.get(random(0, starRegions.size()));
            TextureRegion petalRegion = regionInfo.first;
            float width = regionInfo.second.x;
            float height = regionInfo.second.y;
            FallingObject star = new FallingObject(petalRegion, width, height, i >= count - count3d);
            star.setRotationSpeed(minRotationSpeed, maxRotationSpeed);
            fallingObjects.add(star);
        }

        // flags
        List<Pair<TextureRegion, PointF>> flagRegions = new ArrayList<>(14);
        flagRegions.add(new Pair<>(buildRegion(info, 785, 369, 57, 50), new PointF(0.0712f, 0.0625f)));
        flagRegions.add(new Pair<>(buildRegion(info, 842, 369, 78, 50), new PointF(0.0975f, 0.0625f)));
        flagRegions.add(new Pair<>(buildRegion(info, 920, 369, 50, 88), new PointF(0.0625f, 0.11f)));
        flagRegions.add(new Pair<>(buildRegion(info, 970, 369, 83, 50), new PointF(0.1037f, 0.0625f)));
        flagRegions.add(new Pair<>(buildRegion(info, 1053, 369, 50, 53), new PointF(0.0625f, 0.0662f)));
        flagRegions.add(new Pair<>(buildRegion(info, 1103, 369, 66, 50), new PointF(0.0825f, 0.0625f)));
        flagRegions.add(new Pair<>(buildRegion(info, 1169, 369, 50, 114), new PointF(0.0625f, 0.1425f)));
        flagRegions.add(new Pair<>(buildRegion(info, 1219, 369, 50, 114), new PointF(0.0625f, 0.1425f)));
        flagRegions.add(new Pair<>(buildRegion(info, 1269, 369, 50, 114), new PointF(0.0625f, 0.1425f)));
        flagRegions.add(new Pair<>(buildRegion(info, 1319, 369, 50, 96), new PointF(0.0625f, 0.12f)));
        flagRegions.add(new Pair<>(buildRegion(info, 1369, 369, 50, 96), new PointF(0.0625f, 0.12f)));
        flagRegions.add(new Pair<>(buildRegion(info, 1419, 369, 50, 96), new PointF(0.0625f, 0.12f)));
        flagRegions.add(new Pair<>(buildRegion(info, 1469, 369, 50, 97), new PointF(0.0625f, 0.1212f)));
        flagRegions.add(new Pair<>(buildRegion(info, 1519, 369, 50, 97), new PointF(0.0625f, 0.1212f)));

        count = 20;
        count3d = 4;
        minRotationSpeed = -270;
        maxRotationSpeed = 270;
        for (int i = 0; i < count; i++) {
            Pair<TextureRegion, PointF> regionInfo = flagRegions.get(random(0, flagRegions.size()));
            TextureRegion region = regionInfo.first;
            float width = regionInfo.second.x;
            float height = regionInfo.second.y;
            FallingObject flag = new FallingObject(region, width, height, i >= count - count3d);
            flag.setRotationSpeed(minRotationSpeed, maxRotationSpeed);
            fallingObjects.add(flag);
        }

        // ornaments
        List<Pair<TextureRegion, PointF>> ornamentRegions = new ArrayList<>(7);
        ornamentRegions.add(new Pair<>(buildRegion(info, 1569, 369, 44, 228), new PointF(0.055f, 0.285f)));
        ornamentRegions.add(new Pair<>(buildRegion(info, 1613, 369, 44, 228), new PointF(0.055f, 0.285f)));
        ornamentRegions.add(new Pair<>(buildRegion(info, 1657, 369, 44, 152), new PointF(0.055f, 0.19f)));
        ornamentRegions.add(new Pair<>(buildRegion(info, 1701, 369, 44, 152), new PointF(0.055f, 0.19f)));
        ornamentRegions.add(new Pair<>(buildRegion(info, 1745, 369, 44, 152), new PointF(0.055f, 0.19f)));
        ornamentRegions.add(new Pair<>(buildRegion(info, 1789, 369, 44, 152), new PointF(0.055f, 0.19f)));
        ornamentRegions.add(new Pair<>(buildRegion(info, 1833, 369, 44, 152), new PointF(0.055f, 0.19f)));

        count = 5;
        count3d = 1;
        minRotationSpeed = -480;
        maxRotationSpeed = 480;
        for (int i = 0; i < count; i++) {
            Pair<TextureRegion, PointF> regionInfo = ornamentRegions.get(random(0, ornamentRegions.size()));
            TextureRegion petalRegion = regionInfo.first;
            float width = regionInfo.second.x;
            float height = regionInfo.second.y;
            FallingObject ornament = new FallingObject(petalRegion, width, height, i >= count - count3d);
            ornament.setRotationSpeed(minRotationSpeed, maxRotationSpeed);
            fallingObjects.add(ornament);
        }
    }
}
