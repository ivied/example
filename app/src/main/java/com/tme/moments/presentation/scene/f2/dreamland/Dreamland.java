package com.tme.moments.presentation.scene.f2.dreamland;

import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class Dreamland extends BaseScene {

    private TextureInfo atlasInfo;
    private Lamp leftLamp;
    private Lamp rightLamp;
    private DecalBatch petalsBatch;
    private DecalBatch lampsBatch;
    private OrangeButterfly orangeButterfly;
    private BlueButterfly blueButterfly;
    private List<Petal> petals;

    @Override protected float getCameraPositionX() {
        return 0.0f;
    }

    @Override protected float getCameraPositionY() {
        return 0.031f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.6277f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.6342f;
    }

    @Override public boolean hasBackgroundEffects() {
        return true;
    }

    @Override protected TextureRegion getFrameRegion() {
        TextureInfo frameInfo = openGLLoader.loadTexture(R.drawable.f2);
        return new TextureRegion(frameInfo.textureHandle, frameInfo.imageWidth, frameInfo.imageHeight,
                0, 0, frameInfo.imageWidth, frameInfo.imageHeight);
    }

    @Override protected void prepareBackgroundEffects(int surfaceWidth, int surfaceHeight) {
        preparePetals(atlasInfo);
        petalsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        petalsBatch.initialize(petals.size());
        petalsBatch.addAll(petals);
    }

    @Override public void onObjectsStateChanged(boolean enabled) {
        super.onObjectsStateChanged(enabled);
        leftLamp.setEnabled(enabled);
        rightLamp.setEnabled(enabled);
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        atlasInfo = openGLLoader.loadTexture(R.drawable.f2_atlas);

        prepareLamps(atlasInfo);
        prepareOrangeButterfly(atlasInfo);
        prepareBlueButterfly(atlasInfo);

        lampsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        lampsBatch.initialize(2);
        lampsBatch.add(leftLamp);
        lampsBatch.add(rightLamp);
    }

    @Override public void release() {
        super.release();
        if (atlasInfo != null) {
            int[] arr = new int[1];
            arr[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, arr, 0);
            GlUtil.checkGlError("Delete textures");
        }
        if (petals != null) {
            petalsBatch.dispose();
            petalsBatch = null;
        }
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        glClear(GL_COLOR_BUFFER_BIT);
        if (mode == Mode.CAMERA) {
            drawCameraInput(cameraTexture);
        }
        glUseProgram(mainProgramHandle);
        if (mode == Mode.IMAGE) {
            drawImage();
        }
        if (config.isObjectsEnabled()) {
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            if (!orangeButterfly.drawOverFrame()) {
                orangeButterfly.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
                orangeButterfly.step(deltaNanos);
            }
            if (!blueButterfly.drawOverFrame()) {
                blueButterfly.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
                blueButterfly.step(deltaNanos);
            }
        }
        drawFrame();
        glUseProgram(mainProgramHandle);
        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        lampsBatch.render(uProjectionMatrixLoc, projectionMatrix);
        leftLamp.step(deltaNanos);
        rightLamp.step(deltaNanos);
        if (config.isObjectsEnabled()) {
            if (leftLamp.isFinished() && rightLamp.isFinished()) {
                setLampsWorkingTimes();
            }
            if (orangeButterfly.drawOverFrame()) {
                orangeButterfly.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
                orangeButterfly.step(deltaNanos);
            }
            if (blueButterfly.drawOverFrame()) {
                blueButterfly.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
                blueButterfly.step(deltaNanos);
            }
        }

        if (config.isBackgroundEffectsEnabled()) {
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            petalsBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Petal petal : petals) {
                petal.step(deltaNanos);
            }
        }
    }

    private void preparePetals(TextureInfo info) {
        List<Pair<TextureRegion, PointF>> petalRegions = new ArrayList<>(6);
        petalRegions.add(new Pair<>(buildRegion(info, 500, 1062, 50, 71), new PointF(0.0625f, 0.0887f)));
        petalRegions.add(new Pair<>(buildRegion(info, 550, 1062, 50, 63), new PointF(0.0625f, 0.0787f)));
        petalRegions.add(new Pair<>(buildRegion(info, 600, 1062, 50, 69), new PointF(0.0625f, 0.0862f)));
        petalRegions.add(new Pair<>(buildRegion(info, 650, 1062, 50, 63), new PointF(0.0625f, 0.0787f)));
        petalRegions.add(new Pair<>(buildRegion(info, 700, 1062, 50, 67), new PointF(0.0625f, 0.0887f)));

        int count = 120;
        int count3d = 80;
        petals = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            Pair<TextureRegion, PointF> regionInfo = petalRegions.get(random(0, petalRegions.size()));
            TextureRegion petalRegion = regionInfo.first;
            float width = regionInfo.second.x;
            float height = regionInfo.second.y;
            petals.add(new Petal(petalRegion, width, height, i < count3d));
        }
    }

    private void prepareLamps(TextureInfo info) {
        int frameWidth = 420;
        int frameHeight = 421;
        float width = 0.525f;
        float height = 0.5262f;
        int inSampleSize = atlasInfo.inSampleSize;
        AnimatedSprite.FrameInfo offFrame = new AnimatedSprite.FrameInfo(0, 0 / inSampleSize, 147 / inSampleSize);
        AnimatedSprite.FrameInfo onFrame = new AnimatedSprite.FrameInfo(1, 420 / inSampleSize, 147 / inSampleSize);
        TextureRegion region1 = buildRegion(info, onFrame.regionX, onFrame.regionY, frameWidth, frameHeight);
        TextureRegion region2 = buildRegion(info, onFrame.regionX, onFrame.regionY, frameWidth, frameHeight);
        leftLamp = new Lamp(region1, width, height, -0.8f, 1 - height / 2, onFrame, offFrame);
        rightLamp = new Lamp(region2, width, height, 0.77f, 1 - height / 2, onFrame, offFrame);
        leftLamp.setEnabled(config.isObjectsEnabled());
        rightLamp.setEnabled(config.isObjectsEnabled());
        setLampsWorkingTimes();
    }

    private void prepareBlueButterfly(TextureInfo info) {
        int inSampleSize = info.inSampleSize;
        float width = 0.1937f;
        float height = 0.1837f;
        List<AnimatedSprite.FrameInfo> frames = new ArrayList<>();
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 155 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 310 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 465 / inSampleSize, 0 / inSampleSize));
        TextureRegion region = buildRegion(info, 0, 0, 155, 147); // region for first frame
        blueButterfly = new BlueButterfly(region, frames, width, height);
    }

    private void prepareOrangeButterfly(TextureInfo info) {
        int inSampleSize = info.inSampleSize;
        float width = 0.2812f;  // real size reduced by 10%
        float height = 0.2778f; // real size reduced by 10%
        List<AnimatedSprite.FrameInfo> frames = new ArrayList<>();
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 568 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 250 / inSampleSize, 568 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 500 / inSampleSize, 568 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 750 / inSampleSize, 568 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 815 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 250 / inSampleSize, 815 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 500 / inSampleSize, 815 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 750 / inSampleSize, 815 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 1062 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 250 / inSampleSize, 1062 / inSampleSize));
        TextureRegion region = buildRegion(atlasInfo, 0, 568, 250, 247); // region for first frame
        orangeButterfly = new OrangeButterfly(region, frames, width, height);
    }

    private void setLampsWorkingTimes() {
        long offTime = random(3500, 6000);
        long onTime = random(5000, 8000);
        leftLamp.setOnTime(onTime);
        leftLamp.setOffTime(offTime);
        rightLamp.setOnTime(onTime);
        rightLamp.setOffTime(offTime);
    }
}
