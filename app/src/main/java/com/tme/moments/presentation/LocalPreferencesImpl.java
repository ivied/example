package com.tme.moments.presentation;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.tme.moments.recording.Quality;

import java.util.Set;

/**
 * @author zoopolitic.
 */
public class LocalPreferencesImpl implements LocalPreferences {

    private static final String QUALITY = "QUALITY";
    private static final String RECORD_DURATION = "RECORD_DURATION";
    private static final String IS_SOUND_ENABLED = "IS_SOUND_ENABLED";
    private static final String DARK_THEME = "DARK_THEME";
    private static final String SHOW_SAVING_CONTENT_INFO_DIALOG = "SHOW_SAVING_CONTENT_INFO_DIALOG";
    private static final String AD_AUDIO_COUNT = "AD_AUDIO_COUNT";

    private final Context context;

    public LocalPreferencesImpl(Application application) {
        this.context = application;
    }

    public SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void clearPrefs() {
        getPreferences().edit().clear().apply();
    }

    public int getInt(String preferenceKey, int preferenceDefaultValue) {
        return getPreferences().getInt(preferenceKey, preferenceDefaultValue);
    }

    public void putInt(String preferenceKey, int preferenceValue) {
        getPreferences().edit().putInt(preferenceKey, preferenceValue).apply();
    }

    public long getLong(String preferenceKey, long preferenceDefaultValue) {
        return getPreferences().getLong(preferenceKey, preferenceDefaultValue);
    }

    public void putLong(String preferenceKey, long preferenceValue) {
        getPreferences().edit().putLong(preferenceKey, preferenceValue).apply();
    }

    public float getFloat(String preferenceKey, float preferenceDefaultValue) {
        return getPreferences().getFloat(preferenceKey, preferenceDefaultValue);
    }

    public void putFloat(String preferenceKey, float preferenceValue) {
        getPreferences().edit().putFloat(preferenceKey, preferenceValue).apply();
    }

    public boolean getBoolean(String preferenceKey, boolean preferenceDefaultValue) {
        return getPreferences().getBoolean(preferenceKey, preferenceDefaultValue);
    }

    public void putBoolean(String preferenceKey, boolean preferenceValue) {
        getPreferences().edit().putBoolean(preferenceKey, preferenceValue).apply();
    }

    public String getString(String preferenceKey, String preferenceDefaultValue) {
        return getPreferences().getString(preferenceKey, preferenceDefaultValue);
    }

    public void putString(String preferenceKey, String preferenceValue) {
        getPreferences().edit().putString(preferenceKey, preferenceValue).apply();
    }

    public void putStringSet(String preferencesKey, Set<String> values) {
        getPreferences().edit().putStringSet(preferencesKey, values).apply();
    }

    public Set<String> getStringSet(String preferencesKey) {
        return getPreferences().getStringSet(preferencesKey, null);
    }

    //    ---------------------------------------------------------------------------------------------

    @Override public Quality getQuality() {
        return Quality.values()[getInt(QUALITY, Quality.MEDIUM.ordinal())]; // medium is default
    }

    @Override public void setQuality(Quality quality) {
        putInt(QUALITY, quality.ordinal());
    }

    @Override public int getRecordDuration() {
        return getInt(RECORD_DURATION, 30); // 30 seconds is default
    }

    @Override public void setRecordDuration(int duration) {
        putInt(RECORD_DURATION, duration);
    }

    @Override public boolean isSoundEnabled() {
        return getBoolean(IS_SOUND_ENABLED, true);
    }

    @Override public void setSoundEnabled(boolean enabled) {
        putBoolean(IS_SOUND_ENABLED, enabled);
    }

    @Override public void setDarkTheme(boolean darkTheme) {
        putBoolean(DARK_THEME, darkTheme);
    }

    @Override public boolean isDarkTheme() {
        return getBoolean(DARK_THEME, false); // default is white
    }

    @Override public void setShowSavingContentInfoDialog(boolean show) {
        putBoolean(SHOW_SAVING_CONTENT_INFO_DIALOG, show);
    }

    @Override public boolean isShowSavingContentInfoDialog() {
        return getBoolean(SHOW_SAVING_CONTENT_INFO_DIALOG, true);
    }

    @Override
    public void musicAdd() {
        putInt(AD_AUDIO_COUNT, getInt(AD_AUDIO_COUNT, 0) + 1);
    }

    @Override
    public boolean isShowMusicAdd() {


        int adCount = getInt(AD_AUDIO_COUNT, 0);
        return  (adCount + 1) % 3 == 0;
    }
}
