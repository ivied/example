package com.tme.moments.presentation;

/**
 * @author zoopolitic.
 */
public class TextureInfo {

    public final int textureHandle;
    public final int imageWidth;
    public final int imageHeight;
    public final int inSampleSize;

    public TextureInfo(int textureHandle, int imageWidth, int imageHeight, int inSampleSize) {
        this.textureHandle = textureHandle;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.inSampleSize = inSampleSize;
    }
}
