package com.tme.moments.presentation.scene.f9.girly.special;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseBlurredScene;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class GirlySpecial extends BaseBlurredScene {

    private final Object lock = new Object();

    private Kitty kitty;
    private Angel angel;
    private Text text;

    private Butterfly orangeButterfly;
    private Butterfly blueButterfly;
    private TextureInfo atlasInfo;
    private boolean blueButterflyActive;

    @Override protected float getCameraPositionX() {
        return -0.037f;
    }

    @Override protected float getCameraPositionY() {
        return -0.044f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.6379f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.7120f;
    }

    @Override protected int getStencilResId() {
        return R.drawable.f9_stencil;
    }

    @Override protected int getOverlayResId() {
        return R.drawable.f9_overlay;
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        super.prepareObjects(surfaceWidth, surfaceHeight);
        atlasInfo = openGLLoader.loadTexture(R.drawable.f9_atlas);
        prepareKitty(atlasInfo);
        prepareAngel(atlasInfo);
        prepareText(atlasInfo);
        prepareBlueButterfly(atlasInfo);
        prepareOrangeButterfly(atlasInfo);
        blueButterflyActive = random();
    }

    @Override protected void onOverlayDrawn(long deltaNanos) {
        boolean objectsEnabled = config.isObjectsEnabled();
        glUseProgram(mainProgramHandle);
        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        text.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
        if (objectsEnabled) {
            text.step(deltaNanos);
        }
        if (objectsEnabled) {
            kitty.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            kitty.step(deltaNanos);

            angel.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            angel.step(deltaNanos);

            if (blueButterfly.isStopped() && orangeButterfly.isStopped()) {
                synchronized (lock) {
                    if (blueButterflyActive) {
                        blueButterfly.setStopped(false);
                    } else {
                        orangeButterfly.setStopped(false);
                    }
                    blueButterflyActive = random();
                }
            }

            blueButterfly.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            blueButterfly.step(deltaNanos);

            orangeButterfly.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            orangeButterfly.step(deltaNanos);
        }
    }

    private void prepareKitty(TextureInfo info) {
        int inSampleSize = info.inSampleSize;
        List<AnimatedSprite.FrameInfo> frames = new ArrayList<>();
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1680 / inSampleSize, 725 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1120 / inSampleSize, 725 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 725 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1280 / inSampleSize, 478 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1000 / inSampleSize, 478 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1560 / inSampleSize, 478 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 560 / inSampleSize, 725 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 840 / inSampleSize, 725 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 280 / inSampleSize, 725 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1400 / inSampleSize, 725 / inSampleSize));
        TextureRegion region = buildRegion(info, 1680, 725, 280, 216); // region for the first frame
        float width = 0.35f;
        float height = 0.27f;
        kitty = new Kitty(region, frames, width, height);
    }

    private void prepareAngel(TextureInfo info) {
        int inSampleSize = info.inSampleSize;
        TextureRegion region = buildRegion(info, 500, 247, 326, 306);

        float width = 0.4075f;
        float height = 0.3825f;
        List<AnimatedSprite.FrameInfo> infoList = new ArrayList<>();
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 978 / inSampleSize, 941 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 652 / inSampleSize, 941 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 326 / inSampleSize, 941 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 0 / inSampleSize, 941 / inSampleSize));
        angel = new Angel(region, infoList, width, height);
    }

    private void prepareText(TextureInfo info) {
        TextureRegion region = buildRegion(info, 1304, 941, 483, 320);
        float width = 0.8944f;
        float height = 0.5925f;
        text = new Text(region, width, height);
        text.setPosition(0.52f, 0.68f, -1.0f);
    }

    private void prepareBlueButterfly(TextureInfo info) {
        int inSampleSize = info.inSampleSize;
        TextureRegion region = buildRegion(info, 0, 553, 250, 231);
        float width = 0.2812f;
        float height = 0.2598f;
        List<AnimatedSprite.FrameInfo> frames = new ArrayList<>();
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 750 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 250 / inSampleSize, 231 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 250 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1000 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1250 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1750 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 231 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1500 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 500 / inSampleSize, 0 / inSampleSize));
        blueButterfly = new Butterfly(region, frames, width, height);
    }

    private void prepareOrangeButterfly(TextureInfo info) {
        int inSampleSize = info.inSampleSize;
        TextureRegion region = buildRegion(info, 0, 0, 250, 247);
        float width = 0.2812f;
        float height = 0.2778f;
        List<AnimatedSprite.FrameInfo> frames = new ArrayList<>();
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1500 / inSampleSize, 231 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1250 / inSampleSize, 231 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1750 / inSampleSize, 231 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1000 / inSampleSize, 231 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 500 / inSampleSize, 231 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 750 / inSampleSize, 231 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 750 / inSampleSize, 478 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 250 / inSampleSize, 478 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 500 / inSampleSize, 478 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 478 / inSampleSize));
        orangeButterfly = new Butterfly(region, frames, width, height);
    }

    @Override public void release() {
        super.release();
        if (atlasInfo != null) {
            int[] values = new int[1];
            values[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, values, 0);
            GlUtil.checkGlError("Release " + getClass().getSimpleName());
        }
    }

}