package com.tme.moments.presentation.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.StringRes;

import com.tme.moments.presentation.utils.AndroidUtils;

import java.lang.ref.WeakReference;

/**
 * @author zoopolitic.
 */
public class ResourceProviderImpl implements ResourceProvider {

    private final WeakReference<Context> contextRef;

    public ResourceProviderImpl(Context context) {
        this.contextRef = new WeakReference<>(context);
    }

    @Override public String getString(@StringRes int stringResId) {
        return contextRef.get() == null ? null : contextRef.get().getString(stringResId);
    }

    @Override public String getString(int stringResId, Object... formatArgs) {
        return contextRef.get() == null ? null : contextRef.get().getString(stringResId, formatArgs);
    }

    @Override public int getColor(int colorResId) {
        return contextRef.get() == null ? 0 : contextRef.get().getResources().getColor(colorResId);
    }

    @Override public int dpToPx(int dp) {
        return contextRef.get() == null ? 0 : AndroidUtils.dpToPx(contextRef.get(), dp);
    }

    @Override public Bitmap decodeBitmap(int drawableResId) {
        Context context = contextRef.get();
        if (context == null) {
            return null;
        }
        return BitmapFactory.decodeResource(context.getResources(), drawableResId);
    }
}
