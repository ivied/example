package com.tme.moments.presentation.scene;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class Glow extends Decal {

    protected float p0x, p0y;
    protected float p1x, p1y;
    protected float p2x, p2y;

    protected float speed;
    protected long startDelayNanos;
    protected float t;

    protected float minSpeed = 0.025f;
    protected float maxSpeed = 0.075f;

    public Glow(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        init();
    }

    public void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }


    public void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float delta = speed * deltaNanos;
        t += delta;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        setX(x);
        setY(y);

        float fadeInThreshold = 0.25f;
        if (t <= fadeInThreshold) { // fade in
            float fraction = t / fadeInThreshold;
            setAlpha(fraction);
        }

        float fadeOutThreshold = 0.75f;
        if (t >= fadeOutThreshold) { // fade out
            float fraction = (t - fadeOutThreshold) / (1 - fadeOutThreshold);
            setAlpha(1 - fraction);
        }

        if (t >= 1) { // reset
            init();
        }
    }

    public void setSpeed(float minSpeed, float maxSpeed) {
        this.minSpeed = minSpeed;
        this.maxSpeed = maxSpeed;
        init();
    }

    protected void init() {
        float z = random(-2f, -1f);
        float deltaZ = -1 - z;
        p0x = random(-1 - deltaZ, 1 + deltaZ);
        p0y = random(-1 - deltaZ, 1 + deltaZ);
        float minIncline = -0.75f - deltaZ;
        float maxIncline = 0.75f + deltaZ;
        p1x = p0x + random(minIncline, maxIncline);
        p1y = p0y + random(minIncline, maxIncline);
        p2x = p0x + random(minIncline, maxIncline);
        p2y = p0y + random(minIncline, maxIncline);

        setStartDelay(random(0, 3000));
        speed = random(minSpeed, maxSpeed) / Const.NANOS_IN_SECOND;
        setRotationZ(0);
        setColor(1, 1, 1, 1);
        setPosition(p0x, p0y, z);
        setAlpha(0);
        t = 0;
    }
}