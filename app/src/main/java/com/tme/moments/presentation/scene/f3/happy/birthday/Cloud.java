package com.tme.moments.presentation.scene.f3.happy.birthday;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Cloud extends Decal {

    private float speedNanos;
    private float startDelayNanos;

    private float startY;
    private float endY;
    private float startX;
    private float endX;

    Cloud(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        init();
    }


    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float delta = speedNanos * deltaNanos;
        translateX(delta);
        float x = getX();

        float xPathLength = Math.abs(endX - startX);
        float fraction = Math.abs(x - startX) / xPathLength;

        float yPathLength = endY - startY;
        float y = startY + fraction * yPathLength;
        setY(y);

        // goes out of screen
        if (x < endX) {
            init();
        }
    }

    private void init() {
        startX = 1 + getWidth() / 2;
        endX = -1 - getWidth() / 2;
        speedNanos = -random(0.11f, 0.13f) / Const.NANOS_IN_SECOND;
        startY = random(-1.0f, 0.2f);
        endY = startY + 0.5f;
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(4000, 6000));
        setPosition(startX, startY, -1.0f);
    }
}
