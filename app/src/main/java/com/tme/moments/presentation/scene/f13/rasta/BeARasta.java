package com.tme.moments.presentation.scene.f13.rasta;

import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.math.Vector3;
import com.tme.moments.presentation.Const;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.opengl.GLES20.GL_ALWAYS;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_EQUAL;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_KEEP;
import static android.opengl.GLES20.GL_REPLACE;
import static android.opengl.GLES20.GL_STENCIL_BUFFER_BIT;
import static android.opengl.GLES20.GL_STENCIL_TEST;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glColorMask;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glStencilFunc;
import static android.opengl.GLES20.glStencilOp;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class BeARasta extends BaseScene {

    private Sprite stencilMask;
    private Sprite overlay;
    private RotatingBackground background;

    private int stencilProgramHandle = -1;
    private int uStencilProjectionMatrixHandle = -1;
    private int aStencilPositionHandle = -1;
    private int aStencilColorHandle = -1;
    private int aStencilTexCoordHandle = -1;

    private TextureInfo atlasInfo;
    private Symbol symbol;
    private List<Weed> weeds;
    private List<Weed> finishedWeeds;
    private DecalBatch batch;
    private DecalBatch weedBatch;

    // overlay animation
    private long scaleWaitTime;
    private boolean scaleDown;
    private float scaleUpSpeed;
    private float scaleDownSpeed;
    private float maxScale;
    private float minScale;
    private int scalesCount;
    private int maxScalesCount;
    private float maxAngle;
    private boolean useRotation;

    private static final float CAMERA_SCALE = 1.4f;
    private TextureRegion weedRegion;

    @Override protected float getCameraPositionX() {
        return 0.0f;
    }

    @Override protected float getCameraPositionY() {
        return 0.0f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.75f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.785f;
    }

    @Override public void init() {
        super.init();
        if (stencilProgramHandle == -1) {
            prepareStencilProgram();
        }
    }

    private void prepareStencilProgram() {
        stencilProgramHandle =
                openGLLoader.createProgram(R.raw.vertex_shader, R.raw.stencil_fragment_shader);

        aStencilPositionHandle = glGetAttribLocation(stencilProgramHandle, "a_position");
        GlUtil.checkLocation(aStencilPositionHandle, "a_position");
        glEnableVertexAttribArray(aStencilPositionHandle);

        aStencilColorHandle = glGetAttribLocation(stencilProgramHandle, "a_color");
        GlUtil.checkLocation(aStencilColorHandle, "a_color");
        glEnableVertexAttribArray(aStencilColorHandle);

        aStencilTexCoordHandle = glGetAttribLocation(stencilProgramHandle, "a_texCoord");
        GlUtil.checkLocation(aStencilTexCoordHandle, "a_texCoord");
        glEnableVertexAttribArray(aStencilTexCoordHandle);

        uStencilProjectionMatrixHandle = glGetUniformLocation(stencilProgramHandle, "u_projTrans");
        GlUtil.checkLocation(uStencilProjectionMatrixHandle, "u_projTrans");
    }

    @Override public boolean hasBackgroundEffects() {
        return true;
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        atlasInfo = openGLLoader.loadTexture(R.drawable.f13_atlas);

        prepareStencilMask();
        prepareOverlay();

        prepareSymbol(atlasInfo);
        batch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        batch.initialize(1);
        batch.add(symbol);

        weeds = new ArrayList<>();
        finishedWeeds = new ArrayList<>();
        weedRegion = buildRegion(atlasInfo, 160, 321, 267, 247);

        weedBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        weedBatch.initialize(100);

        float scaleDownTime = 0.1f;
        float scaleUpTime = 0.1f;
        maxAngle = random() ? 10f : -10f;
        minScale = 0.9f * CAMERA_SCALE;
        maxScale = 1.0f * CAMERA_SCALE;
        scalesCount = 0;
        scaleUpSpeed = ((maxScale - minScale) / scaleUpTime) / Const.NANOS_IN_SECOND;
        scaleDownSpeed = ((maxScale - minScale) / scaleDownTime) / Const.NANOS_IN_SECOND;
        maxScalesCount = random(3, 6);
        scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(random(3000, 4000));
    }

    private void prepareSymbol(TextureInfo info) {
        // insert order matters, last should be Hand symbol
        List<Pair<TextureRegion, PointF>> infos = new ArrayList<>();
        infos.add(new Pair<>(buildRegion(info, 0, 0, 160, 160), new PointF(0.2f, 0.2f)));
        infos.add(new Pair<>(buildRegion(info, 160, 0, 160, 150), new PointF(0.2f, 0.1875f)));
        infos.add(new Pair<>(buildRegion(info, 0, 160, 160, 161), new PointF(0.2f, 0.2012f)));
        infos.add(new Pair<>(buildRegion(info, 160, 160, 160, 160), new PointF(0.2f, 0.2f)));
        infos.add(new Pair<>(buildRegion(info, 0, 321, 160, 282), new PointF(0.2f, 0.3525f)));
        symbol = new Symbol(infos);
    }

    @Override protected void prepareBackgroundEffects(int surfaceWidth, int surfaceHeight) {
        prepareBackground();
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        boolean objectsEnabled = config.isObjectsEnabled();
        glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glUseProgram(mainProgramHandle);
        if (config.isBackgroundEffectsEnabled()) {
            background.step(deltaNanos);
        }
        background.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);

        if (!objectsEnabled) {
            overlay.setScale(CAMERA_SCALE);
            stencilMask.setScale(CAMERA_SCALE);
            overlay.setRotationZ(0);
            stencilMask.setRotationZ(0);
        } else {
            if (scaleWaitTime > 0) {
                scaleWaitTime -= deltaNanos;
            } else {
                if (scaleDown) {
                    float deltaScale = scaleDownSpeed * deltaNanos;
                    float newScale = Math.max(minScale, overlay.getScaleX() - deltaScale);
                    overlay.setScale(newScale);
                    stencilMask.setScale(newScale);
                    if (useRotation) {
                        float fraction = 1 - ((newScale - minScale) / (maxScale - minScale)); // 1 minus because scale is decreasing
                        float angle = maxAngle * fraction;
                        overlay.setRotationZ(angle);
                        stencilMask.setRotationZ(angle);
                    }

                    if (newScale == minScale) {
                        scaleDown = false;
                        throwWeeds();
                    }
                } else {
                    float deltaScale = scaleUpSpeed * deltaNanos;
                    float newScale = Math.min(maxScale, overlay.getScaleX() + deltaScale);
                    overlay.setScale(newScale);
                    stencilMask.setScale(newScale);
                    if (useRotation) {
                        float fraction = 1 - ((newScale - minScale) / (maxScale - minScale)); // 1 minus because scale is decreasing
                        float angle = maxAngle * fraction;
                        overlay.setRotationZ(angle);
                        stencilMask.setRotationZ(angle);
                    }
                    if (newScale == maxScale) {
                        scaleDown = true;
                        useRotation = random();
                        maxAngle = random() ? 10f : -10f;
                        if (++scalesCount >= maxScalesCount) {
                            scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(random(4000, 5000));
                            scalesCount = 0;
                            maxScalesCount = random(3, 6);
                        }
                    }
                }
            }
        }

        glEnable(GL_STENCIL_TEST);

        glColorMask(false, false, false, false);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

        // draw mask
        glUseProgram(stencilProgramHandle);
        stencilMask.draw(uStencilProjectionMatrixHandle, projectionMatrix, aStencilPositionHandle,
                aStencilTexCoordHandle, aStencilColorHandle);

        glColorMask(true, true, true, true);
        glStencilFunc(GL_EQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

        if (objectsEnabled) {
            glDisable(GL_STENCIL_TEST);
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            weedBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Weed weed : weeds) {
                weed.step(deltaNanos);
                if (weed.isFinished()) {
                    finishedWeeds.add(weed);
                }
            }
            if (!finishedWeeds.isEmpty()) {
                weeds.removeAll(finishedWeeds);
                weedBatch.removeAll(finishedWeeds);
                finishedWeeds.clear();
            }
            glEnable(GL_STENCIL_TEST);
        }
        if (mode == Mode.IMAGE) {
            glUseProgram(mainProgramHandle);
            drawImage();
        } else {
            drawCameraInput(cameraTexture);
        }

        glDisable(GL_STENCIL_TEST);
        glUseProgram(mainProgramHandle);
        overlay.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);

        if (objectsEnabled) {
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            glUniformMatrix4fv(uProjectionMatrixLoc, 1, false, projectionMatrix, 0);
            batch.render(uProjectionMatrixLoc, projectionMatrix);
            symbol.step(deltaNanos);
        }
    }

    private void throwWeeds() {
        float width = 0.3337f;
        float height = 0.3087f;
        int count = random(3, 8);
        for (int i = 0; i < count; i++) {
            Weed weed = new Weed(weedRegion, width, height);
            weeds.add(weed);
            weedBatch.add(weed);
        }
    }

    @Override public void release() {
        super.release();
        glDeleteProgram(stencilProgramHandle);
        stencilProgramHandle = -1;
        if (atlasInfo != null) {
            int[] values = new int[1];
            values[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, values, 0);
            GlUtil.checkGlError("Release " + getClass().getSimpleName());
        }
    }

    private void prepareBackground() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f13_background);
        int textureHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        TextureRegion region = new TextureRegion(textureHandle, w, h, 0, 0, w, h);
        float scale = calculateBackgroundScale();
        float currentRotation = 0;
        if (background != null) {
            currentRotation = background.getRotation().getAngleAround(Vector3.Z);
        }
        background = new RotatingBackground(region, getProjectionWidth(), getProjectionHeight());
        background.setRotationZ(currentRotation);
        background.setPosition(0, 0, -1.0f);
        background.setScale(scale);
    }

    private void prepareStencilMask() {
        TextureInfo stencilInfo = openGLLoader.loadTexture(R.drawable.f13_stencil);
        int w = stencilInfo.imageWidth;
        int h = stencilInfo.imageHeight;
        TextureRegion region = new TextureRegion(stencilInfo.textureHandle, w, h, 0, 0, w, h);
        float width = 1.1412f;
        float height = 1.22f;
        stencilMask = new Sprite(region, width, height);
        stencilMask.setPosition(0.0f, 0.0f, -1);
        stencilMask.setScale(CAMERA_SCALE);
    }

    private void prepareOverlay() {
        TextureInfo overlayInfo = openGLLoader.loadTexture(R.drawable.f13_overlay);
        int w = overlayInfo.imageWidth;
        int h = overlayInfo.imageHeight;
        TextureRegion region = new TextureRegion(overlayInfo.textureHandle, w, h, 0, 0, w, h);
        float width = 1.1412f;
        float height = 1.22f;
        overlay = new Sprite(region, width, height);
        overlay.setPosition(0.0f, 0.0f, -1);
        overlay.setScale(CAMERA_SCALE);
    }
}
