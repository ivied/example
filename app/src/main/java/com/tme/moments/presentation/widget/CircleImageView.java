package com.tme.moments.presentation.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;

import com.tme.moments.R;

/**
 * Circle image view with foreground feature
 *
 * @author zoopolitic.
 */
public class CircleImageView extends ForegroundImageView {

    private static final ScaleType SCALE_TYPE = ScaleType.CENTER_CROP;

    private static final Bitmap.Config BITMAP_CONFIG = Bitmap.Config.ARGB_8888;
    private static final int COLORDRAWABLE_DIMENSION = 2;

    private final RectF drawableRect = new RectF();

    private final RectF topLeftRect = new RectF();
    private final RectF topRightRect = new RectF();
    private final RectF bottomLeftRect = new RectF();
    private final RectF bottomRightRect = new RectF();

    private final Matrix shaderMatrix = new Matrix();
    private final Paint bitmapPaint = new Paint();

    private Bitmap bitmap;
    private BitmapShader bitmapShader;
    private int bitmapWidth;
    private int bitmapHeight;
    private boolean isCircle;

    private boolean notRoundTopLeft;
    private boolean notRoundTopRight;
    private boolean notRoundBottomLeft;
    private boolean notRoundBottomRight;

    private float currentCornerRadius = -1;
    private float cornerRadius = -1;

    private ColorFilter colorFilter;

    private boolean ready;
    private boolean setupPending;

    public CircleImageView(Context context) {
        this(context, null);
    }

    public CircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleImageView);
        cornerRadius = a.getDimensionPixelSize(R.styleable.CircleImageView_civ_corner_radius, 0);
        notRoundTopLeft = a.getBoolean(R.styleable.CircleImageView_civ_not_round_top_left, false);
        notRoundTopRight = a.getBoolean(R.styleable.CircleImageView_civ_not_round_top_right, false);
        notRoundBottomLeft = a.getBoolean(R.styleable.CircleImageView_civ_not_round_bottom_left, false);
        notRoundBottomRight = a.getBoolean(R.styleable.CircleImageView_civ_not_round_bottom_right, false);
        isCircle = a.getBoolean(R.styleable.CircleImageView_civ_circle, false);
        a.recycle();
        init();
    }

    private void init() {
        super.setScaleType(SCALE_TYPE);
        ready = true;

        if (setupPending) {
            setup();
            setupPending = false;
        }
    }

    @Override
    public ScaleType getScaleType() {
        return SCALE_TYPE;
    }

    @Override
    public void setScaleType(ScaleType scaleType) {
        if (scaleType != SCALE_TYPE) {
            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", scaleType));
        }
    }

    @Override
    public void setAdjustViewBounds(boolean adjustViewBounds) {
        if (adjustViewBounds) {
            throw new IllegalArgumentException("adjustViewBounds not supported.");
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (bitmap == null) {
            return;
        }

        if (isCircle) {
            canvas.drawCircle(drawableRect.centerX(), drawableRect.centerY(), currentCornerRadius, bitmapPaint);
        } else {
            canvas.drawRoundRect(drawableRect, currentCornerRadius, currentCornerRadius, bitmapPaint);
            if (notRoundTopLeft) {
                canvas.drawRect(topLeftRect, bitmapPaint);
            }
            if (notRoundTopRight) {
                canvas.drawRect(topRightRect, bitmapPaint);
            }
            if (notRoundBottomLeft) {
                canvas.drawRect(bottomLeftRect, bitmapPaint);
            }
            if (notRoundBottomRight) {
                canvas.drawRect(bottomRightRect, bitmapPaint);
            }
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        setup();
    }

    @Override
    public void setPadding(int left, int top, int right, int bottom) {
        super.setPadding(left, top, right, bottom);
        setup();
    }

    @Override
    public void setPaddingRelative(int start, int top, int end, int bottom) {
        super.setPaddingRelative(start, top, end, bottom);
        setup();
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        initializeBitmap();
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        initializeBitmap();
    }

    @Override
    public void setImageResource(@DrawableRes int resId) {
        super.setImageResource(resId);
        initializeBitmap();
    }

    @Override
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        initializeBitmap();
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        if (cf == colorFilter) {
            return;
        }

        colorFilter = cf;
        applyColorFilter();
        invalidate();
    }

    @Override
    public ColorFilter getColorFilter() {
        return colorFilter;
    }

    private void applyColorFilter() {
        if (bitmapPaint != null) {
            bitmapPaint.setColorFilter(colorFilter);
        }
    }

    private Bitmap getBitmapFromDrawable(Drawable drawable) {
        if (drawable == null) {
            return null;
        }

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        try {
            Bitmap bitmap;

            if (drawable instanceof ColorDrawable) {
                bitmap = Bitmap.createBitmap(COLORDRAWABLE_DIMENSION, COLORDRAWABLE_DIMENSION, BITMAP_CONFIG);
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), BITMAP_CONFIG);
            }

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void initializeBitmap() {
        bitmap = getBitmapFromDrawable(getDrawable());
        setup();
    }

    public float getCurrentCornerRadius() {
        return currentCornerRadius;
    }

    public void setCurrentCornerRadius(float radius) {
        this.currentCornerRadius = radius;
        invalidate();
    }

    public float getCornerRadius() {
        return cornerRadius;
    }

    public void setCircle(boolean circle) {
        if (isCircle == circle) {
            return;
        }
        if (circle) {
            currentCornerRadius = Math.min(drawableRect.height() / 2.0f, drawableRect.width() / 2.0f);
        } else {
            currentCornerRadius = cornerRadius;
        }
        invalidate();
    }

    private void setup() {
        if (!ready) {
            setupPending = true;
            return;
        }

        int w = getWidth();
        int h = getHeight();
        if (w == 0 && h == 0) {
            return;
        }

        if (bitmap == null) {
            invalidate();
            return;
        }

        bitmapShader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

        bitmapPaint.setAntiAlias(true);
        bitmapPaint.setShader(bitmapShader);

        bitmapHeight = bitmap.getHeight();
        bitmapWidth = bitmap.getWidth();

        topLeftRect.set(0, 0, w / 2, h / 2);
        topRightRect.set(w / 2, 0, w, h / 2);
        bottomLeftRect.set(0, h / 2, w / 2, h);
        bottomRightRect.set(w / 2, h / 2, w, h);

        drawableRect.set(calculateBounds());
        if (isCircle) {
            currentCornerRadius = getCircleCornerRadius();
        } else {
            /*  if currentCornerRadius is not -1 - the value was changed by transition and therefore
                 we don't need to change it to corner radius from xml to avoid "blink" effect,
                 when TextSizeTransition transition already set needed value but after this imageView
                 was laid out - setup called again and corner radius is set to xml value which is
                 not fit to current state of transition
             */
            if (currentCornerRadius == -1) {
                currentCornerRadius = cornerRadius;
            }
        }

        applyColorFilter();
        updateShaderMatrix();
        invalidate();
    }

    private float getCircleCornerRadius() {
        return Math.min(drawableRect.height() / 2.0f, drawableRect.width() / 2.0f);
    }

    private RectF calculateBounds() {
        int availableWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        int availableHeight = getHeight() - getPaddingTop() - getPaddingBottom();

        if (isCircle) {
            int sideLength = Math.min(availableWidth, availableHeight);

            float left = getPaddingLeft() + (availableWidth - sideLength) / 2f;
            float top = getPaddingTop() + (availableHeight - sideLength) / 2f;

            return new RectF(left, top, left + sideLength, top + sideLength);
        } else {
            return new RectF(getPaddingLeft(), getPaddingTop(), availableWidth, availableHeight);
        }
    }

    private void updateShaderMatrix() {
        float scale;
        float dx = 0;
        float dy = 0;

        shaderMatrix.set(null);

        if (bitmapWidth * drawableRect.height() > drawableRect.width() * bitmapHeight) {
            scale = drawableRect.height() / (float) bitmapHeight;
            dx = (drawableRect.width() - bitmapWidth * scale) * 0.5f;
        } else {
            scale = drawableRect.width() / (float) bitmapWidth;
            dy = (drawableRect.height() - bitmapHeight * scale) * 0.5f;
        }

        shaderMatrix.setScale(scale, scale);
        shaderMatrix.postTranslate((int) (dx + 0.5f) + drawableRect.left, (int) (dy + 0.5f) + drawableRect.top);

        bitmapShader.setLocalMatrix(shaderMatrix);
    }
}
