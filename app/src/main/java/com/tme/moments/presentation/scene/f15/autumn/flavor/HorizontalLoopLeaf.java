package com.tme.moments.presentation.scene.f15.autumn.flavor;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class HorizontalLoopLeaf extends LoopLeaf {

    HorizontalLoopLeaf(TextureRegion region, float width, float height) {
        super(region, width, height);
    }

    @Override protected void init() {
        p0x = -1.25f;
        p0y = random(-0.75f, 0.75f);
        p1x = random(2.0f, 2.8f);
        p1y = random(0.5f, 1.0f);
        p2x = random(-2.8f, -2.0f);
        p2y = random(0.5f, 1.0f);
        p3x = 1.25f;
        p3y = random(-0.75f, 0.75f);
        speed = random(0.2f, 0.27f) / Const.NANOS_IN_SECOND;
        rotationSpeed = random(-150f, 150f) / Const.NANOS_IN_SECOND;
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(3000, 8000));
        setRotationZ(90);
        setColor(1, 1, 1, 1);

        float z = -1 - Math.max(getWidth(), getHeight()) / 2;
        setPosition(p0x, p0y, z);
        setScale(INITIAL_SCALE);
        setAlpha(1);
        inflated = false;
        t = 0;
    }
}
