package com.tme.moments.presentation.recyclerview;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;

/**
 * @author zoopolitic.
 */
public class DisableableLayoutManager extends LinearLayoutManager {

    private boolean scrollEnabled = true;

    public DisableableLayoutManager(Context context) {
        super(context);
    }

    public DisableableLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public DisableableLayoutManager(Context context, AttributeSet attrs, int defStyleAttr,
                                    int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setScrollEnabled(boolean scrollEnabled) {
        this.scrollEnabled = scrollEnabled;
    }

    @Override public boolean canScrollHorizontally() {
        return scrollEnabled && super.canScrollHorizontally();
    }

    @Override public boolean canScrollVertically() {
        return scrollEnabled && super.canScrollVertically();
    }
}
