package com.tme.moments.presentation.scene.f16.christmas;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.math.MathUtils;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Santa extends Sprite {

    private float speed;
    private float scaleSpeedNanos;

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float t;

    private float startDelayNanos;

    private boolean toLeft;

    Santa(TextureRegion region, float width, float height) {
        super(region, width, height);
        setTextureRegion(region);
        setDimensions(width, height);
        init();
        setStartDelay(2500);
    }

    private void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float deltaT = speed * deltaNanos;
        t += deltaT;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        float t1 = t + 0.3f;
        float x1 = (1 - t1) * (1 - t1) * p0x + 2 * (1 - t1) * t1 * p1x + t1 * t1 * p2x;
        float y1 = (1 - t1) * (1 - t1) * p0y + 2 * (1 - t1) * t1 * p1y + t1 * t1 * p2y;
        float angle = MathUtils.angleBetweenLines(x, y, x, y1, x, y, x1, y1);
        if (angle != 0) {
            angle += toLeft ? 80 : -80;
            setRotationZ(y1 >= y ? -angle : -angle + 180);
        }

        setX(x);
        setY(y);

        float deltaScale = scaleSpeedNanos * deltaNanos;
        float scale = getScaleY();
        scale = Math.max(0.7f, scale - deltaScale);
        setScaleX(toLeft ? -scale : scale);
        setScaleY(scale);

        if (t >= 1) {
            init();
        }

        update();
    }

    private void init() {
        t = 0;
        float halfWidth = getWidth() / 2;
        toLeft = random();
        p0x = toLeft ? 1 + halfWidth : -1 - halfWidth;
        p0y = random(0.2f, 0.7f);
        p1x = random(-0.5f, 0.5f);
        p1y = random(0.0f, 1.2f);
        p2x = toLeft ? -1 - halfWidth : 1 + halfWidth;
        p2y = random(0.4f, 1.25f);
        scaleSpeedNanos = random(0.05f, 0.1f) / Const.NANOS_IN_SECOND;
        speed = random(0.16f, 0.21f) / Const.NANOS_IN_SECOND;
        setColor(1, 1, 1, 1);
        setScaleX(toLeft ? -1 : 1);
        setScaleY(1);
        setPosition(p0x, p0y, -1.0f);
        setStartDelay(random(4500, 5500));
    }
}
