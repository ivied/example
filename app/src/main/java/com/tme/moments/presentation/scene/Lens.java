package com.tme.moments.presentation.scene;

import com.tme.moments.R;

/**
 * @author zoopolitic.
 */
public class Lens extends BaseBlurredScene {

    @Override protected int getStencilResId() {
        return R.drawable.round_lens;
    }

    @Override protected int getOverlayResId() {
        return R.drawable.round_lens;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.5555f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.5555f;
    }

    @Override protected float getCameraPositionX() {
        return 0.0f;
    }

    @Override protected float getCameraPositionY() {
        return 0.0f;
    }

    @Override protected float getOverlayWidth() {
        return getDesiredCameraWidthFraction() * getProjectionWidth();
    }

    @Override protected float getOverlayHeight() {
        return getDesiredCameraHeightFraction() * getProjectionHeight();
    }
}
