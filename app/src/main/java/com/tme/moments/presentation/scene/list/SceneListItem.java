package com.tme.moments.presentation.scene.list;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.StringRes;

import com.tme.moments.BR;
import com.tme.moments.R;
import com.tme.moments.presentation.recyclerview.ListItem;
import com.tme.moments.presentation.scene.Scene;

/**
 * @author zoopolitic.
 */
public class SceneListItem extends BaseObservable implements ListItem {

    public final int iconResId;
    public final Scene scene;
    final @StringRes int frameTitleResId;

    private boolean selected;

    SceneListItem(int iconResId, Scene scene, int frameTitleResId) {
        this.iconResId = iconResId;
        this.scene = scene;
        this.frameTitleResId = frameTitleResId;
    }

    @Override public int getLayoutResId() {
        return R.layout.scene_list_item;
    }

    @Bindable
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        notifyPropertyChanged(BR.selected);
    }
}
