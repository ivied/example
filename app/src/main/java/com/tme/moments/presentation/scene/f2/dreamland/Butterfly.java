package com.tme.moments.presentation.scene.f2.dreamland;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.math.MathUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author zoopolitic.
 */
abstract class Butterfly extends AnimatedSprite {

    protected float p0x, p0y; // start point
    protected float p1x, p1y; // control point (bezier)
    protected float p2x, p2y; // end point
    protected float speed;
    protected float zSpeed;
    protected float deflateSpeed;
    protected float startDelayNanos;

    protected final boolean useAutoRotation;
    protected float t;


    Butterfly(TextureRegion region, List<FrameInfo> frameInfoList, float width,
              float height, boolean useAutoRotation) {
        super(region, frameInfoList, width, height);
        this.useAutoRotation = useAutoRotation;
        init();
    }

    void setStartDelayMillis(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    protected abstract void init();

    protected abstract boolean drawOverFrame();

    @Override public void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float deltaT = speed * deltaNanos;
        float deltaZ = zSpeed * deltaNanos;
        t += deltaT;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        if (useAutoRotation) {
            float t1 = t + 0.3f;
            float x1 = (1 - t1) * (1 - t1) * p0x + 2 * (1 - t1) * t1 * p1x + t1 * t1 * p2x;
            float y1 = (1 - t1) * (1 - t1) * p0y + 2 * (1 - t1) * t1 * p1y + t1 * t1 * p2y;
            float angle = MathUtils.angleBetweenLines(x, y, x, y1, x, y, x1, y1);
            if (angle != 0) {
                setRotationZ(y1 >= y ? -angle : -angle + 180);
            }

        }
        setX(x);
        setY(y);
        translateZ(deltaZ);

        float minScale = 0.1f;
        float scale = getScaleX();
        if (scale > minScale) {
            float deltaScale = deflateSpeed * deltaNanos;
            scale -= deltaScale;
            scale = Math.max(minScale, scale);
            setScale(scale);
        }

        float fadeOutBorder = 0.75f;
        if (t >= fadeOutBorder) {
            float fraction = (1 - t) / (1 - fadeOutBorder);
            setAlpha(fraction);
        }

        if (t >= 1) {
            init();
            setAlpha(1);
            t = 0;
        }
        super.step(deltaNanos);
    }
}
