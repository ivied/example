package com.tme.moments.presentation.utils;

import java.util.Objects;

public interface Consumer<T> {
    
    void accept(T t);
    
    default java.util.function.Consumer<T> andThen(Consumer<? super T> after) {
        Objects.requireNonNull(after);
        return (T t) -> { accept(t); after.accept(t); };
    }
}
