package com.tme.moments.presentation.core;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.tme.moments.BuildConfig;
import com.tme.moments.presentation.bitmap.FileUtils;
import com.tme.moments.presentation.utils.AndroidUtils;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * @author zoopolitic.
 */
public class App extends Application implements HasActivityInjector {

    @Inject DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;
    @Inject Config config;


    private InterstitialAd interstitialAd;
    private RewardedVideoAd rewardedVideoAd;
    private RewardedVideoAd rewardedMusicVideoAd;

    private static App instance;

    private CoreComponent component;

    public CoreComponent coreComponent() {
        return component;
    }

    @Override public void onCreate() {
        super.onCreate();
        instance = this;

        CrashlyticsCore core = new CrashlyticsCore.Builder()
                .disabled(BuildConfig.DEBUG)
                .build();
        Fabric.with(this, new Crashlytics.Builder().core(core).build(), new Crashlytics());

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashlyticsTree());
        }

        buildAndInjectTopComponent();
        MobileAds.initialize(this, BuildConfig.ADMOB_APP_ID);
        config.setDensity(getResources().getDisplayMetrics().density);
        AndroidUtils.printSupportedMediaCodecs();
        initAd();
        FileUtils.clearAllLocalCopiesOfFilesAsync(this);
    }

    private void initAd() {
            if (interstitialAd == null) {


                interstitialAd = new InterstitialAd(this);
                interstitialAd.setAdUnitId(BuildConfig.CAPTURE_INTERSTITIAL_AD_UNIT);
                interstitialAd.loadAd(new AdRequest.Builder().build());
                interstitialAd.setAdListener(new AdListener() {

                    @Override
                    public void onAdClosed() {
                        // Load the next interstitial.
                        interstitialAd.loadAd(new AdRequest.Builder().build());
                    }

                });
            }




        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        reloadRewardAd();
        rewardedMusicVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        reloadRewardMusicAd();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, intentFilter);
    }


    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!rewardedVideoAd.isLoaded()){
                reloadRewardAd();
                reloadRewardMusicAd();
            }

            reloadAd();
        }
    };

    public InterstitialAd getInterstitialAd() {
        return interstitialAd;
    }


    public RewardedVideoAd getRewardAd() {
        return rewardedVideoAd;
    }

    public RewardedVideoAd getMusicRewardAd() {
        return rewardedMusicVideoAd;
    }

    protected void buildAndInjectTopComponent() {
        component = DaggerCoreComponent.builder()
                .coreModule(new CoreModule(this))
                .build();
        component.inject(this);
    }

    public static App getInstance() {
        return instance;
    }


    @Override public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    public void reloadAd() {
        interstitialAd.loadAd(new AdRequest.Builder().build());
    }


    public void reloadRewardAd(){

        rewardedVideoAd.loadAd(BuildConfig.PREVIEW_REWARD_AD_UNIT, new AdRequest.Builder().build());
    }

    public void reloadRewardMusicAd(){

        rewardedMusicVideoAd.loadAd(BuildConfig.MUSIC_REWARD_AD_UNIT, new AdRequest.Builder().build());
    }
}
