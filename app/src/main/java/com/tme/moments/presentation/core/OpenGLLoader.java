package com.tme.moments.presentation.core;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.support.annotation.DrawableRes;
import android.util.Log;

import com.tme.moments.gles.GlUtil;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.bitmap.BitmapLoadUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import timber.log.Timber;

import static android.opengl.GLES20.GL_CLAMP_TO_EDGE;
import static android.opengl.GLES20.GL_COMPILE_STATUS;
import static android.opengl.GLES20.GL_FRAGMENT_SHADER;
import static android.opengl.GLES20.GL_INFO_LOG_LENGTH;
import static android.opengl.GLES20.GL_LINEAR;
import static android.opengl.GLES20.GL_LINK_STATUS;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_S;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_T;
import static android.opengl.GLES20.GL_VERTEX_SHADER;
import static android.opengl.GLES20.glAttachShader;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glCompileShader;
import static android.opengl.GLES20.glCreateProgram;
import static android.opengl.GLES20.glCreateShader;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteShader;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glGenTextures;
import static android.opengl.GLES20.glGetProgramInfoLog;
import static android.opengl.GLES20.glGetProgramiv;
import static android.opengl.GLES20.glGetShaderInfoLog;
import static android.opengl.GLES20.glGetShaderiv;
import static android.opengl.GLES20.glLinkProgram;
import static android.opengl.GLES20.glShaderSource;
import static android.opengl.GLES20.glTexParameterf;
import static android.opengl.GLES20.glTexParameteri;

/**
 * @author zoopolitic.
 */
public class OpenGLLoader {

    private static final String TAG = OpenGLLoader.class.getSimpleName();

    private final Context context;
    private int maxBitmapSize = 0;

    OpenGLLoader(Context context) {
        this.context = context;
    }

    public TextureInfo loadTexture(@DrawableRes int resourceId) {
        return loadTexture(resourceId, 0, 0);
    }

    public TextureInfo loadTexture(@DrawableRes int resourceId, int requiredWidth,
                                   int requiredHeight) {
        updateMaxBitmapSizeIfNeeded();
        final BitmapLoadUtils.DecodeResult result = BitmapLoadUtils
                .decodeSampledBitmapFromResource(context, resourceId, maxBitmapSize, maxBitmapSize);
        Bitmap bitmap;
        if (requiredWidth > 0 && requiredHeight > 0) {
            bitmap = Bitmap.createScaledBitmap(result.bitmap, requiredWidth, requiredHeight, false);
        } else {
            bitmap = result.bitmap;
        }
        if (bitmap == null) {
            return null;
        }
        int textureHandle = loadTexture(bitmap);
        if (textureHandle <= 0) {
            bitmap.recycle();
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        // Recycle the bitmap, since its data has been loaded into OpenGL.
        bitmap.recycle();
        return new TextureInfo(textureHandle, width, height, result.inSampleSize);
    }

    public TextureInfo loadTexture(Uri imageUri) {
        updateMaxBitmapSizeIfNeeded();
        int requiredWidth = maxBitmapSize;
        int requiredHeight = maxBitmapSize;
        final BitmapLoadUtils.DecodeResult result = BitmapLoadUtils
                .decodeSampledBitmapFromUri(context, imageUri, requiredWidth, requiredHeight);
        Bitmap bitmap = result.bitmap;
        if (bitmap == null) {
            return null;
        }
        int textureHandle = loadTexture(bitmap);
        if (textureHandle <= 0) {
            bitmap.recycle();
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        // Recycle the bitmap, since its data has been loaded into OpenGL.
        bitmap.recycle();
        return new TextureInfo(textureHandle, width, height, result.inSampleSize);
    }

    private void updateMaxBitmapSizeIfNeeded() {
        if (maxBitmapSize == 0) {
            maxBitmapSize = BitmapLoadUtils.calculateMaxBitmapSize(context);
            int[] maxSize = new int[1];
            GLES20.glGetIntegerv(GLES20.GL_MAX_TEXTURE_SIZE, maxSize, 0);
            int EGLMaxSize = maxSize[0];
            Log.d(TAG, "maxBitmapSize: " + maxBitmapSize + ", EGLMaxSize=" + EGLMaxSize);
            maxBitmapSize = Math.min(maxBitmapSize, EGLMaxSize);
        }
    }

    public static int loadTexture(final Bitmap bitmap) {
        final int[] textureIds = new int[1];

        glGenTextures(1, textureIds, 0);
        GlUtil.checkGlError("Gen textures");

        if (textureIds[0] == 0) {
            Timber.e("Couldn't load resource");
            return -1;
        }

        if (bitmap == null) {
            glDeleteTextures(1, textureIds, 0);
            return -1;
        }

        // Bind to the texture in OpenGL
        glBindTexture(GL_TEXTURE_2D, textureIds[0]);
        GlUtil.checkGlError("bind texture");

        // Set filtering
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        // Load the bitmap into the bound texture.
        GLUtils.texImage2D(GL_TEXTURE_2D, 0, bitmap, 0);
        GlUtil.checkGlError("texImage2D");

        glBindTexture(GL_TEXTURE_2D, 0);

        return textureIds[0];
    }

    public int createProgram(int vertexShaderRawId, int fragmentShaderRawId) {
        final int programId = glCreateProgram();
        GlUtil.checkGlError("Create program");
        if (programId == 0) {
            return -1;
        }
        String vertexShader = readTextFromRaw(context, vertexShaderRawId);
        String fragmentShader = readTextFromRaw(context, fragmentShaderRawId);
        return createProgram(vertexShader, fragmentShader);
    }

    public int createProgram(String vertexShader, String fragmentShader) {
        final int programId = glCreateProgram();
        GlUtil.checkGlError("Create program");
        if (programId == 0) {
            return -1;
        }
        int vertexShaderId = createShader(GL_VERTEX_SHADER, vertexShader);
        int fragmentShaderId = createShader(GL_FRAGMENT_SHADER, fragmentShader);
        glAttachShader(programId, vertexShaderId);
        glAttachShader(programId, fragmentShaderId);
        glLinkProgram(programId);
        final int[] linkStatus = new int[1];
        glGetProgramiv(programId, GL_LINK_STATUS, linkStatus, 0);
        if (linkStatus[0] == 0) {
            final int[] length = new int[1];
            glGetProgramiv(programId, GL_INFO_LOG_LENGTH, length, 0);
            if (length[0] > 0) {
                String log = glGetProgramInfoLog(programId);
                Timber.e(String.format("Error linking program. Message: %s", log));
            }
            glDeleteProgram(programId);
            return -1;
        }
        GlUtil.checkGlError("Link program");
        return programId;
    }

    private static int createShader(int type, final String shaderText) {
        final int shaderId = glCreateShader(type);
        if (shaderId == 0) {
            return -1;
        }
        glShaderSource(shaderId, shaderText);
        GlUtil.checkGlError("Shader source");
        glCompileShader(shaderId);
        final int[] compileStatus = new int[1];
        glGetShaderiv(shaderId, GL_COMPILE_STATUS, compileStatus, 0);
        if (compileStatus[0] == 0) {
            final int[] length = new int[1];
            glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, length, 0);
            if (length[0] > 0) {
                String log = glGetShaderInfoLog(shaderId);
                String shaderType = type == GL_VERTEX_SHADER ? "vertex" : "fragment";
                Timber.e("Error compiling %s shader. Message: %s \n Shader: \n %s",
                        shaderType,
                        log,
                        shaderText
                );
            }
            glDeleteShader(shaderId);
            return -1;
        }
        return shaderId;
    }

    private static String readTextFromRaw(Context context, int resourceId) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            BufferedReader bufferedReader = null;
            try {
                InputStream inputStream = context.getResources().openRawResource(resourceId);
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                    stringBuilder.append("\r\n");
                }
            } finally {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            }
        } catch (IOException | Resources.NotFoundException ioex) {
            ioex.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
