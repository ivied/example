package com.tme.moments.presentation.scene.f18.paper.speech;

import android.graphics.PointF;
import android.support.v4.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.presentation.Const;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseBlurredScene;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class PaperSpeechBubble extends BaseBlurredScene {

    private static final long VERTICAL_BEE_DELAY_INTERVAL = 200; // millis

    private TextureInfo atlasInfo;
    private Bird bird;
    private List<VerticalBee> verticalBees;
    private List<HorizontalBee> horizontalBees;
    private List<Circle> circles;
    private DecalBatch circlesBatch;
    private boolean verticalBeesActive;

    // overlay animation
    private long scaleWaitTime;
    private boolean scaleUp;
    private float scaleUpSpeed;
    private float scaleDownSpeed;
    private float maxScale;
    private float minScale;
    private int scalesCount;
    private int maxScalesCount;
    private float maxAngle;
    private boolean useRotation;

    @Override protected float getCameraPositionX() {
        return 0.0296f;
    }

    @Override protected float getCameraPositionY() {
        return 0.084f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.7037f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.67f;
    }

    @Override protected float getOverlayPositionX() {
        return 0;
    }

    @Override protected float getOverlayPositionY() {
        return 0;
    }

    @Override protected int getStencilResId() {
        return R.drawable.f18_stencil;
    }

    @Override protected int getOverlayResId() {
        return R.drawable.f18_overlay;
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        super.prepareObjects(surfaceWidth, surfaceHeight);
        atlasInfo = openGLLoader.loadTexture(R.drawable.f18_atlas);
        prepareBirds(atlasInfo);
        prepareVerticalBees(atlasInfo);
        prepareHorizontalBees(atlasInfo, 2000); // initial delay
        prepareCircles(atlasInfo);
        circlesBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        circlesBatch.initialize(circles.size());
        circlesBatch.addAll(circles);
        verticalBeesActive = random();

        float scaleDownTime = 0.1f;
        float scaleUpTime = 0.1f;
        maxAngle = random() ? 10f : -10f;
        minScale = 1.0f;
        maxScale = 1.1f;
        scaleUpSpeed = ((maxScale - minScale) / scaleUpTime) / Const.NANOS_IN_SECOND;
        scaleDownSpeed = ((maxScale - minScale) / scaleDownTime) / Const.NANOS_IN_SECOND;
        maxScalesCount = random(2, 5);
        scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(random(3000, 4000));
    }

    @Override protected void onBlurredImageDrawn(long deltaNanos) {
        super.onBlurredImageDrawn(deltaNanos);
        boolean objectsEnabled = config.isObjectsEnabled();
        if (!objectsEnabled) {
            overlay.setScale(1);
            stencilMask.setScale(1);
            overlay.setRotationZ(0);
            stencilMask.setRotationZ(0);
        } else {
            if (scaleWaitTime > 0) {
                scaleWaitTime -= deltaNanos;
            } else {
                if (scaleUp) {
                    float deltaScale = scaleUpSpeed * deltaNanos;
                    float newScale = Math.min(maxScale, overlay.getScaleX() + deltaScale);
                    overlay.setScale(newScale);
                    stencilMask.setScale(newScale);
                    if (useRotation) {
                        float fraction = (newScale - minScale) / (maxScale - minScale);
                        float angle = maxAngle * fraction;
                        overlay.setRotationZ(angle);
                        stencilMask.setRotationZ(angle);
                    }

                    if (newScale == maxScale) {
                        scaleUp = false;
                    }
                } else {
                    float deltaScale = scaleDownSpeed * deltaNanos;
                    float newScale = Math.max(minScale, overlay.getScaleX() - deltaScale);
                    overlay.setScale(newScale);
                    stencilMask.setScale(newScale);
                    if (useRotation) {
                        float fraction = (newScale - minScale) / (maxScale - minScale);
                        float angle = maxAngle * fraction;
                        overlay.setRotationZ(angle);
                        stencilMask.setRotationZ(angle);
                    }
                    if (newScale == minScale) {
                        scaleUp = true;
                        useRotation = random();
                        maxAngle = random() ? 10f : -10f;
                        if (++scalesCount == maxScalesCount) {
                            scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(random(4000, 5000));
                            scalesCount = 0;
                            maxScalesCount = random(2, 5);
                        }
                    }
                }
            }
        }

        glUseProgram(mainProgramHandle);
        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        circlesBatch.render(uProjectionMatrixLoc, projectionMatrix);
        for (Circle circle : circles) {
            circle.setFrozen(!objectsEnabled);
            circle.step(deltaNanos);
        }
    }

    @Override protected void onOverlayDrawn(long deltaNanos) {
        super.onOverlayDrawn(deltaNanos);
        boolean objectsEnabled = config.isObjectsEnabled();
        if (objectsEnabled) {
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            bird.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            bird.step(deltaNanos);

            if (verticalBeesActive) {
                boolean allFinished = true;
                for (VerticalBee bee : verticalBees) {
                    bee.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
                    bee.step(deltaNanos);
                    if (!bee.isFinished()) {
                        allFinished = false;
                    }
                }
                if (allFinished) {
                    setupBees();
                }
            } else {
                boolean allFinished = true;
                for (HorizontalBee bee : horizontalBees) {
                    bee.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
                    bee.step(deltaNanos);
                    if (!bee.isFinished()) {
                        allFinished = false;
                    }
                }
                if (allFinished) {
                    setupBees();
                }
            }
        }
    }

    @Override public void release() {
        super.release();
        if (atlasInfo != null) {
            int[] values = new int[1];
            values[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, values, 0);
            GlUtil.checkGlError("Delete texture");
        }
    }

    private void prepareBirds(TextureInfo info) {
        List<List<AnimatedSprite.FrameInfo>> birds = new ArrayList<>();
        int inSampleSize = info.inSampleSize;
        int frameWidth = 240;
        int frameHeight = 205;
        float width = 0.3f;
        float height = 0.2562f;

        List<AnimatedSprite.FrameInfo> blueBird = new ArrayList<>();
        blueBird.add(new AnimatedSprite.FrameInfo(blueBird.size(), 240 / inSampleSize, 685 / inSampleSize));
        blueBird.add(new AnimatedSprite.FrameInfo(blueBird.size(), 240 / inSampleSize, 480 / inSampleSize));
        blueBird.add(new AnimatedSprite.FrameInfo(blueBird.size(), 0 / inSampleSize, 480 / inSampleSize));
        blueBird.add(new AnimatedSprite.FrameInfo(blueBird.size(), 480 / inSampleSize, 480 / inSampleSize));

        List<AnimatedSprite.FrameInfo> greenBird = new ArrayList<>();
        greenBird.add(new AnimatedSprite.FrameInfo(greenBird.size(), 0 / inSampleSize, 890 / inSampleSize));
        greenBird.add(new AnimatedSprite.FrameInfo(greenBird.size(), 720 / inSampleSize, 685 / inSampleSize));
        greenBird.add(new AnimatedSprite.FrameInfo(greenBird.size(), 0 / inSampleSize, 685 / inSampleSize));
        greenBird.add(new AnimatedSprite.FrameInfo(greenBird.size(), 480 / inSampleSize, 275 / inSampleSize));

        List<AnimatedSprite.FrameInfo> orangeBird = new ArrayList<>();
        orangeBird.add(new AnimatedSprite.FrameInfo(orangeBird.size(), 480 / inSampleSize, 890 / inSampleSize));
        orangeBird.add(new AnimatedSprite.FrameInfo(orangeBird.size(), 720 / inSampleSize, 890 / inSampleSize));
        orangeBird.add(new AnimatedSprite.FrameInfo(orangeBird.size(), 240 / inSampleSize, 890 / inSampleSize));
        orangeBird.add(new AnimatedSprite.FrameInfo(orangeBird.size(), 480 / inSampleSize, 685 / inSampleSize));

        List<AnimatedSprite.FrameInfo> pinkBird = new ArrayList<>();
        pinkBird.add(new AnimatedSprite.FrameInfo(pinkBird.size(), 720 / inSampleSize, 480 / inSampleSize));
        pinkBird.add(new AnimatedSprite.FrameInfo(pinkBird.size(), 0 / inSampleSize, 275 / inSampleSize));
        pinkBird.add(new AnimatedSprite.FrameInfo(pinkBird.size(), 240 / inSampleSize, 275 / inSampleSize));
        pinkBird.add(new AnimatedSprite.FrameInfo(pinkBird.size(), 720 / inSampleSize, 275 / inSampleSize));

        TextureRegion region = buildRegion(info, blueBird.get(0).regionX, blueBird.get(0).regionY, frameWidth, frameHeight);

        birds.add(blueBird);
        birds.add(greenBird);
        birds.add(pinkBird);
        birds.add(orangeBird);

        bird = new Bird(region, birds, width, height);
    }

    private void prepareHorizontalBees(TextureInfo info, long delayMillis) {
        int inSampleSize = info.inSampleSize;
        boolean flyLeft = random();
        int count = random(4, 7);
        int frameWidth = 140;
        int frameHeight = 123;
        List<AnimatedSprite.FrameInfo> frames = new ArrayList<>();
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 280 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 420 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 140 / inSampleSize, 0 / inSampleSize));
        float minFlockIntervalX = 0.2f;
        float maxFlockIntervalX = 0.35f;
        float minFlockIntervalY = -0.12f;
        float maxFlockIntervalY = 0.12f;
        float width = 0.175f;
        float height = 0.1537f;
        float startX = flyLeft ? 1.2f : -1.2f;
        float startY = random(-0.7f + height / 2, -0.4f - height / 2);
        horizontalBees = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            TextureRegion region = buildRegion(info, 280, 0, frameWidth, frameHeight); // region for first frame
            float speed = 0.5f;
            float x = startX;
            float intervalX = random(minFlockIntervalX, maxFlockIntervalX);
            float intervalY = random(minFlockIntervalY, maxFlockIntervalY);
            float y = startY + intervalY;
            if (flyLeft) {
                speed = -speed;
                startX += intervalX;
            } else {
                startX -= intervalX;
            }
            float inclineSpeed = random(0.05f, 0.1f);
            float inclineY = random(0.035f, 0.05f);
            float hangTimeInSeconds = random(0.5f, 2.0f);
            HorizontalBee bee = new HorizontalBee(region, frames, width, height, x, y, speed, inclineY, inclineSpeed, hangTimeInSeconds);
            bee.setStartDelay(delayMillis);
            if (!flyLeft) {
                bee.setScaleX(-1); // flip texture
            }
            horizontalBees.add(bee);
        }
    }

    private void prepareVerticalBees(TextureInfo info) {
        List<AnimatedSprite.FrameInfo> frames = new ArrayList<>();
        int inSampleSize = info.inSampleSize;
        int frameWidth = 150;
        int frameHeight = 151;
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 372 / inSampleSize, 124 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 522 / inSampleSize, 124 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 672 / inSampleSize, 124 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 822 / inSampleSize, 124 / inSampleSize));
        float width = 0.1875f;
        float height = 0.1887f;

        int count = 7;
        verticalBees = new ArrayList<>(count);
        float totalBeesWidth = width * count;
        float totalFreeSpace = 2 - totalBeesWidth;
        float margin = totalFreeSpace / (count + 1);
        float nextX = -1 + width / 2 + margin;
        long delay = 2000;
        for (int i = 0; i < count; i++) {
            TextureRegion region = buildRegion(info, 372, 124, frameWidth, frameHeight); // region for first frame
            VerticalBee bee = new VerticalBee(region, frames, width, height, nextX);
            bee.setDelayMillis(delay);
            verticalBees.add(bee);
            nextX += width / 2 + width / 2 + margin;
            delay += VERTICAL_BEE_DELAY_INTERVAL;
        }
    }

    private void setupBees() {
        verticalBeesActive = random();
        long delayMillis = random(3000, 5000);
        if (verticalBeesActive) {
            for (VerticalBee bee : verticalBees) {
                bee.setFinished(false);
                bee.setDelayMillis(delayMillis);
                delayMillis += VERTICAL_BEE_DELAY_INTERVAL;
            }
        } else {
            prepareHorizontalBees(atlasInfo, delayMillis);
        }
    }

    @Override public void onObjectsStateChanged(boolean enabled) {
        if (enabled) {
            circles.clear();
        }
        super.onObjectsStateChanged(enabled);
    }

    private void prepareCircles(TextureInfo info) {
        if (circles != null && !circles.isEmpty()) {
            return;
        }
        float width = 0.155f;
        float height = 0.155f;
        int regionWidth = 124;
        int regionHeight = 124;
        List<Pair<TextureRegion, PointF>> regionInfos = new ArrayList<>();
        regionInfos.add(new Pair<>(buildRegion(info, 0, 124, regionWidth, regionHeight), new PointF(width, height)));
        regionInfos.add(new Pair<>(buildRegion(info, 248, 124, regionWidth, regionHeight), new PointF(width, height)));
        regionInfos.add(new Pair<>(buildRegion(info, 808, 0, regionWidth, regionHeight), new PointF(width, height)));
        regionInfos.add(new Pair<>(buildRegion(info, 560, 0, regionWidth, regionHeight), new PointF(width, height)));
        regionInfos.add(new Pair<>(buildRegion(info, 124, 124, regionWidth, regionHeight), new PointF(width, height)));
        regionInfos.add(new Pair<>(buildRegion(info, 684, 0, regionWidth, regionHeight), new PointF(width, height)));
        circles = new ArrayList<>();
        for (int i = 0; i < 750; i++) {
            Pair<TextureRegion, PointF> regionInfo = regionInfos.get(random(regionInfos.size()));
            TextureRegion region = regionInfo.first;
            float w = regionInfo.second.x;
            float h = regionInfo.second.y;
            circles.add(new Circle(region, w, h));
        }
    }
}