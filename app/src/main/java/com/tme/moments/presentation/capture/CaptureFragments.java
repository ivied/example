package com.tme.moments.presentation.capture;

import android.support.v4.app.Fragment;

import com.tme.moments.presentation.audio.player.AudioPlayerFragment;
import com.tme.moments.presentation.drawer.DrawerFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.android.support.FragmentKey;
import dagger.multibindings.IntoMap;

/**
 * @author zoopolitic.
 */
@Module(subcomponents = {DrawerFragment.Component.class, AudioPlayerFragment.Component.class})
public abstract class CaptureFragments {

    @Binds
    @IntoMap
    @FragmentKey(DrawerFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindDrawerFragmentInjectorFactory(DrawerFragment.Component.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(AudioPlayerFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindAudioPlayerFragmentInjectorFactory(AudioPlayerFragment.Component.Builder builder);
}
