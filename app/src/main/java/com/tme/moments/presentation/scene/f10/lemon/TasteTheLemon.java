package com.tme.moments.presentation.scene.f10.lemon;

import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_ALWAYS;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_EQUAL;
import static android.opengl.GLES20.GL_KEEP;
import static android.opengl.GLES20.GL_REPLACE;
import static android.opengl.GLES20.GL_STENCIL_BUFFER_BIT;
import static android.opengl.GLES20.GL_STENCIL_TEST;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glColorMask;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glStencilFunc;
import static android.opengl.GLES20.glStencilOp;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class TasteTheLemon extends BaseScene {

    private int stencilProgramHandle = -1;
    private int uStencilProjectionMatrixHandle = -1;
    private int aStencilPositionHandle = -1;
    private int aStencilColorHandle = -1;
    private int aStencilTexCoordHandle = -1;

    private Sprite stencilMask;
    private FloatingOverlay overlay;
    private Sprite background;

    private TextureInfo atlasInfo;
    private Citrus citrus;
    private Bird bird;
    private Piece piece;
    private List<FallingCircle> fallingCircles;
    private DecalBatch objectsBatch;
    private float citrusOriginX;
    private float citrusOriginY;

    @Override protected float getCameraPositionX() {
        return -0.05f;
    }

    @Override protected float getCameraPositionY() {
        return -0.055f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.7f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.86f;
    }

    @Override public void init() {
        super.init();
        if (stencilProgramHandle == -1) {
            prepareStencilProgram();
        }
    }

    @Override public boolean hasBackgroundEffects() {
        return true;
    }

    private void prepareStencilProgram() {
        stencilProgramHandle =
                openGLLoader.createProgram(R.raw.vertex_shader, R.raw.stencil_fragment_shader);

        aStencilPositionHandle = glGetAttribLocation(stencilProgramHandle, "a_position");
        GlUtil.checkLocation(aStencilPositionHandle, "a_position");
        glEnableVertexAttribArray(aStencilPositionHandle);

        aStencilColorHandle = glGetAttribLocation(stencilProgramHandle, "a_color");
        GlUtil.checkLocation(aStencilColorHandle, "a_color");
        glEnableVertexAttribArray(aStencilColorHandle);

        aStencilTexCoordHandle = glGetAttribLocation(stencilProgramHandle, "a_texCoord");
        GlUtil.checkLocation(aStencilTexCoordHandle, "a_texCoord");
        glEnableVertexAttribArray(aStencilTexCoordHandle);

        uStencilProjectionMatrixHandle = glGetUniformLocation(stencilProgramHandle, "u_projTrans");
        GlUtil.checkLocation(uStencilProjectionMatrixHandle, "u_projTrans");
    }

    @Override public void onBackgroundEffectsStateChanged(boolean enabled) {
        super.onBackgroundEffectsStateChanged(enabled);
        if (!enabled) {
            overlay.setX(0);
            overlay.setY(0);
        }
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        prepareOverlay();
        prepareBackground();
        prepareStencilMask();

        atlasInfo = openGLLoader.loadTexture(R.drawable.f10_atlas);
        prepareBirds(atlasInfo);

        prepareCitrus(atlasInfo);

        preparePiece(atlasInfo);
        prepareCircles(atlasInfo);
        objectsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        objectsBatch.initialize(fallingCircles.size() + 2);
        objectsBatch.add(piece);
        objectsBatch.add(citrus);
        objectsBatch.addAll(fallingCircles);
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        boolean objectsEnabled = config.isObjectsEnabled();
        glUseProgram(mainProgramHandle);
        background.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation,
                aTexCoordLocation, aColorLocation);
        glEnable(GL_STENCIL_TEST);

        glColorMask(false, false, false, false);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

        // draw mask
        glUseProgram(stencilProgramHandle);
        stencilMask.draw(uStencilProjectionMatrixHandle, projectionMatrix, aStencilPositionHandle,
                aStencilTexCoordHandle, aStencilColorHandle);

        glColorMask(true, true, true, true);
        glStencilFunc(GL_EQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

        if (mode == Mode.IMAGE) {
            glUseProgram(mainProgramHandle);
            drawImage();
        } else {
            drawCameraInput(cameraTexture);
        }
        glDisable(GL_STENCIL_TEST);
        glUseProgram(mainProgramHandle);
        overlay.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation,
                aTexCoordLocation, aColorLocation);
        if (config.isBackgroundEffectsEnabled()) {
            overlay.step(deltaNanos);
        }
        float deltaX = overlay.getX();
        float deltaY = overlay.getY();

        stencilMask.setX(getCameraPositionX() + deltaX);
        stencilMask.setY(getCameraPositionY() + deltaY);

        citrus.setX(citrusOriginX + deltaX);
        citrus.setY(citrusOriginY + deltaY);

        if (objectsEnabled) {
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            objectsBatch.render(uProjectionMatrixLoc, projectionMatrix);
            piece.step(deltaNanos);
            for (FallingCircle fallingCircle : fallingCircles) {
                fallingCircle.step(deltaNanos);
            }
            citrus.step(deltaNanos);
            bird.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            bird.step(deltaNanos);
        }
    }

    private void prepareOverlay() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f10_overlay);
        int textureHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        TextureRegion region = new TextureRegion(textureHandle, w, h, 0, 0, w, h);
        float width = getProjectionWidth();
        float height = getProjectionHeight();
        float scaleFactor = (width + FloatingOverlay.OFFSET * 2) / width;
        overlay = new FloatingOverlay(region, width, height);
        overlay.setScale(scaleFactor);
        overlay.setPosition(0, 0, -1);
    }

    private void prepareStencilMask() {
        TextureInfo stencilInfo = openGLLoader.loadTexture(R.drawable.f10_stencil);
        int w = stencilInfo.imageWidth;
        int h = stencilInfo.imageHeight;
        TextureRegion region = new TextureRegion(stencilInfo.textureHandle, w, h, 0, 0, w, h);
        float width = getDesiredCameraWidthFraction() * getProjectionWidth();
        float height = getDesiredCameraHeightFraction() * getProjectionHeight();
        float overlayWidth = overlay.getWidth();
        float scaleFactor = (overlayWidth + FloatingOverlay.OFFSET * 2) / overlayWidth;
        stencilMask = new Sprite(region, width, height);
        stencilMask.setScale(scaleFactor);
        stencilMask.setPosition(getCameraPositionX(), getCameraPositionY(), -1);
    }

    private void prepareBackground() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f10_background);
        int textureHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        TextureRegion region = new TextureRegion(textureHandle, w, h, 0, 0, w, h);
        background = new Sprite(region, getProjectionWidth(), getProjectionHeight());
        background.setPosition(0, 0, -1);
    }

    private void prepareCitrus(TextureInfo info) {
        List<Pair<TextureRegion, PointF>> regionInfos = new ArrayList<>();
        regionInfos.add(new Pair<>(buildRegion(info, 540, 659, 413, 408), new PointF(0.7648f, 0.7555f)));
        regionInfos.add(new Pair<>(buildRegion(info, 0, 1069, 411, 410), new PointF(0.7611f, 0.7592f)));
        regionInfos.add(new Pair<>(buildRegion(info, 823, 1070, 406, 409), new PointF(0.7537f, 0.7592f)));
        Pair<TextureRegion, PointF> regionInfo = regionInfos.get(random(regionInfos.size()));
        TextureRegion region = regionInfo.first;
        float width = regionInfo.second.x;
        float height = regionInfo.second.y;
        float overlayWidth = overlay.getWidth();
        float scaleFactor = (overlayWidth + FloatingOverlay.OFFSET * 2) / overlayWidth;
        citrus = new Citrus(region, width, height);
        citrus.setScale(scaleFactor + 0.03f);
        citrusOriginX = 0.53f;
        citrusOriginY = 0.63f;
        citrus.setPosition(citrusOriginX, citrusOriginY, -1.0f);
    }

    private void prepareBirds(TextureInfo info) {
        List<List<AnimatedSprite.FrameInfo>> birds = new ArrayList<>();
        int inSampleSize = info.inSampleSize;
        int frameWidth = 240;
        int frameHeight = 205;
        float width = 0.3f;
        float height = 0.2562f;

        List<AnimatedSprite.FrameInfo> blueBird = new ArrayList<>();
        blueBird.add(new AnimatedSprite.FrameInfo(blueBird.size(), 720 / inSampleSize, 410 / inSampleSize));
        blueBird.add(new AnimatedSprite.FrameInfo(blueBird.size(), 1200 / inSampleSize, 410 / inSampleSize));
        blueBird.add(new AnimatedSprite.FrameInfo(blueBird.size(), 0 / inSampleSize, 205 / inSampleSize));
        blueBird.add(new AnimatedSprite.FrameInfo(blueBird.size(), 1267 / inSampleSize, 0 / inSampleSize));

        List<AnimatedSprite.FrameInfo> grayBird = new ArrayList<>();
        grayBird.add(new AnimatedSprite.FrameInfo(grayBird.size(), 1507 / inSampleSize, 0 / inSampleSize));
        grayBird.add(new AnimatedSprite.FrameInfo(grayBird.size(), 1027 / inSampleSize, 0 / inSampleSize));
        grayBird.add(new AnimatedSprite.FrameInfo(grayBird.size(), 240 / inSampleSize, 205 / inSampleSize));
        grayBird.add(new AnimatedSprite.FrameInfo(grayBird.size(), 960 / inSampleSize, 205 / inSampleSize));

        List<AnimatedSprite.FrameInfo> greenBird = new ArrayList<>();
        greenBird.add(new AnimatedSprite.FrameInfo(greenBird.size(), 0 / inSampleSize, 410 / inSampleSize));
        greenBird.add(new AnimatedSprite.FrameInfo(greenBird.size(), 480 / inSampleSize, 410 / inSampleSize));
        greenBird.add(new AnimatedSprite.FrameInfo(greenBird.size(), 240 / inSampleSize, 410 / inSampleSize));
        greenBird.add(new AnimatedSprite.FrameInfo(greenBird.size(), 1440 / inSampleSize, 205 / inSampleSize));

        List<AnimatedSprite.FrameInfo> pinkBird = new ArrayList<>();
        pinkBird.add(new AnimatedSprite.FrameInfo(pinkBird.size(), 1200 / inSampleSize, 205 / inSampleSize));
        pinkBird.add(new AnimatedSprite.FrameInfo(pinkBird.size(), 720 / inSampleSize, 205 / inSampleSize));
        pinkBird.add(new AnimatedSprite.FrameInfo(pinkBird.size(), 480 / inSampleSize, 205 / inSampleSize));
        pinkBird.add(new AnimatedSprite.FrameInfo(pinkBird.size(), 960 / inSampleSize, 410 / inSampleSize));

        TextureRegion region = buildRegion(info, blueBird.get(0).regionX, blueBird.get(0).regionY, frameWidth, frameHeight);

        birds.add(blueBird);
        birds.add(greenBird);
        birds.add(pinkBird);
        birds.add(grayBird);

        bird = new Bird(region, birds, width, height);
    }

    private void prepareCircles(TextureInfo info) {
        List<Pair<TextureRegion, PointF>> infos = new ArrayList<>();
        infos.add(new Pair<>(buildRegion(info, 411, 1069, 411, 410), new PointF(0.5137f, 0.5125f)));
        infos.add(new Pair<>(buildRegion(info, 1361, 659, 407, 410), new PointF(0.5087f, 0.5125f)));
        infos.add(new Pair<>(buildRegion(info, 953, 659, 408, 410), new PointF(0.51f, 0.5125f)));
        infos.add(new Pair<>(buildRegion(info, 1229, 1069, 410, 410), new PointF(0.5125f, 0.5125f)));

        int count = 10;
        fallingCircles = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            Pair<TextureRegion, PointF> regionInfo = infos.get(random(infos.size()));
            TextureRegion region = regionInfo.first;
            float width = regionInfo.second.x;
            float height = regionInfo.second.y;
            FallingCircle circle = new FallingCircle(region, width, height);
            fallingCircles.add(circle);
        }
    }

    private void preparePiece(TextureInfo info) {
        List<Pair<TextureRegion, PointF>> infos = new ArrayList<>();
        infos.add(new Pair<>(buildRegion(info, 180, 659, 180, 295), new PointF(0.225f, 0.3687f)));
        infos.add(new Pair<>(buildRegion(info, 360, 659, 180, 299), new PointF(0.225f, 0.3687f)));
        infos.add(new Pair<>(buildRegion(info, 747, 0, 280, 187), new PointF(0.35f, 0.2337f)));
        infos.add(new Pair<>(buildRegion(info, 1670, 410, 195, 249), new PointF(0.2437f, 0.3112f)));
        infos.add(new Pair<>(buildRegion(info, 0, 0, 249, 180), new PointF(0.3112f, 0.225f)));
        infos.add(new Pair<>(buildRegion(info, 249, 0, 238, 180), new PointF(0.2975f, 0.225f)));
        infos.add(new Pair<>(buildRegion(info, 487, 0, 260, 186), new PointF(0.325f, 0.2325f)));
        infos.add(new Pair<>(buildRegion(info, 1440, 410, 230, 212), new PointF(0.2875f, 0.265f)));
        infos.add(new Pair<>(buildRegion(info, 0, 659, 180, 256), new PointF(0.225f, 0.32f)));
        piece = new Piece(infos);
    }

    @Override public void release() {
        super.release();
        glDeleteProgram(stencilProgramHandle);
        stencilProgramHandle = -1;
        if (atlasInfo != null) {
            int[] arr = new int[1];
            arr[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, arr, 0);
            GlUtil.checkGlError("Release " + getClass().getSimpleName());
        }
    }
}