package com.tme.moments.presentation.base;

import android.os.Bundle;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * @author zoopolitic.
 */
public abstract class BasePresenter<V> implements Presenter<V> {

    private CompositeDisposable compositeDisposable;

    protected V view;

    @Override public void attachView(V v) {
        if (v == null) {
            throw new NullPointerException("View cannot be null");
        }
        this.view = v;
    }

    @Override public void detachView() {
        safeDispose(compositeDisposable);
        this.view = null;
    }

    @Override public void onSaveInstanceState(Bundle outState) {
        // save state if needed
    }

    @Override public void onRestoreInstanceState(Bundle savedInstanceState) {
        // restore state if needed
    }

    protected Disposable disposeOnDetach(Disposable newDisposable) {
        initDisposableIfNeeded();
        compositeDisposable.add(newDisposable);
        return newDisposable;
    }

    private void initDisposableIfNeeded() {
        if (compositeDisposable == null || compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }
    }

    protected boolean safeDispose(Disposable disposable) {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
            return true;
        }
        return false;
    }

}
