package com.tme.moments.presentation.scene.f2.dreamland;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Petal extends Decal {

    private enum Direction {
        LEFT, RIGHT, BOTTOM
    }

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point 1
    private float p2x, p2y; // control point 2
    private float p3x, p3y; // end point

    private final boolean rotateAllAxis;
    private float z;
    private float speed;
    private float rotationSpeed;
    private float t;
    private long startDelayNanos;
    private Direction direction;
    private boolean inited;

    Petal(TextureRegion region, float width, float height, boolean rotateAllAxis) {
        this.rotateAllAxis = rotateAllAxis;
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        float minZ = -3.0f;
        // take a more than -1.0f (closest possible Z) as a maxZ and then clamp
        // it to [-1, -5] range. This will give more objects with -1.0 Z value
        float maxZ = 0.0f;

        z = random(minZ, maxZ);
        // reduce maxZ by half of biggest side of the object
        // to not being clipped during X and Y rotation
        z = Math.min(z, -1.0f - Math.max(getWidth(), getHeight()) / 2);
        boolean rotateLeft = random();
        direction = Direction.BOTTOM;
        boolean useSideDirection = random(5) == 1;
        if (useSideDirection) {
            boolean left = random();
            direction = left ? Direction.LEFT : Direction.RIGHT;
        }
        rotationSpeed = rotateLeft
                ? random(-175f, -140f) / Const.NANOS_IN_SECOND
                : random(140f, 175f) / Const.NANOS_IN_SECOND;
        init();
    }

    void step(long deltaNanos) {
        if (startDelayNanos >= 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float delta = speed * deltaNanos;
        t += delta;
        float single = (1 - t);
        float quadratic = (1 - t) * (1 - t);
        float cube = (1 - t) * (1 - t) * (1 - t);
        float x = cube * p0x + 3 * t * quadratic * p1x + 3 * t * t * single * p2x + t * t * t * p3x;
        float y = cube * p0y + 3 * t * quadratic * p1y + 3 * t * t * single * p2y + t * t * t * p3y;

        float deltaRotation = rotationSpeed * deltaNanos;
        rotateZ(deltaRotation);
        if (rotateAllAxis) {
            rotateX(deltaRotation);
            rotateY(deltaRotation);
        }
        setX(x);
        setY(y);

        if (t >= 1) {
            init();
        }
    }

    private void init() {
        float currX = getX();
        float currY = getY();

        float deltaZ = -1.0f - z; // -1 is a maxZ
        boolean outOfScreen = currY <= -1 - getHeight() / 2 - deltaZ;
        if (!inited || outOfScreen) {
            inited = true;
            speed = random(0.5f, 1.0f) / Const.NANOS_IN_SECOND;
            startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 15000));
            switch (direction) {
                case LEFT:
                    p0x = random(0.5f - deltaZ, 1.0f + deltaZ);
                    break;
                case RIGHT:
                    p0x = random(-1.0f - deltaZ, -0.5f + deltaZ);
                    break;
                case BOTTOM:
                    p0x = random(-1.0f - deltaZ, 1.0f + deltaZ);
                    break;
            }
            p0y = 1 + getHeight() / 2 + deltaZ;
            setRotation(0, 0, 0);
        } else {
            p0x = currX;
            p0y = currY;
        }
        float xMin = 0;
        float xMax = 0;
        float yMin = -0.3f;
        float yMax = -0.2f;
        switch (direction) {
            case LEFT:
                xMin = -0.1f;
                xMax = -0.03f;
                break;
            case RIGHT:
                xMin = 0.03f;
                xMax = 0.1f;
                break;
            case BOTTOM:
                boolean moveLeft = random();
                xMin = moveLeft ? -0.1f : 0.03f;
                xMax = moveLeft ? -0.03f : 0.1f;
                break;
        }
        p1x = p0x + random(xMin, xMax);
        p1y = p0y + random(yMin, yMax);

        p2x = p1x + random(xMin, xMax);
        p2y = p1y + random(yMin, yMax);

        p3x = p2x + random(xMin, xMax);
        p3y = p2y + random(yMin, yMax);
        t = 0;
        setPosition(p0x, p0y, z);
    }
}
