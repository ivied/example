package com.tme.moments.presentation.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * @param <T>
 * @author zoopolitic
 *         <p>
 *         To use class override {@link #getLayoutResId(int)}
 *         and return proper layoutResId for every list item
 */
public abstract class DataBoundAdapter<T> extends RecyclerView.Adapter<DataBoundViewHolder> {

    protected List<T> items;
    protected LayoutInflater layoutInflater;

    public DataBoundAdapter() {
        items = new ArrayList<>();
    }

    public DataBoundAdapter(List<T> items) {
        this.items = items;
    }

    protected abstract int getLayoutResId(int position);

    @Override
    public int getItemViewType(int position) {
        return getLayoutResId(position);
    }

    @Override
    public DataBoundViewHolder onCreateViewHolder(ViewGroup parent, int layoutId) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        return DataBoundViewHolder.create(layoutInflater, parent, layoutId);
    }

    @Override
    public void onBindViewHolder(DataBoundViewHolder holder, int position) {
        holder.bindTo(getItem(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public T getItem(int position) {
        if (position < 0 || position > getItemCount() - 1) {
            return null;
        }
        return items.get(position);
    }

    public List<T> getItems() {
        return items;
    }

    public void addAll(List<? extends T> items) {
        this.items.addAll(items);
    }

    public void addAll(int position, List<? extends T> items) {
        this.items.addAll(position, items);
    }

    public void add(T item) {
        items.add(item);
    }

    public void remove(T item) {
        items.remove(item);
    }

    public void remove(int position) {
        items.remove(position);
    }

    public void removeAll(List<? extends T> items) {
        this.items.removeAll(items);
    }

    public int indexOf(T item) {
        return items.indexOf(item);
    }

    public void set(int position, T item) {
        items.set(position, item);
    }

    public void add(int position, T item) {
        items.add(position, item);
    }

    public void clear() {
        items.clear();
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }
}
