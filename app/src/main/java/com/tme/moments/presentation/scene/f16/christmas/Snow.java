package com.tme.moments.presentation.scene.f16.christmas;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Snow extends Decal {

    enum Direction {
        LEFT, RIGHT
    }

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point 1
    private float p2x, p2y; // control point 2
    private float p3x, p3y; // end point

    private final float z;
    private final Direction direction;
    private float speed;
    private float t;
    private long startDelayNanos;
    private boolean inited;

    Snow(TextureRegion region, float width, float height, float z,
         Direction direction) {
        this.z = z;
        this.direction = direction;
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        init();
    }

    void step(long deltaNanos) {
        if (startDelayNanos >= 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float delta = speed * deltaNanos;
        t += delta;
        float single = (1 - t);
        float quadratic = (1 - t) * (1 - t);
        float cube = (1 - t) * (1 - t) * (1 - t);
        float x = cube * p0x + 3 * t * quadratic * p1x + 3 * t * t * single * p2x + t * t * t * p3x;
        float y = cube * p0y + 3 * t * quadratic * p1y + 3 * t * t * single * p2y + t * t * t * p3y;

        setX(x);
        setY(y);

        if (t >= 1) {
            init();
        }
    }

    private void init() {
        float currX = getX();
        float currY = getY();

        float deltaZ = -1.0f - z; // -1 is a maxZ
        boolean outOfScreen = direction == Direction.LEFT
                ? currX <= -1 - getWidth() / 2 - deltaZ
                : currX >= 1 + getWidth() / 2 + deltaZ;
        if (!inited || outOfScreen) {
            inited = true;
            speed = random(3.5f, 5.0f) / Const.NANOS_IN_SECOND;
            startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 15000));
            switch (direction) {
                case LEFT:
                    p0x = 1 + getWidth() / 2 + deltaZ;
                    break;
                case RIGHT:
                    p0x = -1 - getWidth() / 2 - deltaZ;
                    break;
            }
            p0y = random(-1 - deltaZ, 2.5f + deltaZ);
        } else {
            p0x = currX;
            p0y = currY;
        }
        float xMin = direction == Direction.LEFT ? -0.17f : 0.075f;
        float xMax = direction == Direction.LEFT ? -0.075f : 0.17f;
        float yMin = -0.2f;
        float yMax = 0.05f;
        p1x = p0x + random(xMin, xMax);
        p1y = p0y + random(yMin, yMax);

        p2x = p1x + random(xMin, xMax);
        p2y = p1y + random(yMin, yMax);

        p3x = p2x + random(xMin, xMax);
        p3y = p2y + random(yMin, yMax);
        t = 0;
        setPosition(p0x, p0y, z);
    }
}
