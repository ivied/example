package com.tme.moments.presentation.drawer;

import android.content.Context;

import com.tme.moments.R;
import com.tme.moments.recording.Quality;

/**
 * @author zoopolitic.
 */
class DialogVideoQualityItem extends DialogListItem {

    final Quality quality;

    DialogVideoQualityItem(Quality quality) {
        this.quality = quality;
    }

    @Override public String getTitle(Context context) {
        switch (quality) {
            case LOW:
                return context.getString(R.string.video_quality_low);
            case MEDIUM:
                return context.getString(R.string.video_quality_medium);
            case HIGH:
                return context.getString(R.string.video_quality_high);
            default:
                return "error";
        }
    }
}
