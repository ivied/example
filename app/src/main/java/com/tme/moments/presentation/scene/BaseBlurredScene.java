package com.tme.moments.presentation.scene;

import android.graphics.SurfaceTexture;
import android.support.annotation.CallSuper;
import android.util.Log;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.utils.GaussianUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import timber.log.Timber;

import static android.opengl.GLES20.GL_ALWAYS;
import static android.opengl.GLES20.GL_CLAMP_TO_EDGE;
import static android.opengl.GLES20.GL_COLOR_ATTACHMENT0;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_EQUAL;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_FRAMEBUFFER;
import static android.opengl.GLES20.GL_FRAMEBUFFER_BINDING;
import static android.opengl.GLES20.GL_FRAMEBUFFER_COMPLETE;
import static android.opengl.GLES20.GL_KEEP;
import static android.opengl.GLES20.GL_LINEAR;
import static android.opengl.GLES20.GL_NEAREST;
import static android.opengl.GLES20.GL_REPLACE;
import static android.opengl.GLES20.GL_RGBA;
import static android.opengl.GLES20.GL_STENCIL_BUFFER_BIT;
import static android.opengl.GLES20.GL_STENCIL_TEST;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_S;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_T;
import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.GL_UNSIGNED_BYTE;
import static android.opengl.GLES20.glBindFramebuffer;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glCheckFramebufferStatus;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glColorMask;
import static android.opengl.GLES20.glDeleteFramebuffers;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glFramebufferTexture2D;
import static android.opengl.GLES20.glGenFramebuffers;
import static android.opengl.GLES20.glGenTextures;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetIntegerv;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glStencilFunc;
import static android.opengl.GLES20.glStencilOp;
import static android.opengl.GLES20.glTexImage2D;
import static android.opengl.GLES20.glTexParameterf;
import static android.opengl.GLES20.glTexParameteri;
import static android.opengl.GLES20.glUniform1f;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;

/**
 * @author zoopolitic.
 */
public abstract class BaseBlurredScene extends BaseScene {

    protected Sprite stencilMask;
    protected Sprite overlay;

    private int stencilProgramHandle = -1;
    private int uStencilProjectionMatrixLoc = -1;
    private int aStencilPositionHandle = -1;
    private int aStencilColorHandle = -1;
    private int aStencilTexCoordHandle = -1;

    private final FloatBuffer blurCameraVertexBuffer;
    private final FloatBuffer blurCameraTextureBuffer;

    private int cameraBlurProgramHandle = -1;
    private int uCameraBlurTransformMHandle = -1;
    private int aCameraBlurBlurPositionHandle = -1;
    private int aCameraBlurBlurTextureHandle = -1;
    private int uCameraBlurTexelHeightOffsetHandle = -1;
    private int uCameraBlurTexelWidthOffsetHandle = -1;

    private int imageBlurProgramHandle = -1;
    private int uImageBlurTransformMHandle = -1;
    private int aImageBlurPositionHandle = -1;
    private int aImageBlurTextureHandle = -1;
    private int uImageBlurHeightOffsetHandle = -1;
    private int uImageBlurWidthOffsetHandle = -1;

    private int offscreenFBOHandle = -1;
    private int offscreenTextureHandle = -1;

    private int blurRadius = 6;
    private float sigma;
    private int[] values = new int[1];

    private static final FloatBuffer FULL_SCREEN_BUF = GlUtil.createFloatBuffer(new float[]{
            -1.0f, 1.0f,    // top left
            -1.0f, -1.0f,   // bottom left
            1.0f, 1.0f,     // top right
            1.0f, -1.0f,    // bottom right
    });

    private static final FloatBuffer FULL_SCREEN_TEX_BUF = GlUtil.createFloatBuffer(new float[]{
            0.0f, 1.0f, // top left
            0.0f, 0.0f, // bottom left
            1.0f, 1.0f, // top right
            1.0f, 0.0f, // bottom right
    });

    private FloatBuffer FULL_SCREEN_IMAGE_BUF;

    private static final FloatBuffer FULL_SCREEN_IMAGE_TEX_BUF = GlUtil.createFloatBuffer(new float[]{
            0.0f, 0.0f, // bottom left
            0.0f, 1.0f, // top left
            1.0f, 0.0f, // bottom right
            1.0f, 1.0f, // top right
    });

    private static final float TEXEL_OFFSET = 0.002f;

    public BaseBlurredScene() {
        int numOfVertices = 4;
        int coordinatesPerVertex = 2;
        int sizeOfFloatInBytes = Float.SIZE / 8;
        blurCameraVertexBuffer = ByteBuffer
                .allocateDirect(numOfVertices * coordinatesPerVertex * sizeOfFloatInBytes)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        blurCameraTextureBuffer = ByteBuffer
                .allocateDirect(numOfVertices * coordinatesPerVertex * sizeOfFloatInBytes)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        sigma = (int) Math.floor(Math.sqrt(Math.log(0.00390625d * Math.sqrt((Math.PI * 2.0d) * Math.pow((double) blurRadius, 2.0d))) * (-2.0d * Math.pow((double) blurRadius, 2.0d))));
        sigma -= sigma % 2;

        FULL_SCREEN_IMAGE_BUF = ByteBuffer
                .allocateDirect(numOfVertices * coordinatesPerVertex * sizeOfFloatInBytes)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
    }

    protected abstract int getStencilResId();

    protected abstract int getOverlayResId();

    protected boolean overlayIsUnderCamera() {
        return false;
    }

    protected float getOverlayWidth() {
        return getProjectionWidth();
    }

    protected float getOverlayHeight() {
        return getProjectionHeight();
    }

    protected float getOverlayPositionX() {
        return 0;
    }

    protected float getOverlayPositionY() {
        return 0;
    }

    @CallSuper
    @Override
    protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        prepareStencilMask();
        prepareOverlay();
        prepareOffscreenFBO(surfaceWidth, surfaceHeight);
    }

    private void prepareStencilMask() {
        TextureInfo info = openGLLoader.loadTexture(getStencilResId());
        int w = info.imageWidth;
        int h = info.imageHeight;
        TextureRegion region = new TextureRegion(info.textureHandle, w, h, 0, 0, w, h);
        float width = getDesiredCameraWidthFraction() * getProjectionWidth();
        float height = getDesiredCameraHeightFraction() * getProjectionHeight();
        stencilMask = new Sprite(region, width, height);
        float cameraX = getCameraPositionX();
        float cameraY = getCameraPositionY();
        stencilMask.setPosition(cameraX, cameraY, -1);
    }

    private void prepareOverlay() {
        TextureInfo info = openGLLoader.loadTexture(getOverlayResId());
        float width = getOverlayWidth();
        float height = getOverlayHeight();
        TextureRegion region = new TextureRegion(info.textureHandle, info.imageWidth, info.imageHeight,
                0, 0, info.imageWidth, info.imageHeight);
        overlay = new Sprite(region, width, height);
        overlay.setPosition(getOverlayPositionX(), getOverlayPositionY(), -1);
    }

    @Override
    public void updateCameraParams() {
        int cameraPreviewWidth = config.getCameraPreviewWidth();
        int cameraPreviewHeight = config.getCameraPreviewHeight();

        float cameraX = getCameraPositionX();
        float cameraY = getCameraPositionY();

        float desiredCameraWidth = getDesiredCameraWidthFraction();
        float desiredCameraHeight = getDesiredCameraHeightFraction();
        // update params of camera output
        fillCameraVertexBuffer(cameraVertexBuffer, cameraPreviewWidth, cameraPreviewHeight,
                surfaceWidth, surfaceHeight, desiredCameraWidth, desiredCameraHeight, cameraX, cameraY);
        fillCameraTextureBuffer(cameraTextureBuffer);

        // update params of blurred camera background output
        desiredCameraWidth = 1; // 1 is for full surface width
        desiredCameraHeight = 1; // 1 is for full surface height
        cameraX = 0.0f;
        cameraY = 0.0f;
        fillCameraVertexBuffer(blurCameraVertexBuffer, cameraPreviewWidth, cameraPreviewHeight,
                surfaceWidth, surfaceHeight, desiredCameraWidth, desiredCameraHeight, cameraX, cameraY);
        fillCameraTextureBuffer(blurCameraTextureBuffer);
    }

    @Override
    protected void prepareImageDecal(TextureInfo imageInfo, int surfaceWidth, int surfaceHeight) {
        super.prepareImageDecal(imageInfo, surfaceWidth, surfaceHeight);
        if (imageInfo != null) {
            prepareBlurredBackgroundImage(imageInfo, surfaceWidth, surfaceHeight);
        }
    }

    private void prepareBlurredBackgroundImage(TextureInfo imageInfo, int surfaceWidth,
                                               int surfaceHeight) {
        int imageWidth = imageInfo.imageWidth;
        int imageHeight = imageInfo.imageHeight;

        float ratioX = (float) imageWidth / surfaceWidth;
        float ratioY = (float) imageHeight / surfaceHeight;

        // find smallest side of the image to fill whole screen with both sides
        float maxRatio = Math.min(ratioX, ratioY);

        // find ratio to adjust biggest side to 1
        float req = 1 / maxRatio;

        // multiply each side by ratio to keep aspect ratio
        ratioX *= req;
        ratioY *= req;

        float width = ratioX * getProjectionWidth();
        float height = ratioY * getProjectionHeight();

        float left = -width / 2;
        float top = height / 2;
        float right = width / 2;
        float bottom = -height / 2;
        FULL_SCREEN_IMAGE_BUF.clear();
        FULL_SCREEN_IMAGE_BUF.put(new float[]{
                left, top,
                left, bottom,
                right, top,
                right, bottom,
        });
        FULL_SCREEN_IMAGE_BUF.flip();
        FULL_SCREEN_IMAGE_BUF.position(0);
    }

    protected void onBlurredImageDrawn(long deltaNanos) {
    }

    protected void onCameraDrawn(long deltaNanos) {
    }

    protected void onOverlayDrawn(long deltaNanos) {

    }

    @Override
    public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        if (mode == Mode.IMAGE && image != null) {
            drawBlurredImage();
        } else {
            if (cameraTexture != null) {
                cameraTexture.updateTexImage();
                // sometimes during heavy cpu work (for instance during recording,
                // switching scenes and switching camera sequentially) getTransformMatrix returns
                // identity matrix for the first call and then on every other call returns
                // "normal" matrix in this case - camera image will be flipped for the first
                // frame drawn after camera switch. So we getting transformM into temp matrix
                // then check if it is not identity and if it isn't - saving it to transformM
                cameraTexture.getTransformMatrix(transformM);
            }
            drawBlurredCamera();
        }
        onBlurredImageDrawn(deltaNanos);

        if (overlayIsUnderCamera()) {
            drawOverlay(deltaNanos);
        }
        glEnable(GL_STENCIL_TEST);

        glColorMask(false, false, false, false);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

        // draw mask
        glUseProgram(stencilProgramHandle);
        stencilMask.draw(uStencilProjectionMatrixLoc, projectionMatrix, aStencilPositionHandle,
                aStencilTexCoordHandle, aStencilColorHandle);

        glColorMask(true, true, true, true);
        glStencilFunc(GL_EQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

        if (mode == Mode.IMAGE) {
            glUseProgram(mainProgramHandle);
            drawImage();
        } else {
            drawCamera();
        }

        glDisable(GL_STENCIL_TEST);
        onCameraDrawn(deltaNanos);
        if (!overlayIsUnderCamera()) {
            drawOverlay(deltaNanos);
            onOverlayDrawn(deltaNanos);
        }
    }

    protected void drawOverlay(long deltaNanos) {
        glUseProgram(mainProgramHandle);
        overlay.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
    }

    private void drawCamera() {
        // during camera switch sometimes initial transform matrix is identity which causes image to rotate
        // so we want to skip this frame
        if (isMatrix4Identity(transformM)) return;
        glUseProgram(cameraProgramHandle);
        glUniformMatrix4fv(uTransformMHandle, 1, false, transformM, 0);
        glVertexAttribPointer(aCameraPositionHandle, 2, GL_FLOAT, false, 0, cameraVertexBuffer);
        glVertexAttribPointer(aCameraTextureHandle, 2, GL_FLOAT, false, 0, cameraTextureBuffer);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }

    private void drawBlurredImage() {
        glUseProgram(imageBlurProgramHandle);

        glGetIntegerv(GL_FRAMEBUFFER_BINDING, values, 0);
        int currentFrameBuffer = values[0];
        glBindFramebuffer(GL_FRAMEBUFFER, offscreenFBOHandle);

        glClear(GL_COLOR_BUFFER_BIT);
        glBindTexture(GL_TEXTURE_2D, image.getTextureRegion().getTextureHandle());

        glUniformMatrix4fv(uImageBlurTransformMHandle, 1, false, GlUtil.IDENTITY_MATRIX, 0);
        glUniform1f(uImageBlurWidthOffsetHandle, TEXEL_OFFSET);
        glUniform1f(uImageBlurHeightOffsetHandle, 0);
        glVertexAttribPointer(aImageBlurPositionHandle, 2, GL_FLOAT, false, 0, FULL_SCREEN_BUF);
        glVertexAttribPointer(aImageBlurTextureHandle, 2, GL_FLOAT, false, 0, FULL_SCREEN_TEX_BUF);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        glBindFramebuffer(GL_FRAMEBUFFER, currentFrameBuffer);

        glBindTexture(GL_TEXTURE_2D, offscreenTextureHandle);
        glUniformMatrix4fv(uImageBlurTransformMHandle, 1, false, GlUtil.IDENTITY_MATRIX, 0);
        glUniform1f(uImageBlurWidthOffsetHandle, 0);
        glUniform1f(uImageBlurHeightOffsetHandle, TEXEL_OFFSET);
        glVertexAttribPointer(aImageBlurPositionHandle, 2, GL_FLOAT, false, 0, FULL_SCREEN_IMAGE_BUF);
        glVertexAttribPointer(aImageBlurTextureHandle, 2, GL_FLOAT, false, 0, FULL_SCREEN_IMAGE_TEX_BUF);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    private void drawBlurredCamera() {
        // during camera switch sometimes initial transform matrix is identity which causes image to rotate
        // so we want to skip this frame
        if (isMatrix4Identity(transformM)) return;
        glUseProgram(cameraBlurProgramHandle);
        glGetIntegerv(GL_FRAMEBUFFER_BINDING, values, 0);
        int currentFrameBuffer = values[0];
        // bind offscreen buffer
        glBindFramebuffer(GL_FRAMEBUFFER, offscreenFBOHandle);
        glClear(GL_COLOR_BUFFER_BIT);
        // first blur pass (horizontal)
        // blur horizontally and draw to offscreen buffer
        glUniformMatrix4fv(uCameraBlurTransformMHandle, 1, false, transformM, 0);
        // bug here, passing texelWidthOffset offset actually blur vertically instead of horizontally.
        // so to blur vertically we need to pass texelWidthOffset, to blur horizontally - pass
        // texelHeightOffset. Second pass works fine though, but should be because of transformation
        // matrix from camera that is applied to texture coordinates in vertex shader. My guess
        // is that blur happens before image rotation although don't get why
        glUniform1f(uCameraBlurTexelWidthOffsetHandle, 0);
        glUniform1f(uCameraBlurTexelHeightOffsetHandle, TEXEL_OFFSET);
        glVertexAttribPointer(aCameraBlurBlurPositionHandle, 2, GL_FLOAT, false, 0, blurCameraVertexBuffer);
        glVertexAttribPointer(aCameraBlurBlurTextureHandle, 2, GL_FLOAT, false, 0, blurCameraTextureBuffer);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        // bind previous buffer
        glBindFramebuffer(GL_FRAMEBUFFER, currentFrameBuffer);

        glUseProgram(imageBlurProgramHandle);
        // bind offscreen texture
        glBindTexture(GL_TEXTURE_2D, offscreenTextureHandle);

        // second blur pass (vertical)
        glUniformMatrix4fv(uImageBlurTransformMHandle, 1, false, transformM, 0);
        glUniform1f(uImageBlurWidthOffsetHandle, 0);
        glUniform1f(uImageBlurHeightOffsetHandle, TEXEL_OFFSET);
        glVertexAttribPointer(aImageBlurPositionHandle, 2, GL_FLOAT, false, 0, blurCameraVertexBuffer);
        glVertexAttribPointer(aImageBlurTextureHandle, 2, GL_FLOAT, false, 0, blurCameraTextureBuffer);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        // unbind offscreen texture
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    /**
     * Prepares the off-screen framebuffer
     */
    private void prepareOffscreenFBO(int width, int height) {
        GlUtil.checkGlError("prepareOffscreenFBO start");

        // Create a texture object and bind it.  This will be the color buffer.
        glGenTextures(1, values, 0);
        GlUtil.checkGlError("glGenTextures");
        offscreenTextureHandle = values[0];   // expected > 0
        glBindTexture(GL_TEXTURE_2D, offscreenTextureHandle);
        GlUtil.checkGlError("glBindTexture " + offscreenTextureHandle);

        // Create texture storage.
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, null);

        // Set parameters.  We're probably using non-power-of-two dimensions, so
        // some values may not be available for use.
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        GlUtil.checkGlError("glTexParameter");

        // Create framebuffer object and bind it.
        glGenFramebuffers(1, values, 0);
        GlUtil.checkGlError("glGenFramebuffers offscreen FBO");
        offscreenFBOHandle = values[0];    // expected > 0
        glBindFramebuffer(GL_FRAMEBUFFER, offscreenFBOHandle);
        GlUtil.checkGlError("glBindFramebuffer offscreen FBO " + offscreenFBOHandle);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, offscreenTextureHandle, 0);
        GlUtil.checkGlError("glFramebufferTexture2D");

        // See if GLES is happy with all this.
        int status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE) {
            throw new RuntimeException("Offscreen FBO not complete, status=" + status);
        }

        // Switch back to the default framebuffer.
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        GlUtil.checkGlError("prepareOffscreenFBO done");
    }

    @Override
    public void init() {
        Timber.e("init");
        super.init();
        if (stencilProgramHandle == -1) {
            prepareStencilProgram();
        }
//        if (cameraBlurProgramHandle == -1) {
//            prepareCameraBlurProgram();
//        }

    }

    @Override
    public void setup(TextureInfo imageInfo, int width, int height) {
        super.setup(imageInfo, width, height);
        updateBlurProgram();
    }

    @Override
    public void release() {
        super.release();

        glDeleteProgram(stencilProgramHandle);
        stencilProgramHandle = -1;

//        glDeleteProgram(cameraBlurProgramHandle);
//        cameraBlurProgramHandle = -1;

        releaseBlurProgram();

        if (offscreenTextureHandle != -1) {
            int[] values = new int[1];
            values[0] = offscreenTextureHandle;
            glDeleteTextures(1, values, 0);
            GlUtil.checkGlError("Delete offscreen texture");
        }

        if (offscreenFBOHandle !=  -1) {
            values[0] = offscreenFBOHandle;
            glDeleteFramebuffers(1, values, 0);
            offscreenFBOHandle = -1;
            GlUtil.checkGlError("Release " + getClass().getSimpleName());
        }
    }

    private void prepareStencilProgram() {
        stencilProgramHandle =
                openGLLoader.createProgram(R.raw.vertex_shader, R.raw.stencil_fragment_shader);

        aStencilPositionHandle = glGetAttribLocation(stencilProgramHandle, "a_position");
        Log.i("location", String.valueOf(stencilProgramHandle));
        GlUtil.checkLocation(aStencilPositionHandle, "a_position");
        glEnableVertexAttribArray(aStencilPositionHandle);

        aStencilColorHandle = glGetAttribLocation(stencilProgramHandle, "a_color");
        GlUtil.checkLocation(aStencilColorHandle, "a_color");
        glEnableVertexAttribArray(aStencilColorHandle);

        aStencilTexCoordHandle = glGetAttribLocation(stencilProgramHandle, "a_texCoord");
        GlUtil.checkLocation(aStencilTexCoordHandle, "a_texCoord");
        glEnableVertexAttribArray(aStencilTexCoordHandle);

        uStencilProjectionMatrixLoc = glGetUniformLocation(stencilProgramHandle, "u_projTrans");
        GlUtil.checkLocation(uStencilProjectionMatrixLoc, "u_projTrans");
    }

    @Override
    public void onCameraModeEnabled() {
        Timber.d("onCameraModeEnabled");
        super.onCameraModeEnabled();
        updateBlurProgram();
    }

    @Override
    public void onImageModeEnabled(TextureInfo imageInfo) {
        Timber.d("onImageModeEnabled");
        super.onImageModeEnabled(imageInfo);
        updateBlurProgram();
    }

    private void releaseBlurProgram() {
        if (imageBlurProgramHandle != -1) {
            glDeleteProgram(imageBlurProgramHandle);
            imageBlurProgramHandle = -1;
        }
    }

    private void updateBlurProgram() {
        releaseBlurProgram();
        prepareBlurProgram(mode == Mode.CAMERA || mode == null);
    }

    private void prepareBlurProgram(boolean cameraEnable) {
        String vertexShader = GaussianUtils.vertexShaderForOptimizedBlurOfRadius(blurRadius, sigma);
        String fragShader = GaussianUtils.fragmentShaderForOptimizedBlurOfRadius(blurRadius, sigma, cameraEnable);
        imageBlurProgramHandle = openGLLoader.createProgram(vertexShader, fragShader);
        cameraBlurProgramHandle = imageBlurProgramHandle;
        uImageBlurTransformMHandle = glGetUniformLocation(imageBlurProgramHandle, "uTransformM");
        uCameraBlurTransformMHandle = uImageBlurTransformMHandle;
        GlUtil.checkLocation(uImageBlurTransformMHandle, "uTransformM");

        uImageBlurWidthOffsetHandle = glGetUniformLocation(imageBlurProgramHandle, "uTexelWidthOffset");
        uCameraBlurTexelWidthOffsetHandle = uImageBlurWidthOffsetHandle;
        GlUtil.checkLocation(uImageBlurWidthOffsetHandle, "uTexelWidthOffset");

        uImageBlurHeightOffsetHandle = glGetUniformLocation(imageBlurProgramHandle, "uTexelHeightOffset");
        uCameraBlurTexelHeightOffsetHandle = uImageBlurHeightOffsetHandle;
        GlUtil.checkLocation(uImageBlurHeightOffsetHandle, "uTexelHeightOffset");

        aImageBlurTextureHandle = glGetAttribLocation(imageBlurProgramHandle, "aTexCoords");
        aCameraBlurBlurTextureHandle = aImageBlurTextureHandle;
        GlUtil.checkLocation(aImageBlurTextureHandle, "aTexCoords");
        glEnableVertexAttribArray(aImageBlurTextureHandle);

        aImageBlurPositionHandle = glGetAttribLocation(imageBlurProgramHandle, "aPosition");
        aCameraBlurBlurPositionHandle = aImageBlurPositionHandle;
        GlUtil.checkLocation(aImageBlurPositionHandle, "aPosition");
        glEnableVertexAttribArray(aImageBlurPositionHandle);
    }
}
