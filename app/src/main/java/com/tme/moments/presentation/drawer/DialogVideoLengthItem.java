package com.tme.moments.presentation.drawer;

import android.annotation.SuppressLint;
import android.content.Context;

import com.tme.moments.R;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author zoopolitic.
 */
class DialogVideoLengthItem extends DialogListItem {

    final int duration;

    DialogVideoLengthItem(int duration) {
        this.duration = duration;
    }

    @Override public String getTitle(Context context) {
        NumberFormat nf = NumberFormat.getInstance(new Locale("en","US"));
        String sDistance = nf.format(duration);
        return context.getString(R.string.video_length_item_title, sDistance);
    }

}
