package com.tme.moments.presentation.drawer;

import com.google.auto.value.AutoValue;
import com.tme.moments.R;

/**
 * @author zoopolitic.
 */
@AutoValue
public abstract class DrawerThemeItem implements DrawerItem {

    public static DrawerThemeItem create() {
        return new AutoValue_DrawerThemeItem();
    }

    @Override public int getLayoutResId() {
        return R.layout.drawer_theme_item;
    }
}
