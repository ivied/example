package com.tme.moments.presentation.scene.f12.music;

import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.math.Vector2;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Guitar extends Decal {

    private static final float MAX_ROTATION_ANGLE = 5f;

    private float speed;
    private float rotationAngle;
    private boolean clockwise;
    private float angleDecelerationSpeed;
    private Interpolator interpolator;
    private long delayNanos;
    private float t;

    Guitar(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        interpolator = new AccelerateDecelerateInterpolator();
        transformationOffset = new Vector2(0, -height / 2); // bottom middle point
        init();
        delayNanos = TimeUnit.MILLISECONDS.toNanos(0);
    }

    void step(long deltaNanos) {
        if (delayNanos > 0) {
            delayNanos -= deltaNanos;
            return;
        }
        float deltaT = speed * deltaNanos;
        t += deltaT;
        t = Math.max(0, Math.min(1.0f, t));
        float interpolatedValue = clockwise
                ? interpolator.getInterpolation(t) * rotationAngle
                : 360 - interpolator.getInterpolation(t) * rotationAngle;
        setRotationZ(interpolatedValue);
        if (t == 1) {
            speed *= -1;
        }
        if (t == 0 && speed <= 0) {
            init();
        }
    }

    private void init() {
        if (rotationAngle == 0) {
            rotationAngle = MAX_ROTATION_ANGLE;
            clockwise = random();
            delayNanos = TimeUnit.MILLISECONDS.toNanos(random(2500, 3500));
            angleDecelerationSpeed = random(0.4f, 0.7f);
        } else {
            rotationAngle = Math.max(0, rotationAngle - angleDecelerationSpeed);
        }
        speed = 1.0f / Const.NANOS_IN_SECOND;
    }
}
