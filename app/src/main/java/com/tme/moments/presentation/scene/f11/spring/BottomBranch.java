package com.tme.moments.presentation.scene.f11.spring;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class BottomBranch extends Decal {

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float speed;
    private float t;
    private long startDelayNanos;

    BottomBranch(TextureRegion region, float width, float height, float p0x, float p0y) {
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        this.p0x = p0x;
        this.p0y = p0y;
        init();
    }

    public void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float delta = speed * deltaNanos;
        t += delta;
        t = Math.max(0.0f, Math.min(1.0f, t));

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        setX(x);
        setY(y);

        if (t == 0 && speed < 0) {
            init();
        }
        if (t == 1) {
            speed *= -1;
        }
    }

    private void init() {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 4000));
        speed = random(0.15f, 0.25f) / Const.NANOS_IN_SECOND;
        p1x = p0x + random(0.03f, 0.06f);
        p1y = p0y - random(0.03f, 0.06f);
        p2x = p1x + random(0.03f, 0.06f);
        p2y = p1y - random(0.03f, 0.06f);
        setPosition(p0x, p0y, -1.0f);
    }
}
