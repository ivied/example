package com.tme.moments.presentation;

/**
 * @author zoopolitic.
 */
public class Const {

    private Const() {
    }

    public static final long NANOS_IN_SECOND = 1_000_000_000L;
    public static final long NANOS_IN_MILLISECOND = 1_000_000L;

}
