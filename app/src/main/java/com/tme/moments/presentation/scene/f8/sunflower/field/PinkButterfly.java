package com.tme.moments.presentation.scene.f8.sunflower.field;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.Const;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class PinkButterfly extends AnimatedSprite {

    private final float speed;
    private float startDelayNanos;

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float t;
    private boolean stopped;
    private float startZ;
    private float endZ;

    PinkButterfly(TextureRegion region, List<FrameInfo> frameInfoList, float width, float height,
                  float speedPerSecond) {
        super(region, frameInfoList, width, height);
        this.speed = speedPerSecond / Const.NANOS_IN_SECOND;
        init();
        t = 0.12f;
        setStartDelay(0);
    }

    private void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    @Override protected int getDesiredFPS() {
        return 25;
    }

    boolean isStopped() {
        return stopped;
    }

    void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

    @Override
    public void draw(int uProjectionMatrixLoc, float[] projectionMatrix, int aPositionHandle,
                     int aTexCoordHandle, int aColorHandle) {
        if (stopped) {
            return;
        }
        super.draw(uProjectionMatrixLoc, projectionMatrix, aPositionHandle, aTexCoordHandle, aColorHandle);
    }

    @Override public void step(long deltaNanos) {
        if (stopped) {
            return;
        }
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float delta = speed * deltaNanos;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;
        float z = startZ + (endZ - startZ) * t;

        setPosition(x, y, z);
        t += delta;

        float fadeOutThreshold = 0.8f;
        if (t >= fadeOutThreshold) {
            float fraction = (t - fadeOutThreshold) / (1 - fadeOutThreshold);
            setAlpha(1 - fraction);
        }

        if (t >= 1) {
            init();
        }
        super.step(deltaNanos);
    }

    private void init() {
        stopped = true;
        startZ = -1;
        endZ = -4f;
        p0x = random(-1.0f, 1.0f);
        p0y = -1 - getHeight();
        p1x = p0x + random(-0.75f, 0.75f);
        p1y = p0y + random(-0.8f, -0.5f);

        if (p1x > p0x) {
            p2x = p1x - endZ;
        } else {
            p2x = p1x + endZ;
        }
        p2y = -1.5f;
        setPosition(p0x, p0y, startZ);
        setAlpha(1);
        t = 0;
        boolean flyLeft = p0x > p2x;
        setScaleX(flyLeft ? 1.25f : -1.25f);
        setScaleY(1.25f);
        setStartDelay(random(1000, 2500));
    }
}
