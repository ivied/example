package com.tme.moments.presentation.scene.f5.kids.fun;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

/**
 * @author zoopolitic.
 */
class Head extends Decal {

    private float moveSpeedNanos;
    private float rotationSpeedNanos;

    private final float minX;
    private final float maxX;
    private final float minY;
    private final float maxY;
    private int shakeCount;
    private int maxShakeCount;
    private boolean moveFromSideToSide;

    Head(TextureRegion region, float width, float height, float x, float y,
         float moveSpeedPerSecond, float rotationSpeedDegreesPerSecond,
         float moveIncline) {
        this.moveSpeedNanos = moveSpeedPerSecond / Const.NANOS_IN_SECOND;
        this.rotationSpeedNanos = rotationSpeedDegreesPerSecond / Const.NANOS_IN_SECOND;
        this.minX = x - moveIncline;
        this.maxX = x + moveIncline;
        this.minY = y - moveIncline;
        this.maxY = y + moveIncline;
        float z = -1.0f;
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        setPosition(x, y, z);
    }

    void setMoveFromSideToSide(boolean moveFromSideToSide) {
        this.moveFromSideToSide = moveFromSideToSide;
    }

    void setMaxShakeCount(int maxShakeCount) {
        this.maxShakeCount = maxShakeCount;
    }

    void resetShakeCount() {
        shakeCount = 0;
    }

    boolean isFinished() {
        return shakeCount == maxShakeCount;
    }

    void step(long deltaNanos) {
        if (shakeCount == maxShakeCount) {
            return;
        }
        if (moveFromSideToSide) {
            float rotateDelta = rotationSpeedNanos * deltaNanos;
            rotateZ(rotateDelta);

            float moveDelta = moveSpeedNanos * deltaNanos;
            float newX = getX() + moveDelta;
            newX = Math.max(minX, Math.min(maxX, newX));
            setX(newX);
            if (newX == maxX || newX == minX) {
                shakeCount++;
                moveSpeedNanos *= -1;
                rotationSpeedNanos = -rotationSpeedNanos;
            }
        } else {
            float moveDelta = moveSpeedNanos * deltaNanos;
            float newY = getY() + moveDelta;
            newY = Math.max(minY, Math.min(maxY, newY));
            setY(newY);
            if (newY == maxY || newY == minY) {
                shakeCount++;
                moveSpeedNanos *= -1;
            }
        }
    }
}
