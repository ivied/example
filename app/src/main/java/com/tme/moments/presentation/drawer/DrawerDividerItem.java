package com.tme.moments.presentation.drawer;

import com.google.auto.value.AutoValue;
import com.tme.moments.R;

/**
 * @author zoopolitic.
 */
@AutoValue
public abstract class DrawerDividerItem implements DrawerItem {

    public static DrawerDividerItem create() {
        return new AutoValue_DrawerDividerItem();
    }

    @Override public int getLayoutResId() {
        return R.layout.drawer_divider_item;
    }
}
