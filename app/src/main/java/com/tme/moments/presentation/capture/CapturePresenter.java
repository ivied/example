package com.tme.moments.presentation.capture;

import android.content.Context;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.tme.moments.R;
import com.tme.moments.math.MathUtils;
import com.tme.moments.presentation.RecordMode;
import com.tme.moments.presentation.base.BasePresenter;
import com.tme.moments.presentation.core.Config;
import com.tme.moments.presentation.utils.MediaUtils;
import com.tme.moments.recording.Quality;
import com.tme.moments.recording.RecordConfig;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import timber.log.Timber;

import static com.tme.moments.presentation.capture.CapturePresenter.ImportStatus.INVALID;
import static com.tme.moments.presentation.capture.CapturePresenter.ImportStatus.VALID;

/**
 * @author zoopolitic.
 */
public class CapturePresenter extends BasePresenter<CapturePresenter.View> {

    private static final String TAG = CapturePresenter.class.getSimpleName();
    private boolean isRewardLoading;

    public interface View {
        void render(CaptureModel model);
    }

    private Config config;
    private boolean recording;
    private boolean capturePhotoInProgress;
    private final Set<String> forceConvertMimeTypes;

    public enum ImportStatus {
        VALID, VALID_NEED_CONVERSION, INVALID
    }

    CapturePresenter(Config config) {
        this.config = config;
        forceConvertMimeTypes = new HashSet<>();
        forceConvertMimeTypes.addAll(Arrays.asList("audio/flac"));
    }

    private void render() {
        // since render thread callbacks can arrive after view is detached
        // we need to check view availability here
        if (view == null) {
            StackTraceElement[] cause = Thread.currentThread().getStackTrace();
            if (cause != null) {
                for (StackTraceElement element : cause) {
                    Log.w(TAG, element.getFileName() + ": " + element.getClassName() + " " + element.getMethodName());
                }
            }
            return;
        }
        Uri imageUri = config.getImageUri();
        boolean imagePicked = imageUri != null;
        boolean pickImageButtonEnabled = !recording && !capturePhotoInProgress;
        boolean sceneCarouselEnabled = !capturePhotoInProgress;
        boolean recordButtonEnabled = !capturePhotoInProgress;
        boolean settingsButtonEnabled = !recording && !capturePhotoInProgress;
        boolean sidebarEnabled = !recording && !capturePhotoInProgress;

        RecordMode mode = config.getRecordMode();
        boolean switchModeButtonEnabled = !recording && !capturePhotoInProgress;
        int switchModeButtonAttrResId = mode == RecordMode.CAPTURE_IMAGE
                ? R.attr.recordVideoIcon
                : R.attr.captureCameraImageIcon;

        boolean actionButtonEnabled = !capturePhotoInProgress;
        int audioImportIcon =  config.isShowMusicAd()?
                R.attr.importAudioBlockedIcon :
                R.attr.importAudioIcon;
        int fastScreensShotIcon = recording
                ? R.drawable.ic_fast_screenshot_disabled
                : R.drawable.ic_fast_screenshot;
        int actionButtonIcon = imagePicked
                ? R.drawable.ic_clear_image
                : R.drawable.ic_switch_camera;

        int objectsButtonIcon = config.isObjectsEnabled()
                ? R.drawable.ic_objects_on
                : R.drawable.ic_objects_off;
        int backgroundEffectsIcon = config.isBackgroundEffectsEnabled()
                ? R.drawable.ic_background_effects_on
                : R.drawable.ic_background_effects_off;
        boolean importAudioButtonEnabled = !recording && !capturePhotoInProgress;
        //noinspection UnnecessaryLocalVariable
        boolean switchModeArrowsButtonVisible = imagePicked;
        //noinspection UnnecessaryLocalVariable
        boolean fastScreenshotButtonEnabled = !recording && !capturePhotoInProgress;
        boolean switchModeArrowsButtonEnabled = !recording && !capturePhotoInProgress;

        boolean soundOfButtons = !recording;

        view.render(CaptureModel.create(pickImageButtonEnabled,
                switchModeButtonAttrResId, switchModeButtonEnabled, switchModeArrowsButtonVisible, switchModeArrowsButtonEnabled,
                actionButtonEnabled, actionButtonIcon, sceneCarouselEnabled, recordButtonEnabled,
                objectsButtonIcon, backgroundEffectsIcon,fastScreensShotIcon, importAudioButtonEnabled,
                settingsButtonEnabled, sidebarEnabled, fastScreenshotButtonEnabled, soundOfButtons,
                audioImportIcon));
    }

    @Override
    public void attachView(View view) {
        super.attachView(view);
        render();
    }

    @Override
    public void detachView() {
        super.detachView();
        capturePhotoInProgress = false;
        if (recording) {
            recording = false;
            deleteOutputFile();
        }
    }

    void setObjectsEnabled(boolean enabled) {
        config.setObjectsEnabled(enabled);
        render();
    }

    void switchObjectsEnabled() {
        boolean enabled = config.isObjectsEnabled();
        config.setObjectsEnabled(!enabled);
        render();
    }

    void setBackgroundEffectsEnabled(boolean enabled) {
        config.setBackgroundEffectsEnabled(enabled);
        render();
    }

    void switchBackgroundEffectsEnabled() {
        boolean enabled = config.isBackgroundEffectsEnabled();
        config.setBackgroundEffectsEnabled(!enabled);
        render();
    }

    private void deleteOutputFile() {
        RecordConfig recordConfig = config.getRecordConfig();
        String path = recordConfig == null ? null : recordConfig.markedOutputPath();
        File outputFile = null;
        if (path != null) {
            outputFile = new File(path);
        }
        if (outputFile != null && outputFile.exists()) {
            if (outputFile.delete()) {
                Log.w(TAG, outputFile.getPath() + " was successfully deleted");
            }
        }
    }

    ImportStatus importAudioFile(@Nullable String path) {
        if (path == null) {
            config.setAudioFilePath(null);
            render();
        } else {
            MediaExtractor extractor = new MediaExtractor();
            try {
                extractor.setDataSource(path);
            } catch (final IOException e) {
                e.printStackTrace();
                Timber.e(e, "MediaExtractor couldn't set data source for provided audio file");
                return INVALID;
            }
            int trackIndex = MediaUtils.selectTrack(extractor, "audio/");
            if (trackIndex < 0) {
                Timber.e("MediaExtractor couldn't select track for provided audio file");
                return INVALID;
            }
            extractor.selectTrack(trackIndex);
            MediaFormat format = extractor.getTrackFormat(trackIndex);
            try {
                String mimeType = format.getString(MediaFormat.KEY_MIME);
                if (mimeType == null) {
                    Timber.e("mime type for selected audio file is NULL");
                    return INVALID;
                }
                if (mimeTypeSupported(mimeType)) {
                    return VALID;
                } else { // currently restrict music only for mp3
                    return INVALID;
                }
            } catch (Exception e) {
                Timber.e(e);
                e.printStackTrace();
                return INVALID;
            }
        }
//        return VALID_NEED_CONVERSION;
        return INVALID;
    }

    private boolean mimeTypeSupported(String mimeType) {
        // currently restrict music only for mp3
        if (!mimeType.startsWith("audio/mpeg")) {
            return false;
        }
        if (forceConvertMimeTypes.contains(mimeType)) {
            return false;
        }
        int mediaCodecCount = MediaCodecList.getCodecCount();
        for (int i = 0; i < mediaCodecCount; i++) {
            MediaCodecInfo info = MediaCodecList.getCodecInfoAt(i);
            for (String type : info.getSupportedTypes()) {
                if (!type.startsWith("audio/")) {
                    continue;
                }
                if (mimeType.equals(type)) {
                    return true;
                }
            }
        }
        return false;
    }

    String buildConversionAudioFilePath(String directoryPath) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);

        String fileName = String.format(
                Locale.getDefault(),
                "CONVERTED_%1$d%2$d%3$d_%4$d%5$d%6$d.aac",
                year, month, day, hour, minute, second);

        File f = new File(directoryPath, fileName);
        f.deleteOnExit();
        return f.getPath();
    }

    boolean prepareScreenshotOutputFile(Context context) {
        File directory = createPublicPicturesDirectory();
        if (directory == null) {
            return false;
        }
        String fileName = buildFileName("IMG_", "jpg");
        File file = new File(directory, fileName);
        String outputPath = file.getPath();
        config.getScreenshotConfig().setOutputPath(outputPath);
        File cacheDir = context.getCacheDir();
        File unmarkedImageFile = new File(cacheDir, "unmarked.jpg");
        if (!deleteAndCreateNewFile(unmarkedImageFile)) {
            return false;
        }
        config.getScreenshotConfig().setUnmarkedOutputPath(unmarkedImageFile.getPath());
        return true;
    }

    @Nullable
    private File createPublicPicturesDirectory() {
        File pictures = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File directory = new File(pictures, "Moments");
        if (!directory.exists()) {
            if (directory.mkdirs()) {
                Log.d(TAG, "Moments pictures folder was created");
            } else {
                return null;
            }
        }
        return directory;
    }

    boolean prepareRecordingConfig(Context context, int maxWidth) {
        Quality quality = config.getQuality();
        File publicDirectory = createPublicMoviesDirectory();
        if (publicDirectory == null) {
            return false;
        }
        File markedVideoFile = new File(publicDirectory, buildFileName("VID_", "mp4"));
        if (!createOnlyNewFile(markedVideoFile)) {
            return false;
        }
        File cacheDir = context.getCacheDir();
        File unmarkedVideoFile = new File(cacheDir, "unmarked.mp4");
        if (!deleteAndCreateNewFile(unmarkedVideoFile)) {
            return false;
        }
        // We avoid the device-specific limitations on width and height by using values that
        // are multiples of 16, which all tested devices seem to be able to handle.
        int size = MathUtils.roundToPreviousNumberByMultiple(maxWidth, 16);
        int bitRate = quality.bitRate;
        int frameRate = 30;
        int iFrameInterval = 1;
        RecordConfig markedRecordConfig = RecordConfig.create(
                markedVideoFile.getPath(),
                unmarkedVideoFile.getPath(),
                bitRate, frameRate,
                iFrameInterval,
                size, size,
                config.isSoundEnabled(), config.getAudioFilePath(), config.getAudioTimeMillis()
        );
        config.setRecordConfig(markedRecordConfig);
        return true;
    }

    @Nullable
    private File createPublicMoviesDirectory() {
        File movies = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        File directory = new File(movies, "Moments");
        if (!directory.exists()) {
            if (directory.mkdirs()) {
                Timber.w("Moments videos folder was created");
            } else {
                return null;
            }
        }
        return directory;
    }

    @Nullable
    private boolean createOnlyNewFile(@NonNull File file) {
        try {
            if (!file.createNewFile()) {
                String message = "Couldn't create file for record video. CreateNewFile failed";
                Timber.e(message);
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            String message = "Couldn't create file for record video. IOException during createNewFile: " + e.getMessage();
            Timber.e(e, message);
            return false;
        }
        return true;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private boolean deleteAndCreateNewFile(@NonNull File file) {
        try {
            file.delete();
        } catch (SecurityException e) {
            e.printStackTrace();
            Timber.e(e);
            return false;
        }
        return createOnlyNewFile(file);
    }

    private String buildFileName(String prefix, String extension) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);

        return String.format(
                Locale.getDefault(),
                "%1$s%2$d%3$d%4$d_%5$d%6$d%7$d.%8$s",
                prefix, year, month, day, hour, minute, second, extension);
    }

    public void setRecording(boolean recording) {
        this.recording = recording;
        render();
    }

    public boolean isRecording() {
        return recording;
    }

    public void requestUpdate() {
        render();
    }

    public void setCapturePhotoInProgress(boolean capturePhotoInProgress) {
        this.capturePhotoInProgress = capturePhotoInProgress;
        render();
    }

    public void switchMode() {
        config.switchMode();
        render();
    }
}
