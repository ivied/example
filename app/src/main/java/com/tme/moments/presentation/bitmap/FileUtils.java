package com.tme.moments.presentation.bitmap;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.tme.moments.presentation.utils.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import timber.log.Timber;

/**
 * @author Peli
 * @author paulburke (ipaulpro)
 * @version 2013-12-11
 */
public class FileUtils {

    private FileUtils() {
    }

    private static String LOCAL_COPIES_OF_FILES_DIR = "localCopiesOfFiles";

    @Nullable
    public static String getPath(final Context context, final Uri uri) {
        if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        try {
            File copyOfUri = getFileForLocalCopy(context);
            if (copyOfUri != null) {
                IOUtils.copy(
                        context.getContentResolver().openInputStream(uri),
                        new FileOutputStream(copyOfUri)
                );
                return copyOfUri.getPath();
            }
        } catch (IOException e) {
            Timber.e(e);
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    private static File getFileForLocalCopy(final Context context) {
        File tempFile = new File(
                getDirForLocalCopies(context),
                String.valueOf(System.currentTimeMillis())
        );
        try {
            if (tempFile.createNewFile()) {
                return tempFile;
            }
        } catch (IOException e) {
            Timber.e(e);
            e.printStackTrace();
        }
        return null;
    }

    @NonNull
    private static File getDirForLocalCopies(final Context context) {
        File tempDir = new File(context.getCacheDir(), LOCAL_COPIES_OF_FILES_DIR);
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
        return tempDir;
    }

    private static void clearAllLocalCopiesOfFiles(final Context context) {
        try {
            com.tme.moments.presentation.utils.FileUtils.cleanDirectory(getDirForLocalCopies(context));
        } catch (IOException e) {
            Timber.e(e);
            e.printStackTrace();
        }
    }

    public static void clearAllLocalCopiesOfFilesAsync(final Context context) {
        new Thread(() -> clearAllLocalCopiesOfFiles(context)).start();
    }

    /**
     * Copies one file into the other with the given paths.
     * In the event that the paths are the same, trying to copy one file to the other
     * will cause both files to become null.
     * Simply skipping this step if the paths are identical.
     */
    public static void copyFile(@NonNull String pathFrom, @NonNull String pathTo) throws IOException {
        if (pathFrom.equalsIgnoreCase(pathTo)) {
            return;
        }

        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
            inputChannel = new FileInputStream(new File(pathFrom)).getChannel();
            outputChannel = new FileOutputStream(new File(pathTo)).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();
        } finally {
            if (inputChannel != null) inputChannel.close();
            if (outputChannel != null) outputChannel.close();
        }
    }

}