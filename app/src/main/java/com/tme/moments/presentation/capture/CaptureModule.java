package com.tme.moments.presentation.capture;

import com.tme.moments.presentation.core.Config;
import com.tme.moments.presentation.core.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * @author zoopolitic.
 */
@Module
public class CaptureModule {

    @Provides
    @ActivityScope
    CapturePresenter providePresenter(Config config) {
        return new CapturePresenter(config);
    }
}
