package com.tme.moments.presentation.utils;

import android.content.pm.PackageManager;

/**
 * @author zoopolitic.
 */
public class PermissionUtils {
    /**
     * Check that all given permissions have been granted by verifying that each entry in then
     * given array is of the value {@link PackageManager#PERMISSION_GRANTED}.
     */
    public static boolean verifyPermissions(int[] grantResults) {
        // At least one result must be checked.
        if (grantResults.length < 1) {
            return false;
        }

        // Verify that each required permission has been granted, otherwise return false.
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static int homManyPermissionsNotGranted(int[] grantResults) {
        int notGrantedPermissions = 0;
        // Verify that each required permission has been granted, otherwise return false.
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                notGrantedPermissions++;
            }
        }
        return notGrantedPermissions;
    }
}

