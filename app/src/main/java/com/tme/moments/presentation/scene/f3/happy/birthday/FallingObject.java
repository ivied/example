package com.tme.moments.presentation.scene.f3.happy.birthday;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class FallingObject extends Decal {

    private static final float INITIAL_SCALE = 0.4f;      // % of the size
    private static final float DEFAULT_MAX_SCALE = 1.0f; // % of the size

    private float speed;
    private float rotationSpeed;

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float fadeOutStartBorder;
    private float fadeOutEndBorder;

    private float startDelayNanos;
    private float t;
    private float maxScale = DEFAULT_MAX_SCALE;
    private boolean inflated;
    private final boolean rotateAllAxis;

    FallingObject(TextureRegion region, float width, float height, boolean rotateAllAxis) {
        this.rotateAllAxis = rotateAllAxis;
        setTextureRegion(region);
        setDimensions(width, height);
        init();
    }

    public void setMaxScale(float maxScale) {
        this.maxScale = maxScale;
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float deltaRotation = rotationSpeed * deltaNanos;
        rotateZ(deltaRotation);
        if (rotateAllAxis) {
            rotateX(deltaRotation);
            rotateY(deltaRotation);
        }

        float delta = speed * deltaNanos;
        t += delta;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        setX(x);
        setY(y);

        if (!inflated) {
            float scale = getScaleX();
            float inflateSpeed = 0.2f / Const.NANOS_IN_SECOND;
            float deltaScale = inflateSpeed * deltaNanos;
            scale += deltaScale;
            scale = Math.min(maxScale, scale);
            setScale(scale);
            if (scale == maxScale) {
                inflated = true;
            }
        } else {
            float scale = getScaleX();
            //noinspection UnnecessaryLocalVariable
            float deflateSpeed = speed;
            float deltaScale = deflateSpeed * deltaNanos;
            scale -= deltaScale;
            scale = Math.max(INITIAL_SCALE, scale);
            setScale(scale);
        }

        if (t >= fadeOutStartBorder) {
            float fraction = (t - fadeOutStartBorder) / (fadeOutEndBorder - fadeOutStartBorder);
            float alpha = Math.max(0, 1 - fraction);
            setAlpha(alpha);
        }

        if (t >= 1 || color.a == 0) {
            init();
        }
    }

    void setRotationSpeed(int minSpeed, int maxSpeed) {
        rotationSpeed = (float) random(minSpeed, maxSpeed) / Const.NANOS_IN_SECOND;
    }

    protected void init() {
        speed = random(0.125f, 0.2f) / Const.NANOS_IN_SECOND;
        // reduce maxZ by half of biggest side of the object
        // to not being clipped during X and Y rotation
        float z = rotateAllAxis
                ? -1.0f - Math.max(getWidth(), getHeight()) / 2
                : -1.0f;
        float deltaZ = -1.0f - z;
        p0x = random(-1.0f, 1.0f);
        p0y = random(1.0f + deltaZ + getHeight() / 2, 1.5f);
        p1x = p0x + random(-0.75f, 0.75f);
        p1y = p0y - random(0.1f, 0.4f);
        p2x = p1x > p0x ? p1x - 0.75f : p1x + 0.75f;
        p2y = random(-1.35f, -1.0f);
        fadeOutStartBorder = random(0.75f, 0.85f);
        fadeOutEndBorder = fadeOutStartBorder + random(0.1f, 0.25f);
        setColor(1, 1, 1, 1);
        setPosition(p0x, p0y, z);
        setScale(INITIAL_SCALE);
        setAlpha(1);
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 6000));
        inflated = false;
        t = 0;
    }
}
