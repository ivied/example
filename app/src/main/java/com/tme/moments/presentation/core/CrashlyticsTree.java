package com.tme.moments.presentation.core;

import android.support.annotation.Nullable;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import timber.log.Timber;

public class CrashlyticsTree extends Timber.Tree {

    private static final String CRASHLYTICS_KEY_PRIORITY = "priority";
    private static final String CRASHLYTICS_KEY_TAG = "tag";
    private static final String CRASHLYTICS_KEY_MESSAGE = "message";

    @Override
    protected void log(int priority, @Nullable String tag, @Nullable String message, @Nullable Throwable t) {
        switch (priority) {
            case Log.VERBOSE:
            default:
                break;
            case Log.DEBUG:
            case Log.INFO:
            case Log.WARN:
                Crashlytics.log(priority, tag, message);
                break;
            case Log.ERROR:
                if (t == null) {
                    Crashlytics.log(message);
                } else {
                    Crashlytics.logException(t);
                }
                break;

        }

    }
}