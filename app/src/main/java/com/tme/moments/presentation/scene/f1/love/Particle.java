package com.tme.moments.presentation.scene.f1.love;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Particle extends Decal {

    enum Direction {
        LEFT, RIGHT
    }

    private static final int MIN_VISIBLE_INTERVAL = 25;
    private static final int MAX_VISIBLE_INTERVAL = 100;
    private static final int MIN_INVISIBLE_INTERVAL = 25;
    private static final int MAX_INVISIBLE_INTERVAL = 100;

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point 1
    private float p2x, p2y; // end point

    private final float z;
    private float speed;
    private float t;
    private long startDelayNanos;
    private final Direction direction;
    private boolean inited;

    private long visibleTimeNanos;
    private long invisibleTimeNanos;
    private float alphaSpeed;
    private boolean visible;

    Particle(TextureRegion region, float width, float height, float z, Direction direction) {
        this.direction = direction;
        this.z = z;
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        init();
    }

    void step(long deltaNanos) {
        if (startDelayNanos >= 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float delta = speed * deltaNanos;
        t += delta;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        setX(x);
        setY(y);

        // handling blink [START]
        if (visible) {
            if (visibleTimeNanos > 0) {
                visibleTimeNanos -= deltaNanos;
            } else {
                float alpha = color.a;
                if (alpha > 0) {
                    float deltaAlpha = alphaSpeed * deltaNanos;
                    float newAlpha = Math.max(0, alpha - deltaAlpha);
                    setAlpha(newAlpha);
                } else {
                    visible = false;
                    invisibleTimeNanos = TimeUnit.MILLISECONDS.toNanos(random(MIN_VISIBLE_INTERVAL, MAX_VISIBLE_INTERVAL));
                }
            }
        } else {
            if (invisibleTimeNanos > 0) {
                invisibleTimeNanos -= deltaNanos;
            } else {
                float alpha = color.a;
                if (alpha < 1) {
                    float deltaAlpha = alphaSpeed * deltaNanos;
                    float newAlpha = Math.min(1, alpha + deltaAlpha);
                    setAlpha(newAlpha);
                } else {
                    visible = true;
                    visibleTimeNanos = TimeUnit.MILLISECONDS.toNanos(random(MIN_INVISIBLE_INTERVAL, MAX_INVISIBLE_INTERVAL));
                }
            }
        }
        // handling blink [END]

        if (t >= 1) {
            init();
        }
    }

    private void init() {
        float currX = getX();
        float currY = getY();
        float w = getWidth();
        float h = getHeight();
        float deltaZ = -1.0f - z; // -1 is a maxZ
        boolean outOfScreen = direction == Direction.RIGHT
                ? (currX >= 1 + deltaZ + w / 2) || (currY >= 1 + deltaZ + h / 2)
                : (currX <= -1 - deltaZ - w / 2) || (currY >= 1 + deltaZ + h / 2);
        if (!inited || outOfScreen) {
            inited = true;
            alphaSpeed = random(2.0f, 3.0f) / Const.NANOS_IN_SECOND;
            // the closer particle to camera the fast it is
            speed = 1.2f / Math.abs(z) / Const.NANOS_IN_SECOND;
            startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 15000));
            switch (direction) {
                case LEFT:
                    p0x = random(-0.8f + deltaZ, 2.0f + deltaZ);
                    if (p0x > 1.0f + deltaZ + w / 2) {
                        p0y = random(-1.15f - deltaZ, 0.8f - deltaZ);
                    } else {
                        p0y = random(-2.5f - deltaZ, -1.0f - deltaZ - h / 2);
                    }
                    break;
                case RIGHT:
                    p0x = random(-2.0f - deltaZ, 0.8f - deltaZ);
                    if (p0x < -1.0f - deltaZ - w / 2) {
                        p0y = random(-1.15f - deltaZ, 0.8f - deltaZ);
                    } else {
                        p0y = random(-2.5f - deltaZ, -1.0f - deltaZ - h / 2);
                    }
                    break;
            }
        } else {
            p0x = currX;
            p0y = currY;
        }
        float xMin = direction == Direction.RIGHT ? 0.13f : -0.18f;
        float xMax = direction == Direction.RIGHT ? 0.18f : -0.13f;
        float yMin = 0.0f;
        float yMax = 0.2f;
        p1x = p0x + random(xMin, xMax);
        p1y = p0y + random(yMin, yMax);

        p2x = p1x + random(xMin, xMax);
        p2y = p1y + random(yMin, yMax);

        t = 0;
        setPosition(p0x, p0y, z);
    }
}
