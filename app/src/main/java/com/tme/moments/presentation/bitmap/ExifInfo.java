package com.tme.moments.presentation.bitmap;

/**
 * @author zoopolitic.
 */
public class ExifInfo {

    private int exifOrientation;
    private int exifDegrees;
    private int exifTranslation;

    public ExifInfo(int exifOrientation, int exifDegrees, int exifTranslation) {
        this.exifOrientation = exifOrientation;
        this.exifDegrees = exifDegrees;
        this.exifTranslation = exifTranslation;
    }

    public int getExifOrientation() {
        return exifOrientation;
    }

    public int getExifDegrees() {
        return exifDegrees;
    }

    public int getExifTranslation() {
        return exifTranslation;
    }

    public void setExifOrientation(int exifOrientation) {
        this.exifOrientation = exifOrientation;
    }

    public void setExifDegrees(int exifDegrees) {
        this.exifDegrees = exifDegrees;
    }

    public void setExifTranslation(int exifTranslation) {
        this.exifTranslation = exifTranslation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExifInfo exifInfo = (ExifInfo) o;

        if (exifOrientation != exifInfo.exifOrientation) return false;
        if (exifDegrees != exifInfo.exifDegrees) return false;
        return exifTranslation == exifInfo.exifTranslation;

    }

    @Override
    public int hashCode() {
        int result = exifOrientation;
        result = 31 * result + exifDegrees;
        result = 31 * result + exifTranslation;
        return result;
    }

    @Override public String toString() {
        return "ExifInfo{" +
                "exifOrientation=" + exifOrientation +
                ", exifDegrees=" + exifDegrees +
                ", exifTranslation=" + exifTranslation +
                '}';
    }
}