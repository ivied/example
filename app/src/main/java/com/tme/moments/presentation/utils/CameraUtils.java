package com.tme.moments.presentation.utils;

import android.arch.core.util.Function;
import android.hardware.Camera;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CameraUtils {

    @Nullable
    public static CameraSizes getSizesWithEqualsAspectRatio(@NonNull final Camera.Parameters parameters,
                                                            final int requiredWidth,
                                                            final int requiredHeight) {
        List<Camera.Size> pictureSizes = parameters.getSupportedPictureSizes();
        Collections.sort(pictureSizes, largestCameraSize());
        Camera.Size largestPictureSizes = pictureSizes.get(0);
        //previews less than frame
        List<Camera.Size> previewSizes = filter(
                parameters.getSupportedPreviewSizes(),
                size -> size.width <= requiredWidth || size.height <= requiredHeight
        );
        //previews with the same aspect ratio as the largesPictureSizes, to avoid cropping preview
        previewSizes = filter(
                previewSizes,
                previewSize -> equalsAspectRatio(largestPictureSizes, previewSize)
        );
        Collections.sort(previewSizes, largestCameraSize());
        for (Camera.Size previewSize : previewSizes) {
            Camera.Size equalsPictureSize = findFirst(pictureSizes, s -> s.equals(previewSize));
            if (equalsPictureSize != null) {
                return new CameraSizes(previewSize, equalsPictureSize);
            }
            Camera.Size pictureSize = findFirst(pictureSizes, s -> equalsAspectRatio(s, previewSize));
            if (pictureSize != null) {
                return new CameraSizes(previewSize, pictureSize);
            }
        }
        return null;
    }

    private static double aspectRatio(@NonNull final Camera.Size size) {
        return (double) size.width / (double) size.height;
    }

    private static boolean equalsAspectRatio(@NonNull final Camera.Size f,
                                             @NonNull final Camera.Size s) {
        return aspectRatio(f) == aspectRatio(s);
    }

    @NonNull
    private static List<Camera.Size> filter(@NonNull final List<Camera.Size> source,
                                            @NonNull final Function<Camera.Size, Boolean> filter) {
        List<Camera.Size> result = new ArrayList<>(source.size());
        for (Camera.Size size : source) {
            if (filter.apply(size)) {
                result.add(size);
            }
        }
        return result;
    }

    @Nullable
    private static Camera.Size findFirst(@NonNull final List<Camera.Size> source,
                                         @NonNull final Function<Camera.Size, Boolean> f) {
        for (Camera.Size size : source) {
            if (f.apply(size)) return size;
        }
        return null;
    }

    private static boolean contains(@NonNull final List<Camera.Size> source,
                                    @NonNull final Function<Camera.Size, Boolean> f) {
        return findFirst(source, f) != null;
    }

    private static Comparator<Camera.Size> largestCameraSize() {
        return (a, b) -> b.width * b.height - a.width * a.height;
    }


    public static class CameraSizes {

        @NonNull
        private Camera.Size previewSize;
        @NonNull
        private Camera.Size pictureSize;

        private CameraSizes(@NonNull Camera.Size previewSize, @NonNull Camera.Size pictureSize) {
            this.previewSize = previewSize;
            this.pictureSize = pictureSize;
        }

        @NonNull
        public Camera.Size previewSize() {
            return previewSize;
        }

        @NonNull
        public Camera.Size pictureSize() {
            return pictureSize;
        }
    }

}
