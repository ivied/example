package com.tme.moments.presentation.scene.list;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.tme.moments.presentation.recyclerview.DataBoundAdapter;
import com.tme.moments.presentation.recyclerview.DataBoundViewHolder;

import java.util.List;

/**
 * @author zoopolitic.
 */
class SceneListAdapter extends DataBoundAdapter<SceneListItem> {

    interface OnFrameClickListener {
        void onFrameClicked(SceneListItem item);
    }

    private SceneListItem selectedItem;
    private boolean clickable = true;

    private final OnFrameClickListener onFrameClickListener;

    @SuppressWarnings("ConstantConditions") SceneListAdapter(List<SceneListItem> frames, @NonNull
            OnFrameClickListener onFrameClickListener) {
        super(frames);
        if (onFrameClickListener == null) {
            throw new NullPointerException("OnFrameClickListener cannot be null");
        }
        for (SceneListItem frame : frames) {
            if (frame.isSelected()) {
                selectedItem = frame;
                break;
            }
        }
        this.onFrameClickListener = onFrameClickListener;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

    @Override protected int getLayoutResId(int position) {
        return getItem(position).getLayoutResId();
    }

    @Override public DataBoundViewHolder onCreateViewHolder(ViewGroup parent, int layoutId) {
        DataBoundViewHolder holder = super.onCreateViewHolder(parent, layoutId);
        holder.itemView.setOnClickListener(v -> {
            if (!clickable) {
                return;
            }
            int position = holder.getAdapterPosition();
            SceneListItem item = getItem(position);
            if (item == selectedItem) {
                return;
            }
            selectedItem.setSelected(false);
            item.setSelected(true);
            selectedItem = item;
            onFrameClickListener.onFrameClicked(item);
        });

        holder.itemView.setSoundEffectsEnabled(false);
        return holder;
    }

    @NonNull SceneListItem getSelectedFrame() {
        return selectedItem;
    }

    int getSelectedSceneIndex() {
        return selectedItem == null ? 0 : indexOf(selectedItem);
    }

    @Override public void set(int position, SceneListItem item) {
        throw new UnsupportedOperationException();
    }

    @Override public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override public void addAll(List<? extends SceneListItem> items) {
        throw new UnsupportedOperationException();
    }

    @Override public void addAll(int position, List<? extends SceneListItem> items) {
        throw new UnsupportedOperationException();
    }

    @Override public void add(SceneListItem item) {
        throw new UnsupportedOperationException();
    }

    @Override public void add(int position, SceneListItem item) {
        throw new UnsupportedOperationException();
    }

    @Override public void remove(SceneListItem item) {
        throw new UnsupportedOperationException();
    }

    @Override public void remove(int position) {
        throw new UnsupportedOperationException();
    }

    @Override public void removeAll(List<? extends SceneListItem> items) {
        throw new UnsupportedOperationException();
    }


}