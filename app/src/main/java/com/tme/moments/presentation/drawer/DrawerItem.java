package com.tme.moments.presentation.drawer;

/**
 * @author zoopolitic.
 */
interface DrawerItem {

    int getLayoutResId();
}
