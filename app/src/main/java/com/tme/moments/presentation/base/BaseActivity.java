package com.tme.moments.presentation.base;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.tme.moments.R;
import com.tme.moments.presentation.utils.AndroidUtils;

import java.util.Locale;


/**
 * @author zoopolitic.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();

    private boolean stopped;

    protected Toast currentToast;

    protected FragmentManager fragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getSupportFragmentManager();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            String appName = getString(R.string.app_name);
            int color = getPrimaryColor();
            ActivityManager.TaskDescription taskDescription =
                    new ActivityManager.TaskDescription(appName, icon, color);
            setTaskDescription(taskDescription);
        }
    }

    public int getAccentColor() {
        final TypedValue value = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorAccent, value, true);
        return value.data;
    }

    public int getPrimaryColor() {
        final TypedValue value = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }

    @Nullable public Drawable getThemedDrawable(int attributeId) {
        return AndroidUtils.getThemedDrawable(this, attributeId);
    }

    public boolean isStopped() {
        return stopped;
    }



    @Override protected void onStart() {
        super.onStart();
        stopped = false;
    }

    @Override protected void onStop() {
        super.onStop();
        stopped = true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!isFinishing()) {
            super.onBackPressed();
        }
    }

    protected int backStackSize() {
        return fragmentManager.getBackStackEntryCount();
    }

    @SuppressWarnings("ConstantConditions")
    protected void setBackButtonVisible(boolean isVisible, @DrawableRes int backIconResId) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(isVisible);
        getSupportActionBar().setDisplayShowHomeEnabled(isVisible);
        getSupportActionBar().setHomeAsUpIndicator(backIconResId);
    }

    @SuppressWarnings("ConstantConditions")
    protected void setActionBarDefaultTitleVisible(boolean isVisible) {
        getSupportActionBar().setDisplayShowTitleEnabled(isVisible);
    }

    protected void scheduleStartPostponedTransition(final View sharedElement) {
        sharedElement.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        sharedElement.getViewTreeObserver().removeOnPreDrawListener(this);
                        supportStartPostponedEnterTransition();
                        return true;
                    }
                });
    }

    // --------------------------------------- Toast ----------------------------------------------


    protected void cancelCurrentToast() {
        if (currentToast != null) {
            currentToast.cancel();
            currentToast = null;
        }
    }

    private void toast(final String text, final int length, final int gravity) {
        if (!isFinishing()) {
            if (!AndroidUtils.isMainThread()) {
                runOnUiThread(() -> {
                    currentToast = Toast.makeText(BaseActivity.this, text, length);
                    currentToast.show();
                });
            } else {
                currentToast = Toast.makeText(this, text, length);
                if (gravity != Gravity.NO_GRAVITY) {
                    currentToast.setGravity(gravity, 0, 0);
                }
                currentToast.show();
            }
        }
    }

    protected void shortToast(String text, int gravity) {
        toast(text, Toast.LENGTH_SHORT, gravity);
    }

    protected void shortToast(String text) {
        toast(text, Toast.LENGTH_SHORT, Gravity.NO_GRAVITY);
    }

    protected void shortToast(@StringRes int resId, int gravity) {
        toast(getString(resId), Toast.LENGTH_SHORT, gravity);
    }

    protected void shortToast(@StringRes int resId) {
        toast(getString(resId), Toast.LENGTH_SHORT, Gravity.NO_GRAVITY);
    }

    protected void longToast(String text, int gravity) {
        toast(text, Toast.LENGTH_LONG, gravity);
    }

    protected void longToast(String text) {
        toast(text, Toast.LENGTH_LONG, Gravity.NO_GRAVITY);
    }

    protected void longToast(@StringRes int resId, int gravity) {
        toast(getString(resId), Toast.LENGTH_LONG, gravity);
    }

    protected void longToast(@StringRes int resId) {
        toast(getString(resId), Toast.LENGTH_LONG, Gravity.NO_GRAVITY);
    }

    public void pop() {
        fragmentManager.popBackStack();
    }

    protected Fragment getFragment(int containerResId) {
        return fragmentManager.findFragmentById(containerResId);
    }

    public void clearBackStack() {
        if (fragmentManager.getBackStackEntryCount() > 0 && !isFinishing()) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    private void fragmentTransactionCheck(@Nullable Fragment f, String nullExceptionMessage,
                                          String finishingExceptionMessage) {
        if (f == null) {
            throw new IllegalArgumentException(nullExceptionMessage);
        }
        if (isFinishing()) {
            throw new IllegalStateException(finishingExceptionMessage);
        }
    }

    protected void hideCheck(Fragment f) {
        fragmentTransactionCheck(f, "Cannot hide NULL fragment", "You are trying to hide fragment while activity is finishing");
    }

    protected void showCheck(Fragment f) {
        fragmentTransactionCheck(f, "Cannot show NULL fragment", "You are trying to show fragment while activity is finishing");
    }

    protected void addCheck(Fragment f) {
        fragmentTransactionCheck(f, "Cannot add NULL fragment", "You are trying to add fragment while activity is finishing");
    }

    protected void removeCheck(Fragment f) {
        fragmentTransactionCheck(f, "Cannot remove NULL fragment", "You are trying to remove fragment while activity is finishing");
    }

    protected void replaceCheck(Fragment f) {
        fragmentTransactionCheck(f, "Cannot replace NULL fragment", "You are trying to replace fragment while activity is finishing");
    }

//    ---------------------------------  REPLACE -----------------------------------------

    public void replaceFragment(Fragment f, int containerId, boolean addToBackStack,
                                String backStackName) {
        replaceCheck(f);

        // if fragment with this class already in back stack - just pop it
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStackName, 0);
        if (fragmentPopped) {
            return;
        }

        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(containerId, f);
        if (addToBackStack) {
            ft.addToBackStack(backStackName);
        }
        if (!isFinishing()) {
            ft.commit();
        }
    }

    public void replaceFragment(Fragment f, int containerId, boolean addToBackStack) {
        replaceFragment(f, containerId, addToBackStack, f.getClass().getName());
    }

    public void replaceFragmentAnimated(Fragment f, int containerId, boolean addToBackStack,
                                        String backStackName,
                                        int enter, int exit, int popEnter, int popExit) {
        replaceCheck(f);

        // if fragment with this class already in back stack - just pop it
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStackName, 0);
        if (fragmentPopped) {
            return;
        }

        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.setCustomAnimations(enter, exit, popEnter, popExit);

        ft.replace(containerId, f);
        if (addToBackStack) {
            ft.addToBackStack(backStackName);
        }
        if (!isFinishing()) {
            ft.commit();
        }
    }

    public void replaceFragmentAnimated(Fragment f, int containerId, boolean addToBackStack,
                                        int enter, int exit, int popEnter, int popExit) {
        replaceFragmentAnimated(f, containerId, addToBackStack, f.getClass().getName(), enter, exit, popEnter, popExit);
    }

//    --------------------------------- ADD -----------------------------------------

    public void addFragment(@NonNull Fragment f, int containerId, boolean addToBackStack) {
        addCheck(f);
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(containerId, f, f.getClass().getName());
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        if (!isFinishing()) {
            ft.commit();
        }
    }

    public void addFragment(@NonNull Fragment f, int containerId) {
        addFragment(f, containerId, true);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void addFragmentWithSharedElement(Fragment f, int containerId, boolean addToBackStack,
                                             View sharedView, String transitionName) {
        addCheck(f);
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(containerId, f, f.getClass().getName());
        if (sharedView != null && !TextUtils.isEmpty(transitionName)) {
            ft.addSharedElement(sharedView, transitionName);
        }
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        if (!isFinishing()) {
            ft.commit();
        }
    }

//    --------------------------------- REMOVE -----------------------------------------

    public void removeFragment(Fragment f) {
        removeCheck(f);
        fragmentManager.beginTransaction().remove(f).commit();
    }

//    --------------------------------- HIDE -----------------------------------------

    public void hideFragment(Fragment f) {
        hideCheck(f);
        fragmentManager.beginTransaction().hide(f).commit();
    }

    public void hideFragmentAnimated(Fragment f, int enter, int exit, int popEnter, int popExit) {
        hideCheck(f);
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(enter, exit, popEnter, popExit)
                .hide(f)
                .commit();
    }

    public void hideFragmentAnimated(Fragment f, int enter, int exit) {
        hideCheck(f);
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(enter, exit)
                .hide(f)
                .commit();
    }

//    --------------------------------- SHOW -----------------------------------------

    public void showFragment(Fragment f) {
        showCheck(f);
        fragmentManager.beginTransaction().show(f).commit();
    }

    public void showFragmentAnimated(Fragment f, int enter, int exit, int popEnter, int popExit) {
        showCheck(f);
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(enter, exit, popEnter, popExit)
                .show(f)
                .commit();
    }

    public void showFragmentAnimated(Fragment f, int enter, int exit) {
        showCheck(f);
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(enter, exit)
                .show(f)
                .commit();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(updateBaseContextLocale(base));
    }

    private Context updateBaseContextLocale(Context context) {
        Configuration config = context.getResources().getConfiguration();
        if (config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
            // Change locale settings in the app.
            Locale locale = new Locale("en".toLowerCase());
            Locale.setDefault(locale);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return updateResourcesLocale(context, locale);
            }

            return updateResourcesLocaleLegacy(context, locale);
        }
        return context;
    }



    @TargetApi(Build.VERSION_CODES.N)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }
}
