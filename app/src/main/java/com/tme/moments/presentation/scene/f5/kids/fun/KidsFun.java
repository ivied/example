package com.tme.moments.presentation.scene.f5.kids.fun;

import android.graphics.PointF;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseBlurredScene;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class KidsFun extends BaseBlurredScene {

    private TextureInfo atlasInfo;
    private DecalBatch leavesBatch;
    private DecalBatch flowersBatch;
    private DecalBatch headsAndBranchesBatch;
    private DecalBatch starsBatch;
    private DecalBatch bubblesBatch;
    private DecalBatch grassBatch;
    private List<Bubble> bubbles;
    private TextureRegion leafRegion;
    private List<Leaf> leaves;
    private List<Leaf> finishedLeaves;
    private List<Star> stars;
    private List<Flower> flowers;
    private List<Grass> grassList;
    private CounterClockwiseBranch leftBranch;
    private ClockwiseBranch rightBranch;
    private boolean isBubblesMoveLeft;

    private Set<Integer> randomHeadPositionsPool;
    private List<Head> heads;
    private long headsShakeWaitTime;
    private int activeHeadsCount;

    @Override protected int getStencilResId() {
        return R.drawable.f5_stencil;
    }

    @Override protected int getOverlayResId() {
        return R.drawable.f5_overlay;
    }

    @Override protected float getCameraPositionX() {
        return 0.0f;
    }

    @Override protected float getCameraPositionY() {
        return -0.2129f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.49f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.6981f;
    }

    @Override public boolean hasBackgroundEffects() {
        return true;
    }

    @Override public void init() {
        super.init();
        isBubblesMoveLeft = random();
    }

    @Override protected void prepareBackgroundEffects(int surfaceWidth, int surfaceHeight) {
        prepareBubbles(atlasInfo);
        prepareStars(atlasInfo);
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        super.prepareObjects(surfaceWidth, surfaceHeight);
        atlasInfo = openGLLoader.loadTexture(R.drawable.f5_atlas);

        randomHeadPositionsPool = new HashSet<>();

        leavesBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        leavesBatch.initialize(100);

        prepareFlowers(atlasInfo);
        flowersBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        flowersBatch.initialize(flowers.size());
        flowersBatch.addAll(flowers);

        activeHeadsCount = 2;
        prepareHeads(atlasInfo);
        prepareBranches(atlasInfo);
        headsAndBranchesBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        headsAndBranchesBatch.initialize(heads.size() + 2); // 2 for branches
        // order of adding matters (overlap priority)
        headsAndBranchesBatch.add(rightBranch);
        headsAndBranchesBatch.add(leftBranch);
        headsAndBranchesBatch.addAll(heads);

        prepareGrass(atlasInfo);
        grassBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        grassBatch.initialize(grassList.size());
        grassBatch.addAll(grassList);

        leaves = new ArrayList<>();
        finishedLeaves = new ArrayList<>();
        leafRegion = buildRegion(atlasInfo, 56, 0, 80, 58);

    }

    @Override protected void onOverlayDrawn(long deltaNanos) {
        super.onOverlayDrawn(deltaNanos);
        boolean objectsEnabled = config.isObjectsEnabled();
        glUseProgram(mainProgramHandle);
        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        headsAndBranchesBatch.render(uProjectionMatrixLoc, projectionMatrix);

        // branches handling
        if (objectsEnabled) {
            rightBranch.step(deltaNanos);
            if (rightBranch.shouldThrowLeaves()) {
                throwLeaves(rightBranch, -0.45f, 0.2f);
                rightBranch.setShouldThrowLeaves(false);
            }
            leftBranch.step(deltaNanos);
            if (leftBranch.shouldThrowLeaves()) {
                throwLeaves(leftBranch, -0.2f, 0.45f);
                leftBranch.setShouldThrowLeaves(false);
            }
        }

        // heads handling
        if (objectsEnabled) {
            if (headsShakeWaitTime > 0) {
                headsShakeWaitTime -= deltaNanos;
            } else {
                boolean allFinished = true;
                for (Head head : heads) {
                    head.step(deltaNanos);
                    if (!head.isFinished()) {
                        allFinished = false;
                    }
                }
                if (allFinished) {
                    headsShakeWaitTime = TimeUnit.MILLISECONDS.toNanos(random(2500, 3500));
                    while (randomHeadPositionsPool.size() < activeHeadsCount) {
                        randomHeadPositionsPool.add(random(heads.size()));
                    }

                    for (Integer position : randomHeadPositionsPool) {
                        Head head = heads.get(position);
                        int maxShakeCount = random(4, 9);
                        if (maxShakeCount % 2 != 0) {
                            maxShakeCount++;
                        }
                        head.setMaxShakeCount(maxShakeCount);
                        head.resetShakeCount();
                        head.setMoveFromSideToSide(random());
                    }
                    randomHeadPositionsPool.clear();
                }
            }
        }

        if (config.isBackgroundEffectsEnabled()) {
            bubblesBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Bubble bubble : bubbles){
                bubble.step(deltaNanos);
            }
        }

        flowersBatch.render(uProjectionMatrixLoc, projectionMatrix);
        if (objectsEnabled) {
            for (Flower flower : flowers) {
                flower.step(deltaNanos);
            }
        }

        if (config.isObjectsEnabled()) {
            leavesBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Leaf leaf : leaves) {
                leaf.step(deltaNanos);
                if (leaf.isFinished()) {
                    finishedLeaves.add(leaf);
                }
            }
            if (!finishedLeaves.isEmpty()) {
                leaves.removeAll(finishedLeaves);
                leavesBatch.removeAll(finishedLeaves);
                finishedLeaves.clear();
            }
        }
    }

    @Override protected void onCameraDrawn(long deltaNanos) {
        super.onCameraDrawn(deltaNanos);
        glUseProgram(mainProgramHandle);
        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        grassBatch.render(uProjectionMatrixLoc, projectionMatrix);
        if (config.isObjectsEnabled()) {
            for (Grass grass : grassList) {
                grass.step(deltaNanos);
            }
        }
    }

    @Override protected void onBlurredImageDrawn(long deltaNanos) {
        super.onBlurredImageDrawn(deltaNanos);
        if (config.isBackgroundEffectsEnabled()) {
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            starsBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Star star : stars) star.step(deltaNanos);
        }
    }

    private void throwLeaves(Decal branch, float minXSpeed, float maxXSpeed) {
        float width = 0.1f;
        float height = 0.0725f;
        int count = random(6, 10);
        float halfWidth = branch.getWidth() / 2;
        float halfHeight = branch.getWidth() / 2;
        for (int i = 0; i < count; i++) {
            float x = random(branch.getX() - halfWidth, branch.getX() + halfWidth);
            float y = random(branch.getY(), branch.getY() + halfHeight);
            Leaf leaf = new Leaf(leafRegion, width, height, x, y, minXSpeed, maxXSpeed, random());
            leaves.add(leaf);
            leavesBatch.add(leaf);
        }
    }

    @Override public void release() {
        super.release();
        if (atlasInfo != null) {
            int[] arr = new int[1];
            arr[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, arr, 0);
            GlUtil.checkGlError("Delete textures");
        }
    }

    private void prepareBranches(TextureInfo info) {
        TextureRegion region = buildRegion(info, 0, 744, 210, 308);
        float width = 0.3888f;  // increased up to 148% because original size is for 1080
        float height = 0.570f;  // increased up to 148% because original size is for 1080
        rightBranch = new ClockwiseBranch(region, width, height);
        rightBranch.setPosition(0.68f, 0.45f, -1.0f);

        region = buildRegion(info, 250, 744, 228, 384);
        width = 0.4222f;    // increased up to 148% because original size is for 1080
        height = 0.7111f;   // increased up to 148% because original size is for 1080
        leftBranch = new CounterClockwiseBranch(region, width, height);
        leftBranch.setPosition(-0.63f, 0.45f, -1.0f);
    }

    private void prepareBubbles(TextureInfo info) {
        TextureRegion bubbleRegion = buildRegion(info, 376, 0, 86, 87);
        float width = 0.1075f;
        float height = 0.1087f;
        int count = 34;
        bubbles = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            bubbles.add(new Bubble(bubbleRegion, width, height, isBubblesMoveLeft));
        }
        bubblesBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        bubblesBatch.initialize(bubbles.size());
        bubblesBatch.addAll(bubbles);
    }

    private void prepareHeads(TextureInfo info) {
        heads = new ArrayList<>();

        // antilope
        TextureRegion region = buildRegion(info, 0, 450, 223, 257);
        float width = 0.4130f;
        float height = 0.4760f;
        float moveSpeed = 0.02f;
        float rotationSpeed = -15;
        float maxIncline = 0.007f;
        float x = -0.55f;
        float y = 0.18f;
        Head antilope = new Head(region, width, height, x, y, moveSpeed, rotationSpeed, maxIncline);

        // koala
        region = buildRegion(info, 462, 0, 158, 157);
        width = 0.2926f;
        height = 0.2908f;
        moveSpeed = 0.02f;
        rotationSpeed = -18;
        maxIncline = 0.007f;
        x = 0.52f;
        y = 0.27f;
        Head koala = new Head(region, width, height, x, y, moveSpeed, rotationSpeed, maxIncline);

        // elephant
        region = buildRegion(info, 478, 744, 481, 394);
        width = 0.8907f;
        height = 0.7296f;
        moveSpeed = 0.035f;
        rotationSpeed = 13;
        maxIncline = 0.01f;
        x = 0.1f;
        y = 0.5f;
        Head elephant = new Head(region, width, height, x, y, moveSpeed, rotationSpeed, maxIncline);

        // giraffa
        region = buildRegion(info, 547, 450, 164, 278);
        width = 0.3037f;
        height = 0.5148f;
        moveSpeed = 0.035f;
        rotationSpeed = -15;
        maxIncline = 0.01f;
        x = -0.83f;
        y = 0.41f;
        Head giraffa = new Head(region, width, height, x, y, moveSpeed, rotationSpeed, maxIncline);

        // zebra
        region = buildRegion(info, 200, 200, 127, 203);
        width = 0.2351f;
        height = 0.3759f;
        moveSpeed = 0.04f;
        rotationSpeed = -15;
        maxIncline = 0.01f;
        x = -0.73f;
        y = -0.13f;
        Head zebra = new Head(region, width, height, x, y, moveSpeed, rotationSpeed, maxIncline);

        // lion
        region = buildRegion(info, 284, 450, 263, 272);
        width = 0.4870f;
        height = 0.5030f;
        moveSpeed = 0.032f;
        rotationSpeed = -15;
        maxIncline = 0.008f;
        x = 0.63f;
        y = -0.17f;
        Head lion = new Head(region, width, height, x, y, moveSpeed, rotationSpeed, maxIncline);

        // hippo
        region = buildRegion(info, 327, 200, 197, 216);
        width = 0.3648f;
        height = 0.4f;
        moveSpeed = 0.029f;
        rotationSpeed = -14;
        maxIncline = 0.01f;
        x = -0.52f;
        y = -0.45f;
        Head hippo = new Head(region, width, height, x, y, moveSpeed, rotationSpeed, maxIncline);

        // cow
        region = buildRegion(info, 655, 0, 163, 196);
        width = 0.30185f;
        height = 0.36296f;
        moveSpeed = 0.032f;
        rotationSpeed = -13;
        maxIncline = 0.008f;
        x = 0.63f;
        y = -0.5f;
        Head cow = new Head(region, width, height, x, y, moveSpeed, rotationSpeed, maxIncline);

        // order matters (overlap priority)
        heads.add(antilope);
        heads.add(koala);
        heads.add(elephant);
        heads.add(giraffa);
        heads.add(zebra);
        heads.add(hippo);
        heads.add(lion);
        heads.add(cow);
    }

    private void prepareFlowers(TextureInfo info) {
        flowers = new ArrayList<>();
        TextureRegion region = buildRegion(info, 0, 0, 56, 42);
        float width = 0.07f;
        float height = 0.0525f;
        float scale = 1.2f;

        // flowers from left to right of the screen

        Flower f1 = new Flower(region, width, height, -0.91f, -0.85f, 0.013f, -12, 0.015f);
        f1.setScale(scale);
        flowers.add(f1);

        Flower f2 = new Flower(region, width, height, -0.72f, -0.88f, 0.009f, -18, 0.01f);
        f2.setScale(scale);
        flowers.add(f2);

        Flower f3 = new Flower(region, width, height, -0.4f, -0.9f, 0.012f, -20, 0.01f);
        f3.setScale(scale);
        flowers.add(f3);

        Flower f4 = new Flower(region, width, height, 0.2f, -0.98f, 0.01f, -13, 0.015f);
        f4.setScale(scale);
        flowers.add(f4);

        Flower f5 = new Flower(region, width, height, 0.3f, -0.87f, 0.011f, -21, 0.016f);
        f5.setScale(scale);
        flowers.add(f5);

        Flower f6 = new Flower(region, width, height, 0.61f, -1.01f, 0.011f, -14, 0.012f);
        f6.setScale(scale);
        flowers.add(f6);

        Flower f7 = new Flower(region, width, height, 0.8f, -0.85f, 0.012f, -18, 0.0112f);
        f7.setScale(scale);
        flowers.add(f7);
    }

    private void prepareGrass(TextureInfo info) {
        grassList = new ArrayList<>();

        TextureRegion region = buildRegion(info, 560, 200, 50, 225);
        float width = 0.0625f;
        float height = 0.2812f;
        grassList.add(new Grass(region, width, height, -0.8f, -0.85f, 0.012f, 12, 0.012f));
        region = buildRegion(info, 524, 200, 36, 220);
        width = 0.045f;
        height = 0.275f;
        grassList.add(new Grass(region, width, height, -0.8f, -0.85f, 0.007f, -10, 0.008f));

        region = buildRegion(info, 711, 450, 42, 290);
        width = 0.0525f;
        height = 0.3625f;
        grassList.add(new Grass(region, width, height, -0.37f, -0.79f, 0.006f, 6, 0.005f));
        region = buildRegion(info, 223, 450, 61, 260);
        width = 0.0762f;
        height = 0.325f;
        grassList.add(new Grass(region, width, height, -0.36f, -0.78f, 0.008f, -9, 0.007f));
        region = buildRegion(info, 761, 200, 34, 250);
        width = 0.0425f;
        height = 0.3125f;
        grassList.add(new Grass(region, width, height, -0.38f, -0.79f, 0.009f, 10, 0.06f));

        region = buildRegion(info, 818, 0, 51, 200);
        width = 0.0637f;
        height = 0.25f;
        grassList.add(new Grass(region, width, height, 0.0f, -0.85f, 0.005f, -6, 0.005f));
        region = buildRegion(info, 620, 0, 35, 195);
        width = 0.0437f;
        height = 0.2437f;
        grassList.add(new Grass(region, width, height, -0.02f, -0.86f, 0.007f, 6, 0.008f));
        region = buildRegion(info, 610, 200, 60, 233);
        width = 0.075f;
        height = 0.2912f;
        grassList.add(new Grass(region, width, height, 0.02f, -0.84f, 0.007f, 4, 0.006f));

        region = buildRegion(info, 210, 744, 40, 350);
        width = 0.05f;
        height = 0.4375f;
        grassList.add(new Grass(region, width, height, 0.33f, -1.0f + height / 2, 0.004f, 5, 0.004f));
        region = buildRegion(info, 670, 200, 37, 238);
        width = 0.0462f;
        height = 0.2975f;
        grassList.add(new Grass(region, width, height, 0.35f, -1.0f + height / 2, 0.007f, -7, 0.006f));
        region = buildRegion(info, 707, 200, 54, 246);
        width = 0.0675f;
        height = 0.3075f;
        grassList.add(new Grass(region, width, height, 0.35f, -1.0f + height / 2, 0.004f, -5, 0.007f));

        region = buildRegion(info, 753, 450, 64, 294);
        width = 0.08f;
        height = 0.3675f;
        grassList.add(new Grass(region, width, height, 0.8f, -1.0f + height / 2, 0.004f, -5, 0.004f));
    }

    private void prepareStars(TextureInfo info) {
        Pair<TextureRegion, PointF> star1 = new Pair<>(buildRegion(info, 0, 200, 200, 200), new PointF(0.25f, 0.25f));
        Pair<TextureRegion, PointF> star2 = new Pair<>(buildRegion(info, 136, 0, 80, 80), new PointF(0.1f, 0.1f));
        Pair<TextureRegion, PointF> star3 = new Pair<>(buildRegion(info, 296, 0, 80, 80), new PointF(0.1f, 0.1f));
        Pair<TextureRegion, PointF> star4 = new Pair<>(buildRegion(info, 216, 0, 80, 80), new PointF(0.1f, 0.1f));
        stars = new ArrayList<>();
        stars.addAll(generateStars(star1, 25, 0.4f, 0.6f));
        stars.addAll(generateStars(star2, 25, 0.8f, 1.2f));
        stars.addAll(generateStars(star3, 25, 0.8f, 1.2f));
        stars.addAll(generateStars(star4, 25, 0.8f, 1.2f));
        starsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        starsBatch.initialize(stars.size());
        starsBatch.addAll(stars);
    }

    private List<Star> generateStars(Pair<TextureRegion, PointF> regionInfo, int count,
                                     float minScale, float maxScale) {
        List<Star> items = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            TextureRegion region = regionInfo.first;
            float width = regionInfo.second.x;
            float height = regionInfo.second.y;
            Star star = new Star(region, width, height);
            star.setScale(random(minScale, maxScale));
            star.setStartDelay(6500);
            items.add(star);
        }
        return items;
    }
}
