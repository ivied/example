package com.tme.moments.presentation.scene.f16.christmas;

import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_ALWAYS;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_EQUAL;
import static android.opengl.GLES20.GL_KEEP;
import static android.opengl.GLES20.GL_REPLACE;
import static android.opengl.GLES20.GL_STENCIL_BUFFER_BIT;
import static android.opengl.GLES20.GL_STENCIL_TEST;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glColorMask;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glStencilFunc;
import static android.opengl.GLES20.glStencilOp;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class MerryChristmas extends BaseScene {

    private Sprite stencilMask;
    private Sprite overlay;
    private Sprite background;

    private int stencilProgramHandle = -1;
    private int uStencilProjectionMatrixHandle = -1;
    private int aStencilPositionHandle = -1;
    private int aStencilColorHandle = -1;
    private int aStencilTexCoordHandle = -1;

    private TextureInfo atlasInfo;
    private TextureInfo capAtlasInfo;
    private DecalBatch snowBatch;
    private Santa santa;
    private Snowflake snowflake;
    private Cap cap;
    private DecalBatch treesBatch;
    private Tree leftTree;
    private Tree rightTree;
    private List<Snow> snow;

    @Override protected float getCameraPositionX() {
        return 0.0f;
    }

    @Override protected float getCameraPositionY() {
        return -0.1f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.6333f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.537f;
    }

    @Override public boolean hasBackgroundEffects() {
        return true;
    }

    @Override public void init() {
        super.init();
        if (stencilProgramHandle == -1) {
            prepareStencilProgram();
        }
    }

    private void prepareStencilProgram() {
        stencilProgramHandle =
                openGLLoader.createProgram(R.raw.vertex_shader, R.raw.stencil_fragment_shader);

        aStencilPositionHandle = glGetAttribLocation(stencilProgramHandle, "a_position");
        GlUtil.checkLocation(aStencilPositionHandle, "a_position");
        glEnableVertexAttribArray(aStencilPositionHandle);

        aStencilColorHandle = glGetAttribLocation(stencilProgramHandle, "a_color");
        GlUtil.checkLocation(aStencilColorHandle, "a_color");
        glEnableVertexAttribArray(aStencilColorHandle);

        aStencilTexCoordHandle = glGetAttribLocation(stencilProgramHandle, "a_texCoord");
        GlUtil.checkLocation(aStencilTexCoordHandle, "a_texCoord");
        glEnableVertexAttribArray(aStencilTexCoordHandle);

        uStencilProjectionMatrixHandle = glGetUniformLocation(stencilProgramHandle, "u_projTrans");
        GlUtil.checkLocation(uStencilProjectionMatrixHandle, "u_projTrans");
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        atlasInfo = openGLLoader.loadTexture(R.drawable.f16_atlas);
        prepareStencilMask();
        prepareOverlay();
        prepareBackground();
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f16_santa_claus);
        TextureRegion region = buildRegion(info, 0, 0, info.imageWidth, info.imageHeight);
        santa = new Santa(region, 1.0975f, 0.4075f);
        capAtlasInfo = openGLLoader.loadTexture(R.drawable.f16_cap_atlas);
        prepareCap(capAtlasInfo);
        prepareTrees(atlasInfo);
    }

    @Override protected void prepareBackgroundEffects(int surfaceWidth, int surfaceHeight) {
        super.prepareBackgroundEffects(surfaceWidth, surfaceHeight);
        prepareSnow(atlasInfo);
        prepareSnowflake(atlasInfo);
        snowBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        snowBatch.initialize(snow.size() + 1); // +1 is for snowflake
        snowBatch.addAll(snow);
        snowBatch.add(snowflake);
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        boolean objectsEnabled = config.isObjectsEnabled();
        boolean backgroundEffectsEnabled = config.isBackgroundEffectsEnabled();

        glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glUseProgram(mainProgramHandle);
        background.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);

        if (objectsEnabled) {
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            santa.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            santa.step(deltaNanos);
        }

        glEnable(GL_STENCIL_TEST);

        glColorMask(false, false, false, false);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

        // draw mask
        glUseProgram(stencilProgramHandle);
        stencilMask.draw(uStencilProjectionMatrixHandle, projectionMatrix, aStencilPositionHandle,
                aStencilTexCoordHandle, aStencilColorHandle);

        glColorMask(true, true, true, true);
        glStencilFunc(GL_EQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

        if (mode == Mode.IMAGE) {
            glUseProgram(mainProgramHandle);
            drawImage();
        } else {
            drawCameraInput(cameraTexture);
        }

        glDisable(GL_STENCIL_TEST);
        glUseProgram(mainProgramHandle);
        overlay.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);

        if (backgroundEffectsEnabled) {
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            snowBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Snow snow : snow) {
                snow.step(deltaNanos);
            }
            snowflake.step(deltaNanos);
        }

        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        treesBatch.render(uProjectionMatrixLoc, projectionMatrix);
        if (objectsEnabled) {
            leftTree.step(deltaNanos);
            rightTree.step(deltaNanos);
        }
        glBindTexture(GL_TEXTURE_2D, capAtlasInfo.textureHandle);
        cap.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
        if (objectsEnabled) {
            cap.step(deltaNanos);
        }
    }

    @Override public void release() {
        super.release();
        glDeleteProgram(stencilProgramHandle);
        stencilProgramHandle = -1;
        int[] arr = new int[2];
        int texturesToDeleteSize = 0;
        if (atlasInfo != null) {
            arr[texturesToDeleteSize] = atlasInfo.textureHandle;
            texturesToDeleteSize++;
        }
        if (capAtlasInfo != null) {
            arr[texturesToDeleteSize] = capAtlasInfo.textureHandle;
            texturesToDeleteSize++;
        }
        if (texturesToDeleteSize != 0) {
            glDeleteTextures(texturesToDeleteSize, arr, 0);
            GlUtil.checkGlError("Release " + getClass().getSimpleName());
        }
        if (snowBatch != null) {
            snowBatch.dispose();
        }
        snow = null;
        snowflake = null;
        santa = null;
    }

    private void prepareTrees(TextureInfo info) {
        TextureRegion treeRegion = buildRegion(info, 0, 88, 130, 214);
        float width = 0.2407f; // size for 1080
        float height = 0.3962f; // size for 1080
        leftTree = new Tree(treeRegion, width, height);
        leftTree.setPosition(-0.75f, -0.4f, -1f);
        rightTree = new Tree(treeRegion, width, height);
        rightTree.setPosition(0.9f, -0.32f, -1f);
        treesBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        treesBatch.initialize(2);
        treesBatch.add(leftTree);
        treesBatch.add(rightTree);
    }

    private void prepareSnow(TextureInfo info) {
        List<Pair<TextureRegion, PointF>> bigSnowInfos = new ArrayList<>();
        bigSnowInfos.add(new Pair<>(buildRegion(info, 130, 88, 175, 209), new PointF(0.21875f, 0.2612f)));  // big1
        bigSnowInfos.add(new Pair<>(buildRegion(info, 0, 487, 250, 185), new PointF(0.3125f, 0.2312f)));    // big2
        bigSnowInfos.add(new Pair<>(buildRegion(info, 250, 487, 250, 185), new PointF(0.3125f, 0.2312f)));  // big3
        List<Pair<TextureRegion, PointF>> snowInfos = new ArrayList<>();
        snowInfos.add(new Pair<>(buildRegion(info, 289, 0, 74, 88), new PointF(0.0925f, 0.11f)));     // small1
        snowInfos.add(new Pair<>(buildRegion(info, 0, 0, 67, 50), new PointF(0.0837f, 0.0625f)));     // small2
        snowInfos.add(new Pair<>(buildRegion(info, 363, 0, 82, 61), new PointF(0.1025f, 0.0762f)));   // small3
        snowInfos.add(new Pair<>(buildRegion(info, 215, 0, 74, 74), new PointF(0.0925f, 0.0925f)));   // fancy1
        snowInfos.add(new Pair<>(buildRegion(info, 141, 0, 74, 67), new PointF(0.0925f, 0.0837f)));   // fancy2
        snowInfos.add(new Pair<>(buildRegion(info, 67, 0, 74, 74), new PointF(0.0925f, 0.0925f)));    // fancy3
        snow = new ArrayList<>();
        Snow.Direction[] directions = Snow.Direction.values();
        Snow.Direction direction = directions[random(directions.length)];
        List<Point> snowRanges = new ArrayList<>();
        snowRanges.add(new Point(550, 600));
        snowRanges.add(new Point(600, 650));
        snowRanges.add(new Point(650, 700));
        snowRanges.add(new Point(700, 750));
        snowRanges.add(new Point(750, 800));
        snowRanges.add(new Point(800, 850));
        snowRanges.add(new Point(850, 900));
        Point range = snowRanges.get(random(snowRanges.size()));
        int count = random(range.x, range.y);
        for (int i = 0; i < count; i++) {
            float minZ = -6;
            float maxZ = -near;
            float z = random(minZ, maxZ);

            int snowPosition = random(3); // 3 is the number of the real snowflake types
            Pair<TextureRegion, PointF> regionInfo;
            if (z >= -1.1f) {
                regionInfo = bigSnowInfos.get(snowPosition);
            } else {
                regionInfo = snowInfos.get(snowPosition);
            }

            TextureRegion region = regionInfo.first;
            float width = regionInfo.second.x;
            float height = regionInfo.second.y;
            snow.add(new Snow(region, width, height, z, direction));
        }
    }

    private void prepareSnowflake(TextureInfo info) {
        List<Pair<TextureRegion, PointF>> snowflakeRegions = new ArrayList<>();
        snowflakeRegions.add(new Pair<>(buildRegion(info, 185, 302, 185, 185), new PointF(0.2312f, 0.2312f)));
        snowflakeRegions.add(new Pair<>(buildRegion(info, 0, 302, 185, 169), new PointF(0.2312f, 0.2112f)));
        snowflakeRegions.add(new Pair<>(buildRegion(info, 305, 88, 185, 185), new PointF(0.2312f, 0.2312f)));
        snowflake = new Snowflake(snowflakeRegions);
    }

    private void prepareStencilMask() {
        TextureInfo stencilInfo = openGLLoader.loadTexture(R.drawable.f16_stencil);
        int w = stencilInfo.imageWidth;
        int h = stencilInfo.imageHeight;
        TextureRegion region = new TextureRegion(stencilInfo.textureHandle, w, h, 0, 0, w, h);
        float width = 1.2666f;
        float height = 1.074f;
        stencilMask = new Sprite(region, width, height);
        stencilMask.setPosition(0.0f, -0.13f, -1);
    }

    private void prepareOverlay() {
        TextureInfo overlayInfo = openGLLoader.loadTexture(R.drawable.f16_overlay);
        int w = overlayInfo.imageWidth;
        int h = overlayInfo.imageHeight;
        TextureRegion region = new TextureRegion(overlayInfo.textureHandle, w, h, 0, 0, w, h);
        float width = 1.9203f;
        float height = 1.5111f;
        overlay = new Sprite(region, width, height);
        overlay.setPosition(0.0f, -0.26f, -1);
    }

    private void prepareBackground() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f16);
        int textureHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        TextureRegion region = new TextureRegion(textureHandle, w, h, 0, 0, w, h);
        background = new Sprite(region, getProjectionWidth(), getProjectionHeight());
        background.setPosition(0, 0, -1);
    }

    private void prepareCap(TextureInfo atlasInfo) {
        int inSampleSize = atlasInfo.inSampleSize;
        List<AnimatedSprite.FrameInfo> frames = new ArrayList<>();
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 315 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 630 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 945 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1260 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 292 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 315 / inSampleSize, 292 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 630 / inSampleSize, 292 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 945 / inSampleSize, 292 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1260 / inSampleSize, 292 / inSampleSize));
        // cap texture's size was reduce by half to not bloat atlas but we keep original size proportion
        float width = 0.7875f;
        float height = 0.73f;
        TextureRegion region = buildRegion(atlasInfo, 0, 0, 315, 292); // region for first frame
        cap = new Cap(region, frames, width, height);
        cap.setPosition(-0.6f, 0.18f, -1.0f);
    }

}