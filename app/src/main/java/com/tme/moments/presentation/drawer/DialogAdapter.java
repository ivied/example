package com.tme.moments.presentation.drawer;

import com.tme.moments.R;
import com.tme.moments.presentation.recyclerview.SimpleDataBoundAdapter;

/**
 * @author zoopolitic.
 */
class DialogAdapter<T> extends SimpleDataBoundAdapter<T> {

    DialogAdapter(OnItemClickListener<T> onItemClickListener) {
        super(onItemClickListener);
    }

    @Override protected int getLayoutResId(int position) {
        return R.layout.dialog_list_item;
    }
}
