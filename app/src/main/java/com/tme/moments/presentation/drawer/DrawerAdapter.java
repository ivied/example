package com.tme.moments.presentation.drawer;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tme.moments.R;
import com.tme.moments.databinding.DrawerDialogBinding;
import com.tme.moments.databinding.DrawerThemeItemBinding;
import com.tme.moments.databinding.DrawerVideoLengthItemBinding;
import com.tme.moments.databinding.DrawerVideoQualityItemBinding;
import com.tme.moments.databinding.DrawerVideoSoundItemBinding;
import com.tme.moments.databinding.DrawerLinkItemBinding;
import com.tme.moments.presentation.core.Config;
import com.tme.moments.presentation.recyclerview.DataBoundAdapter;
import com.tme.moments.presentation.recyclerview.DataBoundViewHolder;
import com.tme.moments.recording.Quality;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zoopolitic.
 */
class DrawerAdapter extends DataBoundAdapter<DrawerItem> {

    private Config config;

    interface OnItemClickListener {
        void onItemClicked(DrawerActionItem item);
    }

    interface OnLinkClickListener {
        void onLinkClick(String url);
    }

    interface OnThemeChangeListener {
        void onThemeChanged();
    }

    private final OnItemClickListener clickListener;
    private final OnLinkClickListener linkClickListener;
    private final OnThemeChangeListener onThemeChangeListener;

    DrawerAdapter(Config config,
                  @NonNull OnItemClickListener clickListener,
                  @NonNull OnLinkClickListener linkClickListener,
                  @NonNull OnThemeChangeListener onThemeChangeListener) {
        this.config = config;
        this.clickListener = clickListener;
        this.linkClickListener = linkClickListener;
        this.onThemeChangeListener = onThemeChangeListener;
    }

    @Override protected int getLayoutResId(int position) {
        return getItem(position).getLayoutResId();
    }

    @Override public DataBoundViewHolder onCreateViewHolder(ViewGroup parent, int layoutId) {
        DataBoundViewHolder holder = super.onCreateViewHolder(parent, layoutId);
        Context context = parent.getContext();
        switch (layoutId) {
            case R.layout.drawer_simple_item:
                holder.itemView.setOnClickListener(v -> {
                    DrawerActionItem item = (DrawerActionItem) getItem(holder.getAdapterPosition());
                    clickListener.onItemClicked(item);
                });
                break;
            case R.layout.drawer_link_item:
                DrawerLinkItemBinding binding = (DrawerLinkItemBinding) holder.binding;
                TextView titleView = binding.title;
                titleView.setPaintFlags(titleView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                holder.itemView.setOnClickListener(v -> {
                    DrawerLinkItem item = (DrawerLinkItem) getItem(holder.getAdapterPosition());
                    linkClickListener.onLinkClick(item.link());
                });
                break;
            case R.layout.drawer_video_length_item:
                setupVideoLengthItem(holder, context);
                break;
            case R.layout.drawer_video_quality_item:
                setupVideoQualityItem(holder, context);
                break;
            case R.layout.drawer_video_sound_item:
                DrawerVideoSoundItemBinding volumeBinding = (DrawerVideoSoundItemBinding) holder.binding;
                volumeBinding.volume.setOnCheckedChangeListener((buttonView, isChecked) -> config.setSoundEnabled(isChecked));
                break;
            case R.layout.drawer_theme_item:
                DrawerThemeItemBinding themeBinding = (DrawerThemeItemBinding) holder.binding;
                themeBinding.theme.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    if (isChecked != config.isDarkTheme()) {
                        config.setDarkTheme(isChecked);
                        onThemeChangeListener.onThemeChanged();
                    }
                });
                break;
        }
        return holder;
    }

    private void setupVideoLengthItem(DataBoundViewHolder holder, Context context) {
        DrawerVideoLengthItemBinding lengthBinding = (DrawerVideoLengthItemBinding) holder.binding;
        View.OnClickListener lengthClickListener = v -> {
            DrawerVideoLengthItem qualityItem = (DrawerVideoLengthItem) getItem(holder.getAdapterPosition());
            List<Integer> lengths = qualityItem.lengths();
            List<DialogVideoLengthItem> items = new ArrayList<>();
            for (Integer length : lengths) {
                DialogVideoLengthItem item = new DialogVideoLengthItem(length);
                item.setChecked(length == config.getRecordDuration());
                items.add(item);
            }
            AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(v.getContext(), R.style.DefaultAlertDialog))
                    .create();
            DrawerDialogBinding dialogBinding =
                    DataBindingUtil.inflate(layoutInflater, R.layout.drawer_dialog, null, false);
            dialogBinding.title.setText(R.string.video_length_label);
            RecyclerView itemsList = dialogBinding.itemsList;
            itemsList.setLayoutManager(new LinearLayoutManager(context));
            DialogAdapter<DialogVideoLengthItem> adapter = new DialogAdapter<>(item -> {
                config.setRecordDuration(item.duration);
                dialog.dismiss();
                notifyItemChanged(holder.getAdapterPosition());
            });
            adapter.addAll(items);
            itemsList.setAdapter(adapter);
            dialog.setView(dialogBinding.getRoot());
            int dialogWidth = context.getResources().getDimensionPixelSize(R.dimen.drawer_dialog_width);
            dialog.show();
            dialog.getWindow().setLayout(dialogWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        };
        lengthBinding.length.setOnClickListener(lengthClickListener);
        lengthBinding.getRoot().setOnClickListener(lengthClickListener);
    }

    private void setupVideoQualityItem(DataBoundViewHolder holder, Context context) {
        DrawerVideoQualityItemBinding qualityBinding = (DrawerVideoQualityItemBinding) holder.binding;
        View.OnClickListener qualityClickListener = v -> {
            DrawerVideoQualityItem qualityItem = (DrawerVideoQualityItem) getItem(holder.getAdapterPosition());
            List<Quality> qualities = qualityItem.qualities();
            List<DialogVideoQualityItem> items = new ArrayList<>();
            for (Quality quality : qualities) {
                DialogVideoQualityItem item = new DialogVideoQualityItem(quality);
                item.setChecked(quality == config.getQuality());
                items.add(item);
            }
            AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(v.getContext(), R.style.DefaultAlertDialog))
                    .create();
            DrawerDialogBinding dialogBinding =
                    DataBindingUtil.inflate(layoutInflater, R.layout.drawer_dialog, null, false);
            dialogBinding.title.setText(R.string.video_quality_label);
            RecyclerView itemsList = dialogBinding.itemsList;
            itemsList.setLayoutManager(new LinearLayoutManager(context));
            DialogAdapter<DialogVideoQualityItem> adapter = new DialogAdapter<>(item -> {
                config.setQuality(item.quality);
                dialog.dismiss();
                notifyItemChanged(holder.getAdapterPosition());
            });
            adapter.addAll(items);
            itemsList.setAdapter(adapter);
            dialog.setView(dialogBinding.getRoot());
            int dialogWidth = context.getResources().getDimensionPixelSize(R.dimen.drawer_dialog_width);
            dialog.show();
            dialog.getWindow().setLayout(dialogWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        };
        qualityBinding.quality.setOnClickListener(qualityClickListener);
        qualityBinding.getRoot().setOnClickListener(qualityClickListener);
    }

    @Override public void onBindViewHolder(DataBoundViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        switch (holder.getItemViewType()) {
            case R.layout.drawer_video_length_item:
                Context context = holder.binding.getRoot().getContext();
                DrawerVideoLengthItemBinding lengthBinding = (DrawerVideoLengthItemBinding) holder.binding;
                lengthBinding.length.setText(context.getString(R.string.video_length_item_title, config.getRecordDuration()));
                break;
            case R.layout.drawer_video_quality_item:
                DrawerVideoQualityItemBinding qualityBinding = (DrawerVideoQualityItemBinding) holder.binding;
                Quality quality = config.getQuality();
                TextView textView = qualityBinding.quality;
                if (quality != null) {
                    switch (quality) {
                        case LOW:
                            textView.setText(R.string.video_quality_low);
                            break;
                        case MEDIUM:
                            textView.setText(R.string.video_quality_medium);
                            break;
                        case HIGH:
                            textView.setText(R.string.video_quality_high);
                            break;
                    }
                }
                break;
            case R.layout.drawer_video_sound_item:
                DrawerVideoSoundItemBinding soundBinding = (DrawerVideoSoundItemBinding) holder.binding;
                SwitchCompat volume = soundBinding.volume;
                volume.setChecked(config.isSoundEnabled());
                soundBinding.getRoot().setOnClickListener(v -> volume.setChecked(!volume.isChecked()));
                break;
            case R.layout.drawer_theme_item:
                DrawerThemeItemBinding themeBinding = (DrawerThemeItemBinding) holder.binding;
                SwitchCompat theme = themeBinding.theme;
                theme.setChecked(config.isDarkTheme());
                themeBinding.getRoot().setOnClickListener(v -> theme.setChecked(!theme.isChecked()));
                break;
        }
    }
}
