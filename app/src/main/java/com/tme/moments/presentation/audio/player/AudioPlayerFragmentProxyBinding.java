package com.tme.moments.presentation.audio.player;

import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.tme.moments.databinding.AudioPlayerLargeFragmentBinding;
import com.tme.moments.databinding.AudioPlayerSmallFragmentBinding;
import com.tme.moments.presentation.widget.MaxHeightConstraintLayout;

class AudioPlayerFragmentProxyBinding {

    public MaxHeightConstraintLayout root;
    public ImageView clearAudio;
    public ImageView play;
    public TextView playedTime;
    public TextView songName;
    public TextView totalDuration;
    public AudioSeekBar seekBar;

    AudioPlayerFragmentProxyBinding(@Nullable ViewDataBinding original) {
        if (original instanceof AudioPlayerSmallFragmentBinding) {
            AudioPlayerSmallFragmentBinding binding = (AudioPlayerSmallFragmentBinding) original;
            root = binding.root;
            clearAudio = binding.clearAudio;
            play = binding.play;
            playedTime = binding.playedTime;
            songName = binding.songName;
            totalDuration = binding.totalDuration;
            seekBar = binding.seekBar;
        } else if (original instanceof AudioPlayerLargeFragmentBinding) {
            AudioPlayerLargeFragmentBinding binding = (AudioPlayerLargeFragmentBinding) original;
            root = binding.root;
            clearAudio = binding.clearAudio;
            play = binding.play;
            playedTime = binding.playedTime;
            songName = binding.songName;
            totalDuration = binding.totalDuration;
            seekBar = binding.seekBar;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
