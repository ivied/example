package com.tme.moments.presentation.scene.f6.peafowl;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.math.Vector3;
import com.tme.moments.presentation.Const;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseBlurredScene;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class LovelyPeafowl extends BaseBlurredScene {

    private TextureInfo atlasInfo;
    private List<Feather> featherList;
    private List<Feather> finishedFeather;
    private List<LongArtifact> longArtifacts;
    private List<ShortArtifact> shortArtifacts;
    private DecalBatch overBatch;
    private DecalBatch underBatch;

    // overlay animation
    private long shakeWaitTime;
    private float rotationSpeed;
    private float maxRotationAngle;
    private TextureRegion featherRegion;
    private TextureRegion flower1Region;
    private TextureRegion flower2Region;

    @Override protected int getStencilResId() {
        return R.drawable.f6_stencil;
    }

    @Override protected int getOverlayResId() {
        return R.drawable.f6;
    }

    @Override protected float getCameraPositionX() {
        return -0.1518f;
    }

    @Override protected float getCameraPositionY() {
        return -0.0648f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.6935f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.5175f;
    }

    @Override protected TextureRegion getFrameRegion() {
        TextureInfo frameInfo = openGLLoader.loadTexture(R.drawable.f6);
        return new TextureRegion(frameInfo.textureHandle, frameInfo.imageWidth, frameInfo.imageHeight,
                0, 0, frameInfo.imageWidth, frameInfo.imageHeight);
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        super.prepareObjects(surfaceWidth, surfaceHeight);
        underBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        overBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        atlasInfo = openGLLoader.loadTexture(R.drawable.f6_atlas);
        featherRegion = buildRegion(atlasInfo, 294, 430, 170, 222);
        flower1Region = buildRegion(atlasInfo, 0, 652, 202, 120);
        flower2Region = buildRegion(atlasInfo, 202, 652, 202, 120);
        featherList = new ArrayList<>();
        finishedFeather = new ArrayList<>();
        prepareLongArtifacts(atlasInfo);
        prepareShortArtifacts(atlasInfo, underBatch, overBatch);
        underBatch.initialize(shortArtifacts.size() / 2);
        overBatch.initialize(longArtifacts.size() + shortArtifacts.size() / 2 + 50); // 50 for feather
        overBatch.addAll(featherList);
        overBatch.addAll(longArtifacts);

        maxRotationAngle = 8f;
        shakeWaitTime = TimeUnit.MILLISECONDS.toNanos(random(3000, 4000));
        rotationSpeed = 42f / Const.NANOS_IN_SECOND;
    }

    @Override protected void onBlurredImageDrawn(long deltaNanos) {
        super.onBlurredImageDrawn(deltaNanos);
        boolean objectsEnabled = config.isObjectsEnabled();

        if (objectsEnabled) {
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            underBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (ShortArtifact shortArtifact : shortArtifacts) shortArtifact.step(deltaNanos);
        }

        if (!objectsEnabled) {
            overlay.setRotationZ(0);
        } else {
            if (shakeWaitTime > 0) {
                shakeWaitTime -= deltaNanos;
            } else {
                float deltaRotation = rotationSpeed * deltaNanos;
                float rotationZ = overlay.getRotation().getAngleAround(Vector3.Z);
                float newRotation = Math.max(0, Math.min(maxRotationAngle, rotationZ + deltaRotation));
                overlay.setRotationZ(newRotation);
                if (newRotation == maxRotationAngle) {
                    rotationSpeed *= -1;
                    throwFeather();
                }
                if (newRotation == 0 && rotationSpeed < 0) {
                    rotationSpeed *= -1;
                    shakeWaitTime = TimeUnit.MILLISECONDS.toNanos(random(5_000, 8_000));
                }
            }
        }
    }

    private void throwFeather() {
        float width = 0.2125f;
        float height = 0.2775f;
        int count = random(6, 10);
        float x = 0.55f;
        float y = 0.4f;
        for (int i = 0; i < count; i++) {
            Feather feather = new Feather(featherRegion, width, height, x, y, random());
            featherList.add(feather);
            overBatch.add(feather);
        }
        width = 0.2525f;
        height = 0.15f;
        Feather flower1 = new Feather(flower1Region, width, height, x, y, false);
        Feather flower2 = new Feather(flower2Region, width, height, x, y, false);
        featherList.add(flower1);
        featherList.add(flower2);
        overBatch.add(flower1);
        overBatch.add(flower2);
    }

    @Override protected void onOverlayDrawn(long deltaNanos) {
        super.onOverlayDrawn(deltaNanos);
        if (config.isObjectsEnabled()) {
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            overBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Feather feather : featherList) {
                feather.step(deltaNanos);
                if (feather.isFinished()) {
                    finishedFeather.add(feather);
                }
            }
            if (!finishedFeather.isEmpty()) {
                featherList.removeAll(finishedFeather);
                overBatch.removeAll(finishedFeather);
                finishedFeather.clear();
            }
            for (LongArtifact longArtifact : longArtifacts) longArtifact.step(deltaNanos);
        }
    }

    @Override public void release() {
        super.release();
        if (atlasInfo != null) {
            int[] values = new int[1];
            values[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, values, 0);
            GlUtil.checkGlError("Delete offscreen texture");
        }
    }

    private void prepareLongArtifacts(TextureInfo info) {
        float width = 0.1837f;
        float height = 0.2687f;
        List<TextureRegion> longArtifactRegions = new ArrayList<>();
        longArtifactRegions.add(buildRegion(info, 147, 430, 147, 215));
        longArtifactRegions.add(buildRegion(info, 294, 215, 147, 215));
        longArtifactRegions.add(buildRegion(info, 282, 0, 147, 215));
        longArtifactRegions.add(buildRegion(info, 0, 215, 147, 215));
        longArtifactRegions.add(buildRegion(info, 147, 215, 147, 215));
        longArtifactRegions.add(buildRegion(info, 0, 430, 147, 215));

        longArtifacts = new ArrayList<>(6);
        for (int i = 0; i < 8; i++) {
            TextureRegion region = longArtifactRegions.get(random(longArtifactRegions.size()));
            longArtifacts.add(new LongArtifact(region, width, height));
        }
    }

    private void prepareShortArtifacts(TextureInfo atlasInfo, DecalBatch underBatch,
                                       DecalBatch overBatch) {
        float width;
        float height;
        width = 0.0587f;
        height = 0.0575f;
        List<TextureRegion> shortArtifactRegions = new ArrayList<>();
        shortArtifactRegions.add(buildRegion(atlasInfo, 141, 0, 47, 46));
        shortArtifactRegions.add(buildRegion(atlasInfo, 235, 0, 47, 46));
        shortArtifactRegions.add(buildRegion(atlasInfo, 188, 0, 47, 46));
        shortArtifactRegions.add(buildRegion(atlasInfo, 47, 0, 47, 46));
        shortArtifactRegions.add(buildRegion(atlasInfo, 0, 0, 47, 46));
        shortArtifactRegions.add(buildRegion(atlasInfo, 94, 0, 47, 46));
        shortArtifacts = new ArrayList<>();

        int shortArtifactsCount = 28;

        for (int i = 0; i < shortArtifactsCount; i++) {
            TextureRegion region = shortArtifactRegions.get(random(shortArtifactRegions.size()));
            ShortArtifact artifact = new ShortArtifact(region, width, height);
            shortArtifacts.add(artifact);
            if (i % 2 == 0) {
                underBatch.add(artifact);
            } else {
                overBatch.add(artifact);
            }
        }
    }
}