package com.tme.moments.presentation.core;

import android.hardware.Camera;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.tme.moments.presentation.LocalPreferences;
import com.tme.moments.presentation.RecordMode;
import com.tme.moments.recording.Quality;
import com.tme.moments.recording.RecordConfig;

/**
 * @author zoopolitic.
 */
public class Config {

    public enum Screen {
        SCENE_LIST,
        AUDIO_PLAYER
    }

    private static final int DEFAULT_CAMERA = Camera.CameraInfo.CAMERA_FACING_FRONT;

    private RecordMode recordMode = RecordMode.CAPTURE_VIDEO;

    private LocalPreferences preferences;
    private String audioFilePath;
    private int orientation;
    private int cameraType = DEFAULT_CAMERA;
    private int cameraPreviewWidth;
    private int cameraPreviewHeight;
    @NonNull
    private ScreenshotConfig screenshotConfig = new ScreenshotConfig();
    private RecordConfig recordConfig;
    private Uri imageUri;
    private int showAdCount = 1;
    private boolean objectsEnabled = true;
    private boolean backgroundEffectsEnabled = true;
    private float density;
    private long audioTimeMillis;
    private Screen currentScreen = Screen.SCENE_LIST;

    public Config(LocalPreferences preferences) {
        this.preferences = preferences;
    }

    public String getAudioFilePath() {
        return audioFilePath;
    }

    public void setAudioFilePath(String audioFilePath) {
        setCurrentScreen(audioFilePath == null ? Screen.SCENE_LIST : Screen.AUDIO_PLAYER);
        this.audioFilePath = audioFilePath;
    }

    public boolean isSoundEnabled() {
        return preferences.isSoundEnabled();
    }

    public void setSoundEnabled(boolean enabled) {
        preferences.setSoundEnabled(enabled);
    }

    public int getRecordDuration() {
        return preferences.getRecordDuration();
    }

    public void setRecordDuration(int duration) {
        preferences.setRecordDuration(duration);
    }

    public int getCameraPreviewWidth() {
        return cameraPreviewWidth;
    }

    public int getCameraPreviewHeight() {
        return cameraPreviewHeight;
    }

    public void setCameraPreviewSize(int width, int height) {
        cameraPreviewWidth = width;
        cameraPreviewHeight = height;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public int getCameraType() {
        return cameraType;
    }

    public void setCameraType(int cameraType) {
        this.cameraType = cameraType;
    }

    public RecordConfig getRecordConfig() {
        return recordConfig;
    }

    public void setRecordConfig(RecordConfig recordConfig) {
        this.recordConfig = recordConfig;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

    public void onAdShown() {
        this.showAdCount++;
    }

    public boolean isShowAd() {
        return showAdCount %3 == 0;
    }

    public void onMusicAdd(){
        preferences.musicAdd();
    }

    public boolean isShowMusicAd(){
        return preferences.isShowMusicAdd();
    }


    public boolean isObjectsEnabled() {
        return objectsEnabled;
    }

    public void setObjectsEnabled(boolean objectsEnabled) {
        this.objectsEnabled = objectsEnabled;
    }

    public boolean isBackgroundEffectsEnabled() {
        return backgroundEffectsEnabled;
    }

    public void setBackgroundEffectsEnabled(boolean backgroundEffectsEnabled) {
        this.backgroundEffectsEnabled = backgroundEffectsEnabled;
    }

    public RecordMode getRecordMode() {
        return recordMode;
    }

    public void switchMode() {
        recordMode = recordMode == RecordMode.CAPTURE_VIDEO ? RecordMode.CAPTURE_IMAGE : RecordMode.CAPTURE_VIDEO;
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }

    public long getAudioTimeMillis() {
        return audioTimeMillis;
    }

    public void setAudioTimeMillis(long audioTime) {
        this.audioTimeMillis = audioTime;
    }

    public Quality getQuality() {
        return preferences.getQuality();
    }

    public void setQuality(Quality quality) {
        preferences.setQuality(quality);
    }

    public boolean isDarkTheme() {
        return preferences.isDarkTheme();
    }

    public void setDarkTheme(boolean darkTheme) {
        preferences.setDarkTheme(darkTheme);
    }

    public Screen getCurrentScreen() {
        return currentScreen;
    }

    public void setCurrentScreen(Screen currentScreen) {
        this.currentScreen = currentScreen;
    }

    public void setShowSavingContentInfoDialog(boolean show) {
        preferences.setShowSavingContentInfoDialog(show);
    }

    public boolean isShowSavingContentInfoDialog() {
        return preferences.isShowSavingContentInfoDialog();
    }

    @NonNull
    public ScreenshotConfig getScreenshotConfig() {
        return screenshotConfig;
    }
}
