package com.tme.moments.presentation;

import com.tme.moments.recording.Quality;

/**
 * @author zoopolitic.
 */
public interface LocalPreferences {
    Quality getQuality();
    void setQuality(Quality quality);
    int getRecordDuration();
    void setRecordDuration(int duration);
    boolean isSoundEnabled();
    void setSoundEnabled(boolean enabled);
    void setDarkTheme(boolean darkTheme);
    boolean isDarkTheme();
    void setShowSavingContentInfoDialog(boolean show);
    boolean isShowSavingContentInfoDialog();
    void musicAdd();
    boolean isShowMusicAdd();
}
