package com.tme.moments.presentation.scene.f8.sunflower.field;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.Const;

import java.util.List;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Bird extends AnimatedSprite {

    private float speed;
    private boolean finished;
    private float inclineSpeed;
    private float maxY;
    private final float initialY;
    private float currentHangTimeNanos;
    private final float hangTimeInSeconds;

    Bird(TextureRegion region, List<FrameInfo> frameInfoList, float width, float height,
         float x, float y, float speedPerSecond, float inclineY, float inclineSpeedPerSecond,
         float hangTimeInSeconds) {
        super(region, frameInfoList, width, height);
        // randomize initial frame to de-synchronize wings swing
        setCurrentFrameIndex(random(4, 8));
        this.initialY = y;
        speed = speedPerSecond / Const.NANOS_IN_SECOND;
        this.inclineSpeed = inclineSpeedPerSecond / Const.NANOS_IN_SECOND;
        this.hangTimeInSeconds = hangTimeInSeconds * Const.NANOS_IN_SECOND;
        currentHangTimeNanos = hangTimeInSeconds;
        maxY = y + inclineY;
        setTextureRegion(region);
        setColor(1, 1, 1, 1);
        setPosition(x, y, -1.0f);
        setDimensions(width, height);
    }

    public boolean isFinished() {
        return finished;
    }

    @Override protected int getDesiredFPS() {
        return 30;
    }

    @Override
    public void step(long deltaNanos) {
        super.step(deltaNanos);

        float deltaIncline = inclineSpeed * deltaNanos;
        float y = getY();
        y += deltaIncline;
        float nextY = Math.min(maxY, Math.max(initialY, y));
        if (nextY == maxY) {
            if (currentHangTimeNanos > 0) {
                currentHangTimeNanos -= deltaNanos;
            } else {
                currentHangTimeNanos = hangTimeInSeconds;
                inclineSpeed = -inclineSpeed;
            }
        } else if (nextY == initialY) {
            if (currentHangTimeNanos > 0) {
                currentHangTimeNanos -= deltaNanos;
            } else {
                currentHangTimeNanos = hangTimeInSeconds;
                inclineSpeed = Math.abs(inclineSpeed);
            }
        } else {
            setY(nextY);
        }

        float deltaX = speed * deltaNanos;
        float x = getX();
        x += deltaX;
        setX(x);
        if ((speed < 0 && x < -1.1f) || (speed > 0 && x > 1.1f)) {
            finished = true;
        }
        update();
    }
}
