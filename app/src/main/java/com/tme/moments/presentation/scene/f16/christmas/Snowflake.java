package com.tme.moments.presentation.scene.f16.christmas;

import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.util.Pair;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
final class Snowflake extends Decal {

    private float zRotationSpeed;
    private float speed;

    private float p0x, p0y;
    private float p1x, p1y;
    private float p2x, p2y;
    private float p3x, p3y;

    private float t;
    private float startDelayNanos;
    private List<Pair<TextureRegion, PointF>> infos;

    Snowflake(@NonNull List<Pair<TextureRegion, PointF>> infos) {
        this.infos = infos;
        init();
        setStartDelay(7000);
    }

    private void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float deltaZ = zRotationSpeed * deltaNanos;
        rotateZ(deltaZ);

        float delta = speed * deltaNanos;
        t += delta;

        float single = (1 - t);
        float quadratic = (1 - t) * (1 - t);
        float cube = (1 - t) * (1 - t) * (1 - t);
        float x = cube * p0x + 3 * t * quadratic * p1x + 3 * t * t * single * p2x + t * t * t * p3x;
        float y = cube * p0y + 3 * t * quadratic * p1y + 3 * t * t * single * p2y + t * t * t * p3y;

        setX(x);
        setY(y);
        if (t >= 1) {
            init();
        }
    }

    protected void init() {
        final boolean fromLeft = random();
        if (fromLeft) {
            p0x = random(-2f, -1f - getWidth() / 2);
            p0y = random(0f, 1.0f);
            p1x = random(-0.5f, 0.5f);
            p1y = random(1.5f, 2f);
            p2x = random(2f, 3f);
            p2y = random(-1.5f, -0.5f);
            p3x = random(-2f, -1f - getWidth() / 2);
            p3y = random(-1.3f, 0);
        } else {
            p0x = random(1f + getWidth() / 2, 2f);
            p0y = random(0f, 1.0f);
            p1x = random(-0.5f, 0.5f);
            p1y = random(1.5f, 2f);
            p2x = random(-3f, -2f);
            p2y = random(-1.5f, -0.5f);
            p3x = random(1f + getWidth() / 2, 2f);
            p3y = random(-1.3f, 0);
        }

        Pair<TextureRegion, PointF> info = infos.get(random(infos.size()));
        setTextureRegion(info.first);
        setDimensions(info.second.x, info.second.y);
        zRotationSpeed = random(270f, 630f) / Const.NANOS_IN_SECOND;
        if (fromLeft) {
            zRotationSpeed *= -1;
        }

        setStartDelay(random(3500, 4500));
        speed = random(0.22f, 0.25f) / Const.NANOS_IN_SECOND;
        setColor(1, 1, 1, 1);
        setPosition(p0x, p0y, -1.0f);
        t = 0;
    }
}
