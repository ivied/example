package com.tme.moments.presentation.recyclerview;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.tme.moments.BR;

public class DataBoundViewHolder extends RecyclerView.ViewHolder {

    public final ViewDataBinding binding;

    public static DataBoundViewHolder create(LayoutInflater inflater, ViewGroup parent,
                                             int layoutId) {
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, layoutId, parent, false);
        return new DataBoundViewHolder(binding);
    }

    protected DataBoundViewHolder(ViewDataBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bindTo(Object variable) {
        binding.setVariable(BR.data, variable);
        binding.executePendingBindings();
    }
}
