package com.tme.moments.presentation.base;

import android.os.Bundle;

/**
 * @author zoopolitic.
 */
interface Presenter<V> {
    void attachView(V v);
    void detachView();
    void onSaveInstanceState(Bundle outState);
    void onRestoreInstanceState(Bundle savedInstanceState);
}
