package com.tme.moments.presentation.recyclerview;

import android.support.annotation.LayoutRes;

/**
 * @author zoopolitic.
 */
public interface ListItem {

    @LayoutRes int getLayoutResId();
}
