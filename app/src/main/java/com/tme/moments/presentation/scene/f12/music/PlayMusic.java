package com.tme.moments.presentation.scene.f12.music;

import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;
import com.tme.moments.presentation.scene.Glow;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_ALWAYS;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_EQUAL;
import static android.opengl.GLES20.GL_KEEP;
import static android.opengl.GLES20.GL_REPLACE;
import static android.opengl.GLES20.GL_STENCIL_BUFFER_BIT;
import static android.opengl.GLES20.GL_STENCIL_TEST;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glColorMask;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glStencilFunc;
import static android.opengl.GLES20.glStencilOp;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class PlayMusic extends BaseScene {

    private final Object lock = new Object();

    private int stencilProgramHandle = -1;
    private int uStencilProjectionMatrixHandle = -1;
    private int aStencilPositionHandle = -1;
    private int aStencilColorHandle = -1;
    private int aStencilTexCoordHandle = -1;

    private TextureInfo atlasInfo;
    private List<Glow> glows;
    private List<Leaf> leaves;
    private DecalBatch glowsBatch;
    private DecalBatch leavesBatch;
    private Butterfly pinkButterfly;
    private Butterfly blueButterfly;
    private Guitar guitar;
    private DecalBatch guitarBatch;
    private Sprite stencilMask;
    private boolean leavesMoveLeft;
    private boolean blueButterflyActive;

    @Override protected float getCameraPositionX() {
        return 0.028f;
    }

    @Override protected float getCameraPositionY() {
        return 0.18f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.85f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.62f;
    }

    @Override protected TextureRegion getFrameRegion() {
        TextureInfo frameInfo = openGLLoader.loadTexture(R.drawable.f12_background);
        return new TextureRegion(frameInfo.textureHandle, frameInfo.imageWidth, frameInfo.imageHeight,
                0, 0, frameInfo.imageWidth, frameInfo.imageHeight);
    }

    @Override public void init() {
        super.init();
        if (stencilProgramHandle == -1) {
            prepareStencilProgram();
        }
        leavesMoveLeft = random();
    }

    private void prepareStencilProgram() {
        stencilProgramHandle =
                openGLLoader.createProgram(R.raw.vertex_shader, R.raw.stencil_fragment_shader);

        aStencilPositionHandle = glGetAttribLocation(stencilProgramHandle, "a_position");
        GlUtil.checkLocation(aStencilPositionHandle, "a_position");
        glEnableVertexAttribArray(aStencilPositionHandle);

        aStencilColorHandle = glGetAttribLocation(stencilProgramHandle, "a_color");
        GlUtil.checkLocation(aStencilColorHandle, "a_color");
        glEnableVertexAttribArray(aStencilColorHandle);

        aStencilTexCoordHandle = glGetAttribLocation(stencilProgramHandle, "a_texCoord");
        GlUtil.checkLocation(aStencilTexCoordHandle, "a_texCoord");
        glEnableVertexAttribArray(aStencilTexCoordHandle);

        uStencilProjectionMatrixHandle = glGetUniformLocation(stencilProgramHandle, "u_projTrans");
        GlUtil.checkLocation(uStencilProjectionMatrixHandle, "u_projTrans");
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        prepareStencilMask();
        atlasInfo = openGLLoader.loadTexture(R.drawable.f12_atlas);
        prepareGlows(atlasInfo);
        blueButterflyActive = random();
        preparePinkButterfly(atlasInfo);
        prepareBlueButterfly(atlasInfo);
        prepareLeaves(atlasInfo);
        prepareGuitar(atlasInfo);
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glUseProgram(mainProgramHandle);
        drawFrame();
        glEnable(GL_STENCIL_TEST);

        glColorMask(false, false, false, false);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

        // draw mask
        glUseProgram(stencilProgramHandle);
        stencilMask.draw(uStencilProjectionMatrixHandle, projectionMatrix, aStencilPositionHandle,
                aStencilTexCoordHandle, aStencilColorHandle);

        glColorMask(true, true, true, true);
        glStencilFunc(GL_EQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

        if (config.isObjectsEnabled()) {
            glDisable(GL_STENCIL_TEST);
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            glowsBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Glow glow : glows) {
                glow.step(deltaNanos);
            }
            glEnable(GL_STENCIL_TEST);
        }

        if (mode == Mode.IMAGE) {
            glUseProgram(mainProgramHandle);
            drawImage();
        } else {
            drawCameraInput(cameraTexture);
        }

        glDisable(GL_STENCIL_TEST);

        glUseProgram(mainProgramHandle);
        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        guitarBatch.render(uProjectionMatrixLoc, projectionMatrix);
        if (config.isObjectsEnabled()) {
            guitar.step(deltaNanos);
        }

        if (config.isObjectsEnabled()) {
            leavesBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Leaf leaf : leaves) {
                leaf.step(deltaNanos);
            }
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            if (blueButterfly.isStopped() && pinkButterfly.isStopped()) {
                synchronized (lock) {
                    if (blueButterflyActive) {
                        blueButterfly.setStopped(false);
                    } else {
                        pinkButterfly.setStopped(false);
                    }
                    blueButterflyActive = random();
                }
            }
            blueButterfly.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            blueButterfly.step(deltaNanos);

            pinkButterfly.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            pinkButterfly.step(deltaNanos);
        }
    }

    private void prepareStencilMask() {
        TextureInfo stencilInfo = openGLLoader.loadTexture(R.drawable.f12_stencil);
        int w = stencilInfo.imageWidth;
        int h = stencilInfo.imageHeight;
        TextureRegion region = new TextureRegion(stencilInfo.textureHandle, w, h, 0, 0, w, h);
        float width = getDesiredCameraWidthFraction() * getProjectionWidth();
        float height = getDesiredCameraHeightFraction() * getProjectionHeight();
        stencilMask = new Sprite(region, width, height);
        stencilMask.setPosition(getCameraPositionX(), getCameraPositionY(), -1);
    }

    private void prepareGlows(TextureInfo info) {
        Pair<TextureRegion, PointF> glow1 = new Pair<>(buildRegion(info, 0, 424, 89, 91), new PointF(0.1112f, 0.1137f));
        Pair<TextureRegion, PointF> glow2 = new Pair<>(buildRegion(info, 89, 424, 89, 91), new PointF(0.1112f, 0.1137f));
        Pair<TextureRegion, PointF> glow3 = new Pair<>(buildRegion(info, 178, 424, 67, 68), new PointF(0.0837f, 0.085f));
        Pair<TextureRegion, PointF> glow4 = new Pair<>(buildRegion(info, 245, 424, 67, 67), new PointF(0.0837f, 0.0837f));

        glows = new ArrayList<>();
        glows.addAll(generateGlows(glow1, 40, 0.85f));
        glows.addAll(generateGlows(glow2, 40, 0.85f));
        glows.addAll(generateGlows(glow3, 15, 0.7f));
        glows.addAll(generateGlows(glow4, 15, 0.7f));
        glowsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        glowsBatch.initialize(glows.size());
        glowsBatch.addAll(glows);
    }

    private List<Glow> generateGlows(Pair<TextureRegion, PointF> infoPair, int count,
                                     float scale) {
        List<Glow> items = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            TextureRegion region = infoPair.first;
            float width = infoPair.second.x;
            float height = infoPair.second.y;
            Glow glow = new Glow(region, width, height);
            glow.setSpeed(0.09f, 0.12f);
            glow.setScale(scale);
            glow.setStartDelay(random(8_000, 10_000));
            items.add(glow);
        }
        return items;
    }

    private void preparePinkButterfly(TextureInfo info) {
        int inSampleSize = info.inSampleSize;
        TextureRegion region = buildRegion(info, 0, 214, 225, 210);
        float width = 0.2812f;
        float height = 0.2625f;
        List<AnimatedSprite.FrameInfo> infoList = new ArrayList<>();
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 900 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 1125 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 1350 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 0 / inSampleSize, 214 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 225 / inSampleSize, 214 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 450 / inSampleSize, 214 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 675 / inSampleSize, 214 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 900 / inSampleSize, 214 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 1125 / inSampleSize, 214 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 1350 / inSampleSize, 214 / inSampleSize));
        pinkButterfly = new PinkButterfly(region, infoList, width, height, false);
        pinkButterfly.setStartDelay(blueButterflyActive ? random(3000, 5000) : 0);
    }

    private void prepareBlueButterfly(TextureInfo info) {
        int inSampleSize = info.inSampleSize;
        TextureRegion region = buildRegion(info, 0, 214, 225, 214);
        float width = 0.2812f;
        float height = 0.2675f;
        List<AnimatedSprite.FrameInfo> infoList = new ArrayList<>();
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 0 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 225 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 450 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 675 / inSampleSize, 0 / inSampleSize));
        blueButterfly = new Butterfly(region, infoList, width, height, true);
        blueButterfly.setStartDelay(blueButterflyActive ? 0 : random(3000, 5000));
    }

    private void prepareLeaves(TextureInfo info) {
        Pair<TextureRegion, PointF> leaf1 = new Pair<>(buildRegion(info, 785, 424, 192, 210), new PointF(0.24f, 0.2625f));
        Pair<TextureRegion, PointF> leaf2 = new Pair<>(buildRegion(info, 977, 424, 192, 237), new PointF(0.24f, 0.2962f));
        Pair<TextureRegion, PointF> leaf3 = new Pair<>(buildRegion(info, 1169, 424, 74, 213), new PointF(0.0925f, 0.2662f));
        Pair<TextureRegion, PointF> leaf4 = new Pair<>(buildRegion(info, 1243, 424, 74, 122), new PointF(0.0925f, 0.1525f));
        Pair<TextureRegion, PointF> leaf5 = new Pair<>(buildRegion(info, 1317, 424, 52, 173), new PointF(0.065f, 0.2162f));
        Pair<TextureRegion, PointF> leaf6 = new Pair<>(buildRegion(info, 1369, 424, 192, 226), new PointF(0.24f, 0.2825f));

        leaves = new ArrayList<>();
        generateLeaves(leaf1, 8, 3);
        generateLeaves(leaf2, 8, 2);
        generateLeaves(leaf3, 8, 3);
        generateLeaves(leaf4, 2, 1);
        generateLeaves(leaf5, 2, 1);
        generateLeaves(leaf6, 2, 1);

        leavesBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        leavesBatch.initialize(leaves.size());
        leavesBatch.addAll(leaves);
    }

    private void prepareGuitar(TextureInfo info) {
        TextureRegion region = buildRegion(info, 312, 424, 473, 999);
        float width = 0.8759f;
        float height = 1.85f;
        guitar = new Guitar(region, width, height);
        guitar.setPosition(-0.45f, -0.1f, -1.0f);
        guitarBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        guitarBatch.initialize(1);
        guitarBatch.add(guitar);
    }

    private void generateLeaves(Pair<TextureRegion, PointF> info, int count, int count3d) {
        for (int i = 0; i < count; i++) {
            TextureRegion region = info.first;
            float width = info.second.x;
            float height = info.second.y;
            leaves.add(new Leaf(region, width, height, leavesMoveLeft, i >= count - count3d));
        }
    }

    @Override public void release() {
        super.release();
        glDeleteProgram(stencilProgramHandle);
        stencilProgramHandle = -1;
        if (atlasInfo != null) {
            int[] values = new int[1];
            values[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, values, 0);
            GlUtil.checkGlError("Release " + getClass().getSimpleName());
        }
    }

}