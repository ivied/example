package com.tme.moments.presentation.utils;

import java.util.Locale;

/**
 * @author zoopolitic.
 */
public class GaussianUtils {

    private GaussianUtils() {
    }

    public static String vertexShaderForOptimizedBlurOfRadius(int blurRadius, float sigma) {
        if (blurRadius < 1) {
            return null;
        }

        // First, generate the normal Gaussian weights for a given sigma
        float[] standardGaussianWeights = new float[blurRadius + 1];
        float sumOfWeights = 0.0f;
        for (int index = 0; index < blurRadius + 1; index++) {
            standardGaussianWeights[index] =
                    (float) ((1.0 / Math.sqrt(2.0 * Math.PI * Math.pow(sigma, 2.0)))
                            * Math.exp(-Math.pow(index, 2.0) / (2.0 * Math.pow(sigma, 2.0))));
            if (index == 0) {
                sumOfWeights += standardGaussianWeights[index];
            } else {
                sumOfWeights += 2.0 * standardGaussianWeights[index];
            }
        }

        // Next, normalize these weights to prevent the clipping of the Gaussian curve at the end of the discrete samples from reducing luminance
        for (int index = 0; index < blurRadius + 1; index++) {
            standardGaussianWeights[index] = standardGaussianWeights[index] / sumOfWeights;
        }

        // From these weights we calculate the offsets to read interpolated values from
        int numberOfOptimizedOffsets = Math.min(blurRadius / 2 + (blurRadius % 2), 7);
        float[] optimizedGaussianOffsets = new float[numberOfOptimizedOffsets];

        for (int currentOptimizedOffset = 0; currentOptimizedOffset < numberOfOptimizedOffsets; currentOptimizedOffset++) {
            float firstWeight = standardGaussianWeights[currentOptimizedOffset * 2 + 1];
            float secondWeight = standardGaussianWeights[currentOptimizedOffset * 2 + 2];

            float optimizedWeight = firstWeight + secondWeight;

            optimizedGaussianOffsets[currentOptimizedOffset] =
                    (firstWeight * (currentOptimizedOffset * 2 + 1)
                            + secondWeight * (currentOptimizedOffset * 2 + 2)) / optimizedWeight;
        }
        Locale locale = Locale.US;
        StringBuilder builder = new StringBuilder();
        // Header
        builder.append(String.format(locale, ""
                        + "uniform mat4 uTransformM;\n\r"
                        + "uniform float uTexelWidthOffset;\n\r"
                        + "uniform float uTexelHeightOffset;\n\r"
                        + "attribute vec2 aPosition;\n\r"
                        + "attribute vec2 aTexCoords;\n\r"
                        + "\n\r"
                        + "varying vec2 blurCoordinates[%d];\n\r"
                        + "\n\r"
                        + "void main() {\n\r"
                        + "\tgl_Position =  vec4(aPosition, 0.0, 1.0);\n\r"
                        + "\tvec2 texCoords = (uTransformM  * vec4(aTexCoords, 0.0, 1.0)).xy;\n\r"
                        + "\n\r"
                        + "\tvec2 singleStepOffset = vec2(uTexelWidthOffset, uTexelHeightOffset);\n\r",
                1 + (numberOfOptimizedOffsets * 2)));

        // Inner offset loop
        builder.append("\tblurCoordinates[0] = texCoords.xy;\n\r");
        for (int currentOptimizedOffset = 0; currentOptimizedOffset < numberOfOptimizedOffsets; currentOptimizedOffset++) {
            builder.append(String.format(locale, ""
                            + "\tblurCoordinates[%d] = texCoords.xy + singleStepOffset * %f;\n\r"
                            + "\tblurCoordinates[%d] = texCoords.xy - singleStepOffset * %f;\n\r",
                    ((currentOptimizedOffset * 2) + 1), optimizedGaussianOffsets[currentOptimizedOffset],
                    ((currentOptimizedOffset * 2) + 2), optimizedGaussianOffsets[currentOptimizedOffset]));
        }

        // Footer
        builder.append("}\n\r");
        return builder.toString();
    }

    public static String fragmentShaderForOptimizedBlurOfRadius(int blurRadius, float sigma,
                                                                boolean useExternalOES) {
        if (blurRadius < 1) {
            blurRadius = 1;
        }

        // First, generate the normal Gaussian weights for a given sigma
        float[] standardGaussianWeights = new float[blurRadius + 1];
        float sumOfWeights = 0.0f;
        for (int index = 0; index < blurRadius + 1; index++) {
            standardGaussianWeights[index] =
                    (float) ((1.0 / Math.sqrt(2.0 * Math.PI * Math.pow(sigma, 2.0)))
                            * Math.exp(-Math.pow(index, 2.0) / (2.0 * Math.pow(sigma, 2.0))));

            if (index == 0) {
                sumOfWeights += standardGaussianWeights[index];
            } else {
                sumOfWeights += 2.0 * standardGaussianWeights[index];
            }
        }

        // Next, normalize these weights to prevent the clipping of the Gaussian curve at the end of the discrete samples from reducing luminance
        for (int index = 0; index < blurRadius + 1; index++) {
            standardGaussianWeights[index] = standardGaussianWeights[index] / sumOfWeights;
        }

        // From these weights we calculate the offsets to read interpolated values from
        int numberOfOptimizedOffsets = Math.min(blurRadius / 2 + (blurRadius % 2), 7);
        int trueNumberOfOptimizedOffsets = blurRadius / 2 + (blurRadius % 2);

        StringBuilder builder = new StringBuilder();

        Locale locale = Locale.US;

        // Header
        builder.append(String.format(
                locale,
                ""
                        + (useExternalOES ? "#extension GL_OES_EGL_image_external : require\n\r\n\r" : "")
                        + "uniform %s inputImageTexture;\n\r"
                        + "uniform highp float uTexelWidthOffset;\n\r"
                        + "uniform highp float uTexelHeightOffset;\n\r"
                        + "\n\r"
                        + "varying highp vec2 blurCoordinates[%d];\n\r"
                        + "\n\r"
                        + "void main() {\n\r"
                        + "\tlowp vec4 sum = vec4(0.0);\n\r",
                useExternalOES ? "samplerExternalOES" : "sampler2D",
                1 + (numberOfOptimizedOffsets * 2)));


        // Inner texture loop
        builder.append(String.format(
                locale,
                "\tsum += texture2D(inputImageTexture, blurCoordinates[0]) * %f;\n\r",
                standardGaussianWeights[0]));

        for (int index = 0; index < numberOfOptimizedOffsets; index++) {
            float firstWeight = standardGaussianWeights[index * 2 + 1];
            float secondWeight = standardGaussianWeights[index * 2 + 2];
            float optimizedWeight = firstWeight + secondWeight;

            builder.append(String.format(
                    locale,
                    "\tsum += texture2D(inputImageTexture, blurCoordinates[%1$d]) * %2$f;\n\r",
                    ((index * 2) + 1), optimizedWeight));

            builder.append(String.format(
                    locale,
                    "\tsum += texture2D(inputImageTexture, blurCoordinates[%1$d]) * %2$f;\n\r",
                    ((index * 2) + 2), optimizedWeight));
        }

        // If the number of required samples exceeds the amount we can pass in via varyings, we have to do dependent texture reads in the fragment shader
        if (trueNumberOfOptimizedOffsets > numberOfOptimizedOffsets) {
            builder.append("\thighp vec2 singleStepOffset = vec2(texelWidthOffset, texelHeightOffset);\n\r");
            for (int currentOverlowTextureRead = numberOfOptimizedOffsets; currentOverlowTextureRead < trueNumberOfOptimizedOffsets; currentOverlowTextureRead++) {
                float firstWeight = standardGaussianWeights[currentOverlowTextureRead * 2 + 1];
                float secondWeight = standardGaussianWeights[currentOverlowTextureRead * 2 + 2];

                float optimizedWeight = firstWeight + secondWeight;
                float optimizedOffset = (firstWeight * (currentOverlowTextureRead * 2 + 1) + secondWeight * (currentOverlowTextureRead * 2 + 2)) / optimizedWeight;

                builder.append(String.format(
                        locale,
                        "\tsum += texture2D(inputImageTexture, blurCoordinates[0] + singleStepOffset * %1$f) * %2$f;\n\r",
                        optimizedOffset, optimizedWeight));

                builder.append(String.format(
                        locale,
                        "\tsum += texture2D(inputImageTexture, blurCoordinates[0] - singleStepOffset * %1$f) * %2$f;\n\r",
                        optimizedOffset, optimizedWeight));
            }
        }

        // Footer
        builder.append("\tgl_FragColor = sum;\n\r}\n\r");
        return builder.toString();
    }
}
