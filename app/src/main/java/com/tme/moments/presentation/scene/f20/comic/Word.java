package com.tme.moments.presentation.scene.f20.comic;

import android.graphics.PointF;
import android.util.Pair;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Word extends Decal {

    private enum Position {
        LEFT, TOP, RIGHT, BOTTOM
    }

    private float t;
    private float speed;
    private float endAngle;
    private boolean rotate;
    private float alphaSpeed;
    private float startDelayNanos;
    private float hangTimeNanos;
    private final List<Pair<TextureRegion, PointF>> infos;
    private boolean inflated;
    private Interpolator interpolator;

    Word(List<Pair<TextureRegion, PointF>> infos) {
        this.infos = infos;
        init();
        interpolator = new OvershootInterpolator(2.5f);
        setStartDelay(3000);
    }

    private void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        if (!inflated) {
            float deltaT = speed * deltaNanos;
            t += deltaT;
            t = Math.max(0, Math.min(1.0f, t));
            if (rotate) {
                float interpolatedAngle = t * endAngle;
                setRotationZ(interpolatedAngle);
            }
            float interpolatedScale = interpolator.getInterpolation(t);
            setScale(interpolatedScale);
            if (t == 1) {
                inflated = true;
            }
        } else {
            if (hangTimeNanos > 0) {
                hangTimeNanos -= deltaNanos;
                return;
            }
            float alpha = color.a;
            float deltaAlpha = alphaSpeed * deltaNanos;
            float newAlpha = Math.max(0, alpha - deltaAlpha);
            setAlpha(newAlpha);
            if (newAlpha == 0) {
                init();
            }
        }
    }

    private void init() {
        Pair<TextureRegion, PointF> info = infos.get(random(infos.size()));
        TextureRegion region = info.first;
        float width = info.second.x;
        float height = info.second.y;
        setTextureRegion(region);
        setDimensions(width, height);
        Position position = Position.values()[random(Position.values().length)];
        float maxSide = Math.max(width, height);
        float margin = 0.07f;
        switch (position) {
            case LEFT:
                float x = -1.0f + maxSide / 2 + margin;
                float y = random(-0.5f, 0.5f);
                setPosition(x, y, -1);
                break;
            case TOP:
                x = random(-1.0f + maxSide / 2 + margin, 1.0f - maxSide / 2 - margin);
                y = random(0.5f, 0.6f);
                setPosition(x, y, -1);
                break;
            case RIGHT:
                x = 1 - maxSide / 2 - margin;
                y = random(-0.4f, 0.4f);
                setPosition(x, y, -1);
                break;
            case BOTTOM:
                x = random(-1.0f + maxSide / 2 + margin, 1.0f - maxSide / 2 - margin);
                y = random(-0.5f, -0.4f);
                setPosition(x, y, -1);
                break;
        }
        setScale(0);
        setAlpha(1);
        t = 0;
        inflated = false;
        speed = 1.75f / Const.NANOS_IN_SECOND;
        boolean rotateLeft = random();
        float minAngle = rotateLeft ? 325 : -360;
        float maxAngle = rotateLeft ? 360 : -325;
        endAngle = random(minAngle, maxAngle);
        setRotationZ(0);
        rotate = random();
        alphaSpeed = 2.0f / Const.NANOS_IN_SECOND;
        hangTimeNanos = random(2000, 2800) * Const.NANOS_IN_MILLISECOND;
        setStartDelay(random(3000, 5000));
    }
}
