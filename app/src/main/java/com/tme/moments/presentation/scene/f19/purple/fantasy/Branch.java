package com.tme.moments.presentation.scene.f19.purple.fantasy;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.math.Vector2;
import com.tme.moments.math.Vector3;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Branch extends Decal {

    private float rotationSpeed;
    private float maxRotationAngle;
    private boolean shouldThrowLeaves;
    private long shakeWaitTime;
    private boolean clockwise;

    Branch(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        transformationOffset = new Vector2(width / 2, height / 2); // top right point
        init();
        shakeWaitTime = TimeUnit.MILLISECONDS.toNanos(random(3_000, 4_000));
    }

    void step(long deltaNanos) {
        if (shakeWaitTime > 0) {
            shakeWaitTime -= deltaNanos;
            return;
        }
        float deltaRotation = rotationSpeed * deltaNanos;
        float rotationZ = getRotation().getAngleAround(Vector3.Z);
        if (clockwise) {
            float newRotation = Math.max(0, Math.min(maxRotationAngle, rotationZ + deltaRotation));
            setRotationZ(newRotation);
            if (newRotation == maxRotationAngle) {
                rotationSpeed *= -1;
                shouldThrowLeaves = true;
            }
            if (newRotation == 0 && rotationSpeed < 0) {
                init();
            }
        } else {
            if (rotationZ == 0) rotationZ = 360f;
            float newRotation = Math.min(360, Math.max(360 - maxRotationAngle, rotationZ + deltaRotation));
            setRotationZ(newRotation);
            if (newRotation == (360 - maxRotationAngle)) {
                rotationSpeed *= -1;
                shouldThrowLeaves = true;
            }
            if (newRotation == 360 && rotationSpeed > 0) {
                init();
            }
        }
    }

    private void init() {
        maxRotationAngle = 5f;
        shakeWaitTime = TimeUnit.MILLISECONDS.toNanos(random(5_000, 8_000));
        clockwise = random();
        rotationSpeed = (clockwise ? 25f : -25f) / Const.NANOS_IN_SECOND;
    }

    boolean shouldThrowLeaves() {
        return shouldThrowLeaves;
    }

    void setShouldThrowLeaves(boolean shouldThrowLeaves) {
        this.shouldThrowLeaves = shouldThrowLeaves;
    }
}
