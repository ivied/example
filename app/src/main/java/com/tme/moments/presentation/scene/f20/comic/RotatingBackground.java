package com.tme.moments.presentation.scene.f20.comic;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class RotatingBackground extends Sprite {

    private static final int MAX_SEQUENTIAL_BEATS_COUNT = 2;

    private final float minScale;
    private final float maxScale;

    private float rotationSpeed;
    private boolean scaleUp = true;
    private float scaleUpSpeed;
    private float scaleDownSpeed;
    private float scaleWaitTime;
    private int beatsCount;

    RotatingBackground(TextureRegion textureRegion, float width,
                       float height, float minScale) {
        super(textureRegion, width, height);
        this.minScale = minScale;
        this.maxScale = minScale * 1.35f;
        init();
        setScale(minScale);
        scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(1000);
    }

    public void step(long deltaNanos) {
        float delta = rotationSpeed * deltaNanos;
        rotateZ(delta);
        if (scaleWaitTime > 0) {
            scaleWaitTime -= deltaNanos;
        } else {
            if (scaleUp) {
                float deltaScale = scaleUpSpeed * deltaNanos;
                float newScale = Math.min(maxScale, getScaleX() + deltaScale);
                setScale(newScale);
                if (newScale == maxScale) {
                    scaleUp = false;
                }
            } else {
                float deltaScale = scaleDownSpeed * deltaNanos;
                float newScale = Math.max(minScale, getScaleX() - deltaScale);
                setScale(newScale);
                if (newScale == minScale) {
                    scaleUp = true;
                    if (++beatsCount == MAX_SEQUENTIAL_BEATS_COUNT) {
                        beatsCount = 0;
                        scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(1000);
                    }
                }
            }
        }
        update();
    }

    private void init() {
        rotationSpeed = (random() ? -20f : 20f) / Const.NANOS_IN_SECOND;
        scaleUpSpeed = 3.0f / Const.NANOS_IN_SECOND;
        scaleDownSpeed = 3.0f / Const.NANOS_IN_SECOND;
    }
}
