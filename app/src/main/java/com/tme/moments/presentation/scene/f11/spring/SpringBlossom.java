package com.tme.moments.presentation.scene.f11.spring;

import android.graphics.PointF;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseBlurredScene;
import com.tme.moments.presentation.scene.BlinkingGlow;
import com.tme.moments.presentation.scene.Glow;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class SpringBlossom extends BaseBlurredScene {

    private TextureInfo atlasInfo;
    private TextureInfo topBirdAtlasInfo;
    private TextureInfo bottomBirdAtlasInfo;
    private DecalBatch glowBatch;
    private DecalBatch objectsBatch;
    private DecalBatch branchBatch;
    private List<BlinkingGlow> glows;

    private Bird topBird;
    private Bird bottomBird;
    private List<FallingObject> fallingObjects;
    private List<FallingObject> finishedFallingObjects;
    private Pair<TextureRegion, PointF> leaf1Info;
    private Pair<TextureRegion, PointF> leaf2Info;
    private Pair<TextureRegion, PointF> petal1Info;
    private Pair<TextureRegion, PointF> petal2Info;
    private Pair<TextureRegion, PointF> petal3Info;
    private Pair<TextureRegion, PointF> flower1Info;
    private Pair<TextureRegion, PointF> flower2Info;

    private PulsingBranch topLeftBranch;
    private PulsingBranch topRightBranch;
    private BottomBranch bottomRightBranch;

    private boolean leftBranchActive;

    @Override protected int getStencilResId() {
        return R.drawable.f11_stencil;
    }

    @Override protected int getOverlayResId() {
        return R.drawable.f11_overlay;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.5537f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.5537f;
    }

    @Override protected float getCameraPositionX() {
        return 0.0f;
    }

    @Override protected float getCameraPositionY() {
        return 0.0f;
    }

    @Override protected TextureRegion getFrameRegion() {
        TextureInfo frameInfo = openGLLoader.loadTexture(R.drawable.f11_overlay);
        return new TextureRegion(frameInfo.textureHandle, frameInfo.imageWidth, frameInfo.imageHeight,
                0, 0, frameInfo.imageWidth, frameInfo.imageHeight);
    }

    @Override public boolean hasBackgroundEffects() {
        return true;
    }

    @Override protected void prepareBackgroundEffects(int surfaceWidth, int surfaceHeight) {
        super.prepareBackgroundEffects(surfaceWidth, surfaceHeight);
        prepareGlow(atlasInfo);
        glowBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        glowBatch.initialize(glows.size());
        glowBatch.addAll(glows);
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        super.prepareObjects(surfaceWidth, surfaceHeight);
        atlasInfo = openGLLoader.loadTexture(R.drawable.f11_atlas);

        prepareTopBird();
        prepareBottomBird();

        prepareBranches(atlasInfo);
        branchBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        branchBatch.initialize(3);
        branchBatch.add(topLeftBranch);
        branchBatch.add(topRightBranch);
        branchBatch.add(bottomRightBranch);

        buildFallingObjects();
        fallingObjects = new ArrayList<>();
        finishedFallingObjects = new ArrayList<>();
        objectsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        objectsBatch.initialize(200); // 200 for falling objects
    }

    @Override protected void onBlurredImageDrawn(long deltaNanos) {
        if (config.isBackgroundEffectsEnabled()) {
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            glowBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Glow glow : glows) {
                glow.step(deltaNanos);
            }
        }
    }

    @Override protected void onOverlayDrawn(long deltaNanos) {
        boolean objectsEnabled = config.isObjectsEnabled();
        glUseProgram(mainProgramHandle);
        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        if (objectsEnabled) {
            objectsBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (FallingObject fo : fallingObjects) {
                fo.step(deltaNanos);
                if (fo.isFinished()) {
                    finishedFallingObjects.add(fo);
                }
            }

            if (!finishedFallingObjects.isEmpty()) {
                fallingObjects.removeAll(finishedFallingObjects);
                objectsBatch.removeAll(finishedFallingObjects);
                finishedFallingObjects.clear();
            }

        }
        branchBatch.render(uProjectionMatrixLoc, projectionMatrix);
        if (objectsEnabled) {
            if (leftBranchActive) {
                topLeftBranch.step(deltaNanos);
                if (topLeftBranch.shouldThrowLeaves()) {
                    throwObjects(-1f, 1f, false);
                    topLeftBranch.setShouldThrowLeaves(false);
                }
                if (topLeftBranch.isFinished()) {
                    activateNextBranch();
                }
            } else {
                topRightBranch.step(deltaNanos);
                if (topRightBranch.shouldThrowLeaves()) {
                    throwObjects(1f, 1f, true);
                    topRightBranch.setShouldThrowLeaves(false);
                }
                if (topRightBranch.isFinished()) {
                    activateNextBranch();
                }
            }
            bottomRightBranch.step(deltaNanos);
        }
        topBird.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
        bottomBird.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
        if (config.isObjectsEnabled()) {
            topBird.step(deltaNanos);
            bottomBird.step(deltaNanos);
        }
    }

    @Override public void release() {
        super.release();
        int[] arr = new int[3];
        int texturesToDeleteSize = 0;
        if (atlasInfo != null) {
            arr[texturesToDeleteSize] = atlasInfo.textureHandle;
            texturesToDeleteSize++;
        }
        if (topBirdAtlasInfo != null) {
            arr[texturesToDeleteSize] = topBirdAtlasInfo.textureHandle;
            texturesToDeleteSize++;
        }
        if (bottomBirdAtlasInfo != null) {
            arr[texturesToDeleteSize] = bottomBirdAtlasInfo.textureHandle;
            texturesToDeleteSize++;
        }
        if (texturesToDeleteSize != 0) {
            glDeleteTextures(texturesToDeleteSize, arr, 0);
            GlUtil.checkGlError("Delete textures");
        }
    }

    private void prepareTopBird() {
        topBirdAtlasInfo = openGLLoader.loadTexture(R.drawable.f11_top_bird_atlas);
        int inSampleSize = topBirdAtlasInfo.inSampleSize;
        float width = 0.5925f;
        float height = 0.3875f;
        List<AnimatedSprite.FrameInfo> frames = new ArrayList<>();
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 474 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 948 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1422 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 310 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 474 / inSampleSize, 310 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 948 / inSampleSize, 310 / inSampleSize));
        TextureRegion region = buildRegion(topBirdAtlasInfo, 0, 0, 474, 310); // region for first frame
        topBird = new Bird(region, frames, width, height);
        topBird.setPosition(-0.35f, 0.58f, -1.0f);
    }

    private void prepareBottomBird() {
        bottomBirdAtlasInfo = openGLLoader.loadTexture(R.drawable.f11_bottom_bird_atlas);
        int inSampleSize = bottomBirdAtlasInfo.inSampleSize;
        float width = 0.3037f;
        float height = 0.6f;
        List<AnimatedSprite.FrameInfo> frames = new ArrayList<>();
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 0 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 243 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 486 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 729 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 972 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1215 / inSampleSize, 0 / inSampleSize));
        frames.add(new AnimatedSprite.FrameInfo(frames.size(), 1458 / inSampleSize, 0 / inSampleSize));
        TextureRegion region = buildRegion(bottomBirdAtlasInfo, 0, 0, 243, 480); // region for first frame
        bottomBird = new Bird(region, frames, width, height);
        bottomBird.setPosition(0.4f, -0.61f, -1.0f);
    }

    private void activateNextBranch() {
        leftBranchActive = random();
        if (leftBranchActive) {
            topLeftBranch.activate();
        } else {
            topRightBranch.activate();
        }
    }

    private void throwObjects(float x, float y, boolean flyLeft) {
        int flowersCount = random(2, 5);
        for (int i = 0; i < flowersCount; i++) {
            boolean useFlower1 = random();
            float scale = useFlower1 ? 0.8f : 0.9f;
            Pair<TextureRegion, PointF> info = useFlower1 ? flower1Info : flower2Info;
            float minRotationSpeed = useFlower1 ? -225f : -315f;
            float maxRotationSpeed = useFlower1 ? 225 : 315f;
            addFallingObject(info, x, y, scale, minRotationSpeed, maxRotationSpeed, flyLeft);
        }

        int leavesCount = random(2, 7);
        for (int i = 0; i < leavesCount; i++) {
            boolean useLeaf1 = random();
            Pair<TextureRegion, PointF> info = useLeaf1 ? leaf1Info : leaf2Info;
            float minRotationSpeed = -180f;
            float maxRotationSpeed = 180f;
            addFallingObject(info, x, y, 1.2f, minRotationSpeed, maxRotationSpeed, flyLeft);
        }
        int petalsCount = random(4, 8);
        for (int i = 0; i < petalsCount; i++) {
            Pair<TextureRegion, PointF> info = random() ? petal1Info : random() ? petal2Info : petal3Info;
            float minRotationSpeed = -480f;
            float maxRotationSpeed = 480f;
            addFallingObject(info, x, y, 0.8f, minRotationSpeed, maxRotationSpeed, flyLeft);
        }
    }

    private void addFallingObject(Pair<TextureRegion, PointF> regionInfo, float x, float y,
                                  float scale, float minRotationSpeed, float maxRotationSpeed,
                                  boolean flyLeft) {
        TextureRegion region = regionInfo.first;
        float width = regionInfo.second.x;
        float height = regionInfo.second.y;
        FallingObject fo = new FallingObject(region, width, height, x, y, flyLeft, minRotationSpeed, maxRotationSpeed);
        fo.setScale(scale);
        fallingObjects.add(fo);
        objectsBatch.add(fo);
    }

    private void prepareBranches(TextureInfo info) {
        TextureRegion region = buildRegion(info, 0, 0, 547, 625);
        float width = 1.0129f;  // size is for 1080 source
        float height = 1.1574f; // size is for 1080 source
        topLeftBranch = new PulsingBranch(region, width, height);
        topLeftBranch.setPosition(-1 + width / 2, 1 - height / 2, -1.0f);

        region = buildRegion(info, 547, 0, 357, 417);
        width = 0.6611f;  // size is for 1080 source
        height = 0.7722f; // size is for 1080 source
        topRightBranch = new PulsingBranch(region, width, height);
        topRightBranch.setPosition(1 - width / 2, 1 - height / 2, -1.0f);

        region = buildRegion(info, 904, 0, 1053, 669);
        width = 1.9481f;  // size is for 1080 source
        height = 1.2388f; // size is for 1080 source
        float x = 1 - width / 2;
        float y = -1 + height / 2;
        bottomRightBranch = new BottomBranch(region, width, height, x, y);
    }

    private void prepareGlow(TextureInfo info) {
        Pair<TextureRegion, PointF> circle1 = new Pair<>(buildRegion(info, 1214, 669, 180, 180), new PointF(0.225f, 0.2237f));
        Pair<TextureRegion, PointF> circle2 = new Pair<>(buildRegion(info, 1394, 669, 180, 180), new PointF(0.225f, 0.2237f));

        glows = new ArrayList<>();
        generateGlow(glows, circle1, 30);
        generateGlow(glows, circle2, 30);
    }

    private void generateGlow(List<BlinkingGlow> glowList, Pair<TextureRegion, PointF> regionInfo,
                              int count) {
        int minVisibleInterval = 250;
        int maxVisibleInterval = 750;
        int minInvisibleInterval = 100;
        int maxInvisibleInterval = 300;
        float minAlphaSpeed = 1.0f;
        float maxAlphaSpeed = 2.0f;
        for (int i = 0; i < count; i++) {
            TextureRegion region = regionInfo.first;
            float width = regionInfo.second.x;
            float height = regionInfo.second.y;
            BlinkingGlow glow = new BlinkingGlow(region, width, height, minVisibleInterval,
                    maxVisibleInterval, minInvisibleInterval, maxInvisibleInterval, minAlphaSpeed, maxAlphaSpeed);
            glow.setScale(random(0.6f, 1.0f));
            glow.setStartDelay(random(3500, 10500));
            glowList.add(glow);
        }
    }

    private void buildFallingObjects() {
        leaf1Info = new Pair<>(buildRegion(atlasInfo, 250, 669, 37, 100), new PointF(0.0462f, 0.125f));
        leaf2Info = new Pair<>(buildRegion(atlasInfo, 287, 669, 37, 100), new PointF(0.0462f, 0.125f));

        petal1Info = new Pair<>(buildRegion(atlasInfo, 1004, 669, 70, 99), new PointF(0.0875f, 0.1237f));
        petal2Info = new Pair<>(buildRegion(atlasInfo, 1074, 669, 70, 89), new PointF(0.0875f, 0.1112f));
        petal3Info = new Pair<>(buildRegion(atlasInfo, 1144, 669, 70, 97), new PointF(0.0875f, 0.1212f));

        flower1Info = new Pair<>(buildRegion(atlasInfo, 0, 669, 170, 165), new PointF(0.2125f, 0.2062f));
        flower2Info = new Pair<>(buildRegion(atlasInfo, 170, 669, 80, 83), new PointF(0.1f, 0.1037f));
    }
}