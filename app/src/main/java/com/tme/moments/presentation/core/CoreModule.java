package com.tme.moments.presentation.core;

import android.app.Application;
import android.content.Context;

import com.tme.moments.presentation.LocalPreferencesImpl;
import com.tme.moments.presentation.LocalPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author zoopolitic.
 */
@Module
class CoreModule {

    private Application application;

    CoreModule(Application application) {
        this.application = application;
    }

    @Provides @Singleton Application provideApplication() {
        return application;
    }

    @Provides @Singleton Context provideContext() {
        return application;
    }

    @Provides @Singleton OpenGLLoader provideTextureLoader(Context context) {
        return new OpenGLLoader(context);
    }

    @Provides @Singleton ResourceProvider provideResourceProvider(Context context) {
        return new ResourceProviderImpl(context);
    }

    @Provides @Singleton LocalPreferences provideLocalPreferences(Application application) {
        return new LocalPreferencesImpl(application);
    }

    @Provides @Singleton Config provideConfig(LocalPreferences preferences) {
        return new Config(preferences);
    }
}
