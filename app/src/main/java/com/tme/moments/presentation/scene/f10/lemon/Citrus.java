package com.tme.moments.presentation.scene.f10.lemon;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Citrus extends Decal {

    private float rotationSpeed;

    Citrus(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        init();
    }

    void step(long deltaNanos) {
        float deltaRotation = rotationSpeed * deltaNanos;
        rotateZ(deltaRotation);
    }

    private void init() {
        boolean clockwise = random();
        float min = 13;
        float max = 19;
        rotationSpeed = (clockwise ? random(min, max) : -random(min, max)) / (float) Const.NANOS_IN_SECOND;
    }
}
