package com.tme.moments.presentation.scene.f8.sunflower.field;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.Const;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class OrangeButterfly extends AnimatedSprite {

    protected float p0x, p0y; // start point
    protected float p1x, p1y; // control point (bezier)
    protected float p2x, p2y; // end point
    protected float speed;
    protected float deflateSpeed;
    protected float startDelayNanos;

    private float t;
    private boolean stopped;

    OrangeButterfly(TextureRegion region, List<FrameInfo> frameInfoList, float width,
                    float height) {
        super(region, frameInfoList, width, height);
        init();
    }

    protected void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    boolean isStopped() {
        return stopped;
    }

    void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

    @Override protected int getDesiredFPS() {
        return 30;
    }

    @Override
    public void draw(int uProjectionMatrixLoc, float[] projectionMatrix, int aPositionHandle,
                     int aTexCoordHandle, int aColorHandle) {
        if (stopped) {
            return;
        }
        super.draw(uProjectionMatrixLoc, projectionMatrix, aPositionHandle, aTexCoordHandle, aColorHandle);
    }

    @Override public void step(long deltaNanos) {
        if (stopped) {
            return;
        }
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float deltaT = speed * deltaNanos;
        t += deltaT;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        setX(x);
        setY(y);

        float minScale = 0.1f;
        float scale = getScaleY();
        if (scale > minScale) {
            float deltaScale = deflateSpeed * deltaNanos;
            scale -= deltaScale;
            scale = Math.max(minScale, scale);
            float scaleX = getScaleX();
            setScaleX(scaleX < 0 ? -scale : scale);
            setScaleY(scale);
        }
        if (t >= 1) {
            init();
            t = 0;
        }
        super.step(deltaNanos);
    }

    private void init() {
        stopped = true;
        float halfWidth = getWidth() / 2;
        boolean moveLeft = random();
        p0x = moveLeft ? random(1 + halfWidth, 2) : random(-2, -1 - halfWidth);
        p0y = random(-1.0f, -0.4f);
        p1x = moveLeft ? random(0, 0.5f) : random(-0.5f, 0);
        p1y = random(-1.0f, -0.4f);
        p2x = moveLeft ? random(-1 - getWidth(), -1 - halfWidth) : random(1 + halfWidth, 1 + getWidth());
        p2y = random(-1.0f, -0.4f);
        setScaleX(moveLeft ? 1f : -1f);
        setScaleY(1);
        setPosition(p0x, p0y, -1.0f);
        setStartDelay(random(1000, 2500));
        deflateSpeed = 0.13f / Const.NANOS_IN_SECOND;
        speed = 0.21f / Const.NANOS_IN_SECOND;
    }
}
