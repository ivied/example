package com.tme.moments.presentation.capture;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Scroller;

import com.tme.moments.math.MathUtils;
import com.tme.moments.presentation.scene.Scene;

/**
 * @author zoopolitic.
 */
public class CustomTouchListener implements View.OnTouchListener {

    private static final int INVALID_POINTER_ID = -1;

    private int ptrId1, ptrId2;
    private float fX, fY, sX, sY;
    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetector gestureDetector;

    // dragging
    private int dragPointerId;
    private float lastTouchX;
    private float lastTouchY;
    private float lastAngle;
    private FlingRunnable currentFlingRunnable;
    private Scene scene;

    CustomTouchListener(Context context) {
        ScaleGestureDetector.OnScaleGestureListener scaleGestureListener = new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            @Override public boolean onScale(ScaleGestureDetector detector) {
                if (scene != null) {
                    scene.zoomChanged(detector.getScaleFactor());
                }
                return true;
            }
        };
        this.scaleGestureDetector = new ScaleGestureDetector(context, scaleGestureListener);
        GestureDetector.OnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                   float velocityY) {
                if (scene == null) {
                    return false;
                }
                if (e1.getPointerCount() > 1 || e2.getPointerCount() > 1) {
                    return false;
                }

                currentFlingRunnable = new FlingRunnable(context);
                float startX = scene.getImageX();
                float startY = scene.getImageY();
                currentFlingRunnable.fling((int) startX, (int) startY, (int) velocityX, (int) velocityY);
                new Thread(currentFlingRunnable).start();

                return true;
            }
        };
        this.gestureDetector = new GestureDetector(context, gestureListener);
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    @Override public boolean onTouch(View v, MotionEvent event) {
        scaleGestureDetector.onTouchEvent(event);
        gestureDetector.onTouchEvent(event);
        final int action = event.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                ptrId1 = event.getPointerId(event.getActionIndex());

                int pointerIndex = event.getActionIndex();
                float x = event.getX(pointerIndex);
                float y = event.getY(pointerIndex);

                lastTouchX = x;
                lastTouchY = y;
                // Save the ID of this pointer (for dragging)
                dragPointerId = event.getPointerId(pointerIndex);

                if (currentFlingRunnable != null) {
                    currentFlingRunnable.cancelFling();
                    currentFlingRunnable = null;
                }
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                ptrId2 = event.getPointerId(event.getActionIndex());
                sX = event.getX(event.findPointerIndex(ptrId1));
                sY = event.getY(event.findPointerIndex(ptrId1));
                fX = event.getX(event.findPointerIndex(ptrId2));
                fY = event.getY(event.findPointerIndex(ptrId2));

                lastTouchX = 0;
                lastTouchY = 0;
                dragPointerId = INVALID_POINTER_ID;
                break;
            case MotionEvent.ACTION_MOVE:
                int pointerCount = event.getPointerCount();
                // handle rotation
                if (pointerCount >= 2 && ptrId1 != INVALID_POINTER_ID && ptrId2 != INVALID_POINTER_ID) {
                    float nfX, nfY, nsX, nsY;
                    nsX = event.getX(event.findPointerIndex(ptrId1));
                    nsY = event.getY(event.findPointerIndex(ptrId1));
                    nfX = event.getX(event.findPointerIndex(ptrId2));
                    nfY = event.getY(event.findPointerIndex(ptrId2));
                    float angle = MathUtils.angleBetweenLines(fX, fY, sX, sY, nfX, nfY, nsX, nsY);
                    if (scene != null) {
                        scene.angleChanged(angle - lastAngle);
                    }
                    lastAngle = angle;
                }

                pointerIndex = event.getActionIndex();
                int pointerId = event.getPointerId(pointerIndex);
                // handle move
                if (pointerId == dragPointerId && pointerCount == 1) {
                    // Find the index of the active pointer and fetch its position
                    pointerIndex = event.findPointerIndex(dragPointerId);

                    x = event.getX(pointerIndex);
                    y = event.getY(pointerIndex);

                    // Calculate the distance moved
                    final float dx = x - lastTouchX;
                    final float dy = y - lastTouchY;

                    if (scene != null) {
                        scene.onMoved(dx, dy);
                    }

                    // Remember this touch position for the next move event
                    lastTouchX = x;
                    lastTouchY = y;
                }
                break;
            case MotionEvent.ACTION_UP:
                ptrId1 = INVALID_POINTER_ID;
                ptrId2 = INVALID_POINTER_ID;
                dragPointerId = INVALID_POINTER_ID;
                break;
            case MotionEvent.ACTION_POINTER_UP:
                pointerIndex = event.getActionIndex();
                pointerId = event.getPointerId(pointerIndex);

                lastAngle = 0;

                // This was our active pointer going up. Choose a new
                // active pointer and adjust accordingly.
                final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                if (pointerId == dragPointerId) {
                    lastTouchX = event.getX(newPointerIndex);
                    lastTouchY = event.getY(newPointerIndex);
                    dragPointerId = event.getPointerId(newPointerIndex);
                }

                if (pointerId == ptrId2) {
                    ptrId2 = INVALID_POINTER_ID;
                }
                if (pointerId == ptrId1) {
                    ptrId2 = INVALID_POINTER_ID;
                    ptrId1 = event.getPointerId(newPointerIndex);
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                dragPointerId = INVALID_POINTER_ID;
                ptrId1 = INVALID_POINTER_ID;
                ptrId2 = INVALID_POINTER_ID;
                break;
        }
        return true;
    }

    private class FlingRunnable implements Runnable {

        private final Scroller scroller;
        private int currentX, currentY;

        FlingRunnable(Context context) {
            scroller = new Scroller(context);
        }

        void cancelFling() {
            scroller.forceFinished(true);
        }

        void fling(int startX, int startY, int velocityX, int velocityY) {
            currentX = startX;
            currentY = startY;

            // allow to scroll anywhere without limits, scene will limit itself
            int minX = Integer.MIN_VALUE;
            int maxX = Integer.MAX_VALUE;
            int minY = Integer.MIN_VALUE;
            int maxY = Integer.MAX_VALUE;
            // If we actually can move, fling the scroller
            if (startX != maxX || startY != maxY) {
                scroller.fling(startX, startY, velocityX, velocityY, minX, maxX, minY, maxY);
            }
        }

        @Override
        public void run() {
            while (!scroller.isFinished()) {
                if (scroller.computeScrollOffset()) {

                    final int newX = scroller.getCurrX();
                    final int newY = scroller.getCurrY();

                    final float dx = newX - currentX;
                    final float dy = newY - currentY;

                    if (scene != null) {
                        scene.onMoved(dx, dy);
                    }

                    currentX = newX;
                    currentY = newY;
                }
            }
        }
    }
}
