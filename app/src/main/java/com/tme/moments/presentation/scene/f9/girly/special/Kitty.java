package com.tme.moments.presentation.scene.f9.girly.special;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.Const;

import java.util.List;

/**
 * @author zoopolitic.
 */
class Kitty extends AnimatedSprite {

    private static final long WAIT_TIME = 200 * Const.NANOS_IN_MILLISECOND;

    private long currentWaitTime = WAIT_TIME;

    Kitty(TextureRegion region, List<FrameInfo> frameInfoList, float width, float height) {
        super(region, frameInfoList, width, height);
        setPosition(0.38f, -0.57f, -1.0f);
    }

    @Override protected int getDesiredFPS() {
        return 18;
    }

    @Override protected void onAnimationReset() {
        currentWaitTime = WAIT_TIME;
    }

    @Override
    public void step(long deltaNanos) {
        if (currentWaitTime > 0) {
            currentWaitTime -= deltaNanos;
            return;
        }
        super.step(deltaNanos);
    }
}
