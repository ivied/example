package com.tme.moments.presentation.scene.f1.love;

import android.graphics.PointF;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.presentation.Rippler;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseBlurredScene;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class LoveIsInTheAir extends BaseBlurredScene {

    private TextureInfo atlasInfo;
    private CometHeadHeart comet;
    private DecalBatch pulsingHeartsBatch;
    private List<PulsingHeart> pulsingHearts;
    private List<PulsingHeart> finishedPulsingHearts;
    private Pair<TextureRegion, PointF> redHeartInfo;
    private Pair<TextureRegion, PointF> beigeHeartInfo;

    private DecalBatch lipsBatch;
    private Lips lips;

    private boolean lipsActive;
    private List<Particle> particles;
    private DecalBatch particlesBatch;

    private Rippler rippler;

    @Override protected int getStencilResId() {
        return R.drawable.f1_stencil;
    }

    @Override protected int getOverlayResId() {
        return R.drawable.f1;
    }

    @Override protected float getCameraPositionX() {
        return -0.01296f;
    }

    @Override protected float getCameraPositionY() {
        return -0.1148f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.5185f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.6685f;
    }

    @Override public boolean hasBackgroundEffects() {
        return true;
    }

    @Override public void init() {
        super.init();
        rippler = new Rippler();
        rippler.init(openGLLoader);
    }

    @Override public void setup(TextureInfo imageInfo, int width, int height) {
        super.setup(imageInfo, width, height);
        rippler.setup(width, height, R.drawable.f1);
    }

    @Override protected void prepareBackgroundEffects(int surfaceWidth, int surfaceHeight) {
        super.prepareBackgroundEffects(surfaceWidth, surfaceHeight);
        rippler.reset();
        prepareParticles(atlasInfo);
        particlesBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        particlesBatch.initialize(particles.size());
        particlesBatch.addAll(particles);
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        super.prepareObjects(surfaceWidth, surfaceHeight);
        atlasInfo = openGLLoader.loadTexture(R.drawable.f1_atlas);
        prepareLips(atlasInfo);
        lipsActive = random();
        prepareComet();
        pulsingHeartsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        pulsingHeartsBatch.initialize(500);
    }

    @Override protected void drawOverlay(long deltaNanos) {
        if (config.isBackgroundEffectsEnabled()) {
            rippler.render();
        } else {
            super.drawOverlay(deltaNanos);
        }
    }

    @Override protected void onOverlayDrawn(long deltaNanos) {
        super.onOverlayDrawn(deltaNanos);
        boolean objectsEnabled = config.isObjectsEnabled();
        boolean backgroundEffectsEnabled = config.isBackgroundEffectsEnabled();
        glUseProgram(mainProgramHandle);
        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        if (backgroundEffectsEnabled) {
            particlesBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Particle particle : particles) {
                particle.step(deltaNanos);
            }
        }
        if (objectsEnabled) {
            if (lipsActive) {
                lipsBatch.render(uProjectionMatrixLoc, projectionMatrix);
                lips.step(deltaNanos);
                if (lips.isFinished()) {
                    lipsActive = random();
                    if (lipsActive) {
                        lips.reset();
                    } else {
                        comet.reset();
                    }
                }
            } else {
                pulsingHeartsBatch.render(uProjectionMatrixLoc, projectionMatrix);
                comet.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation,
                        aTexCoordLocation, aColorLocation);
                comet.step(deltaNanos);
                int count = comet.generatedHeartsCount();
                if (count > 0) {
                    for (int i = 0; i < count; i++) {
                        boolean useBeigeHeart = random(4) == 0; // 25% chance
                        Pair<TextureRegion, PointF> info = useBeigeHeart ? beigeHeartInfo : redHeartInfo;
                        TextureRegion region = info.first;
                        float width = info.second.x;
                        float height = info.second.y;
                        float x = comet.getNextTailHeartX();
                        float y = comet.getNextTailHeartY();
                        PulsingHeart heart = new PulsingHeart(region, width, height, x, y);
                        pulsingHearts.add(heart);
                        pulsingHeartsBatch.add(heart);
                    }
                }
                for (PulsingHeart heart : pulsingHearts) {
                    heart.step(deltaNanos);
                    if (heart.isFinished()) {
                        finishedPulsingHearts.add(heart);
                    }
                }
                if (!finishedPulsingHearts.isEmpty()) {
                    pulsingHearts.removeAll(finishedPulsingHearts);
                    pulsingHeartsBatch.removeAll(finishedPulsingHearts);
                    finishedPulsingHearts.clear();
                }
                // if comet finished and all pulsing hearts finished = reset
                if (comet.isFinished() && pulsingHearts.isEmpty()) {
                    lipsActive = random();
                    if (lipsActive) {
                        lips.reset();
                    } else {
                        comet.reset();
                    }
                }
            }
        }
    }

    @Override public void release() {
        super.release();
        rippler.release();
        comet = null;
        if (finishedPulsingHearts != null) {
            finishedPulsingHearts.clear();
        }
        if (pulsingHearts != null) {
            pulsingHearts.clear();
        }
        if (atlasInfo != null) {
            int[] arr = new int[1];
            arr[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, arr, 0);
            GlUtil.checkGlError("Delete textures");
        }
        if (pulsingHeartsBatch != null) {
            pulsingHeartsBatch.dispose();
        }
    }

    private void prepareLips(TextureInfo info) {
        List<Pair<TextureRegion, PointF>> regionInfos = new ArrayList<>();
        regionInfos.add(new Pair<>(buildRegion(info, 215, 222, 227, 140), new PointF(0.2837f, 0.175f)));
        regionInfos.add(new Pair<>(buildRegion(info, 0, 222, 215, 140), new PointF(0.2687f, 0.175f)));
        lips = new Lips(regionInfos);
        lipsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        lipsBatch.initialize(1);
        lipsBatch.add(lips);
    }

    private void prepareComet() {
        TextureRegion region = buildRegion(atlasInfo, 250, 90, 160, 132); // heart 3
        float width = 0.2f;
        float height = 0.165f;
        comet = new CometHeadHeart(region, width, height);
        redHeartInfo = new Pair<>(buildRegion(atlasInfo, 250, 90, 160, 132), new PointF(0.2f, 0.165f));
        beigeHeartInfo = new Pair<>(buildRegion(atlasInfo, 90, 90, 160, 132), new PointF(0.2f, 0.165f));
        pulsingHearts = new ArrayList<>(100);
        finishedPulsingHearts = new ArrayList<>(100);
    }

    private void prepareParticles(TextureInfo info) {
        List<Pair<TextureRegion, PointF>> regionInfos = new ArrayList<>();
        // particles
        regionInfos.add(new Pair<>(buildRegion(info, 270, 0, 90, 90), new PointF(0.1125f, 0.1125f)));
        regionInfos.add(new Pair<>(buildRegion(info, 180, 0, 90, 90), new PointF(0.1125f, 0.1125f)));
        regionInfos.add(new Pair<>(buildRegion(info, 90, 0, 90, 90), new PointF(0.1125f, 0.1125f)));
        regionInfos.add(new Pair<>(buildRegion(info, 0, 0, 90, 90), new PointF(0.1125f, 0.1125f)));
        regionInfos.add(new Pair<>(buildRegion(info, 0, 90, 90, 90), new PointF(0.1125f, 0.1125f)));
        int count = 500;
        particles = new ArrayList<>();
        Particle.Direction direction = random() ? Particle.Direction.RIGHT : Particle.Direction.LEFT;
        for (int i = 0; i < count; i++) {
            Pair<TextureRegion, PointF> regionInfo = regionInfos.get(random(regionInfos.size()));
            TextureRegion region = regionInfo.first;
            float width = regionInfo.second.x;
            float height = regionInfo.second.y;
            float z = random(-3.0f, -1.0f);
            Particle particle = new Particle(region, width, height, z, direction);
            if (z >= -1.1f) {
                particle.setScale(0.84f);
            } else {
                particle.setScale(0.4f);
            }
            particles.add(particle);
        }
    }
}
