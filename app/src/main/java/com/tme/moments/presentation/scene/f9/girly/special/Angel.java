package com.tme.moments.presentation.scene.f9.girly.special;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.Const;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Angel extends AnimatedSprite {

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float speed;
    private float t;

    private float startDelayNanos;

    Angel(TextureRegion region, List<FrameInfo> frameInfoList, float width, float height) {
        super(region, frameInfoList, width, height);
        init();
    }

    private void setStartDelay(long millis) {
        this.startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    private void init() {
        boolean moveLeft = random();
        float size = Math.max(getWidth(), getHeight());
        if (moveLeft) {
            p0x = 1 + size;
            p0y = random(-0.75f, 0.75f);
            p1x = random(-0.5f, 0.5f);
            p1y = random(-0.75f, 0.75f);
            p2x = random(-1 - size / 2, -1 - size);
            p2y = random(-0.5f, 0.5f);
        } else {
            p0x = -1 - size;
            p0y = random(-0.75f, 0.75f);
            p1x = random(-0.5f, 0.5f);
            p1y = random(-0.75f, 0.75f);
            p2x = random(1 + size / 2, 1 + size);
            p2y = random(-0.5f, 0.5f);
        }
        setScaleX(moveLeft ? -1 : 1);
        setPosition(p0x, p0y, -1.0f);
        setRotationZ(moveLeft ? 45 : -45);
        setStartDelay(random(3500, 5000));
        speed = random(0.222f, 0.298f) / Const.NANOS_IN_SECOND;
        t = 0;
    }

    @Override protected int getDesiredFPS() {
        return 25;
    }

    @Override public void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float deltaT = speed * deltaNanos;
        t += deltaT;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        setX(x);
        setY(y);

        if (t >= 1) {
            init();
        }
        super.step(deltaNanos);
    }
}

