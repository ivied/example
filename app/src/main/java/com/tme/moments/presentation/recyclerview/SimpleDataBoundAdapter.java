package com.tme.moments.presentation.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * @author zoopolitic.
 */
public abstract class SimpleDataBoundAdapter<T> extends DataBoundAdapter<T> {

    public interface OnItemClickListener<T> {
        void onItemClick(T item);
    }

    private final OnItemClickListener<T> onItemClickListener;

    protected SimpleDataBoundAdapter(OnItemClickListener<T> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    @Override public DataBoundViewHolder onCreateViewHolder(ViewGroup parent, int layoutId) {
        DataBoundViewHolder holder = super.onCreateViewHolder(parent, layoutId);
        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(v -> {
                int position = holder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    T item = getItem(holder.getAdapterPosition());
                    onItemClickListener.onItemClick(item);
                }
            });
        }
        return holder;
    }
}
