package com.tme.moments.presentation.scene.f5.kids.fun;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.math.Vector2;
import com.tme.moments.math.Vector3;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class ClockwiseBranch extends Decal {

    private float rotationSpeed;
    private float maxRotationAngle;
    private boolean shouldThrowLeaves;
    private long shakeWaitTime;

    ClockwiseBranch(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        transformationOffset = new Vector2(0, -height / 2);
        maxRotationAngle = 10f;
        shakeWaitTime = TimeUnit.MILLISECONDS.toNanos(random(5_000, 8_000));
        rotationSpeed = 50f / Const.NANOS_IN_SECOND;
    }

    void step(long deltaNanos) {
        if (shakeWaitTime > 0) {
            shakeWaitTime -= deltaNanos;
            return;
        }
        float deltaRotation = rotationSpeed * deltaNanos;
        float rotationZ = getRotation().getAngleAround(Vector3.Z);
        float newRotation = Math.max(0, Math.min(maxRotationAngle, rotationZ + deltaRotation));
        setRotationZ(newRotation);
        if (newRotation == maxRotationAngle) {
            rotationSpeed *= -1;
            shouldThrowLeaves = true;
        }
        if (newRotation == 0 && rotationSpeed < 0) {
            rotationSpeed *= -1;
            shakeWaitTime = TimeUnit.MILLISECONDS.toNanos(random(5_000, 8_000));
        }
    }

    boolean shouldThrowLeaves() {
        return shouldThrowLeaves;
    }

    void setShouldThrowLeaves(boolean shouldThrowLeaves) {
        this.shouldThrowLeaves = shouldThrowLeaves;
    }
}
