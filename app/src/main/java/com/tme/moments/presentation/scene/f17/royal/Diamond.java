package com.tme.moments.presentation.scene.f17.royal;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Diamond extends Decal {

    private float rotationSpeed;
    private long startDelayNanos;
    private boolean rotationEnabled = true;

    Diamond(TextureRegion region, float width, float height, float x, float y) {
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        setPosition(x, y, -1.0f);
        rotationSpeed = (random() ? 60f : -60f) / Const.NANOS_IN_SECOND;
    }

    void setRotationEnabled(boolean rotationEnabled) {
        if (this.rotationEnabled == rotationEnabled) {
            return;
        }
        this.rotationEnabled = rotationEnabled;
        if (!rotationEnabled) {
            setRotationZ(0);
        }
    }

    private void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    void step(long deltaNanos) {
        if (!rotationEnabled) {
            return;
        }
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float deltaRotation = rotationSpeed * deltaNanos;
        rotateZ(deltaRotation);
    }
}
