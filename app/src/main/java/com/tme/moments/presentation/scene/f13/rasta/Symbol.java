package com.tme.moments.presentation.scene.f13.rasta;

import android.graphics.PointF;
import android.util.Pair;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Symbol extends Decal {

    private float t;
    private float speed;
    private float endAngle;
    private boolean rotate;
    private float alphaSpeed;
    private float startDelayNanos;
    private float hangTimeNanos;
    private boolean inflated;
    private final List<Pair<TextureRegion, PointF>> infos;
    private Interpolator interpolator;
    private float minScale;

    Symbol(List<Pair<TextureRegion, PointF>> infos) {
        this.infos = infos;
        init();
        interpolator = new OvershootInterpolator(2.5f);
    }

    private void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        if (!inflated) {
            float deltaT = speed * deltaNanos;
            t += deltaT;
            t = Math.max(0, Math.min(1.0f, t));
            if (rotate) {
                float interpolatedAngle = t * endAngle;
                setRotationZ(interpolatedAngle);
            }
            float interpolatedScale = interpolator.getInterpolation(t) * minScale;
            setScale(interpolatedScale);
            if (t == 1) {
                inflated = true;
            }
        } else {
            if (hangTimeNanos > 0) {
                hangTimeNanos -= deltaNanos;
                return;
            }
            float alpha = color.a;
            float deltaAlpha = alphaSpeed * deltaNanos;
            float newAlpha = Math.max(0, alpha - deltaAlpha);
            setAlpha(newAlpha);
            if (newAlpha == 0) {
                init();
            }
        }
    }

    private void init() {
        int position = random(infos.size());
        Pair<TextureRegion, PointF> info = infos.get(position);
        TextureRegion region = info.first;
        float width = info.second.x;
        float height = info.second.y;
        boolean handSymbol = position == infos.size() - 1;
        minScale = handSymbol ? 2.0f : 2.75f;
        setTextureRegion(region);
        setDimensions(width, height);
        float maxSide = Math.max(width, height) * minScale;
        float x = random(-1 + maxSide / 2, 1 - maxSide / 2);
        float minY = -1.0f + maxSide / 2;
        float maxY = -0.1f - maxSide / 2;
        float y = random(minY, Math.max(minY, maxY));
        float z = -1.0f;
        setPosition(x, y, z);
        setScale(0);
        setAlpha(1);
        setRotationZ(0);
        t = 0;
        inflated = false;
        speed = 1.75f / Const.NANOS_IN_SECOND;
        boolean rotateLeft = random();
        float minAngle = rotateLeft ? 325 : -360;
        float maxAngle = rotateLeft ? 360 : -325;
        endAngle = random(minAngle, maxAngle);
        setRotationZ(0);
        rotate = random();
        alphaSpeed = 2.5f / Const.NANOS_IN_SECOND;
        setStartDelay(random(3000, 5000));
        hangTimeNanos = random(2000, 2800) * Const.NANOS_IN_MILLISECOND;
    }
}
