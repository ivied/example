package com.tme.moments.presentation.utils;

import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * @author zoopolitic.
 */
public class MediaUtils {

    private static final boolean VERBOSE = false;
    private static final String TAG = MediaUtils.class.getSimpleName();

    /**
     * search first track index matched specific MIME
     *
     * @param extractor media extractor
     * @param mimeType  "video/" or "audio/"
     * @return track index, -1 if not found
     */
    public static int selectTrack(final MediaExtractor extractor, final String mimeType) {
        final int numTracks = extractor.getTrackCount();
        MediaFormat format;
        String mime;
        for (int i = 0; i < numTracks; i++) {
            format = extractor.getTrackFormat(i);
            mime = format.getString(MediaFormat.KEY_MIME);
            if (mime.startsWith(mimeType)) {
                if (VERBOSE)
                    Log.d(TAG, "Extractor selected track " + i + " (" + mime + "): " + format);
                return i;
            }
        }
        return -1;
    }

    public static int extractInt(@NonNull MediaFormat format, @NonNull String key,
                                 int defaultValue) {
        if (!format.containsKey(key)) {
            return defaultValue;
        }
        try {
            return format.getInteger(key);
        }
        catch (NullPointerException e) { /* no such field */ }
        catch (ClassCastException e) { /* field of different type */ }
        return defaultValue;
    }

    public static long extractLong(@NonNull MediaFormat format, @NonNull String key,
                                   long defaultValue) {
        if (!format.containsKey(key)) {
            return defaultValue;
        }
        try {
            return format.getLong(key);
        }
        catch (NullPointerException e) { /* no such field */ }
        catch (ClassCastException e) { /* field of different type */ }
        return defaultValue;
    }
}
