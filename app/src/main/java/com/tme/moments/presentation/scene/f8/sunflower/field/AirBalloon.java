package com.tme.moments.presentation.scene.f8.sunflower.field;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class AirBalloon extends Decal {

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float speed;
    private float t;

    private float startDelayNanos;

    AirBalloon(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        setScale(1.1f);
        init();
    }

    public void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float deltaT = speed * deltaNanos;
        t += deltaT;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        setX(x);
        setY(y);

        if (t >= 1) {
            init();
        }
    }

    private void init() {
        boolean moveLeft = random();
        float width = getWidth() * getScaleX();
        if (moveLeft) {
            p0x = 1 + width / 2;
            p0y = random(0.25f, 0.75f);
            p1x = random(-0.75f, 0.75f);
            p1y = random(0.5f, 1.0f);
            p2x = random(-1 - width / 2, -1 - width);
            p2y = random(0f, 1.5f);
        } else {
            p0x = -1 - width / 2;
            p0y = random(0.25f, 0.75f);
            p1x = random(-0.75f, 0.75f);
            p1y = random(0.5f, 1.0f);
            p2x = random(1 + width / 2, 1 + width);
            p2y = random(0f, 1.5f);
        }
        setRotationZ(0);
        setPosition(p0x, p0y, -1.0f);
        this.startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(6500, 8500));
        speed = random(0.05f, 0.066f) / Const.NANOS_IN_SECOND;
        t = 0;
    }
}
