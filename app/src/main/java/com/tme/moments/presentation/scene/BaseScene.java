package com.tme.moments.presentation.scene;

import android.graphics.SurfaceTexture;
import android.opengl.Matrix;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.math.Vector3;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.core.App;
import com.tme.moments.presentation.core.Config;
import com.tme.moments.presentation.core.OpenGLLoader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.inject.Inject;

import timber.log.Timber;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;
import static android.opengl.GLES20.glViewport;
import static com.tme.moments.presentation.scene.BaseScene.Mode.CAMERA;
import static com.tme.moments.presentation.scene.BaseScene.Mode.IMAGE;

/**
 * @author zoopolitic.
 */
public abstract class BaseScene implements Scene {

    public enum Mode {
        CAMERA, IMAGE
    }

    @Inject
    protected OpenGLLoader openGLLoader;
    @Inject
    protected Config config;

    //  ================================== General ==================================

    protected float[] projectionMatrix = new float[16];
    protected Mode mode;

    protected int surfaceWidth;
    protected int surfaceHeight;

    protected float left;
    protected float right;
    protected float bottom;
    protected float top;
    protected float near;
    protected float far;

    //  ================================== Camera ==================================

    protected final FloatBuffer cameraVertexBuffer;
    protected final FloatBuffer cameraTextureBuffer;
    protected float[] transformM = new float[16];

    //  handles
    protected int cameraProgramHandle = -1;
    protected int aCameraPositionHandle = -1;
    protected int aCameraTextureHandle = -1;
    protected int uTransformMHandle = -1;

    //  ================================== Drawing objects ==================================

    //  handles
    protected int mainProgramHandle = -1;
    protected int uProjectionMatrixLoc = -1;
    protected int aPositionLocation = -1;
    protected int aColorLocation = -1;
    protected int aTexCoordLocation = -1;

    //  ================================== Drawing image ==================================

    @Nullable
    protected Sprite image;
    @Nullable
    protected Sprite frame;

    public BaseScene() {
        App.getInstance().coreComponent().inject(this);
        int numOfVertices = 4;
        int coordinatesPerVertex = 2;
        int sizeOfFloatInBytes = Float.SIZE / 8;
        cameraVertexBuffer = ByteBuffer
                .allocateDirect(numOfVertices * coordinatesPerVertex * sizeOfFloatInBytes)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        cameraTextureBuffer = ByteBuffer
                .allocateDirect(numOfVertices * coordinatesPerVertex * sizeOfFloatInBytes)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
    }

    protected abstract void prepareObjects(int surfaceWidth, int surfaceHeight);

    protected void prepareBackgroundEffects(int surfaceWidth, int surfaceHeight) {
    }

    protected TextureRegion getFrameRegion() {
        return null;
    }

    protected boolean isMatrix4Identity(float[] matrix) {
        return matrix[0] == 1.0f && matrix[1] == 0.0f && matrix[2] == 0.0f && matrix[3] == 0.0f
                && matrix[4] == 0.0f && matrix[5] == 1.0f && matrix[6] == 0.0f && matrix[7] == 0.0f
                && matrix[8] == 0.0f && matrix[9] == 0.0f && matrix[10] == 1.0f && matrix[11] == 0.0f
                && matrix[12] == 0.0f && matrix[13] == 0.0f && matrix[14] == 0.0f && matrix[15] == 1.0f;
    }

//    ======================================= Init [START] =========================================


    @Override public void init() {
        if (mainProgramHandle == -1) {
            prepareMainProgram();
        }
        if (cameraProgramHandle == -1) {
            prepareCameraProgram();
        }
    }

    private void prepareMainProgram() {
        Timber.d("prepare main program");
        mainProgramHandle =
                openGLLoader.createProgram(R.raw.vertex_shader, R.raw.fragment_shader);

        aPositionLocation = glGetAttribLocation(mainProgramHandle, "a_position");
        GlUtil.checkLocation(aPositionLocation, "a_position");
        glEnableVertexAttribArray(aPositionLocation);

        aColorLocation = glGetAttribLocation(mainProgramHandle, "a_color");
        GlUtil.checkLocation(aColorLocation, "a_color");
        glEnableVertexAttribArray(aColorLocation);

        aTexCoordLocation = glGetAttribLocation(mainProgramHandle, "a_texCoord");
        GlUtil.checkLocation(aTexCoordLocation, "a_texCoord");
        glEnableVertexAttribArray(aTexCoordLocation);

        uProjectionMatrixLoc = glGetUniformLocation(mainProgramHandle, "u_projTrans");
        GlUtil.checkLocation(uProjectionMatrixLoc, "u_projTrans");
    }

    private void prepareCameraProgram() {
        Timber.d("prepare camera program");
        cameraProgramHandle = openGLLoader.createProgram(
                R.raw.camera_vertex_shader, R.raw.camera_fragment_shader);
        uTransformMHandle = glGetUniformLocation(cameraProgramHandle, "uTransformM");
        GlUtil.checkLocation(uTransformMHandle, "uTransformM");

        aCameraTextureHandle = glGetAttribLocation(cameraProgramHandle, "aTexCoords");
        GlUtil.checkLocation(aCameraTextureHandle, "aTexCoords");

        aCameraPositionHandle = glGetAttribLocation(cameraProgramHandle, "aPosition");
        GlUtil.checkLocation(aCameraPositionHandle, "aPosition");
        glEnableVertexAttribArray(aCameraPositionHandle);
    }

//    ======================================= Init [END] ===========================================

//    ======================================= Setup [START] ========================================


    @CallSuper
    @Override
    public void setup(TextureInfo imageInfo, int width, int height) {
        Timber.d("Setup scene");
        surfaceWidth = width;
        surfaceHeight = height;

        glViewport(0, 0, width, height);

        mode = config.getImageUri() == null ? CAMERA : IMAGE;

        setupProjectionMatrix(width, height);

        updateCameraParams();

        prepareObjects(width, height);

        if (hasBackgroundEffects()) {
            prepareBackgroundEffects(width, height);
        }

        prepareImageDecal(imageInfo, width, height);

        prepareFrame();
    }

    private void setupProjectionMatrix(float width, int height) {
        // Create a new perspective projection matrix. The height will stay the same
        // while the width will vary as per aspect ratio.
        final float ratio = width / height;
        left = -ratio;
        right = ratio;
        bottom = -1.0f;
        top = 1.0f;

        near = 1f;
        far = 50.0f;

        Matrix.setIdentityM(projectionMatrix, 0);
        Matrix.frustumM(projectionMatrix, 0, left, right, bottom, top, near, far);
    }

    @Override
    public void updateCameraParams() {
        if (surfaceWidth == 0 && surfaceHeight == 0) {
            return;
        }
        Timber.d("Update camera params");
        fillCameraTextureBuffer(cameraTextureBuffer);

        int cameraPreviewWidth = config.getCameraPreviewWidth();
        int cameraPreviewHeight = config.getCameraPreviewHeight();

        float desiredWidth = getDesiredCameraWidthFraction();
        float desiredHeight = getDesiredCameraHeightFraction();

        float cameraX = getCameraPositionX();
        float cameraY = getCameraPositionY();

        fillCameraVertexBuffer(cameraVertexBuffer, cameraPreviewWidth, cameraPreviewHeight,
                surfaceWidth, surfaceHeight, desiredWidth, desiredHeight, cameraX, cameraY);
    }

    /**
     * Fills camera vertex buffer
     *
     * @param vertexBuffer        buffer to fill
     * @param cameraPreviewWidth  preview width of the camera obtained during camera open
     * @param cameraPreviewHeight preview height of the camera obtained during camera open
     * @param surfaceWidth        width of the surface
     * @param surfaceHeight       height of the surface
     * @param desiredWidth        desired width of the camera <i><b>in openGL screen coordinates</b></i>
     * @param desiredHeight       desired height of the camera <i><b>in openGL screen coordinates</b></i>
     * @param x                   desired X position of the camera <i><b>in openGL screen coordinates</b></i>
     * @param y                   desired Y position of the camera <i><b>in openGL screen coordinates</b></i>
     */
    void fillCameraVertexBuffer(FloatBuffer vertexBuffer,
                                int cameraPreviewWidth, int cameraPreviewHeight,
                                int surfaceWidth, int surfaceHeight,
                                float desiredWidth, float desiredHeight,
                                float x, float y) {
        int desiredWidthPx = Math.round(desiredWidth * surfaceWidth);
        int desiredHeightPx = Math.round(desiredHeight * surfaceHeight);

        // calculate how much should we scale image texture by X and Y to fit surface width and height
        float scaleX = desiredWidthPx / (float) cameraPreviewWidth;
        float scaleY = desiredHeightPx / (float) cameraPreviewHeight;

        // pick max multiplier to fit smallest side
        float multiplier = Math.max(scaleX, scaleY);

        // translate pixel sizes to openGL screen coordinates
        float openGLWidth = ((float) cameraPreviewWidth / surfaceWidth) * getProjectionWidth();
        float openGLHeight = ((float) cameraPreviewHeight / surfaceHeight) * getProjectionHeight();
        // scale openGL width and height with calculated multiplier
        float width = openGLWidth * multiplier;
        float height = openGLHeight * multiplier;

        float[] vertices = new float[8];
        vertices[0] = x - width / 2;    // top left x
        vertices[1] = y + height / 2;   // top left y

        vertices[2] = x - width / 2;    // bottom left x
        vertices[3] = y - height / 2;   // bottom left y

        vertices[4] = x + width / 2;    // top right x
        vertices[5] = y + height / 2;   // top right y

        vertices[6] = x + width / 2;    // bottom right x
        vertices[7] = y - height / 2;   // bottom right y

        vertexBuffer.clear();
        vertexBuffer.put(vertices);
        vertexBuffer.flip();
        vertexBuffer.position(0);
    }

    /**
     * Fills buffer for camera texture.
     *
     * @param textureBuffer buffer to fill
     */
    void fillCameraTextureBuffer(FloatBuffer textureBuffer) {
        textureBuffer.clear();
        textureBuffer.put(new float[]{
                0.0f, 1.0f,   // top left
                0.0f, 0.0f,   // bottom left
                1.0f, 1.0f,   // top right
                1.0f, 0.0f,   // bottom right
        });
        textureBuffer.flip();
        textureBuffer.position(0);
    }

    /**
     * Returns desired camera width as a fraction from 0 to 1.
     * 1 is for full surface width
     */
    protected float getDesiredCameraWidthFraction() {
        return 1.0f;
    }

    /**
     * Returns desired camera height as a fraction from 0 to 1
     * 1 is for full surface height
     */
    protected float getDesiredCameraHeightFraction() {
        return 1.0f;
    }

    /**
     * Returns camera X position <i><b>in openGL screen coordinates</b></i>
     */
    protected float getCameraPositionX() {
        return 0f;
    }

    /**
     * Returns camera Y position <i><b>in openGL screen coordinates</b></i>
     */
    protected float getCameraPositionY() {
        return 0f;
    }

    protected void prepareImageDecal(TextureInfo imageInfo, int surfaceWidth, int surfaceHeight) {
        if (imageInfo == null) {
            return;
        }
        int imageWidth = imageInfo.imageWidth;
        int imageHeight = imageInfo.imageHeight;

        int desiredWidth = Math.round(getDesiredCameraWidthFraction() * surfaceWidth);
        int desiredHeight = Math.round(getDesiredCameraHeightFraction() * surfaceHeight);

        // calculate how much should we scale image texture by X and Y to fit surface width and height
        float scaleX = desiredWidth / (float) imageWidth;
        float scaleY = desiredHeight / (float) imageHeight;

        // pick max multiplier to fit smallest side
        float multiplier = Math.max(scaleX, scaleY);

        // translate pixel sizes to openGL screen coordinates
        float openGLWidth = ((float) imageWidth / surfaceWidth) * getProjectionWidth();
        float openGLHeight = ((float) imageHeight / surfaceHeight) * getProjectionHeight();
        // scale openGL width and height with calculated multiplier
        float width = openGLWidth * multiplier;
        float height = openGLHeight * multiplier;

        int imageHandle = imageInfo.textureHandle;
        TextureRegion region = new TextureRegion(imageHandle, imageWidth, imageHeight, 0, 0, imageWidth, imageHeight);
        image = new Sprite(region, width, height);
        image.setPosition(getCameraPositionX(), getCameraPositionY(), -1.0f);
    }

    private void prepareFrame() {
        if (frame != null) {
            return;
        }
        TextureRegion region = getFrameRegion();
        if (region == null) {
            return;
        }
        float totalWidth = getProjectionWidth();
        float totalHeight = getProjectionHeight();
        frame = new Sprite(region, totalWidth, totalHeight);
        frame.setPosition(0, 0, -1.0f);
    }

    protected final float getProjectionWidth() {
        return right - left;
    }

    protected final float getProjectionHeight() {
        return top - bottom;
    }

    //    ======================================= Setup [END] ======================================

    @CallSuper
    @Override public void release() {
        Timber.w("Release scene");
        glDeleteProgram(mainProgramHandle);
        GlUtil.checkGlError("Delete main program");
        glDeleteProgram(cameraProgramHandle);
        GlUtil.checkGlError("Delete camera program");

        int[] values = new int[1];
        if (frame != null) {
            values[0] = frame.getTextureRegion().getTextureHandle();
            glDeleteTextures(1, values, 0);
            GlUtil.checkGlError("Delete frame");
        }

        mainProgramHandle = -1;
        cameraProgramHandle = -1;

        mainProgramHandle = -1;
        uProjectionMatrixLoc = -1;
        aPositionLocation = -1;
        aColorLocation = -1;
        aTexCoordLocation = -1;

        cameraProgramHandle = -1;
        aCameraPositionHandle = -1;
        uTransformMHandle = -1;
        aCameraTextureHandle = -1;

        image = null;
        frame = null;
    }

    @Override public void angleChanged(float deltaAngle) {
        if (mode == Mode.IMAGE && image != null) {
            image.rotateZ(deltaAngle);
        }
    }

    @Override public void zoomChanged(float zoom) {
        if (mode == Mode.IMAGE) {
            float width = image.getWidth();
            float height = image.getHeight();
            float currentZoom = image.getScaleX();

            final float maxZoom = 3.0f;
            final float minXZoom = getDesiredCameraWidthFraction() * getProjectionWidth() / width;
            final float minYZoom = getDesiredCameraHeightFraction() * getProjectionHeight() / height;
            final float minZoom = Math.max(minXZoom, minYZoom);
            currentZoom *= zoom;
            currentZoom = Math.max(minZoom, Math.min(currentZoom, maxZoom));
            image.setScale(currentZoom, currentZoom);
        }
    }

    @Override public void onMoved(float dx, float dy) {
        if (mode == Mode.IMAGE && !(dx == 0 && dy == 0) && image != null) {
            // make move a 50% faster
            dx *= 1.5f;
            dy *= 1.5f;
            Vector3 position = image.getPosition();
            float x = position.x;
            float y = position.y;
            dx = dx / surfaceWidth;
            // touch Y is inverted comparing to openGL coordinates
            dy = -dy / surfaceHeight;
            // x, y are coordinates of the center of the image, so we don't allow image
            // to go outside of the screen more then half of its size
            dx = Math.max(Math.min(dx, right - x), left - x);
            dy = Math.max(Math.min(dy, top - y), bottom - y);
            image.translate(dx, dy, 0);
        }
    }

    protected float calculateBackgroundScale() {
        // pick left edge middle point
        float p1x = -1;
        float p1y = 0;
        // calculate sin and cos of 45 degrees
        float cos45 = (float) Math.cos(Math.PI / 4);
        float sin45 = (float) Math.sin(Math.PI / 4);
        // calculate x,y for selected edge middle point after rotation by 45 degrees
        // since it will be the most inclined point of the rectangle and corners of the
        // square in this point will be less then drawing surface so we need to increase
        // scale to avoid this effect
        float p2x = p1x * cos45 + p1y * sin45;
        float p2y = p1x * sin45 + p1y * cos45;
        // target point is the position of taken middle of the edge of the square
        // that should be in order to avoid black corners effect
        float targetX = -1;
        float targetY = 1;
        // calculate how much we should increase scale so taken middle point will
        // be target point after rotation by 45 degrees. Doesn't matter whether
        // we calculate it by targetX/p2x or targetY/p2y - scale will be the same
        return targetX / p2x;
    }

    @Override public float getImageX() {
        if (image != null) {
            return surfaceWidth / 2 + image.getPosition().x / getProjectionWidth() * surfaceWidth;
        }
        return -1;
    }

    @Override public float getImageY() {
        if (image != null) {
            return surfaceHeight / 2 + image.getPosition().y / getProjectionHeight() * surfaceHeight;
        }
        return -1;
    }

    @Override public void onCameraModeEnabled() {
        mode = CAMERA;
        image = null;
        updateCameraParams();
    }

    @Override public void onImageModeEnabled(TextureInfo imageInfo) {
        mode = IMAGE;
        prepareImageDecal(imageInfo, surfaceWidth, surfaceHeight);
    }

    @Override public boolean hasBackgroundEffects() {
        return false;
    }

    @Override public void onObjectsStateChanged(boolean enabled) {
        if (enabled) {
            prepareObjects(surfaceWidth, surfaceHeight);
        }
    }

    @Override public void onBackgroundEffectsStateChanged(boolean enabled) {
        if (enabled && hasBackgroundEffects()) {
            prepareBackgroundEffects(surfaceWidth, surfaceHeight);
        }
    }

    //    ================================= Common draw [START] =============I don=======================

    protected void drawImage() {
        if (image == null) {
            return;
        }
        image.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
    }

    protected void drawFrame() {
        if (frame == null) {
            return;
        }
        frame.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
    }

    @Override
    public void drawCameraInput(SurfaceTexture cameraTexture) {
        glUseProgram(cameraProgramHandle);
        if (cameraTexture != null) {
            cameraTexture.updateTexImage();
            cameraTexture.getTransformMatrix(transformM);
        }
        // during camera switch sometimes initial transform matrix is identity which causes image to rotate
        // so we want to skip this frame
        if (isMatrix4Identity(transformM)) return;
        glUniformMatrix4fv(uTransformMHandle, 1, false, transformM, 0);
        glVertexAttribPointer(aCameraPositionHandle, 2, GL_FLOAT, false, 0, cameraVertexBuffer);
        glVertexAttribPointer(aCameraTextureHandle, 2, GL_FLOAT, false, 0, cameraTextureBuffer);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }


    protected TextureRegion buildRegion(TextureInfo info, int x, int y, int regionWidthPixels,
                                        int regionHeightPixels) {
        int texHandle = info.textureHandle;
        int texWidth = info.imageWidth;
        int texHeight = info.imageHeight;
        int inSampleSize = info.inSampleSize;
        return new TextureRegion(texHandle, texWidth, texHeight,
                x / inSampleSize, y / inSampleSize,
                regionWidthPixels / inSampleSize, regionHeightPixels / inSampleSize);
    }
    //    ================================= Common draw [END] ======================================
}
