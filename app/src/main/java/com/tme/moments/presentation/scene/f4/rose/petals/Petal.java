package com.tme.moments.presentation.scene.f4.rose.petals;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Petal extends Decal {

    private float maxZ;
    private float zSpeed;
    private float xSpeed;
    private float ySpeed;
    private float rotationSpeed;
    private float alphaSpeed;

    private float startDelayNanos;

    Petal(TextureRegion textureRegion, float width, float height) {
        setTextureRegion(textureRegion);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        init();
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(3500, 8500));
    }

    void step(long deltaNanos) {
        if (startDelayNanos >= 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float z = getZ();

        if (z > maxZ) {
            float deltaZ = zSpeed * deltaNanos;
            float deltaX = xSpeed * deltaNanos;
            float deltaY = ySpeed * deltaNanos;
            translate(deltaX, deltaY, deltaZ);
            float deltaRotation = rotationSpeed * deltaNanos;
            rotateX(deltaRotation);
            rotateY(deltaRotation);
            rotateZ(deltaRotation);
        }

        if (z <= maxZ) {
            float alpha = color.a;
            if (alpha > 0) {
                float deltaAlpha = alphaSpeed * deltaNanos;
                float newAlpha = Math.max(0, alpha - deltaAlpha);
                setAlpha(newAlpha);
            } else {
                init();
            }
        }
    }

    private void init() {
        float scale = random(1.8f, 2.2f);
        // reduce z by half of biggest side of the object
        // to not being clipped during X and Y rotation
        float maxSize = Math.max(getWidth() * scale, getHeight() * scale);
        float z = random(-1.0f + maxSize / 2, 0 + maxSize / 2);
        maxZ = -12.0f + maxSize;
        float min = 3;
        float max = -3;
        float x = random(min, max);
        float y = random(min, max);
        zSpeed = random(-4.0f, -2.5f) / Const.NANOS_IN_SECOND;
        xSpeed = random(-2.0f, 2.0f) / Const.NANOS_IN_SECOND;
        ySpeed = random(-2.0f, 2.0f) / Const.NANOS_IN_SECOND;
        rotationSpeed = random(140f, 170f) / Const.NANOS_IN_SECOND;
        alphaSpeed = 3.33f / Const.NANOS_IN_SECOND;
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 5000));
        setAlpha(1.0f);
        float minStartAngle = 25f;
        float maxStartAngle = 50f;
        setRotation(random(minStartAngle, maxStartAngle), random(minStartAngle, maxStartAngle), random(minStartAngle, maxStartAngle));
        setScale(scale);
        setPosition(x, y, z);
    }
}
