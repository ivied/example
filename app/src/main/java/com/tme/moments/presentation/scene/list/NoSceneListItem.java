package com.tme.moments.presentation.scene.list;

import com.tme.moments.R;
import com.tme.moments.presentation.scene.Scene;

/**
 * @author zoopolitic.
 */
public class NoSceneListItem extends SceneListItem {

    NoSceneListItem(Scene scene) {
        super(-1, scene, R.string.no_frame);
    }

    @Override public int getLayoutResId() {
        return R.layout.no_scene_list_item;
    }
}
