package com.tme.moments.presentation.scene.f18.paper.speech;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.Const;

import java.util.List;

/**
 * @author zoopolitic.
 */
class VerticalBee extends AnimatedSprite {

    private final static float TOP_Y = -0.4f;

    private long delayNanos;
    private final float x;

    private float speedY;
    private float upVelocity;
    private float downVelocity;
    private boolean reachedTop;
    private float deflateSpeed;
    private boolean finished;

    VerticalBee(TextureRegion region, List<FrameInfo> frames, float width, float height,
                float x) {
        super(region, frames, width, height);
        this.x = x;
        setColor(1, 1, 1, 1);
        init();
    }

    void setDelayMillis(long delayMillis) {
        delayNanos = delayMillis * Const.NANOS_IN_MILLISECOND;
    }

    @Override protected int getDesiredFPS() {
        return 25;
    }

    boolean isFinished() {
        return finished;
    }

    void setFinished(boolean finished) {
        this.finished = finished;
    }

    @Override public void step(long deltaNanos) {
        if (finished) {
            return;
        }
        if (delayNanos > 0) {
            delayNanos -= deltaNanos;
            return;
        }

        float deltaY = speedY * deltaNanos;
        float y = getY();
        if (!reachedTop) {
            speedY += upVelocity;
            speedY = Math.max(speedY, 0.05f / Const.NANOS_IN_SECOND);
            float newY = Math.min(TOP_Y, y + deltaY);
            setY(newY);
            if (newY == TOP_Y) {
                reachedTop = true;
            }
        } else {
            speedY += downVelocity;
            float minY = -1 - getHeight() / 2;
            float newY = Math.max(minY, y - deltaY);
            setY(newY);

            float scale = getScaleX();
            float deltaScale = deflateSpeed * deltaNanos;
            scale -= deltaScale;
            scale = Math.max(0.5f, scale);
            setScale(scale);

            if (newY == minY) {
                finished = true;
                init();
            }
        }
        super.step(deltaNanos);
    }

    private void init() {
        setScale(1);
        float y = -1 - getHeight() / 2;
        float z = -1.0f;
        setPosition(x, y, z);
        speedY = 0.86f / Const.NANOS_IN_SECOND;
        downVelocity = 0.04f / Const.NANOS_IN_SECOND;
        upVelocity = -0.01f / Const.NANOS_IN_SECOND;
        deflateSpeed = 0.2f / Const.NANOS_IN_SECOND;
        reachedTop = false;
    }
}
