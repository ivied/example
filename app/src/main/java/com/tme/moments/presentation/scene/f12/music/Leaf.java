package com.tme.moments.presentation.scene.f12.music;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Leaf extends Decal {

    private final boolean moveLeft;
    private final boolean rotateAllAxis;

    private float speed;
    private float rotationSpeed;

    private float p0x, p0y;
    private float p1x, p1y;
    private float p2x, p2y;
    private float p3x, p3y;

    private float startDelayNanos;
    private float t;

    Leaf(TextureRegion region, float width, float height, boolean moveLeft,
         boolean rotateAllAxis) {
        this.moveLeft = moveLeft;
        this.rotateAllAxis = rotateAllAxis;
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        init();
        setStartDelay(random(2000, 12000));
    }

    private void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    void step(long deltaNanos) {
        if (startDelayNanos >= 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float deltaT = speed * deltaNanos;
        t += deltaT;

        float deltaRotation = rotationSpeed * deltaNanos;
        rotateZ(deltaRotation);
        if (rotateAllAxis) {
            rotateX(deltaRotation);
            rotateY(deltaRotation);
        }

        float single = (1 - t);
        float quadratic = (1 - t) * (1 - t);
        float cube = (1 - t) * (1 - t) * (1 - t);
        float x = cube * p0x + 3 * t * quadratic * p1x + 3 * t * t * single * p2x + t * t * t * p3x;
        float y = cube * p0y + 3 * t * quadratic * p1y + 3 * t * t * single * p2y + t * t * t * p3y;

        setX(x);
        setY(y);

        if (t >= 1) {
            init();
        }
    }

    private void init() {
        // reduce maxZ by half of biggest side of the object
        // to not being clipped during X and Y rotation
        float z = rotateAllAxis
                ? -1.0f - Math.max(getWidth(), getHeight()) / 2
                : -1.0f;
        float deltaZ = -1 - z;
        float minY = -0.63f;
        float maxY = 0.7f;
        if (moveLeft) {
            p0x = 1 + getHeight() / 2 + deltaZ;
            p0y = random(minY, maxY);

            p1x = random(0.0f, 1.0f);
            p1y = random(minY, maxY);

            p2x = random(-1.0f, 0.0f);
            p2y = random(minY, maxY);

            p3x = -1 - getWidth() / 2 - deltaZ;
            p3y = random(-0.4f, 0.6f);
        } else {
            p0x = -1 - getHeight() / 2 - deltaZ;
            p0y = random(minY, maxY);

            p1x = random(-1.0f, 0.0f);
            p1y = random(minY, maxY);

            p2x = random(0.0f, 1.0f);
            p2y = random(minY, maxY);

            p3x = 1 + getWidth() / 2 + deltaZ;
            p3y = random(minY, maxY);
        }

        speed = random(0.12f, 0.18f) / Const.NANOS_IN_SECOND;
        rotationSpeed = random(-120f, 120f) / Const.NANOS_IN_SECOND;
        setScale(random(0.45f, 1.0f));
        setStartDelay(random(0, 10000));
        setColor(1, 1, 1, 1);
        setPosition(p0x, p0y, z);
        t = 0;
    }
}
