package com.tme.moments.presentation.scene.f7.emotional.hardcore;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.math.MathUtils;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Drop extends Decal {

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private final boolean leftFlow;
    private float speed;
    private float alphaSpeed;
    private float startDelayNanos;
    private float t;

    Drop(TextureRegion region, float width, float height, boolean leftFlow) {
        this.leftFlow = leftFlow;
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        init();
    }

    void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    void step(long deltaNanos) {
        if (startDelayNanos >= 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float deltaT = speed * deltaNanos;
        t += deltaT;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        float t1 = t + 0.3f;
        float x1 = (1 - t1) * (1 - t1) * p0x + 2 * (1 - t1) * t1 * p1x + t1 * t1 * p2x;
        float y1 = (1 - t1) * (1 - t1) * p0y + 2 * (1 - t1) * t1 * p1y + t1 * t1 * p2y;
        float angle = MathUtils.angleBetweenLines(x, y, x, y1, x, y, x1, y1);
        setRotationZ(-angle);

        setX(x);
        setY(y);

        float alpha = color.a;
        if (alpha < 1) {
            float deltaAlpha = alphaSpeed * deltaNanos;
            alpha += deltaAlpha;
            alpha = Math.min(1, alpha);
            setAlpha(alpha);
        }
        if (t >= 1) {
            init();
        }
    }

    private void init() {
        if (leftFlow) {
            p0x = -0.6f;
            p0y = -0.04f;

            p1x = -0.9f;
            p1y = -0.4f;

            p2x = -1.0f;
            p2y = -1 - getHeight() / 2;
            setScaleX(-1);
        } else {
            p0x = 0.5f;
            p0y = -0.1f;

            p1x = 0.8f;
            p1y = -0.65f;

            p2x = 0.9f;
            p2y = -1 - getHeight() / 2;
            setScaleX(1);
        }
        speed = 0.3f / Const.NANOS_IN_SECOND;
        alphaSpeed = 1.0f / Const.NANOS_IN_SECOND;
        t = 0;
        setColor(1, 1, 1, 1);
        setAlpha(0);
        setPosition(p0x, p0y, -1.0f);
    }
}
