package com.tme.moments.presentation.scene;

import android.graphics.SurfaceTexture;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glUseProgram;

/**
 * @author zoopolitic.
 */
public class NoFrameScene extends BaseScene {

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        glClear(GL_COLOR_BUFFER_BIT);
        if (mode == Mode.CAMERA) {
            drawCameraInput(cameraTexture);
        }
        if (mode == Mode.IMAGE) {
            glUseProgram(mainProgramHandle);
            drawImage();
        }
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {

    }
}