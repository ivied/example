package com.tme.moments.presentation.scene.f4.rose.petals;

import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.support.v4.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.Rippler;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_STENCIL_BUFFER_BIT;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class RosePetals extends BaseScene {

    private DecalBatch redBatch;
    private DecalBatch pinkBatch;
    private List<Petal> petals;
    private List<Particle> particles;
    private DecalBatch particlesBatch;
    private Rippler rippler;

    private Sprite overlay;
    private Sprite background;
    private TextureInfo redAtlas;
    private TextureInfo pinkAtlas;
    private TextureInfo atlasInfo;

    @Override protected float getCameraPositionX() {
        return 0.0888f;
    }

    @Override protected float getCameraPositionY() {
        return 0.1259f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.7574f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.75f;
    }

    @Override public boolean hasBackgroundEffects() {
        return true;
    }

    @Override public void init() {
        super.init();
        rippler = new Rippler();
        rippler.init(openGLLoader);
    }

    @Override public void setup(TextureInfo imageInfo, int width, int height) {
        super.setup(imageInfo, width, height);
        rippler.setup(width, height, R.drawable.f4_overlay);
    }

    @Override protected void prepareBackgroundEffects(int surfaceWidth, int surfaceHeight) {
        super.prepareBackgroundEffects(surfaceWidth, surfaceHeight);
        rippler.reset();
        atlasInfo = openGLLoader.loadTexture(R.drawable.f4_atlas);
        prepareParticles(atlasInfo);
        particlesBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        particlesBatch.initialize(particles.size());
        particlesBatch.addAll(particles);
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        prepareBackground();
        prepareOverlay();
        preparePetals();
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        boolean objectsEnabled = config.isObjectsEnabled();
        glUseProgram(mainProgramHandle);
        background.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation,
                aTexCoordLocation, aColorLocation);
        if (mode == Mode.IMAGE) {
            glUseProgram(mainProgramHandle);
            drawImage();
        } else {
            drawCameraInput(cameraTexture);
        }
        glUseProgram(mainProgramHandle);
        if (config.isBackgroundEffectsEnabled()) {
            rippler.render();
        } else {
            overlay.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation,
                    aTexCoordLocation, aColorLocation);
        }

        if (config.isBackgroundEffectsEnabled()) {
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            particlesBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Particle particle : particles) {
                particle.step(deltaNanos);
            }
        }

        if (objectsEnabled) {
            glUseProgram(mainProgramHandle);
            glUniformMatrix4fv(uProjectionMatrixLoc, 1, false, projectionMatrix, 0);
            glBindTexture(GL_TEXTURE_2D, redAtlas.textureHandle);
            redBatch.render(uProjectionMatrixLoc, projectionMatrix);
            glBindTexture(GL_TEXTURE_2D, pinkAtlas.textureHandle);
            pinkBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Petal petal : petals) {
                petal.step(deltaNanos);
            }
        }
    }

    @Override public void release() {
        super.release();
        rippler.release();
        int[] values = new int[3];
        int texturesToDeleteSize = 0;
        if (redAtlas != null) {
            values[texturesToDeleteSize] = redAtlas.textureHandle;
            texturesToDeleteSize++;
        }
        if (pinkAtlas != null) {
            values[texturesToDeleteSize] = pinkAtlas.textureHandle;
            texturesToDeleteSize++;
        }
        if (atlasInfo != null) {
            values[texturesToDeleteSize] = atlasInfo.textureHandle;
            texturesToDeleteSize++;
        }
        if (texturesToDeleteSize != 0) {
            glDeleteTextures(texturesToDeleteSize, values, 0);
            GlUtil.checkGlError("Release " + getClass().getSimpleName());
        }
    }

    private void prepareOverlay() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f4_overlay);
        int textureHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        TextureRegion region = new TextureRegion(textureHandle, w, h, 0, 0, w, h);
        overlay = new Sprite(region, getProjectionWidth(), getProjectionHeight());
        overlay.setPosition(0, 0, -1);
    }

    private void prepareBackground() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f4_background);
        int textureHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        TextureRegion region = new TextureRegion(textureHandle, w, h, 0, 0, w, h);
        background = new Sprite(region, getProjectionWidth(), getProjectionHeight());
        background.setPosition(0, 0, -1);
    }

    private void preparePetals() {
        petals = new ArrayList<>();

        List<Pair<TextureRegion, PointF>> redInfos = new ArrayList<>();
        List<Pair<TextureRegion, PointF>> pinkInfos = new ArrayList<>();
        redAtlas = openGLLoader.loadTexture(R.drawable.f4_red_petals);

        redInfos.add(new Pair<>(buildRegion(redAtlas, 0, 0, 800, 757), new PointF(1.0f, 0.9462f)));
        redInfos.add(new Pair<>(buildRegion(redAtlas, 800, 0, 800, 923), new PointF(1.0f, 1.1537f)));
        redInfos.add(new Pair<>(buildRegion(redAtlas, 0, 923, 800, 898), new PointF(1.0f, 1.1225f)));

        pinkAtlas = openGLLoader.loadTexture(R.drawable.f4_pink_petals);

        pinkInfos.add(new Pair<>(buildRegion(pinkAtlas, 0, 0, 850, 841), new PointF(1.0625f, 1.0512f)));
        pinkInfos.add(new Pair<>(buildRegion(pinkAtlas, 850, 0, 850, 861), new PointF(1.0625f, 1.0762f)));
        pinkInfos.add(new Pair<>(buildRegion(pinkAtlas, 0, 861, 850, 846), new PointF(1.0625f, 1.0575f)));

        int pinkCount = 6;
        int redCount = 3;
        redBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        redBatch.initialize(redCount);
        pinkBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        pinkBatch.initialize(pinkCount);
        for (int i = 0; i < redCount; i++) {
            int index = i == redCount - 1 ? 0 : i;
            addPetal(petals, redInfos, index, redBatch);
        }
        for (int i = 0; i < pinkCount; i++) {
            int index = random(0, pinkInfos.size());
            addPetal(petals, pinkInfos, index, pinkBatch);
        }
    }

    private void prepareParticles(TextureInfo info) {
        List<Pair<TextureRegion, PointF>> regionInfos = new ArrayList<>();
        // particles
        regionInfos.add(new Pair<>(buildRegion(info, 0, 0, 90, 90), new PointF(0.1125f, 0.1125f)));
        regionInfos.add(new Pair<>(buildRegion(info, 90, 0, 90, 90), new PointF(0.1125f, 0.1125f)));
        regionInfos.add(new Pair<>(buildRegion(info, 180, 0, 90, 90), new PointF(0.1125f, 0.1125f)));
        regionInfos.add(new Pair<>(buildRegion(info, 270, 0, 90, 90), new PointF(0.1125f, 0.1125f)));
        regionInfos.add(new Pair<>(buildRegion(info, 360, 0, 90, 90), new PointF(0.1125f, 0.1125f)));
        Pair<TextureRegion, PointF> bubbleInfo = new Pair<>(buildRegion(info, 450, 0, 86, 87), new PointF(0.1075f, 0.1087f));
        int count = 500;
        particles = new ArrayList<>();
        Particle.Direction direction = random() ? Particle.Direction.RIGHT : Particle.Direction.LEFT;
        for (int i = 0; i < count; i++) {
            float z = random(-3.0f, -1.0f);
            float scale;
            Pair<TextureRegion, PointF> regionInfo;
            if (z >= -1.1f) { // big particle
                boolean useBubble = random(3) == 0; // 33% chance
                regionInfo = useBubble ? bubbleInfo : regionInfos.get(random(regionInfos.size()));
                scale = 0.84f;
            } else if (z < -1.1f && z > -1.5f){ // medium particle
                boolean useBubble = random(3) == 0; // 33% chance
                regionInfo = useBubble ? bubbleInfo : regionInfos.get(random(regionInfos.size()));
                scale = 0.4f;
            } else { // small particle
                regionInfo = regionInfos.get(random(regionInfos.size()));
                scale = 0.4f;
            }
            TextureRegion region = regionInfo.first;
            float width = regionInfo.second.x;
            float height = regionInfo.second.y;
            Particle particle = new Particle(region, width, height, z, direction);
            particle.setScale(scale);
            particles.add(particle);
        }
    }

    private void addPetal(List<Petal> petals, List<Pair<TextureRegion, PointF>> infos, int index,
                          DecalBatch batch) {
        Pair<TextureRegion, PointF> info = infos.get(index);
        TextureRegion region = info.first;
        float width = info.second.x;
        float height = info.second.y;
        Petal petal = new Petal(region, width, height);
        petals.add(petal);
        batch.add(petal);
    }
}