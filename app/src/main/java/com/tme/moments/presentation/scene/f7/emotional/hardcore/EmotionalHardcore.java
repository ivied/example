package com.tme.moments.presentation.scene.f7.emotional.hardcore;

import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.presentation.Rippler;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glUseProgram;

/**
 * @author zoopolitic.
 */
public class EmotionalHardcore extends BaseScene {

    private Rippler rippler;
    private Heart heart;
    private DecalBatch dropsBatch;
    private List<Drop> drops;
    private TextureInfo atlasInfo;
    private Fairy fairy;

    @Override protected float getCameraPositionX() {
        return 0.0f;
    }

    @Override protected float getCameraPositionY() {
        return -0.037f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.8444f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.8222f;
    }

    @Override public boolean hasBackgroundEffects() {
        return true;
    }

    @Override protected TextureRegion getFrameRegion() {
        TextureInfo frameInfo = openGLLoader.loadTexture(R.drawable.f7);
        return new TextureRegion(frameInfo.textureHandle, frameInfo.imageWidth, frameInfo.imageHeight,
                0, 0, frameInfo.imageWidth, frameInfo.imageHeight);
    }

    @Override protected void prepareBackgroundEffects(int surfaceWidth, int surfaceHeight) {
        super.prepareBackgroundEffects(surfaceWidth, surfaceHeight);
        rippler.reset();
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        atlasInfo = openGLLoader.loadTexture(R.drawable.f7_atlas);
        TextureRegion heartRegion = buildRegion(atlasInfo, 0, 0, 157, 150);
        heart = new Heart(heartRegion, 0.1962f, 0.1875f);

        List<Pair<TextureRegion, PointF>> infos = new ArrayList<>();
        infos.add(new Pair<>(buildRegion(atlasInfo, 157, 0, 100, 100), new PointF(0.125f, 0.125f)));
        infos.add(new Pair<>(buildRegion(atlasInfo, 257, 0, 110, 199), new PointF(0.1375f, 0.2487f)));
        infos.add(new Pair<>(buildRegion(atlasInfo, 367, 0, 120, 175), new PointF(0.15f, 0.2187f)));
        infos.add(new Pair<>(buildRegion(atlasInfo, 487, 0, 130, 137), new PointF(0.1625f, 0.1712f)));
        infos.add(new Pair<>(buildRegion(atlasInfo, 617, 0, 65, 220), new PointF(0.0812f, 0.275f)));
        infos.add(new Pair<>(buildRegion(atlasInfo, 682, 0, 60, 194), new PointF(0.075f, 0.2425f)));

        drops = new ArrayList<>();
        long startDelay = 0;
        long interval = 530;
        for (int i = 0; i < 6; i++) {
            Pair<TextureRegion, PointF> info = infos.get(i);
            TextureRegion region = info.first;
            float width = info.second.x;
            float height = info.second.y;
            Drop drop = new Drop(region, width, height, true);
            drop.setStartDelay(startDelay);
            startDelay += interval;
            drops.add(drop);
        }
        startDelay = 0;
        for (int i = 0; i < 6; i++) {
            Pair<TextureRegion, PointF> info = infos.get(i);
            TextureRegion region = info.first;
            float width = info.second.x;
            float height = info.second.y;
            Drop drop = new Drop(region, width, height, false);
            drop.setStartDelay(startDelay);
            startDelay += interval;
            drops.add(drop);
        }
        dropsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        dropsBatch.initialize(drops.size());
        dropsBatch.addAll(drops);

        List<AnimatedSprite.FrameInfo> infoList = new ArrayList<>();
        int inSampleSize = atlasInfo.inSampleSize;
        int frameWidth = 320;
        int frameHeight = 291;
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 742 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 1062 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 1382 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 1702 / inSampleSize, 0 / inSampleSize));
        float width = 0.4f;
        float height = 0.3637f;
        TextureRegion angelRegion = buildRegion(atlasInfo, 742, 0, frameWidth, frameHeight);
        fairy = new Fairy(angelRegion, infoList, width, height);
    }

    @Override public void init() {
        super.init();
        rippler = new Rippler();
        rippler.init(openGLLoader);
    }

    @Override public void setup(TextureInfo imageInfo, int width, int height) {
        super.setup(imageInfo, width, height);
        rippler.setup(width, height, R.drawable.f7);
    }

    @Override public void release() {
        super.release();
        rippler.release();
        if (atlasInfo != null) {
            int[] values = new int[1];
            values[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, values, 0);
            GlUtil.checkGlError("Delete texture");
        }
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        glClear(GL_COLOR_BUFFER_BIT);
        if (mode == Mode.CAMERA) {
            drawCameraInput(cameraTexture);
        } else {
            glUseProgram(mainProgramHandle);
            drawImage();
        }
        if (config.isBackgroundEffectsEnabled()) {
            rippler.render();
        } else {
            glUseProgram(mainProgramHandle);
            drawFrame();
        }

        if (config.isObjectsEnabled()) {
            glUseProgram(mainProgramHandle);

            heart.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            heart.step(deltaNanos);

            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            dropsBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Drop drop : drops) {
                drop.step(deltaNanos);
            }
            fairy.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            fairy.step(deltaNanos);
        }
    }

}