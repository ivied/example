package com.tme.moments.presentation.scene.f12.music;

import com.tme.moments.graphics.TextureRegion;

import java.util.List;

/**
 * @author zoopolitic.
 */
public class PinkButterfly extends Butterfly {

    PinkButterfly(TextureRegion region,
                  List<FrameInfo> frameInfoList, float width, float height,
                  boolean rotateSprite) {
        super(region, frameInfoList, width, height, rotateSprite);
    }

    @Override protected void init() {
        super.init();
        setScaleY(1.15f);
        setScaleX(flyLeft ? 1.15f : -1.15f);
    }
}
