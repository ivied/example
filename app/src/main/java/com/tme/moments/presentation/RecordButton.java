package com.tme.moments.presentation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.tme.moments.R;
import com.tme.moments.presentation.core.Config;
import com.tme.moments.presentation.utils.AndroidUtils;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

/**
 * @author zoopolitic.
 */
public class RecordButton extends View {

    public interface RecordListener {
        void onRecordTimedOut();
    }

    private static final String TAG = RecordButton.class.getSimpleName();

    private RectF rectF = new RectF();

    private Paint buttonPaint;
    private Paint circlePaint;
    private Paint progressPaint;

    private boolean recording;
    private long totalTimeNanos;

    private float startAngle;
    private float sweepAngle;
    private float sweepStep;

    private int recordColor;
    private int captureVideoColor;
    private int disabledColor;
    private int captureImageColor;
    private int progressOffset;
    private int progressWidth;
    private int iconPadding;

    private Drawable captureVideoIcon;
    private Drawable captureImageIcon;
    private Drawable stopRecordIcon;

    private UpdateThread.ViewHandler viewHandler;
    private UpdateThread updateThread;
    private RecordListener listener;

    private Config config;

    public RecordButton(Context context) {
        this(context, null);
    }

    public RecordButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecordButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        Resources res = getResources();

        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.RecordButton, defStyleAttr, 0);
        captureVideoColor = array.getColor(R.styleable.RecordButton_captureVideoColor, res.getColor(R.color.colorRecordDark));
        captureImageColor = array.getColor(R.styleable.RecordButton_captureImageColor, res.getColor(R.color.capture_image_button_color_dark));
        disabledColor = array.getColor(R.styleable.RecordButton_disabledColor, res.getColor(R.color.capture_button_disabled_color_dark));
        recordColor = array.getColor(R.styleable.RecordButton_recordBackgroundColor, res.getColor(R.color.white));
        progressOffset = array.getDimensionPixelSize(R.styleable.RecordButton_progressOffset, AndroidUtils.dpToPx(context, 2));
        progressWidth = array.getDimensionPixelSize(R.styleable.RecordButton_progressLineWidth, AndroidUtils.dpToPx(context, 3));
        iconPadding = array.getDimensionPixelSize(R.styleable.RecordButton_iconPadding, 0);
        captureVideoIcon = array.getDrawable(R.styleable.RecordButton_captureVideoIcon);
        captureImageIcon = array.getDrawable(R.styleable.RecordButton_captureImageIcon);
        stopRecordIcon = array.getDrawable(R.styleable.RecordButton_stopRecordIcon);
        buttonPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        buttonPaint.setStyle(Paint.Style.FILL);

        circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        circlePaint.setColor(recordColor);
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setStrokeWidth(progressWidth);

        progressPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        progressPaint.setColor(captureVideoColor);
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setStrokeWidth(progressWidth);

        viewHandler = new UpdateThread.ViewHandler(this);
        reset();
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public void reset() {
        recording = false;
        startAngle = 270;
        sweepAngle = 0;
    }

    public void setRecordListener(RecordListener listener) {
        this.listener = listener;
    }

    private void onRecordTimedOut() {
        if (listener != null) {
            listener.onRecordTimedOut();
        }
    }

    private void onRecordFinished() {
        reset();
        invalidate();
    }

    public void setRecording(boolean recording) {
        if (config.getRecordMode() == RecordMode.CAPTURE_IMAGE) {
            return;
        }
        if (this.recording == recording) {
            return;
        }
        this.recording = recording;
        if (recording) {
            updateThread = new UpdateThread(viewHandler, totalTimeNanos);
            updateThread.start();
        } else if (updateThread != null) {
            updateThread.finish();
        }
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int size = Math.min(getMeasuredWidth(), getMeasuredHeight());
        setMeasuredDimension(size, size);
    }

    @Override protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        int offset = progressWidth / 2;
        int left = offset + getPaddingLeft();
        int top = offset + getPaddingTop();
        int right = w - offset - getPaddingRight();
        int bottom = h - offset - getPaddingBottom();
        rectF.left = left;
        rectF.top = top;
        rectF.right = right;
        rectF.bottom = bottom;

        int iconLeft = left + progressOffset + iconPadding;
        int iconTop = top + progressOffset + iconPadding;
        int iconRight = right - progressOffset - iconPadding;
        int iconBottom = bottom - progressOffset - iconPadding;

        captureVideoIcon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
        captureImageIcon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
        stopRecordIcon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
    }

    @Override protected void onDraw(Canvas canvas) {
        drawButton(canvas);
        drawProgress(canvas);
        drawIcon(canvas);
    }

    private void drawButton(Canvas canvas) {
        float sidePadding = Math.max(getPaddingLeft(), getPaddingRight());
        float topBottomPadding = Math.max(getPaddingTop(), getPaddingBottom());
        float maxPadding = Math.max(sidePadding, topBottomPadding);
        float width = getWidth();
        float cx = width / 2;
        float cy = width / 2;
        float radius = width / 2 - progressWidth - progressOffset - maxPadding;
        int buttonColor;
        if (!isEnabled()) {
            buttonColor = disabledColor;
        } else {
            if (config.getRecordMode() == RecordMode.CAPTURE_VIDEO) {
                buttonColor = recording ? recordColor : captureVideoColor;
            } else {
                buttonColor = captureImageColor;
            }
        }
        buttonPaint.setColor(buttonColor);
        canvas.drawCircle(cx, cy, radius, buttonPaint);
    }

    private void drawProgress(Canvas canvas) {
        canvas.drawArc(rectF, 0, 360, false, circlePaint);
        canvas.drawArc(rectF, startAngle, sweepAngle, false, progressPaint);
    }

    private void drawIcon(Canvas canvas) {
        if (config.getRecordMode() == RecordMode.CAPTURE_VIDEO) {
            if (recording) {
                stopRecordIcon.draw(canvas);
            } else {
                captureVideoIcon.draw(canvas);
            }
        } else {
            captureImageIcon.draw(canvas);
        }
    }

    private void updateProgress(long deltaNanos) {
        float progress = sweepStep * deltaNanos;
        sweepAngle += progress;
        invalidate();
    }

    public void setTotalTime(long time, TimeUnit timeUnit) {
        if (time <= 0) {
            throw new IllegalArgumentException("Total time must be positive value > 0");
        }
        totalTimeNanos = timeUnit.toNanos(time);
        sweepStep = (float) 360 / totalTimeNanos;
    }

    private static class UpdateThread extends Thread {

        static final long SLEEP_INTERVAL_MILLIS = 20;

        private final long totalTimeNanos;
        private long elapsedTimeNanos;
        private long startTimeNanos;
        private long lastTimeNanos;

        private boolean finished;
        private ViewHandler handler;

        UpdateThread(ViewHandler handler, long totalTimeNanos) {
            this.handler = handler;
            this.totalTimeNanos = totalTimeNanos;
        }

        @Override
        public void run() {
            startTimeNanos = System.nanoTime();
            lastTimeNanos = startTimeNanos;
            while (elapsedTimeNanos < totalTimeNanos && !finished) {
                long currTime = System.nanoTime();
                elapsedTimeNanos = currTime - startTimeNanos;
                long delta = currTime - lastTimeNanos;
                lastTimeNanos = currTime;
                handler.sendUpdateProgress(delta);
                try {
                    Thread.sleep(SLEEP_INTERVAL_MILLIS);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    handler.sendRecordFinished();
                    finish();
                }
            }
            if (elapsedTimeNanos >= totalTimeNanos) {
                handler.sendRecordTimedOut();
            }
            handler.sendRecordFinished();
        }

        void finish() {
            // reset elapsed time to not trigger onRecordTimeOut call since termination
            // was triggered to because of timeout
            elapsedTimeNanos = 0;
            finished = true;
        }

        private static class ViewHandler extends Handler {

            private static final int MSG_UPDATE_PROGRESS = 0;
            private static final int MSG_RECORD_FINISHED = 1;
            private static final int MSG_RECORD_TIMED_OUT = 2;

            private WeakReference<RecordButton> weakView;

            ViewHandler(RecordButton weakView) {
                this.weakView = new WeakReference<>(weakView);
            }

            void sendUpdateProgress(long delta) {
                sendMessage(obtainMessage(MSG_UPDATE_PROGRESS, delta));
            }

            void sendRecordTimedOut() {
                sendMessage(obtainMessage(MSG_RECORD_TIMED_OUT));
            }

            void sendRecordFinished() {
                sendMessage(obtainMessage(MSG_RECORD_FINISHED));
            }

            @Override public void handleMessage(Message msg) {
                int what = msg.what;

                RecordButton button = weakView.get();
                if (button == null) {
                    Log.w(TAG, "RenderHandler.handleMessage: weak ref is null");
                    return;
                }

                switch (what) {
                    case MSG_UPDATE_PROGRESS:
                        long delta = (long) msg.obj;
                        button.updateProgress(delta);
                        break;
                    case MSG_RECORD_TIMED_OUT:
                        button.onRecordTimedOut();
                        break;
                    case MSG_RECORD_FINISHED:
                        button.onRecordFinished();
                        break;
                }
            }
        }
    }
}
