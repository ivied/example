package com.tme.moments.presentation.scene.f15.autumn.flavor;

import android.graphics.SurfaceTexture;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class AutumnFlavor extends BaseScene {

    private TextureRegion frameRegion;
    private TextureInfo atlasInfo;
    private DecalBatch batch;
    private List<Leaf> leaves;
    private List<LoopLeaf> loopLeaves;

    @Override protected TextureRegion getFrameRegion() {
        return frameRegion;
    }

    @Override protected float getCameraPositionX() {
        return -0.088f;
    }

    @Override protected float getCameraPositionY() {
        return 0;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.7055f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.9138f;
    }

    @SuppressWarnings("UnnecessaryLocalVariable") @Override
    protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        prepareFrame();

        atlasInfo = openGLLoader.loadTexture(R.drawable.f15_leaves_atlas);

        boolean flyLeft = random();
        leaves = new ArrayList<>();

        TextureRegion leaf1 = buildRegion(atlasInfo, 584, 0, 270, 295);
        TextureRegion leaf2 = buildRegion(atlasInfo, 854, 0, 270, 333);
        TextureRegion leaf3 = buildRegion(atlasInfo, 1124, 0, 270, 291);
        TextureRegion leaf4 = buildRegion(atlasInfo, 458, 0, 74, 122);
        TextureRegion leaf5 = buildRegion(atlasInfo, 532, 0, 52, 173);

        float width = 0.3375f;
        float height = 0.3687f;
        leaves.addAll(createNewLeaves(leaf1, width, height, 27, flyLeft));

        width = 0.3375f;
        height = 0.4162f;
        leaves.addAll(createNewLeaves(leaf2, width, height, 26, flyLeft));

        width = 0.3375f;
        height = 0.3637f;
        leaves.addAll(createNewLeaves(leaf3, width, height, 7, flyLeft));

        loopLeaves = new ArrayList<>();
        loopLeaves.add(new LoopLeaf(leaf5, 0.065f, 0.2162f));
        loopLeaves.add(new LoopLeaf(leaf4, 0.0925f, 0.1525f));

        batch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        batch.initialize(leaves.size() + loopLeaves.size());
        batch.addAll(leaves);
        batch.addAll(loopLeaves);
    }

    private void prepareFrame() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f15);
        int textureHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        frameRegion = new TextureRegion(textureHandle, w, h, 0, 0, w, h);
    }

    private List<Leaf> createNewLeaves(TextureRegion region, float width, float height,
                                       int count, boolean flyLeft) {
        List<Leaf> leaves = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Leaf leaf = new Leaf(region, width, height, flyLeft);
            leaf.setScale(0.81f); // decrease size
            leaves.add(leaf);
        }
        return leaves;
    }

    @Override public void release() {
        super.release();
        if (atlasInfo != null) {
            int[] arr = new int[1];
            arr[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, arr, 0);
            GlUtil.checkGlError("Release " + getClass().getSimpleName());
        }
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        glClear(GL_COLOR_BUFFER_BIT);
        if (mode == Mode.CAMERA) {
            drawCameraInput(cameraTexture);
        }
        glUseProgram(mainProgramHandle);
        if (mode == Mode.IMAGE) {
            drawImage();
        }
        drawFrame();

        if (config.isObjectsEnabled()) {
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            glUniformMatrix4fv(uProjectionMatrixLoc, 1, false, projectionMatrix, 0);
            batch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Leaf leaf : leaves) leaf.step(deltaNanos);
            for (LoopLeaf leaf : loopLeaves) leaf.step(deltaNanos);
        }
    }
}