package com.tme.moments.presentation.scene;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class BlinkingGlow extends Glow {

    private long visibleTimeNanos;
    private long invisibleTimeNanos;
    private float alphaSpeed;
    private boolean visible;
    private int minVisibleInterval;
    private int maxVisibleInterval;
    private int minInvisibleInterval;
    private int maxInvisibleInterval;

    public BlinkingGlow(TextureRegion region, float width, float height, int minVisibleInterval,
                        int maxVisibleInterval, int minInvisibleInterval,
                        int maxInvisibleInterval, float minAlphaSpeed, float maxAlphaSpeed) {
        super(region, width, height);
        this.minVisibleInterval = minVisibleInterval;
        this.maxVisibleInterval = maxVisibleInterval;
        this.minInvisibleInterval = minInvisibleInterval;
        this.maxInvisibleInterval = maxInvisibleInterval;
        alphaSpeed = random(minAlphaSpeed, maxAlphaSpeed) / Const.NANOS_IN_SECOND;
    }

    @Override public void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float deltaT = speed * deltaNanos;
        t += deltaT;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        setX(x);
        setY(y);

        if (visible) {
            if (visibleTimeNanos > 0) {
                visibleTimeNanos -= deltaNanos;
            } else {
                float alpha = color.a;
                if (alpha > 0) {
                    float deltaAlpha = alphaSpeed * deltaNanos;
                    float newAlpha = Math.max(0, alpha - deltaAlpha);
                    setAlpha(newAlpha);
                } else {
                    visible = false;
                    invisibleTimeNanos = TimeUnit.MILLISECONDS.toNanos(random(minVisibleInterval, maxVisibleInterval));
                }
            }
        } else {
            if (invisibleTimeNanos > 0) {
                invisibleTimeNanos -= deltaNanos;
            } else {
                float alpha = color.a;
                if (alpha < 1) {
                    float deltaAlpha = alphaSpeed * deltaNanos;
                    float newAlpha = Math.min(1, alpha + deltaAlpha);
                    setAlpha(newAlpha);
                } else {
                    visible = true;
                    visibleTimeNanos = TimeUnit.MILLISECONDS.toNanos(random(minInvisibleInterval, maxInvisibleInterval));
                }
            }
        }

        if (t >= 1) { // reset
            init();
        }
    }
}
