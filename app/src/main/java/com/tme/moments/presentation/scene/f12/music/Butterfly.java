package com.tme.moments.presentation.scene.f12.music;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.math.MathUtils;
import com.tme.moments.presentation.Const;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Butterfly extends AnimatedSprite {

    private float speed;
    private float startDelayNanos;

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float t;
    private boolean stopped;
    private float endZ;
    private float startZ;
    private final boolean rotateSprite;
    protected boolean flyLeft;

    Butterfly(TextureRegion region, List<FrameInfo> frameInfoList, float width, float height,
              boolean rotateSprite) {
        super(region, frameInfoList, width, height);
        this.rotateSprite = rotateSprite;
        init();
    }

    void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    boolean isStopped() {
        return stopped;
    }

    void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

    @Override protected int getDesiredFPS() {
        return 25;
    }

    @Override
    public void draw(int uProjectionMatrixLoc, float[] projectionMatrix, int aPositionHandle,
                     int aTexCoordHandle, int aColorHandle) {
        if (stopped) {
            return;
        }
        super.draw(uProjectionMatrixLoc, projectionMatrix, aPositionHandle, aTexCoordHandle, aColorHandle);
    }

    @Override public void step(long deltaNanos) {
        if (stopped) {
            return;
        }
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float delta = speed * deltaNanos;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;
        float z = startZ + (endZ - startZ) * t;

        if (rotateSprite) {
            float t1 = t + 0.3f;
            float x1 = (1 - t1) * (1 - t1) * p0x + 2 * (1 - t1) * t1 * p1x + t1 * t1 * p2x;
            float y1 = (1 - t1) * (1 - t1) * p0y + 2 * (1 - t1) * t1 * p1y + t1 * t1 * p2y;
            float angle = MathUtils.angleBetweenLines(x, y, x, y1, x, y, x1, y1);
            if (angle != 0) {
                setRotationZ(y1 >= y ? -angle : -angle + 180);
            }
        }

        setPosition(x, y, z);

        t += delta;

        float fadeOutThreshold = 0.8f;
        if (t >= fadeOutThreshold) {
            float fraction = (t - fadeOutThreshold) / (1 - fadeOutThreshold);
            setAlpha(1 - fraction);
        }

        if (t >= 1) {
            init();
        }
        super.step(deltaNanos);
    }

    protected void init() {
        t = 0;
        stopped = true;
        startZ = -1;
        endZ = -3f;
        flyLeft = random();
        p0x = flyLeft ? random(0.0f, 1.0f) : random(-1.0f, 0.0f);
        p0y = -1 - getHeight();
        p1x = flyLeft ? p0x - random(0.25f, 0.5f) : p0x + random(0.25f, 0.5f);
        p1y = p0y + random(-1.2f, -0.5f);

        p2x = flyLeft ? random(endZ, p1x - 0.5f) : random(p1x + 0.5f, -endZ);
        p2y = 0.5f;

        speed = 0.2f / Const.NANOS_IN_SECOND;
        setPosition(p0x, p0y, startZ);
        setAlpha(1);
        setScaleX(flyLeft ? 1 : -1);
        setStartDelay(random(2000, 4000));
    }

}
