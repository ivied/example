package com.tme.moments.presentation.scene.f8.sunflower.field;

import android.graphics.SurfaceTexture;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.opengl.GLES20.GL_ALWAYS;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_EQUAL;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_KEEP;
import static android.opengl.GLES20.GL_REPLACE;
import static android.opengl.GLES20.GL_STENCIL_BUFFER_BIT;
import static android.opengl.GLES20.GL_STENCIL_TEST;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TRIANGLE_STRIP;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glColorMask;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glStencilFunc;
import static android.opengl.GLES20.glStencilOp;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class WelcomeToFlowerField extends BaseScene {

    private static final int MIN_BIRDS = 3;
    private static final int MAX_BIRDS = 6;

    private final Object lock = new Object();

    private Sprite stencilMask;
    private Sprite overlay;

    private int stencilProgramHandle = -1;
    private int uStencilProjectionMatrixHandle = -1;
    private int aStencilPositionHandle = -1;
    private int aStencilColorHandle = -1;
    private int aStencilTexCoordHandle = -1;

    private DecalBatch beforeBannerBatch;
    private DecalBatch behindBannerBatch;
    private DecalBatch sunflowerBatch;
    private TextureInfo atlasInfo;

    private List<Sunflower> sunflowers;
    private List<Bird> beforeBannerBirds;
    private List<Bird> behindBannerBirds;
    private List<Cloud> clouds;
    private AirBalloon airBalloon;
    private PinkButterfly pinkButterfly;
    private OrangeButterfly orangeButterfly;
    private TextureRegion backgroundRegion;
    private boolean flyLeft = true;
    private long birdsWaitTimeNanos = 0;
    private boolean pinkButterflyActive;

    @Override protected TextureRegion getFrameRegion() {
        return backgroundRegion;
    }

    @Override protected float getCameraPositionX() {
        return -0.01296f;
    }

    @Override protected float getCameraPositionY() {
        return 0.3314f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.8258f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.44f;
    }

    @Override public void init() {
        super.init();
        if (stencilProgramHandle == -1) {
            prepareStencilProgram();
        }
    }

    private void prepareStencilProgram() {
        stencilProgramHandle =
                openGLLoader.createProgram(R.raw.vertex_shader, R.raw.stencil_fragment_shader);

        aStencilPositionHandle = glGetAttribLocation(stencilProgramHandle, "a_position");
        GlUtil.checkLocation(aStencilPositionHandle, "a_position");
        glEnableVertexAttribArray(aStencilPositionHandle);

        aStencilColorHandle = glGetAttribLocation(stencilProgramHandle, "a_color");
        GlUtil.checkLocation(aStencilColorHandle, "a_color");
        glEnableVertexAttribArray(aStencilColorHandle);

        aStencilTexCoordHandle = glGetAttribLocation(stencilProgramHandle, "a_texCoord");
        GlUtil.checkLocation(aStencilTexCoordHandle, "a_texCoord");
        glEnableVertexAttribArray(aStencilTexCoordHandle);

        uStencilProjectionMatrixHandle = glGetUniformLocation(stencilProgramHandle, "u_projTrans");
        GlUtil.checkLocation(uStencilProjectionMatrixHandle, "u_projTrans");
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        prepareBackground();
        prepareStencilMask();
        prepareOverlay();

        atlasInfo = openGLLoader.loadTexture(R.drawable.f8_atlas);

        prepareSunflowers(atlasInfo);
        sunflowerBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        sunflowerBatch.initialize(sunflowers.size());
        sunflowerBatch.addAll(sunflowers);

        beforeBannerBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        behindBannerBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        int beforeCount = 0;
        int behindCount = 0;
        prepareAirBalloon(atlasInfo);
        behindBannerBatch.add(airBalloon);
        behindCount++;
        prepareClouds(atlasInfo, behindBannerBatch);
        behindCount += 3;// 3 clouds
        beforeBannerBirds = new ArrayList<>();
        behindBannerBirds = new ArrayList<>();
        // initial birds delay
        birdsWaitTimeNanos = TimeUnit.MILLISECONDS.toNanos(2000);
        int birdsCount = prepareBirds(atlasInfo, beforeBannerBatch, behindBannerBatch);
        behindCount += birdsCount;
        beforeCount += birdsCount;
        preparePinkButterfly(atlasInfo);
        beforeCount++;
        prepareOrangeButterfly(atlasInfo);
        beforeCount++;
        beforeBannerBatch.initialize(beforeCount);
        behindBannerBatch.initialize(behindCount);
        pinkButterflyActive = true;
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        boolean objectsEnabled = config.isObjectsEnabled();
        glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glUseProgram(mainProgramHandle);
        drawFrame();
        glEnable(GL_STENCIL_TEST);

        glColorMask(false, false, false, false);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

        // draw mask
        glUseProgram(stencilProgramHandle);
        stencilMask.draw(uStencilProjectionMatrixHandle, projectionMatrix, aStencilPositionHandle,
                aStencilTexCoordHandle, aStencilColorHandle);

        glColorMask(true, true, true, true);
        glStencilFunc(GL_EQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

        // draw clouds
        boolean allFinished = true;
        if (objectsEnabled) {
            glDisable(GL_STENCIL_TEST);
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            behindBannerBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Cloud cloud : clouds) {
                cloud.step(deltaNanos);
            }
            if (birdsWaitTimeNanos > 0) {
                birdsWaitTimeNanos -= deltaNanos;
            } else {
                for (Bird bird : behindBannerBirds) {
                    bird.step(deltaNanos);
                    if (!bird.isFinished()) {
                        allFinished = false;
                    }
                }
            }
            glEnable(GL_STENCIL_TEST);
        }

        if (mode == Mode.IMAGE) {
            glUseProgram(mainProgramHandle);
            drawImage();
        } else {
            drawCameraInput(cameraTexture);
        }

        glDisable(GL_STENCIL_TEST);
        glUseProgram(mainProgramHandle);
        overlay.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);

        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        sunflowerBatch.render(uProjectionMatrixLoc, projectionMatrix);
        if (objectsEnabled) {
            for (Sunflower sunflower : sunflowers) {
                sunflower.step(deltaNanos);
            }
        }
        if (objectsEnabled) {
            airBalloon.step(deltaNanos);
            beforeBannerBatch.render(uProjectionMatrixLoc, projectionMatrix);
            if (birdsWaitTimeNanos <= 0) {
                for (Bird bird : beforeBannerBirds) {
                    bird.step(deltaNanos);
                    if (!bird.isFinished()) {
                        allFinished = false;
                    }
                }
                if (allFinished) {
                    birdsWaitTimeNanos = TimeUnit.MILLISECONDS.toNanos(random(5_000, 8_000));
                    flyLeft = random();
                    prepareBirds(atlasInfo, beforeBannerBatch, behindBannerBatch);
                }
            }

            if (orangeButterfly.isStopped() && pinkButterfly.isStopped()) {
                synchronized (lock) {
                    if (pinkButterflyActive) {
                        pinkButterfly.setStopped(false);
                    } else {
                        orangeButterfly.setStopped(false);
                    }
                    pinkButterflyActive = random();
                }
            }
            orangeButterfly.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            orangeButterfly.step(deltaNanos);

            pinkButterfly.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation, aTexCoordLocation, aColorLocation);
            pinkButterfly.step(deltaNanos);
        }
    }

    @Override public void release() {
        super.release();
        glDeleteProgram(stencilProgramHandle);
        stencilProgramHandle = -1;
        if (atlasInfo != null) {
            int[] values = new int[1];
            values[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, values, 0);
            GlUtil.checkGlError("Release " + getClass().getSimpleName());
        }
    }

    private void prepareBackground() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f8_background);
        int texHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        backgroundRegion = new TextureRegion(texHandle, w, h, 0, 0, w, h);
    }

    private void prepareStencilMask() {
        TextureInfo stencilInfo = openGLLoader.loadTexture(R.drawable.f8_stencil);
        int w = stencilInfo.imageWidth;
        int h = stencilInfo.imageHeight;
        TextureRegion region = new TextureRegion(stencilInfo.textureHandle, w, h, 0, 0, w, h);
        float width = getDesiredCameraWidthFraction() * getProjectionWidth();
        float height = getDesiredCameraHeightFraction() * getProjectionHeight();
        stencilMask = new Sprite(region, width, height);
        stencilMask.setPosition(0.0f, 0.32f, -1);
    }

    private void prepareOverlay() {
        TextureInfo overlayInfo = openGLLoader.loadTexture(R.drawable.f8_overlay);
        int w = overlayInfo.imageWidth;
        int h = overlayInfo.imageHeight;
        TextureRegion region = new TextureRegion(overlayInfo.textureHandle, w, h, 0, 0, w, h);
        float width = getProjectionWidth();
        float height = getProjectionHeight();
        overlay = new Sprite(region, width, height);
        overlay.setPosition(0.0f, 0.0f, -1);
    }

    private void prepareClouds(TextureInfo info, DecalBatch behindBannerBatch) {
        clouds = new ArrayList<>(3);

        TextureRegion region = buildRegion(info, 370, 73, 267, 86);
        float width = 0.3337f;
        float height = 0.1075f;
        float speed = -0.030f;
        float x = 0.0f;
        float startY = -0.27f;
        float endY = -0.15f;
        Cloud c1 = new Cloud(region, width, height, x, startY, endY, speed);
        clouds.add(c1);
        behindBannerBatch.add(c1);

        region = buildRegion(info, 0, 0, 148, 53);
        width = 0.185f;
        height = 0.066f;
        speed = -0.035f;
        x = 1 + width / 2;
        startY = -0.28f;
        endY = 0.05f;
        Cloud c2 = new Cloud(region, width, height, x, startY, endY, speed);
        c2.setStartDelay(4000);
        clouds.add(c2);
        behindBannerBatch.add(c2);

        region = buildRegion(info, 0, 196, 444, 151);
        width = 0.555f;
        height = 0.1887f;
        speed = -0.046f;
        x = 1 + width / 2;
        startY = 0.45f;
        endY = 1.22f;
        Cloud c3 = new Cloud(region, width, height, x, startY, endY, speed);
        c3.setStartDelay(10_000);
        clouds.add(c3);
        behindBannerBatch.add(c3);
    }

    private void prepareAirBalloon(TextureInfo info) {
        TextureRegion region = buildRegion(info, 733, 73, 85, 123);
        float width = 0.1062f;
        float height = 0.1537f;
        airBalloon = new AirBalloon(region, width, height);
    }

    private int prepareBirds(TextureInfo info, DecalBatch beforeBannerBatch,
                             DecalBatch behindBannerBatch) {
        int inSampleSize = info.inSampleSize;

        float startX = flyLeft ? 1.2f : -1.2f;
        float startY = random(-0.25f, 0.8f);

        int count = random(MIN_BIRDS, MAX_BIRDS);

        List<AnimatedSprite.FrameInfo> infoList = new ArrayList<>();
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 740 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 148 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 666 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 296 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 222 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 370 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 592 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 444 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 518 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 814 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 888 / inSampleSize, 0 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 0 / inSampleSize, 73 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 296 / inSampleSize, 73 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 222 / inSampleSize, 73 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 148 / inSampleSize, 73 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 74 / inSampleSize, 73 / inSampleSize));
        beforeBannerBatch.removeAll(beforeBannerBirds);
        behindBannerBatch.removeAll(behindBannerBirds);
        beforeBannerBirds.clear();
        behindBannerBirds.clear();
        float minFlockIntervalX = 0.18f;
        float maxFlockIntervalX = 0.27f;
        float minFlockIntervalY = -0.12f;
        float maxFlockIntervalY = 0.12f;
        for (int i = 0; i < count; i++) {
            TextureRegion region = buildRegion(info, 296, 73, 74, 73);
            float width = 0.0925f;
            float height = 0.0912f;
            float speed = 0.3f;
            float x = startX;
            float intervalX = random(minFlockIntervalX, maxFlockIntervalX);
            float intervalY = random(minFlockIntervalY, maxFlockIntervalY);
            float y = startY + intervalY;
            if (flyLeft) {
                speed = -speed;
                startX += intervalX;
            } else {
                startX -= intervalX;
            }
            float inclineSpeed = random(0.05f, 0.1f);
            float inclineY = random(0.035f, 0.05f);
            float hangTimeInSeconds = random(0.5f, 2.0f);
            Bird bird = new Bird(region, infoList, width, height, x, y, speed, inclineY, inclineSpeed, hangTimeInSeconds);
            if (!flyLeft) {
                bird.setScaleX(-1); // flip texture
            }
            boolean beforeBanner = random();
            if (beforeBanner) {
                beforeBannerBirds.add(bird);
                beforeBannerBatch.add(bird);
            } else {
                float scale = 0.9f;
                // if bird is behind banner - make it little bit smaller
                bird.setScaleX(flyLeft ? scale : -scale);
                bird.setScaleY(scale);
                behindBannerBirds.add(bird);
                behindBannerBatch.add(bird);
            }
        }
        return MAX_BIRDS;
    }

    private void prepareSunflowers(TextureInfo info) {
        sunflowers = new ArrayList<>();
        TextureRegion region = buildRegion(info, 637, 73, 96, 93);
        float width = 0.12f;
        float height = 0.1162f;
        float x = -0.15f;
        float y = -0.78f;
        float moveSpeedPerSecond = 0.005f;
        float rotationSpeedDegreesPerSecond = -15;
        float maxIncline = 0.005f;
        Sunflower s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        sunflowers.add(s);

        x = -0.57f;
        y = -0.52f;
        moveSpeedPerSecond = -0.007f;
        rotationSpeedDegreesPerSecond = 12;
        s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        sunflowers.add(s);

        x = 0.83f;
        y = -0.55f;
        moveSpeedPerSecond = 0.015f;
        rotationSpeedDegreesPerSecond = -8;
        s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        sunflowers.add(s);

        x = -1.02f;
        y = -0.6f;
        moveSpeedPerSecond = 0.012f;
        rotationSpeedDegreesPerSecond = -8;
        s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        sunflowers.add(s);

        x = -0.68f;
        y = -0.75f;
        moveSpeedPerSecond = -0.015f;
        rotationSpeedDegreesPerSecond = 10;
        s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        sunflowers.add(s);

        x = -0.55f;
        y = -1.01f;
        moveSpeedPerSecond = 0.012f;
        rotationSpeedDegreesPerSecond = -10;
        s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        s.setScale(1.5f);
        sunflowers.add(s);

        x = -0.4f;
        y = -0.75f;
        moveSpeedPerSecond = 0.016f;
        rotationSpeedDegreesPerSecond = -12;
        s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        s.setScale(1.25f);
        sunflowers.add(s);

        x = 0.07f;
        y = -0.6f;
        moveSpeedPerSecond = -0.015f;
        rotationSpeedDegreesPerSecond = 9;
        s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        sunflowers.add(s);

        x = 0.23f;
        y = -0.82f;
        moveSpeedPerSecond = -0.016f;
        rotationSpeedDegreesPerSecond = 12;
        s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        s.setScale(1.25f);
        sunflowers.add(s);

        x = 0.5f;
        y = -0.68f;
        moveSpeedPerSecond = -0.014f;
        rotationSpeedDegreesPerSecond = 15;
        s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        s.setScale(1.15f);
        sunflowers.add(s);

        x = 0.75f;
        y = -1.0f;
        moveSpeedPerSecond = 0.013f;
        rotationSpeedDegreesPerSecond = -15;
        s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        s.setScale(1.5f);
        sunflowers.add(s);

        x = 1.05f;
        y = -0.75f;
        moveSpeedPerSecond = -0.014f;
        rotationSpeedDegreesPerSecond = 15;
        s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        s.setScale(1.15f);
        sunflowers.add(s);

        x = -0.15f;
        y = -0.9f;
        moveSpeedPerSecond = -0.012f;
        rotationSpeedDegreesPerSecond = 15;
        s = new Sunflower(region, width, height, x, y, moveSpeedPerSecond, rotationSpeedDegreesPerSecond, maxIncline);
        sunflowers.add(s);
    }

    private void preparePinkButterfly(TextureInfo info) {
        int inSampleSize = info.inSampleSize;
        List<AnimatedSprite.FrameInfo> infoList = new ArrayList<>();
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 178 / inSampleSize, 362 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 622 / inSampleSize, 196 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 444 / inSampleSize, 196 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 0 / inSampleSize, 362 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 356 / inSampleSize, 362 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 0 / inSampleSize, 528 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 712 / inSampleSize, 362 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 178 / inSampleSize, 528 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 534 / inSampleSize, 362 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 800 / inSampleSize, 196 / inSampleSize));
        TextureRegion region = buildRegion(info, infoList.get(0).regionX, infoList.get(0).regionY, 178, 166);
        float width = 0.2225f;
        float height = 0.2075f;
        float speedPerSecond = 0.2f;
        pinkButterfly = new PinkButterfly(region, infoList, width, height, speedPerSecond);
    }

    private void prepareOrangeButterfly(TextureInfo info) {
        int inSampleSize = info.inSampleSize;
        List<AnimatedSprite.FrameInfo> infoList = new ArrayList<>();
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 250 / inSampleSize, 1022 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 606 / inSampleSize, 528 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 356 / inSampleSize, 528 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 250 / inSampleSize, 775 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 500 / inSampleSize, 775 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 0 / inSampleSize, 775 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 750 / inSampleSize, 775 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 0 / inSampleSize, 1022 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 500 / inSampleSize, 1022 / inSampleSize));
        infoList.add(new AnimatedSprite.FrameInfo(infoList.size(), 750 / inSampleSize, 1022 / inSampleSize));
        TextureRegion region = buildRegion(info, infoList.get(0).regionX, infoList.get(0).regionY, 250, 247);
        float width = 0.2225f;  // make it same size as pink
        float height = 0.2075f; // make it same size as pink
        orangeButterfly = new OrangeButterfly(region, infoList, width, height);
    }
}