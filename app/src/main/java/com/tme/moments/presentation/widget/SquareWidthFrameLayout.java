package com.tme.moments.presentation.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * @author zoopolitic.
 */
public class SquareWidthFrameLayout extends FrameLayout {

    public SquareWidthFrameLayout(@NonNull Context context) {
        super(context);
    }

    public SquareWidthFrameLayout(@NonNull Context context,
                                  @Nullable
                                          AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareWidthFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs,
                                  int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int height = getMeasuredHeight();
        //noinspection SuspiciousNameCombination
        setMeasuredDimension(height, height);
    }
}
