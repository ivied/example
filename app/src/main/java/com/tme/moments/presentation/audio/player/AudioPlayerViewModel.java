package com.tme.moments.presentation.audio.player;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * @author zoopolitic.
 */
public class AudioPlayerViewModel extends ViewModel {

    private MutableLiveData<String> name;
    private MutableLiveData<Long> totalDurationMillis;
    private MutableLiveData<Long> playedInfo;
    private MutableLiveData<Boolean> playing;
    private MutableLiveData<Boolean> active;

    public MutableLiveData<String> getName() {
        if (name == null) {
            name = new MutableLiveData<>();
        }
        return name;
    }

    public MutableLiveData<Long> getTotalDurationMillis() {
        if (totalDurationMillis == null) {
            totalDurationMillis = new MutableLiveData<>();
        }
        return totalDurationMillis;
    }

    public MutableLiveData<Long> getPlayedDuration() {
        if (playedInfo == null) {
            playedInfo = new MutableLiveData<>();
        }
        return playedInfo;
    }

    public MutableLiveData<Boolean> getPlaying() {
        if (playing == null) {
            playing = new MutableLiveData<>();
        }
        return playing;
    }

    public MutableLiveData<Boolean> getActive() {
        if (active == null) {
            active = new MutableLiveData<>();
        }
        return active;
    }
}
