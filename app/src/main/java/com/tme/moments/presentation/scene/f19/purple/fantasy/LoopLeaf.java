package com.tme.moments.presentation.scene.f19.purple.fantasy;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class LoopLeaf extends Decal {

    protected static final float INITIAL_SCALE = 0.7f;      // % of the size
    protected static final float DEFAULT_MAX_SCALE = 1.0f;  // % of the size

    protected float speed;
    protected float rotationSpeed;

    protected float p0x, p0y; // start point
    protected float p1x, p1y; // control point (bezier)
    protected float p2x, p2y; // end point
    protected float p3x, p3y;

    protected float startDelayNanos;
    protected float t;
    protected float maxScale = DEFAULT_MAX_SCALE;
    protected boolean inflated;

    LoopLeaf(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        init();
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float delta = speed * deltaNanos;
        t += delta;

        float single = (1 - t);
        float quadratic = (1 - t) * (1 - t);
        float cube = (1 - t) * (1 - t) * (1 - t);
        float x = cube * p0x + 3 * t * quadratic * p1x + 3 * t * t * single * p2x + t * t * t * p3x;
        float y = cube * p0y + 3 * t * quadratic * p1y + 3 * t * t * single * p2y + t * t * t * p3y;

        float deltaRotation = rotationSpeed * deltaNanos;
        rotateX(deltaRotation);
        rotateY(deltaRotation);
        rotateZ(deltaRotation);

        setX(x);
        setY(y);
        if (t >= 1) {
            init();
        }

        if (!inflated) {
            float scale = getScaleX();
            float inflateSpeed = 0.2f / Const.NANOS_IN_SECOND;
            float deltaScale = inflateSpeed * deltaNanos;
            scale += deltaScale;
            scale = Math.min(maxScale, scale);
            setScale(scale);
            if (scale == maxScale) {
                inflated = true;
            }
        } else {
            float scale = getScaleX();
            float deflateSpeed = speed;
            float deltaScale = deflateSpeed * deltaNanos;
            scale -= deltaScale;
            scale = Math.max(INITIAL_SCALE, scale);
            setScale(scale);
        }
    }

    protected void init() {
        // reduce maxZ by half of biggest side of the object
        // to not being clipped during X and Y rotation
        float halfWidth = getWidth() / 2;
        float halfHeight = getHeight() / 2;
        float z = -1.0f - Math.max(halfWidth, halfHeight);
        float deltaZ = -1.0f - z;
        final boolean leftLoop = random();
        if (leftLoop) {
            p0x = random(0.5f, 1.5f);
            p0y = random(1.0f + halfHeight + deltaZ, 1.25f + halfHeight + deltaZ);
            p1x = random(-1.5f, -0.5f);
            p1y = random(-1.5f, -0.5f);
            p2x = random(-1, 0);
            p2y = random(2, 2.75f);
            p3x = random(0.5f, 1.5f);
            p3y = random(-1 - getHeight() / 2, -1 - getHeight() / 2);
        } else {
            p0x = random(-1.5f, -0.5f);
            p0y = random(1.0f + halfHeight + deltaZ, 1.25f + halfHeight + deltaZ);
            p1x = random(0.5f, 1.5f);
            p1y = random(-1.5f, -0.5f);
            p2x = random(0, 1);
            p2y = random(2, 2.75f);
            p3x = random(-1.5f, -0.5f);
            p3y = random(-1 - getHeight() / 2, -1 - getHeight() / 2);
        }

        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(3000, 8000));
        speed = random(0.15f, 0.22f) / Const.NANOS_IN_SECOND;
        rotationSpeed = random(-150f, 150f) / Const.NANOS_IN_SECOND;
        setRotationZ(0);
        setColor(1, 1, 1, 1);
        setPosition(p0x, p0y, z);
        setScale(INITIAL_SCALE);
        setAlpha(1);
        inflated = false;
        t = 0;
    }
}
