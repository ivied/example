package com.tme.moments.presentation.scene.f16.christmas;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Cap extends AnimatedSprite {

    private long pauseTime;
    private int maxIndex;
    private boolean reverse;

    Cap(TextureRegion region,
        List<FrameInfo> frames, float width, float height) {
        super(region, frames, width, height);
        maxIndex = random(3, frames.size());
    }

    @Override protected int getDesiredFPS() {
        return 15;
    }

    public void step(long deltaNanos) {
        if (pauseTime > 0) {
            pauseTime -= deltaNanos;
            return;
        }
        // last frame of animation was drawn - reset
        if (currentFrameIndex == maxIndex && !reverse) {
            changeRegion(currentFrameIndex);
            updateTimeCounterNanos -= deltaNanos;
            if (updateTimeCounterNanos <= 0) {
                reverse = true;
            }
        } else {
            updateTimeCounterNanos -= deltaNanos;
            if (updateTimeCounterNanos <= 0) {
                updateTimeCounterNanos = updateIntervalNanos;
                changeRegion(reverse ? currentFrameIndex-- : currentFrameIndex++);
                if (currentFrameIndex == 0) {
                    reverse = false;
                    pauseTime = TimeUnit.MILLISECONDS.toNanos(maxIndex < 7 ? random(50) : random(0, 2500));
                    maxIndex = random(3, frames.size());
                }
            }
        }
        update();
    }
}
