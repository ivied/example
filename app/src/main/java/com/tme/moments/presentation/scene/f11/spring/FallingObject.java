package com.tme.moments.presentation.scene.f11.spring;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class FallingObject extends Decal {

    private final boolean flyLeft;
    private float speedY;
    private float speedX;
    private float rotationSpeed;
    private float minY;
    private float minX;
    private float maxX;
    private boolean finished;
    private float minRotationSpeed;
    private float maxRotationSpeed;

    FallingObject(TextureRegion region, float width, float height, float x, float y,
                  boolean flyLeft, float minRotationSpeed, float maxRotationSpeed) {
        this.flyLeft = flyLeft;
        this.minRotationSpeed = minRotationSpeed;
        this.maxRotationSpeed = maxRotationSpeed;
        setTextureRegion(region);
        setDimensions(width, height);
        init(x, y);
    }

    public boolean isFinished() {
        return finished;
    }

    void step(long deltaNanos) {
        float deltaX = speedX * deltaNanos;
        float deltaY = speedY * deltaNanos;
        translate(deltaX, deltaY, 0);

        float deltaRotation = rotationSpeed * deltaNanos;
        rotateZ(deltaRotation);

        float x = getX();
        float y = getY();
        if (y <= minY || x <= minX || x >= maxX) {
            finished = true;
        }
    }

    private void init(float x, float y) {
        speedY = -random(0.2f, 0.55f) / Const.NANOS_IN_SECOND;
        speedX = flyLeft
                ? random(-0.6f, 0.0f) / Const.NANOS_IN_SECOND
                : random(0.0f, 0.6f) / Const.NANOS_IN_SECOND;
        setColor(1, 1, 1, 1);
        minY = -1 - getHeight() / 2;
        minX = -1 - getWidth() / 2;
        maxX = 1 + getWidth() / 2;
        rotationSpeed = random(minRotationSpeed, maxRotationSpeed) / Const.NANOS_IN_SECOND; // degrees per second
        float z = -1.0f;
        setPosition(x, y, z);
    }
}