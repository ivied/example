package com.tme.moments.presentation.scene;

import android.graphics.SurfaceTexture;

import com.tme.moments.presentation.TextureInfo;

/**
 * @author zoopolitic.
 */
public interface Scene {

    /**
     * Initialize scene, should be called from surfaceCreated callback
     * <p>
     * <b><i>Should be called from thread that owns EGL context</i></b>
     */
    void init();

    /**
     * Setup scene and initialize drawing objects, should be called from surfaceChanged callback
     * <p>
     * <b><i>Should be called from thread that owns EGL context</i></b>
     *
     * @param imageInfo image texture info
     * @param width     width of the surface
     * @param height    height of the surface
     */
    void setup(TextureInfo imageInfo, int width, int height);

    /**
     * Called when camera params changed and scene should be updated
     */
    void updateCameraParams();

    /**
     * Releases resources associated with scene
     * <p>
     * Can be called without {@link Scene#setup(TextureInfo, int, int)} method invocation
     * <p>
     * <b><i>Should be called from thread that owns EGL context</i></b>
     */
    void release();

    /**
     * Render scene
     * <p>
     * <b><i>Should be called from thread that owns EGL context</i></b>
     *
     * @param deltaNanos    time passed from the previous render call
     * @param cameraTexture surfaceTexture of the camera
     */
    void render(long deltaNanos, SurfaceTexture cameraTexture);

    void drawCameraInput(SurfaceTexture cameraTexture);

    /**
     * Indicates that {@link com.tme.moments.presentation.scene.BaseScene.Mode} has been
     * changed
     */
    void onCameraModeEnabled();

    void onImageModeEnabled(TextureInfo imageInfo);

    void angleChanged(float deltaAngle);

    void zoomChanged(float zoom);

    void onMoved(float dx, float dy);

    void onObjectsStateChanged(boolean enabled);

    void onBackgroundEffectsStateChanged(boolean enabled);

    /**
     * Calculate and return X coordinate of the middle of image in pixels
     *
     * @return X coordinate of the middle of image in pixels
     */
    float getImageX();

    /**
     * Calculate and return Y coordinate of the middle of image in pixels
     *
     * @return Y coordinate of the middle of image in pixels
     */
    float getImageY();

    /**
     * Returns whether scene supports any background effects or not
     *
     * @return true if scene supports any background effects or not
     */
    boolean hasBackgroundEffects();
}
