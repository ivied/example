package com.tme.moments.presentation.audio.list;

import android.content.ContentUris;
import android.databinding.BaseObservable;
import android.net.Uri;

import com.tme.moments.R;
import com.tme.moments.presentation.recyclerview.ListItem;

public class AudioListItem extends BaseObservable implements ListItem {

    public final String path;
    public final String name;
    public final Uri thumbnail;


    public AudioListItem(String path, String name, Uri thumbnail) {
        this.path = path;
        this.name = name;
        this.thumbnail = thumbnail;
    }

    @Override public int getLayoutResId() {
        return R.layout.audio_list_item;
    }

}

