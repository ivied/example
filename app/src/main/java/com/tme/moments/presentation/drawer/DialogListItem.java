package com.tme.moments.presentation.drawer;

import android.content.Context;

/**
 * @author zoopolitic.
 */
public abstract class DialogListItem {

    private boolean isChecked;

    public abstract String getTitle(Context context);

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
