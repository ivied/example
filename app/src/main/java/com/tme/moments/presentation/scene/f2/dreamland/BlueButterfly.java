package com.tme.moments.presentation.scene.f2.dreamland;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.Const;

import java.util.List;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class BlueButterfly extends Butterfly {

    BlueButterfly(TextureRegion region,
                  List<FrameInfo> frameInfoList, float width, float height) {
        super(region, frameInfoList, width, height, true);
        setStartDelayMillis(5000);
    }

    @Override protected int getDesiredFPS() {
        return 25;
    }

    @Override protected void init() {
        p0x = -1.25f - getWidth();
        p0y = random(-0.5f, 0.5f);
        p1x = random(-0.75f, 0.0f);
        p1y = random(-0.75f, 0.2f);
        p2x = 1.25f;
        p2y = random(-1f, 1f);
        zSpeed = -0.25f / Const.NANOS_IN_SECOND;
        setScale(1);
        setPosition(p0x, p0y, -1.0f);
        setStartDelayMillis(random(2500, 4500));
        deflateSpeed = 0.02f / Const.NANOS_IN_SECOND;
        speed = 0.22f / Const.NANOS_IN_SECOND;
    }

    @Override protected boolean drawOverFrame() {
        return t < 0.5f;
    }
}
