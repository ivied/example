package com.tme.moments.presentation.core;

import javax.annotation.Nullable;


public class ScreenshotConfig {

    @Nullable
    private String outputPath;
    @Nullable
    private String unmarkedOutputPath;
    private boolean fastScreenshot;
    @Nullable
    private Throwable throwable;

    @Nullable
    public String getOutputPath() {
        return outputPath;
    }

    public void setOutputPath(@Nullable String outputPath) {
        this.outputPath = outputPath;
    }

    public boolean isFastScreenshot() {
        return fastScreenshot;
    }

    public void setFastScreenshot(boolean fastScreenshot) {
        this.fastScreenshot = fastScreenshot;
    }

    @Nullable
    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(@Nullable Throwable throwable) {
        this.throwable = throwable;
    }

    @Nullable
    public String getUnmarkedOutputPath() {
        return unmarkedOutputPath;
    }

    public void setUnmarkedOutputPath(@Nullable String unmarkedOutputPath) {
        this.unmarkedOutputPath = unmarkedOutputPath;
    }

    public void clear() {
        outputPath = null;
        throwable = null;
        fastScreenshot = false;
        unmarkedOutputPath = null;
    }
}
