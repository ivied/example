package com.tme.moments.presentation.scene.f2.dreamland;

import android.util.Pair;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author zoopolitic.
 */
class Lamp extends Decal {

    private enum State {
        TURNING_ON, WORKING, TURNING_OFF, NOT_WORKING
    }

    private List<Pair<AnimatedSprite.FrameInfo, Long>> turnOnFrames;
    private List<Pair<AnimatedSprite.FrameInfo, Long>> turnOffFrames;
    private int turnOnFrameIndex;
    private int turnOffFrameIndex;

    private AnimatedSprite.FrameInfo currFrame;
    private final AnimatedSprite.FrameInfo onFrame;
    private final AnimatedSprite.FrameInfo offFrame;

    private long drawTime;
    private long onTime;
    private long offTime;

    private boolean finished = true;
    private boolean enabled = true;

    private State state = State.TURNING_ON;

    Lamp(TextureRegion region, float width, float height, float x, float y,
         AnimatedSprite.FrameInfo onFrame, AnimatedSprite.FrameInfo offFrame) {
        setTextureRegion(region);
        setDimensions(width, height);
        setPosition(x, y, -1.0f);
        setColor(1, 1, 1, 1);
        this.onFrame = onFrame;
        this.offFrame = offFrame;
        region.setX(offFrame.regionX);
        region.setY(offFrame.regionY);
        finished = false;
        initTurnOnFrames(onFrame, offFrame);
        initTurnOffFrames(onFrame, offFrame);
    }


    private void addFrame(List<Pair<AnimatedSprite.FrameInfo, Long>> frames,
                          AnimatedSprite.FrameInfo frame, long nanos) {
        frames.add(new Pair<>(frame, TimeUnit.MILLISECONDS.toNanos(nanos)));
    }

    private void initTurnOnFrames(AnimatedSprite.FrameInfo onFrame,
                                  AnimatedSprite.FrameInfo offFrame) {
        turnOnFrames = new ArrayList<>();
        addFrame(turnOnFrames, offFrame, 200);
        addFrame(turnOnFrames, onFrame, 300);
        addFrame(turnOnFrames, offFrame, 5);
        addFrame(turnOnFrames, onFrame, 5);
        addFrame(turnOnFrames, offFrame, 5);
        addFrame(turnOnFrames, onFrame, 5);
        addFrame(turnOnFrames, offFrame, 600);
        addFrame(turnOnFrames, onFrame, 800);
        addFrame(turnOnFrames, offFrame, 5);
        addFrame(turnOnFrames, onFrame, 1); // last frame should be ON, time doesn't matter
    }

    private void initTurnOffFrames(AnimatedSprite.FrameInfo onFrame,
                                   AnimatedSprite.FrameInfo offFrame) {
        turnOffFrames = new ArrayList<>();
        addFrame(turnOffFrames, offFrame, 100);
        addFrame(turnOffFrames, onFrame, 300);
        addFrame(turnOffFrames, offFrame, 1);
        addFrame(turnOffFrames, onFrame, 1);
        addFrame(turnOffFrames, offFrame, 1);
        addFrame(turnOffFrames, onFrame, 1);
        addFrame(turnOffFrames, offFrame, 1); // last frame should be OFF, time doesn't matter
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        if (enabled) {
            handleTurnOn();
        } else {
            setFrame(offFrame);
        }
    }

    public boolean isFinished() {
        return finished;
    }

    void setOnTime(long onTime) {
        this.onTime = onTime;
    }

    void setOffTime(long offTime) {
        this.offTime = offTime;
    }

    void step(long deltaNanos) {
        if (!enabled) {
            return;
        }
        if (drawTime > 0) {
            drawTime -= deltaNanos;
            return;
        }
        switch (state) {
            case TURNING_ON:
                handleTurnOn();
                break;
            case WORKING:
                handleWorking();
                break;
            case TURNING_OFF:
                handleTurnOff();
                break;
            case NOT_WORKING:
                handleNotWorking();
                break;
        }
    }

    private void handleTurnOn() {
        finished = false;
        Pair<AnimatedSprite.FrameInfo, Long> frame = turnOnFrames.get(turnOnFrameIndex++);
        drawTime = frame.second;
        setFrame(frame.first);
        if (turnOnFrameIndex == turnOnFrames.size() - 1) {
            state = State.WORKING;
            turnOnFrameIndex = 0;
        }
    }

    private void handleWorking() {
        if (drawTime < 0 && currFrame == onFrame) {
            state = State.TURNING_OFF;
        } else {
            setFrame(onFrame);
            drawTime = TimeUnit.MILLISECONDS.toNanos(onTime);
        }
    }

    private void handleTurnOff() {
        Pair<AnimatedSprite.FrameInfo, Long> frame = turnOffFrames.get(turnOffFrameIndex++);
        drawTime = frame.second;
        setFrame(frame.first);
        if (turnOffFrameIndex == turnOffFrames.size() - 1) {
            state = State.NOT_WORKING;
            turnOffFrameIndex = 0;
        }
    }

    private void handleNotWorking() {
        if (drawTime < 0 && currFrame == offFrame) {
            finished = true;
            state = State.TURNING_ON;
        } else {
            setFrame(offFrame);
            drawTime = TimeUnit.MILLISECONDS.toNanos(offTime);
        }
    }

    private void setFrame(AnimatedSprite.FrameInfo frame) {
        TextureRegion region = getTextureRegion();
        region.setX(frame.regionX);
        region.setY(frame.regionY);
        setTextureRegion(region); // update UV-s
        currFrame = frame;
    }

}
