package com.tme.moments.presentation.preview;

import android.app.Dialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.tme.moments.R;
import com.tme.moments.databinding.DialogSavingContentInfoBinding;

public class SavingContentInfoDialog extends AppCompatDialogFragment {

    private DialogSavingContentInfoBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_saving_content_info, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = DataBindingUtil.bind(view);
        if (binding == null) {
            throw new NullPointerException("DialogSavingContentInfoBinding is null");
        }
        binding.okButton.setOnClickListener(v -> {
            ((OnCloseSavingContentInfoDialogListener) getActivity())
                    .onSubmitSavingContentInfoDialog(binding.doNotShowCheckBox.isChecked());
            getDialog().dismiss();
        });
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AppCompatDialog(
                new ContextThemeWrapper(getContext(), R.style.DefaultAlertDialog)
        );
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new InsetDrawable(
                            new ColorDrawable(Color.TRANSPARENT),
                            (int) getContext().getResources()
                                    .getDimension(R.dimen.saving_content_info_dialog_margin)
                    ));
            window.setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
        }
        binding.scrollView.scrollTo(0, binding.scrollView.getBottom());
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getActivity() instanceof OnCloseSavingContentInfoDialogListener) {
            ((OnCloseSavingContentInfoDialogListener) getActivity())
                    .onCloseSavingContentInfoDialog();
        }
    }

    interface OnCloseSavingContentInfoDialogListener {
        void onSubmitSavingContentInfoDialog(boolean doNotShowAgain);
        void onCloseSavingContentInfoDialog();
    }
}
