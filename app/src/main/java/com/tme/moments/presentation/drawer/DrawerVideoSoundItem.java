package com.tme.moments.presentation.drawer;

import com.google.auto.value.AutoValue;
import com.tme.moments.R;

/**
 * @author zoopolitic.
 */
@AutoValue
public abstract class DrawerVideoSoundItem implements DrawerItem {

    @Override public int getLayoutResId() {
        return R.layout.drawer_video_sound_item;
    }

    public static DrawerVideoSoundItem create() {
        return new AutoValue_DrawerVideoSoundItem();
    }
}
