package com.tme.moments.presentation.scene.f17.royal;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.scene.BlinkingGlow;
import com.tme.moments.presentation.scene.Glow;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Circle extends Glow {

    Circle(TextureRegion region, float width, float height) {
        super(region, width, height);
    }

    @Override protected void init() {
        super.init();
        setScale(random(0.7f, 1.7f));
    }
}
