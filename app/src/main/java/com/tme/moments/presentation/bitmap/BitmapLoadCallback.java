package com.tme.moments.presentation.bitmap;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

/**
 * @author zoopolitic.
 */
public interface BitmapLoadCallback {

    void onBitmapLoaded(@NonNull Bitmap bitmap, @NonNull ExifInfo exifInfo,
                        @NonNull String imageInputPath);

    void onFailure(@NonNull Exception bitmapWorkerException);
}