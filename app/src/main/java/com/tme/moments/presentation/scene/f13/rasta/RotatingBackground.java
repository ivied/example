package com.tme.moments.presentation.scene.f13.rasta;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.Const;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class RotatingBackground extends Sprite {

    private float rotationSpeed;

    RotatingBackground(TextureRegion textureRegion, float width,
                       float height) {
        super(textureRegion, width, height);
        init();
    }

    public void step(long deltaNanos) {
        float delta = rotationSpeed * deltaNanos;
        rotateZ(delta);
        update();
    }

    private void init() {
        rotationSpeed = (random() ? -20f : 20f) / Const.NANOS_IN_SECOND;
    }
}
