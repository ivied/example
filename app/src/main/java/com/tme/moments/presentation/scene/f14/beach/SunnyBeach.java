
package com.tme.moments.presentation.scene.f14.beach;

import android.graphics.SurfaceTexture;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.Rippler;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_ALWAYS;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_EQUAL;
import static android.opengl.GLES20.GL_KEEP;
import static android.opengl.GLES20.GL_REPLACE;
import static android.opengl.GLES20.GL_STENCIL_BUFFER_BIT;
import static android.opengl.GLES20.GL_STENCIL_TEST;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glColorMask;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glStencilFunc;
import static android.opengl.GLES20.glStencilOp;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class SunnyBeach extends BaseScene {

    private int stencilProgramHandle = -1;
    private int uStencilProjectionMatrixHandle = -1;
    private int aStencilPositionHandle = -1;
    private int aStencilColorHandle = -1;
    private int aStencilTexCoordHandle = -1;

    private Sprite overlay;
    private Sprite background;
    private Sprite stencilMask;

    private TextureInfo atlasInfo;
    private DecalBatch circlesBatch;
    private List<Circle> circles;
    private Rippler rippler;

    @Override protected float getCameraPositionX() {
        return 0.029f;
    }

    @Override protected float getCameraPositionY() {
        return 0.022f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.5879f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.6268f;
    }

    @Override public boolean hasBackgroundEffects() {
        return true;
    }

    @Override public void init() {
        super.init();
        if (stencilProgramHandle == -1) {
            prepareStencilProgram();
        }
        rippler = new Rippler();
        rippler.init(openGLLoader);
    }

    @Override protected void prepareBackgroundEffects(int surfaceWidth, int surfaceHeight) {
        super.prepareBackgroundEffects(surfaceWidth, surfaceHeight);
        rippler.reset();
    }

    @Override public void setup(TextureInfo imageInfo, int width, int height) {
        super.setup(imageInfo, width, height);
        rippler.setup(width, height, R.drawable.f14_overlay);
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        prepareBackground();
        prepareOverlay();
        prepareStencilMask();
        atlasInfo = openGLLoader.loadTexture(R.drawable.f14_atlas);
        prepareCircles(atlasInfo);
    }

    private void prepareStencilProgram() {
        stencilProgramHandle =
                openGLLoader.createProgram(R.raw.vertex_shader, R.raw.stencil_fragment_shader);

        aStencilPositionHandle = glGetAttribLocation(stencilProgramHandle, "a_position");
        GlUtil.checkLocation(aStencilPositionHandle, "a_position");
        glEnableVertexAttribArray(aStencilPositionHandle);

        aStencilColorHandle = glGetAttribLocation(stencilProgramHandle, "a_color");
        GlUtil.checkLocation(aStencilColorHandle, "a_color");
        glEnableVertexAttribArray(aStencilColorHandle);

        aStencilTexCoordHandle = glGetAttribLocation(stencilProgramHandle, "a_texCoord");
        GlUtil.checkLocation(aStencilTexCoordHandle, "a_texCoord");
        glEnableVertexAttribArray(aStencilTexCoordHandle);

        uStencilProjectionMatrixHandle = glGetUniformLocation(stencilProgramHandle, "u_projTrans");
        GlUtil.checkLocation(uStencilProjectionMatrixHandle, "u_projTrans");
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glUseProgram(mainProgramHandle);
        background.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation,
                aTexCoordLocation, aColorLocation);
        glEnable(GL_STENCIL_TEST);

        glColorMask(false, false, false, false);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

        // draw mask
        glUseProgram(stencilProgramHandle);
        stencilMask.draw(uStencilProjectionMatrixHandle, projectionMatrix, aStencilPositionHandle,
                aStencilTexCoordHandle, aStencilColorHandle);

        glColorMask(true, true, true, true);
        glStencilFunc(GL_EQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

        if (mode == Mode.IMAGE) {
            glUseProgram(mainProgramHandle);
            drawImage();
        } else {
            drawCameraInput(cameraTexture);
        }

        glDisable(GL_STENCIL_TEST);
        if (config.isBackgroundEffectsEnabled()) {
            rippler.render();
        } else {
            glUseProgram(mainProgramHandle);
            overlay.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation,
                    aTexCoordLocation, aColorLocation);
        }
        if (config.isObjectsEnabled()) {
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            circlesBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Circle circle : circles) circle.step(deltaNanos);
        }
    }

    private void prepareStencilMask() {
        TextureInfo stencilInfo = openGLLoader.loadTexture(R.drawable.f14_stencil);
        int w = stencilInfo.imageWidth;
        int h = stencilInfo.imageHeight;
        TextureRegion region = new TextureRegion(stencilInfo.textureHandle, w, h, 0, 0, w, h);
        float width = getDesiredCameraWidthFraction() * getProjectionWidth();
        float height = getDesiredCameraHeightFraction() * getProjectionHeight();
        stencilMask = new Sprite(region, width, height);
        stencilMask.setPosition(getCameraPositionX(), getCameraPositionY(), -1);
    }

    private void prepareOverlay() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f14_overlay);
        int textureHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        TextureRegion region = new TextureRegion(textureHandle, w, h, 0, 0, w, h);
        overlay = new Sprite(region, getProjectionWidth(), getProjectionHeight());
        overlay.setPosition(0, 0, -1);
    }

    private void prepareBackground() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f14_background);
        int textureHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        TextureRegion region = new TextureRegion(textureHandle, w, h, 0, 0, w, h);
        background = new Sprite(region, getProjectionWidth(), getProjectionHeight());
        background.setPosition(0, 0, -1);
    }

    private void prepareCircles(TextureInfo info) {
        TextureRegion like = buildRegion(info, 0, 0, 120, 120);
        TextureRegion love = buildRegion(info, 120, 0, 120, 120);
        TextureRegion rofl = buildRegion(info, 240, 0, 120, 120);
        TextureRegion surprised = buildRegion(info, 360, 0, 120, 120);

        circles = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            circles.add(createCircle(like));
        }
        circles.add(createCircle(love));
        circles.add(createCircle(love));
        circles.add(createCircle(rofl));
        circles.add(createCircle(surprised));

        circlesBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        circlesBatch.initialize(circles.size());
        circlesBatch.addAll(circles);
    }

    private Circle createCircle(TextureRegion region) {
        float width = 0.165f;
        float height = 0.165f;
        float delayInSeconds = random(0.0f, 5.0f);
        Circle circle = new Circle(region, width, height);
        circle.setStartDelay(delayInSeconds);
        return circle;
    }

    @Override public void release() {
        super.release();
        glDeleteProgram(stencilProgramHandle);
        stencilProgramHandle = -1;
        rippler.release();
        if (atlasInfo != null) {
            int[] values = new int[1];
            values[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, values, 0);
            GlUtil.checkGlError("Delete birds");
        }
    }
}