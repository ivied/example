package com.tme.moments.presentation.scene.f8.sunflower.field;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Cloud extends Decal {

    private final float speedNanos;
    private final float startY;
    private final float endY;

    private float startDelayNanos;
    private float endX;
    private float startX;

    Cloud(TextureRegion region, float width, float height, float currX, float startY,
          float endY, float speedPerSecond) {
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        this.endY = endY;
        this.startY = startY;
        this.startX = 1 + width / 2;
        this.endX = -1 - width / 2;
        speedNanos = speedPerSecond / Const.NANOS_IN_SECOND;
        float xPathLength = Math.abs(endX - startX);
        float fraction = getFraction(currX, startX, xPathLength);
        float y = getY(fraction);
        setPosition(currX, y, -1.0f);
    }

    void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float delta = speedNanos * deltaNanos;
        translateX(delta);
        float x = getX();

        float xPathLength = Math.abs(endX - startX);
        float fraction = getFraction(x, startX, xPathLength);
        float y = getY(fraction);
        setY(y);

        // goes out of screen
        if (x < endX) {
            setX(startX);
            setY(startY);
            setStartDelay(random(3000, 5000));
        }
    }

    private float getY(float fraction) {
        float yPathLength = endY - startY;
        return startY + fraction * yPathLength;
    }

    private float getFraction(float currX, float startX, float xPathLength) {
        return Math.abs(currX - startX) / xPathLength;
    }
}
