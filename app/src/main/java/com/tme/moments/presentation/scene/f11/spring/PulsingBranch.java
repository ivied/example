package com.tme.moments.presentation.scene.f11.spring;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class PulsingBranch extends Decal {

    private static final float MIN_SCALE = 1.0f;
    private static final float MAX_SCALE = 1.03f;

    private boolean shouldThrowLeaves;
    private long scaleWaitTime;
    private float scaleUpSpeed;
    private float scaleDownSpeed;
    private boolean scaleUp = true;
    private int shakeCount;
    private int maxShakeCount;
    private boolean finished = true;

    PulsingBranch(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        init();
    }

    private void init() {
        maxShakeCount = random(2, 5);
        shakeCount = 0;
        scaleUpSpeed = 0.35f / Const.NANOS_IN_SECOND;
        scaleDownSpeed = 0.2f / Const.NANOS_IN_SECOND;
        scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(random(5000, 8000));
    }

    boolean isFinished() {
        return finished;
    }

    void activate() {
        finished = false;
        init();
    }

    void step(long deltaNanos) {
        if (finished) {
            return;
        }
        if (scaleWaitTime > 0) {
            scaleWaitTime -= deltaNanos;
        } else {
            if (scaleUp) {
                float deltaScale = scaleUpSpeed * deltaNanos;
                float newScale = Math.min(MAX_SCALE, getScaleX() + deltaScale);
                setScale(newScale);
                if (newScale == MAX_SCALE) {
                    scaleUp = false;
                }
            } else {
                float deltaScale = scaleDownSpeed * deltaNanos;
                float newScale = Math.max(MIN_SCALE, getScaleX() - deltaScale);
                setScale(newScale);
                if (newScale == MIN_SCALE) {
                    if (shakeCount == 0) {
                        shouldThrowLeaves = true;
                    }
                    shakeCount++;
                    scaleUp = true;
                    if (shakeCount == maxShakeCount) {
                        finished = true;
                    }
                }
            }
        }
    }

    boolean shouldThrowLeaves() {
        return shouldThrowLeaves;
    }

    void setShouldThrowLeaves(boolean shouldThrowLeaves) {
        this.shouldThrowLeaves = shouldThrowLeaves;
    }
}
