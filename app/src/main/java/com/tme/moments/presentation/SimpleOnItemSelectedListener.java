package com.tme.moments.presentation;

import android.view.View;
import android.widget.AdapterView;

/**
 * @author zoopolitic.
 */
public class SimpleOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

    @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override public void onNothingSelected(AdapterView<?> parent) {

    }
}

