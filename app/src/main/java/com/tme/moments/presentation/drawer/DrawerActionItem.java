package com.tme.moments.presentation.drawer;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.tme.moments.R;

/**
 * @author zoopolitic.
 */
@AutoValue
public abstract class DrawerActionItem implements DrawerItem {

    @Override public int getLayoutResId() {
        return R.layout.drawer_simple_item;
    }

    static DrawerActionItem create(@NonNull String title, Drawable icon, Runnable action) {
        return new AutoValue_DrawerActionItem(title, icon, action);
    }

    public abstract String title();
    public abstract Drawable icon();
    public abstract Runnable action();
}
