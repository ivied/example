package com.tme.moments.presentation;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;

import com.tme.moments.presentation.base.BaseActivity;
import com.tme.moments.presentation.capture.CaptureActivity;

import java.util.Locale;

/**
 * @author zoopolitic.
 */
public class SplashActivity extends BaseActivity {

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, CaptureActivity.class));
        finish();
    }
}
