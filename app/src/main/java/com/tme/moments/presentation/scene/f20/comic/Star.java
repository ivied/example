package com.tme.moments.presentation.scene.f20.comic;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Star extends Decal {

    private enum Direction {
        LEFT, TOP, RIGHT, BOTTOM
    }

    private float speedX;
    private float speedY;
    private float startDelayNanos;
    private float rotationSpeed;
    private float alphaSpeed;

    Star(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        init();
        startDelayNanos = 0;
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float deltaX = speedX * deltaNanos;
        float deltaY = speedY * deltaNanos;
        float deltaRotation = rotationSpeed * deltaNanos;
        translate(deltaX, deltaY, 0);
        rotateZ(deltaRotation);

        float alpha = color.a;
        if (alpha < 1) {
            float deltaAlpha = alphaSpeed * deltaNanos;
            setAlpha(Math.min(alpha + deltaAlpha, 1.0f));
        }

        float halfWidth = getWidth() / 2;
        float halfHeight = getHeight() / 2;
        float minY = -1 - halfHeight;
        float maxY = 1 + halfHeight;
        float minX = -1 - halfWidth;
        float maxX = 1 + halfWidth;
        float x = getX();
        float y = getY();
        if (x <= minX || x >= maxX || y <= minY || y >= maxY) {
            init();
        }
    }

    private void init() {
        Direction[] directions = Direction.values();
        Direction direction = directions[random(0, directions.length)];
        float x = random(-0.5f, 0.5f);
        float y = random(-0.5f, 0.5f);
        switch (direction) {
            case LEFT:
                speedX = random(-0.2f, -0.1f) / Const.NANOS_IN_SECOND;
                speedY = random(-0.2f, 0.2f) / Const.NANOS_IN_SECOND;
                break;
            case TOP:
                speedX = random(-0.2f, 0.2f) / Const.NANOS_IN_SECOND;
                speedY = random(-0.2f, -0.1f) / Const.NANOS_IN_SECOND;
                break;
            case RIGHT:
                speedX = random(0.1f, 0.2f) / Const.NANOS_IN_SECOND;
                speedY = random(-0.2f, 0.2f) / Const.NANOS_IN_SECOND;
                break;
            case BOTTOM:
                speedX = random(-0.2f, 0.2f) / Const.NANOS_IN_SECOND;
                speedY = random(0.1f, 0.2f) / Const.NANOS_IN_SECOND;
                break;
        }
        rotationSpeed = random(-180f, 180f) / Const.NANOS_IN_SECOND;
        alphaSpeed = 0.3f / Const.NANOS_IN_SECOND;
        setScale(random(0.4f, 1.0f));
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 5000));
        setPosition(x, y, -1.0f);
        setAlpha(0);
    }
}
