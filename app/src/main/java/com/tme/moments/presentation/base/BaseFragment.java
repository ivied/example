package com.tme.moments.presentation.base;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.tme.moments.presentation.utils.AndroidUtils;

/**
 * @author zoopolitic.
 */
public abstract class BaseFragment extends Fragment {

    private static final String TAG = BaseFragment.class.getSimpleName();

    protected Toast currentToast;

    /**
     * Returns true if fragment handled back press and activity should not perform its default
     * back press actions
     *
     * @return true if fragment handled back press
     */
    public boolean onBackPressed() {
        return false;
    }

    // ------------------------------ Toast ---------------------------------

    protected void cancelCurrentToast() {
        if (currentToast != null) {
            currentToast.cancel();
            currentToast = null;
        }
    }

    private void toast(final String text, final int length) {
        Activity activity = getActivity();
        if (isAdded() && activity != null && !activity.isFinishing()) {
            if (!AndroidUtils.isMainThread()) {
                activity.runOnUiThread(() -> {
                    currentToast = Toast.makeText(activity, text, length);
                    currentToast.show();
                });
            } else {
                currentToast = Toast.makeText(activity, text, length);
                currentToast.show();
            }
        }
    }

    protected void shortToast(String text) {
        toast(text, Toast.LENGTH_SHORT);
    }

    protected void shortToast(@StringRes int resId) {
        Activity activity = getActivity();
        // getString can throw Exception in case when fragment is detached
        if (activity != null && isAdded()) {
            String message = getString(resId);
            toast(message, Toast.LENGTH_SHORT);
        }
    }

    protected void longToast(String text) {
        toast(text, Toast.LENGTH_LONG);
    }

    protected void longToast(int resId) {
        Activity activity = getActivity();
        // getString can throw Exception in case when fragment is detached
        if (activity != null && isAdded()) {
            String message = getString(resId);
            toast(message, Toast.LENGTH_LONG);
        }
    }

    @Nullable public Drawable getThemedDrawable(int attributeId) {
        return AndroidUtils.getThemedDrawable(getActivity(), attributeId);
    }
}
