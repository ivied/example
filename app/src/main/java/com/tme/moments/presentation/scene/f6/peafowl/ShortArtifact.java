package com.tme.moments.presentation.scene.f6.peafowl;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
final class ShortArtifact extends Decal {

    private float rotationSpeed;
    private float speedY;
    private float speedX;
    private float startDelayNanos;
    private float alphaSpeed;

    ShortArtifact(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        init();
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float rotationDelta = rotationSpeed * deltaNanos;
        rotateZ(rotationDelta);
        float deltaY = speedY * deltaNanos;
        float deltaX = speedX * deltaNanos;
        translateY(deltaY);
        translateX(deltaX);
        float y = getY();

        float deltaAlpha = alphaSpeed * deltaNanos;
        float alpha = color.a;

        if (alpha < 1.0f) { // fade in
            alpha = Math.min(1.0f, alpha + deltaAlpha);
            setAlpha(alpha);
        }
        if (y <= -1 - getHeight() / 2) {
            init();
        }
    }

    protected void init() {
        float minP0X = 0.3f;
        float maxP0X = 0.8f;
        float minP0Y = -0.65f;
        float maxP0Y = -0.4f;
        float x = random(minP0X, maxP0X);
        float y = random(minP0Y, maxP0Y);
        float speedYPerSecond = random(-0.5f, -0.25f);
        float speedXPerSecond = random(-0.65f, 0.3f);
        float rotationSpeedPerSecond = random(-250, 250);
        speedY = speedYPerSecond / Const.NANOS_IN_SECOND;
        speedX = speedXPerSecond / Const.NANOS_IN_SECOND;
        alphaSpeed = 4f / Const.NANOS_IN_SECOND;
        rotationSpeed = rotationSpeedPerSecond / Const.NANOS_IN_SECOND;
        setColor(1, 1, 1, 1);
        setPosition(x, y, -1f);
        setRotationZ(0);
        setAlpha(0);
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(random(0, 5000));
    }
}
