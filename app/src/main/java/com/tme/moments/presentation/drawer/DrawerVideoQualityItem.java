package com.tme.moments.presentation.drawer;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.tme.moments.R;
import com.tme.moments.recording.Quality;

import java.util.List;

/**
 * @author zoopolitic.
 */
@AutoValue
public abstract class DrawerVideoQualityItem implements DrawerItem {

    @Override public int getLayoutResId() {
        return R.layout.drawer_video_quality_item;
    }

    static DrawerVideoQualityItem create(@NonNull List<Quality> qualities) {
        return new AutoValue_DrawerVideoQualityItem(qualities);
    }

    public abstract List<Quality> qualities();
}
