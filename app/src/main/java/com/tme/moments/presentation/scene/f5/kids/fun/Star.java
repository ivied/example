package com.tme.moments.presentation.scene.f5.kids.fun;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.presentation.Const;
import com.tme.moments.presentation.scene.Glow;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class Star extends Glow {

    private float rotationSpeed;

    public Star(TextureRegion region, float width, float height) {
        super(region, width, height);
    }

    @Override public void step(long deltaNanos) {
        super.step(deltaNanos);
        float deltaRotation = rotationSpeed * deltaNanos;
        rotateZ(deltaRotation);
    }

    @Override protected void init() {
        super.init();
        rotationSpeed = (float) random(-180, 180) / Const.NANOS_IN_SECOND;
    }
}
