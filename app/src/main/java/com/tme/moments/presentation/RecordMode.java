package com.tme.moments.presentation;

/**
 * @author zoopolitic.
 */
public enum RecordMode {
    CAPTURE_VIDEO,
    CAPTURE_IMAGE
}
