package com.tme.moments.presentation.scene.f7.emotional.hardcore;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Heart extends Sprite {

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float speed;
    private float initialInflateSpeed;
    private float inflateVelocity;
    private float inflateSpeed;
    private float deflateSpeed;
    private float startDelayNanos;
    private boolean inflated;
    private float t;

    Heart(TextureRegion textureRegion, float width, float height) {
        super(textureRegion, width, height);
        init();
        setStartDelay(0);
    }

    private void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    void step(long deltaNanos) {
        if (startDelayNanos >= 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        if (!inflated) {
            float targetScale = 1.0f;
            float scale = getScaleX();
            float deltaScale = inflateSpeed * deltaNanos;
            inflateSpeed += inflateVelocity;
            scale += deltaScale;
            scale = Math.min(targetScale, scale);
            setScale(scale);
            if (scale == targetScale) {
                inflated = true;
            } else {
                update();
                return;
            }
        }

        if (t < 1) {
            float deltaT = speed * deltaNanos;
            t += deltaT;

            float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
            float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

            setX(x);
            setY(y);

            float scale = getScaleX();
            float deltaScale = deflateSpeed * deltaNanos;
            scale -= deltaScale;
            scale = Math.max(0.35f, scale);
            setScale(scale);
        } else {
            float scale = getScaleX();
            if (scale > 0) {
                float deltaScale = deflateSpeed * deltaNanos;
                scale -= deltaScale;
                scale = Math.max(0f, scale);
                setScale(scale);
            } else {
                init();
            }
        }
        update();
    }

    private void init() {
        p0x = -0.41f;
        p0y = -0.32f;

        p1x = -0.55f;
        p1y = -0.5f;

        p2x = -0.54f;
        p2y = -0.79f;
        speed = random(0.35f, 0.45f) / Const.NANOS_IN_SECOND;
        initialInflateSpeed = 0.25f / Const.NANOS_IN_SECOND;
        inflateVelocity = 0.02f / Const.NANOS_IN_SECOND;
        inflateSpeed = initialInflateSpeed;
        deflateSpeed = 0.25f / Const.NANOS_IN_SECOND;
        inflated = false;
        setStartDelay(random(1000, 2000));
        t = 0;
        setColor(1, 1, 1, 1);
        setScale(0);
        setPosition(p0x, p0y, -1.0f);
    }
}
