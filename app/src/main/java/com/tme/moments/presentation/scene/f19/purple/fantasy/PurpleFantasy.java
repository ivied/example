package com.tme.moments.presentation.scene.f19.purple.fantasy;

import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.util.Pair;

import com.tme.moments.R;
import com.tme.moments.gles.GlUtil;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.DecalBatch;
import com.tme.moments.graphics.g3d.Sprite;
import com.tme.moments.presentation.Rippler;
import com.tme.moments.presentation.TextureInfo;
import com.tme.moments.presentation.scene.BaseScene;

import java.util.ArrayList;
import java.util.List;

import static android.opengl.GLES20.GL_ALWAYS;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_EQUAL;
import static android.opengl.GLES20.GL_KEEP;
import static android.opengl.GLES20.GL_REPLACE;
import static android.opengl.GLES20.GL_STENCIL_BUFFER_BIT;
import static android.opengl.GLES20.GL_STENCIL_TEST;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glColorMask;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteTextures;
import static android.opengl.GLES20.glDisable;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glStencilFunc;
import static android.opengl.GLES20.glStencilOp;
import static android.opengl.GLES20.glUseProgram;
import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
public class PurpleFantasy extends BaseScene {

    private int stencilProgramHandle = -1;
    private int uStencilProjectionMatrixHandle = -1;
    private int aStencilPositionHandle = -1;
    private int aStencilColorHandle = -1;
    private int aStencilTexCoordHandle = -1;

    private Sprite stencilMask;
    private Sprite overlay;
    private Sprite background;

    private TextureInfo atlasInfo;
    private DecalBatch glowBatch;
    private DecalBatch branchBatch;
    private DecalBatch objectsBatch;
    private DecalBatch flowersBatch;
    private List<RotationGlow> glowList;
    private List<Flower> flowers;
    private Branch branch;
    private Rippler rippler;

    private List<FallingObject> objects;
    private List<FallingObject> finishedObjects;
    private Pair<TextureRegion, PointF> leaf1Info;
    private Pair<TextureRegion, PointF> leaf2Info;
    private Pair<TextureRegion, PointF> flowerInfo;

    @Override protected float getCameraPositionX() {
        return -0.144f;
    }

    @Override protected float getCameraPositionY() {
        return -0.105f;
    }

    @Override protected float getDesiredCameraWidthFraction() {
        return 0.363f;
    }

    @Override protected float getDesiredCameraHeightFraction() {
        return 0.423f;
    }

    @Override public boolean hasBackgroundEffects() {
        return true;
    }

    @Override public void init() {
        super.init();
        if (stencilProgramHandle == -1) {
            prepareStencilProgram();
        }
        rippler = new Rippler();
        rippler.init(openGLLoader);
    }

    @Override public void setup(TextureInfo imageInfo, int width, int height) {
        super.setup(imageInfo, width, height);
        rippler.setup(width, height, R.drawable.f19_overlay);
    }

    @Override protected void prepareBackgroundEffects(int surfaceWidth, int surfaceHeight) {
        super.prepareBackgroundEffects(surfaceWidth, surfaceHeight);
        rippler.reset();
        prepareFlowers(atlasInfo);
        flowersBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        flowersBatch.initialize(flowers.size());
        flowersBatch.addAll(flowers);
    }

    private void prepareStencilProgram() {
        stencilProgramHandle =
                openGLLoader.createProgram(R.raw.vertex_shader, R.raw.stencil_fragment_shader);

        aStencilPositionHandle = glGetAttribLocation(stencilProgramHandle, "a_position");
        GlUtil.checkLocation(aStencilPositionHandle, "a_position");
        glEnableVertexAttribArray(aStencilPositionHandle);

        aStencilColorHandle = glGetAttribLocation(stencilProgramHandle, "a_color");
        GlUtil.checkLocation(aStencilColorHandle, "a_color");
        glEnableVertexAttribArray(aStencilColorHandle);

        aStencilTexCoordHandle = glGetAttribLocation(stencilProgramHandle, "a_texCoord");
        GlUtil.checkLocation(aStencilTexCoordHandle, "a_texCoord");
        glEnableVertexAttribArray(aStencilTexCoordHandle);

        uStencilProjectionMatrixHandle = glGetUniformLocation(stencilProgramHandle, "u_projTrans");
        GlUtil.checkLocation(uStencilProjectionMatrixHandle, "u_projTrans");
    }

    @Override protected void prepareObjects(int surfaceWidth, int surfaceHeight) {
        atlasInfo = openGLLoader.loadTexture(R.drawable.f19_atlas);
        prepareStencilMask();
        prepareBackground();
        prepareOverlay();

        prepareGlow(atlasInfo);
        glowBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        glowBatch.initialize(glowList.size());
        glowBatch.addAll(glowList);

        prepareFallingObjects(atlasInfo);
        objects = new ArrayList<>();
        finishedObjects = new ArrayList<>();
        objectsBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        objectsBatch.initialize(100);

        prepareBranch(atlasInfo);
        branchBatch = new DecalBatch(aPositionLocation, aTexCoordLocation, aColorLocation);
        branchBatch.initialize(1);
        branchBatch.add(branch);
    }

    @Override public void render(long deltaNanos, SurfaceTexture cameraTexture) {
        glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        boolean objectsEnabled = config.isObjectsEnabled();
        boolean backgroundEffectsEnabled = config.isBackgroundEffectsEnabled();
        glUseProgram(mainProgramHandle);
        background.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation,
                aTexCoordLocation, aColorLocation);
        glEnable(GL_STENCIL_TEST);

        glColorMask(false, false, false, false);
        glStencilFunc(GL_ALWAYS, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

        // draw mask
        glUseProgram(stencilProgramHandle);
        stencilMask.draw(uStencilProjectionMatrixHandle, projectionMatrix, aStencilPositionHandle,
                aStencilTexCoordHandle, aStencilColorHandle);

        glColorMask(true, true, true, true);
        glStencilFunc(GL_EQUAL, 1, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

        glDisable(GL_STENCIL_TEST);
        if (backgroundEffectsEnabled) {
            rippler.render();
        } else {
            glUseProgram(mainProgramHandle);
            overlay.draw(uProjectionMatrixLoc, projectionMatrix, aPositionLocation,
                    aTexCoordLocation, aColorLocation);
        }
        if (objectsEnabled) {
            glUseProgram(mainProgramHandle);
            glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
            glowBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (RotationGlow glow : glowList) {
                glow.step(deltaNanos);
            }
        }
        glEnable(GL_STENCIL_TEST);

        if (mode == Mode.IMAGE) {
            glUseProgram(mainProgramHandle);
            drawImage();
        } else {
            drawCameraInput(cameraTexture);
        }

        glDisable(GL_STENCIL_TEST);

        glUseProgram(mainProgramHandle);
        glBindTexture(GL_TEXTURE_2D, atlasInfo.textureHandle);
        if (backgroundEffectsEnabled) {
            flowersBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (Flower flower : flowers) flower.step(deltaNanos);
        }
        if (objectsEnabled) {
            objectsBatch.render(uProjectionMatrixLoc, projectionMatrix);
            for (FallingObject fallingObject : objects) {
                fallingObject.step(deltaNanos);
                if (fallingObject.isFinished()) {
                    finishedObjects.add(fallingObject);
                }
            }
            if (!finishedObjects.isEmpty()) {
                objects.removeAll(finishedObjects);
                objectsBatch.removeAll(finishedObjects);
                finishedObjects.clear();
            }
        }
        branchBatch.render(uProjectionMatrixLoc, projectionMatrix);
        if (objectsEnabled) {
            branch.step(deltaNanos);
            if (branch.shouldThrowLeaves()) {
                throwObjects();
                branch.setShouldThrowLeaves(false);
            }
        }
    }

    private void prepareStencilMask() {
        TextureInfo stencilInfo = openGLLoader.loadTexture(R.drawable.f19_stencil);
        int w = stencilInfo.imageWidth;
        int h = stencilInfo.imageHeight;
        TextureRegion region = new TextureRegion(stencilInfo.textureHandle, w, h, 0, 0, w, h);
        float width = getDesiredCameraWidthFraction() * getProjectionWidth();
        float height = getDesiredCameraHeightFraction() * getProjectionHeight();
        stencilMask = new Sprite(region, width, height);
        stencilMask.setPosition(getCameraPositionX(), getCameraPositionY(), -1);
    }

    private void throwObjects() {
        int flowersCount = random(1, 4);
        for (int i = 0; i < flowersCount; i++) {
            TextureRegion region = flowerInfo.first;
            float width = flowerInfo.second.x;
            float height = flowerInfo.second.y;
            float x = 1.0f;
            float y = 1.0f;
            FallingObject flower = new FallingObject(region, width, height, x, y, -225f, 225f);
            objects.add(flower);
            objectsBatch.add(flower);
        }
        int count = random(7, 13);
        for (int i = 0; i < count; i++) {
            boolean useLeaf2 = random(6) == 0; // leaf2 should be 20% of total count
            Pair<TextureRegion, PointF> leafInfo = useLeaf2 ? leaf2Info : leaf1Info;
            TextureRegion region = leafInfo.first;
            float width = leafInfo.second.x;
            float height = leafInfo.second.y;
            float x = 1.0f;
            float y = 1.0f;
            FallingObject leaf = new FallingObject(region, width, height, x, y, -180f, 180f);
            leaf.setScale(0.95f);
            objects.add(leaf);
            objectsBatch.add(leaf);
        }
    }

    private void prepareOverlay() {
        TextureInfo overlayInfo = openGLLoader.loadTexture(R.drawable.f19_overlay);
        int w = overlayInfo.imageWidth;
        int h = overlayInfo.imageHeight;
        TextureRegion region = new TextureRegion(overlayInfo.textureHandle, w, h, 0, 0, w, h);
        float width = getProjectionWidth();
        float height = getProjectionHeight();
        overlay = new Sprite(region, width, height);
        overlay.setPosition(0, 0, -1);
    }

    private void prepareBackground() {
        TextureInfo info = openGLLoader.loadTexture(R.drawable.f19_background);
        int textureHandle = info.textureHandle;
        int w = info.imageWidth;
        int h = info.imageHeight;
        TextureRegion region = new TextureRegion(textureHandle, w, h, 0, 0, w, h);
        background = new Sprite(region, getProjectionWidth(), getProjectionHeight());
        background.setPosition(0, 0, -1);
    }

    private void prepareFallingObjects(TextureInfo info) {
        leaf1Info = new Pair<>(buildRegion(info, 180, 0, 130, 72), new PointF(0.1625f, 0.09f));
        leaf2Info = new Pair<>(buildRegion(info, 60, 0, 120, 71), new PointF(0.15f, 0.0887f));
        flowerInfo = new Pair<>(buildRegion(info, 0, 0, 60, 63), new PointF(0.075f, 0.0787f));
    }

    private void prepareGlow(TextureInfo info) {
        List<Pair<TextureRegion, PointF>> infos = new ArrayList<>(8);
        infos.add(new Pair<>(buildRegion(info, 100, 100, 100, 100), new PointF(0.125f, 0.125f)));
        infos.add(new Pair<>(buildRegion(info, 0, 100, 100, 100), new PointF(0.125f, 0.125f)));
        infos.add(new Pair<>(buildRegion(info, 670, 0, 100, 100), new PointF(0.125f, 0.125f)));
        infos.add(new Pair<>(buildRegion(info, 770, 0, 100, 100), new PointF(0.125f, 0.125f)));
        infos.add(new Pair<>(buildRegion(info, 310, 0, 90, 87), new PointF(0.1125f, 0.1087f)));
        infos.add(new Pair<>(buildRegion(info, 490, 0, 90, 87), new PointF(0.1125f, 0.1087f)));
        infos.add(new Pair<>(buildRegion(info, 400, 0, 90, 87), new PointF(0.1125f, 0.1087f)));
        infos.add(new Pair<>(buildRegion(info, 580, 0, 90, 87), new PointF(0.1125f, 0.1087f)));
        glowList = new ArrayList<>();
        int count = 14;
        for (int i = 0; i < 4; i++) {
            Pair<TextureRegion, PointF> regionInfo = infos.get(i);
            glowList.addAll(generateGlow(regionInfo, count, 0.65f, 0.95f));
        }
        for (int i = 4; i < infos.size(); i++) {
            Pair<TextureRegion, PointF> regionInfo = infos.get(i);
            glowList.addAll(generateGlow(regionInfo, count, 0.5f, 0.8f));
        }
    }

    private List<RotationGlow> generateGlow(Pair<TextureRegion, PointF> regionInfo, int count,
                                            float minScale, float maxScale) {
        List<RotationGlow> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            TextureRegion region = regionInfo.first;
            float width = regionInfo.second.x;
            float height = regionInfo.second.y;
            RotationGlow glow = new RotationGlow(region, width, height);
            glow.setScale(random(minScale, maxScale));
            list.add(glow);
        }
        return list;
    }

    private void prepareFlowers(TextureInfo info) {
        List<Pair<TextureRegion, PointF>> infoPairs = new ArrayList<>();
        infoPairs.add(new Pair<>(buildRegion(info, 523, 100, 176, 160), new PointF(0.22f, 0.2f)));
        infoPairs.add(new Pair<>(buildRegion(info, 357, 100, 166, 160), new PointF(0.2075f, 0.2f)));
        infoPairs.add(new Pair<>(buildRegion(info, 0, 420, 177, 160), new PointF(0.2212f, 0.2f)));
        infoPairs.add(new Pair<>(buildRegion(info, 664, 260, 163, 160), new PointF(0.2037f, 0.2f)));
        infoPairs.add(new Pair<>(buildRegion(info, 341, 260, 164, 160), new PointF(0.205f, 0.2f)));
        infoPairs.add(new Pair<>(buildRegion(info, 699, 100, 168, 160), new PointF(0.21f, 0.2f)));
        infoPairs.add(new Pair<>(buildRegion(info, 0, 260, 172, 160), new PointF(0.215f, 0.2f)));
        infoPairs.add(new Pair<>(buildRegion(info, 172, 260, 169, 160), new PointF(0.2112f, 0.2f)));
        infoPairs.add(new Pair<>(buildRegion(info, 505, 260, 159, 160), new PointF(0.1987f, 0.2f)));
        infoPairs.add(new Pair<>(buildRegion(info, 200, 100, 157, 160), new PointF(0.1962f, 0.2f)));

        flowers = new ArrayList<>();
        int count = 20;
        boolean moveLeft = random();
        for (int i = 0; i < count; i++) {
            Pair<TextureRegion, PointF> infoPair = infoPairs.get(random(infoPairs.size()));
            TextureRegion region = infoPair.first;
            float width = infoPair.second.x;
            float height = infoPair.second.y;
            Flower flower = new Flower(region, width, height, moveLeft);
            flower.setScale(random(0.5f, 0.7f));
            flowers.add(flower);
        }
    }

    private void prepareBranch(TextureInfo info) {
        TextureRegion region = buildRegion(info, 177, 420, 595, 504);
        float width = 1.1018f;
        float height = 0.9333f;
        branch = new Branch(region, width, height);
        float x = 0.86f;
        float y = 0.67f;
        float z = -1.0f;
        branch.setPosition(x, y, z);
    }

    @Override public void release() {
        super.release();
        rippler.release();
        glDeleteProgram(stencilProgramHandle);
        stencilProgramHandle = -1;
        if (atlasInfo != null) {
            int[] values = new int[1];
            values[0] = atlasInfo.textureHandle;
            glDeleteTextures(1, values, 0);
            GlUtil.checkGlError("Release " + getClass().getSimpleName());
        }
    }
}