package com.tme.moments.presentation.preview;

import android.support.v4.app.Fragment;

import com.tme.moments.presentation.drawer.DrawerFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.android.support.FragmentKey;
import dagger.multibindings.IntoMap;

/**
 * @author zoopolitic.
 */
@Module(subcomponents = DrawerFragment.Component.class)
public abstract class PreviewFragments {

    @Binds
    @IntoMap
    @FragmentKey(DrawerFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindDrawerFragmentInjectorFactory(DrawerFragment.Component.Builder builder);
}
