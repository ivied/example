package com.tme.moments.presentation.audio.player;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.tme.moments.R;
import com.tme.moments.presentation.SimpleSeekBarChangeListener;
import com.tme.moments.presentation.base.BaseFragment;
import com.tme.moments.presentation.core.Config;
import com.tme.moments.presentation.utils.AndroidUtils;
import com.tme.moments.presentation.utils.MediaUtils;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import timber.log.Timber;

import static android.databinding.DataBindingUtil.bind;

/**
 * @author zoopolitic.
 */
public class AudioPlayerFragment extends BaseFragment {

    @Subcomponent
    public interface Component extends AndroidInjector<AudioPlayerFragment> {
        @Subcomponent.Builder
        abstract class Builder extends AndroidInjector.Builder<AudioPlayerFragment> {
        }
    }

    private static final String TAG = AudioPlayerFragment.class.getSimpleName();

    public interface OnAudioClearListener {
        void onAudioCleared();
    }

    private static final long PLAYER_UPDATE_INTERVAL_MILLIS = 100;

    @Inject
    Config config;

    private AudioPlayerFragmentProxyBinding binding;
    private AudioPlayerViewModel model;
    @Nullable
    private MediaPlayer mediaPlayer;
    private OnAudioClearListener onAudioClearListener;
    private String currentPath;

    public static AudioPlayerFragment newInstance() {
        Bundle args = new Bundle();
        AudioPlayerFragment fragment = new AudioPlayerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        try {
            onAudioClearListener = (OnAudioClearListener) context;
        } catch (ClassCastException e) {
            throw new IllegalStateException(getClass().getSimpleName()
                    + " should only be attached to activity that implements "
                    + OnAudioClearListener.class.getSimpleName());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(
                AndroidUtils.isScreenSmall(getContext()) ?
                        R.layout.audio_player_small_fragment :
                        R.layout.audio_player_large_fragment,
                container,
                false
        );
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = new AudioPlayerFragmentProxyBinding(bind(view));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupAudioPlayerControls();
        model = ViewModelProviders.of(this).get(AudioPlayerViewModel.class);
        setupViewModel(model);
        String path = config.getAudioFilePath();
        init(path);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (currentPath != null) {
            init(currentPath);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseMediaPlayer();
    }

    @SuppressWarnings("ConstantConditions")
    public void changeFile(@NonNull String newPath) {
        if (!newPath.equals(currentPath)) {
            init(newPath);
        }
    }

    private void init(@NonNull String path) {
        //noinspection ConstantConditions
        if (path == null) {
            throw new NullPointerException("Can't init AudioPlayerFragment, audio file path is null");
        }
        MediaExtractor extractor = new MediaExtractor();
        try {
            extractor.setDataSource(path);
        } catch (final IOException e) {
            e.printStackTrace();
            Timber.e(e);
            onAudioClearListener.onAudioCleared();
            return;
        }
        int trackIndex = MediaUtils.selectTrack(extractor, "audio/");
        if (trackIndex < -1) {
            Timber.e("Couldn't select track. Audio path=" + path);
            onAudioClearListener.onAudioCleared();
            return;
        }
        binding.seekBar.setRecording(false);
        binding.seekBar.setProgress(0);
        binding.playedTime.setText(formatDuration(0));
        extractor.selectTrack(trackIndex);
        MediaFormat format = extractor.getTrackFormat(trackIndex);
        String name = getAudioName(path);
        long durationUs = MediaUtils.extractLong(format, MediaFormat.KEY_DURATION, 0);
        long durationMillis = durationUs / 1000;
        model.getName().setValue(name);
        model.getTotalDurationMillis().setValue(durationMillis);
        model.getPlayedDuration().setValue(null);
        model.getActive().setValue(true);
        model.getPlaying().setValue(false);
        if (!setupPlayer(path)) {
            onAudioClearListener.onAudioCleared();
            return;
        }
        currentPath = path;
        binding.play.setOnClickListener(v -> {
            if (mediaPlayer != null) {
                model.getPlaying().setValue(!mediaPlayer.isPlaying());
            }
        });
    }

    private String getAudioName(@NonNull String path) {
        String where = String.format("%s=?", MediaStore.Audio.Media.DATA);
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor cursor = getActivity().getContentResolver().query(
                uri,
                null,
                where,
                new String[]{path},
                null);

        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            String name = cursor.getString(8);
            cursor.close();
            return name;
        }

        MediaMetadataRetriever metaRetriver;
        metaRetriver = new MediaMetadataRetriever();
        metaRetriver.setDataSource(path);
        return metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
    }

    private void setupViewModel(AudioPlayerViewModel model) {
        model.getName().observe(this, name -> binding.songName.setText(name));
        model.getPlayedDuration().observe(this, playedDuration -> {
            if (playedDuration == null) {
                return;
            }
            binding.playedTime.setText(formatDuration(playedDuration));
            binding.seekBar.setProgress(playedDuration.intValue());
        });
        model.getTotalDurationMillis().observe(this, totalMillis -> {
            if (totalMillis == null) {
                totalMillis = 0L;
            }
            binding.totalDuration.setText(formatDuration(totalMillis));
        });
        model.getActive().observe(this, isActive -> {
            if (isActive == null) {
                isActive = true;
            }
            if (isActive) {
                setViewGroupEnabled(binding.root, true);
            } else {
                if (mediaPlayer != null) {
                    mediaPlayer.stop();
                }
                binding.seekBar.removeCallbacks(updateCallback);
                setViewGroupEnabled(binding.root, false);
            }
        });
        model.getPlaying().observe(this, isPlaying -> {
            if (isPlaying == null) {
                isPlaying = false;
            }
            if (isPlaying) {
                if (mediaPlayer != null) {
                    mediaPlayer.start();
                }
                binding.seekBar.postDelayed(updateCallback, PLAYER_UPDATE_INTERVAL_MILLIS);
            } else {
                binding.seekBar.removeCallbacks(updateCallback);
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                }
            }
            binding.play.setImageDrawable(getThemedDrawable(isPlaying ? R.attr.audioPauseIcon : R.attr.audioPlayIcon));
        });
    }

    private void setViewGroupEnabled(ViewGroup viewGroup, boolean enabled) {
        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            viewGroup.getChildAt(i).setEnabled(enabled);
        }
    }

    public String formatDuration(long millis) {
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis));
        return String.format(Locale.ENGLISH, "%02d:%02d", minutes, seconds);
    }

    private void releaseMediaPlayer() {
        if (mediaPlayer == null) {
            return;
        }
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
    }

    private void setupAudioPlayerControls() {
        SeekBar seekBar = binding.seekBar;
        ImageView clearSong = binding.clearAudio;
        ImageView play = binding.play;
        clearSong.setOnClickListener(v -> {
            releaseMediaPlayer();
            currentPath = null;
            onAudioClearListener.onAudioCleared();
        });
        play.setOnClickListener(v -> {
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                } else {
                    mediaPlayer.start();
                }
            }
        });
        seekBar.setOnSeekBarChangeListener(new SimpleSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                config.setAudioTimeMillis(progress);
                if (fromUser) {
                    seekBar.removeCallbacks(updateCallback);
                    if (mediaPlayer != null) {
                        mediaPlayer.seekTo(progress);
                    }
                    updateAudioPlayerInfo();
                }
            }
        });
    }

    private final Runnable updateCallback = new Runnable() {
        @Override
        public void run() {
            if (mediaPlayer != null) {
                updateAudioPlayerInfo();
            }
        }
    };

    private void updateAudioPlayerInfo() {
        if (mediaPlayer != null) {
            int millis = mediaPlayer.getCurrentPosition();
            model.getPlayedDuration().setValue((long) millis);
            if (mediaPlayer.isPlaying()) {
                binding.seekBar.postDelayed(updateCallback, PLAYER_UPDATE_INTERVAL_MILLIS);
            }
        }
    }

    private boolean setupPlayer(String path) {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setLooping(true);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(path);
            mediaPlayer.prepare();
            SeekBar seekBar = binding.seekBar;
            seekBar.setMax(mediaPlayer.getDuration());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            Timber.e(e);
            return false;
        }
    }

    public void onRecordStarted() {
        model.getActive().postValue(false);
        binding.seekBar.setRecording(true);
        binding.play.setImageDrawable(getThemedDrawable(R.attr.audioPauseIcon));
    }

    public void onProgressChanged(long audioTimeMillis) {
        model.getPlayedDuration().postValue(audioTimeMillis);
    }
}
