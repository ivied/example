package com.tme.moments.presentation.scene.f1.love;

import android.graphics.PointF;
import android.util.Pair;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Lips extends Decal {

    private float t;
    private float speed;
    private float alphaSpeed;
    private float startDelayNanos;
    private boolean rotate;
    private final List<Pair<TextureRegion, PointF>> infos;
    private boolean inflated;
    private boolean finished;
    private boolean scaleUp;
    private float pulseUpSpeed;
    private float pulseDownSpeed;
    private float scaleWaitTime;
    private float minScale;
    private float maxScale;
    private float scaleDecreaseValue;
    private Interpolator interpolator;
    private float endAngle;
    private float fadeOutScaleThreshold;

    Lips(List<Pair<TextureRegion, PointF>> infos) {
        this.infos = infos;
        init();
        interpolator = new OvershootInterpolator(2.5f);
    }

    private void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    boolean isFinished() {
        return finished;
    }

    void reset() {
        finished = false;
        init();
    }

    void step(long deltaNanos) {
        if (finished) {
            return;
        }
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }


        if (!inflated) {
            float deltaT = speed * deltaNanos;
            t += deltaT;
            t = Math.max(0, Math.min(1.0f, t));
            if (rotate) {
                float interpolatedAngle = t * endAngle;
                setRotationZ(interpolatedAngle);
            }
            float interpolatedScale = interpolator.getInterpolation(t);
            setScale(interpolatedScale);
            if (t == 1) {
                inflated = true;
            }
        } else {
            if (getScaleX() <= fadeOutScaleThreshold) {
                float deltaAlpha = alphaSpeed * deltaNanos;
                float newAlpha = Math.max(0, color.a - deltaAlpha);
                setAlpha(newAlpha);
                if (newAlpha == 0) {
                    finished = true;
                }
            }

            if (scaleWaitTime > 0) {
                scaleWaitTime -= deltaNanos;
            } else {
                if (scaleUp) {
                    float deltaScale = pulseUpSpeed * deltaNanos;
                    float newScale = Math.min(maxScale, getScaleX() + deltaScale);
                    setScale(newScale);
                    if (newScale == maxScale) {
                        scaleUp = false;
                    }
                } else {
                    float deltaScale = pulseDownSpeed * deltaNanos;
                    float newScale = Math.max(minScale, getScaleX() - deltaScale);
                    setScale(newScale);
                    if (newScale == minScale) {
                        scaleUp = true;
                        scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(650);
                        minScale -= scaleDecreaseValue;
                        maxScale -= scaleDecreaseValue;
                    }
                }
            }
        }
    }

    private void init() {
        Pair<TextureRegion, PointF> info = infos.get(random(infos.size()));
        TextureRegion region = info.first;
        float width = info.second.x;
        float height = info.second.y;
        setTextureRegion(region);
        setDimensions(width, height);
        float x = random(-0.36f, 0.36f);
        float y = x < -0.1f ? random(-0.62f, 0.39f) : random(-0.33f, 0.39f);
        float z = -1.0f;
        setPosition(x, y, z);
        setScale(0);
        setAlpha(1);
        t = 0;
        speed = 1.75f / Const.NANOS_IN_SECOND;
        inflated = false;
        alphaSpeed = 1.5f / Const.NANOS_IN_SECOND;
        setStartDelay(random(3000, 5000));
        scaleWaitTime = TimeUnit.MILLISECONDS.toNanos(650);

        fadeOutScaleThreshold = 0.5f;
        scaleUp = true;
        pulseUpSpeed = 1.5f / Const.NANOS_IN_SECOND;
        pulseDownSpeed = 1.5f / Const.NANOS_IN_SECOND;
        scaleDecreaseValue = 0.2f;
        float initialScale = 1.0f;
        minScale = initialScale - scaleDecreaseValue;
        maxScale = initialScale + scaleDecreaseValue;
        boolean rotateLeft = random();
        float minAngle = rotateLeft ? 325 : -360;
        float maxAngle = rotateLeft ? 360 : -325;
        endAngle = random(minAngle, maxAngle);
        rotate = random();
        setRotationZ(0);
    }
}
