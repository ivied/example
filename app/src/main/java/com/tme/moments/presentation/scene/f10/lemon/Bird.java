package com.tme.moments.presentation.scene.f10.lemon;

import com.tme.moments.graphics.AnimatedSprite;
import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.math.MathUtils;
import com.tme.moments.presentation.Const;

import java.util.List;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Bird extends AnimatedSprite {

    private float p0x, p0y; // start point
    private float p1x, p1y; // control point (bezier)
    private float p2x, p2y; // end point

    private float speed;
    private float t;

    private final List<List<FrameInfo>> birds;
    private float startDelayNanos;
    private boolean moveLeft;

    Bird(TextureRegion region, List<List<FrameInfo>> birds, float width, float height) {
        super(region, birds.get(0), width, height);
        this.birds = birds;
        init();
        setStartDelay(6000); // initial delay
    }

    private void setStartDelay(float startDelayMillis) {
        this.startDelayNanos = startDelayMillis * Const.NANOS_IN_MILLISECOND;
    }

    @Override protected int getDesiredFPS() {
        return 20;
    }

    @Override public void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }

        float deltaT = speed * deltaNanos;
        t += deltaT;

        float x = (1 - t) * (1 - t) * p0x + 2 * (1 - t) * t * p1x + t * t * p2x;
        float y = (1 - t) * (1 - t) * p0y + 2 * (1 - t) * t * p1y + t * t * p2y;

        float t1 = t + 0.3f;
        float x1 = (1 - t1) * (1 - t1) * p0x + 2 * (1 - t1) * t1 * p1x + t1 * t1 * p2x;
        float y1 = (1 - t1) * (1 - t1) * p0y + 2 * (1 - t1) * t1 * p1y + t1 * t1 * p2y;
        float angle = MathUtils.angleBetweenLines(x, y, x, y1, x, y, x1, y1);
        if (angle != 0) {
            angle += moveLeft ? 90 : -90;
            setRotationZ(y1 >= y ? -angle : -angle + 180);
        }

        setX(x);
        setY(y);

        if (t >= 1) {
            init();
        }
        super.step(deltaNanos);
    }

    private void init() {
        this.frames = birds.get(random(birds.size()));
        moveLeft = random();
        if (moveLeft) {
            p0x = 1 + getWidth() / 2;
            p0y = random(-0.4f, 1f);
            p1x = random(-0.5f, 0.5f);
            p1y = random(-0.4f, 0.75f);
            p2x = random(-1 - getWidth() / 2, -1 - getWidth());
            p2y = random(-0.4f, 1f);
        } else {
            p0x = -1 - getWidth() / 2;
            p0y = random(-0.4f, 1f);
            p1x = random(-0.5f, 0.5f);
            p1y = random(-0.4f, 0.75f);
            p2x = random(1 + getWidth() / 2, 1 + getWidth());
            p2y = random(-0.4f, 1f);
        }
        setScaleX(moveLeft ? -1 : 1);
        setPosition(p0x, p0y, -1.0f);
        setStartDelay(random(5000, 7000));
        speed = random(0.25f, 0.35f) / Const.NANOS_IN_SECOND;
        t = 0;
    }
}
