package com.tme.moments.presentation.drawer;

import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;
import com.tme.moments.R;

import java.util.List;

/**
 * @author zoopolitic.
 */
@AutoValue
public abstract class DrawerVideoLengthItem implements DrawerItem {

    @Override public int getLayoutResId() {
        return R.layout.drawer_video_length_item;
    }

    public static DrawerVideoLengthItem create(@NonNull List<Integer> lengths) {
        return new AutoValue_DrawerVideoLengthItem(lengths);
    }

    public abstract List<Integer> lengths();
}
