package com.tme.moments.presentation.scene.f13.rasta;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Weed extends Decal {

    private enum Direction {
        LEFT, TOP, RIGHT, BOTTOM
    }

    private float speedY;
    private float speedX;
    private float rotationSpeed;
    private float alphaSpeed;
    private long startDelayNanos;
    private boolean finished;
    private float maxSide;

    Weed(TextureRegion region, float width, float height) {
        setTextureRegion(region);
        setDimensions(width, height);
        Direction[] directions = Direction.values();
        Direction direction = directions[random(0, directions.length)];
        float x = 0;
        float y = 0;
        float z = -1.0f;
        float minMainSpeed = 0.25f;
        float maxMainSpeed = 0.35f;
        float minSecondarySpeed = -0.3f;
        float maxSecondarySpeed = 0.3f;
        switch (direction) {
            case LEFT:
                x = random(-0.6f, -0.5f);
                y = random(-0.3f, 0.75f);
                speedX = -random(minMainSpeed, maxMainSpeed) / Const.NANOS_IN_SECOND;
                speedY = random(minSecondarySpeed, maxSecondarySpeed) / Const.NANOS_IN_SECOND;
                break;
            case TOP:
                x = random(-0.75f, 0.75f);
                y = random(0.65f, 0.75f);
                speedX = random(minSecondarySpeed, maxSecondarySpeed) / Const.NANOS_IN_SECOND;
                speedY = random(minMainSpeed, maxMainSpeed) / Const.NANOS_IN_SECOND;
                break;
            case RIGHT:
                x = random(0.5f, 0.6f);
                y = random(-0.3f, 0.75f);
                speedX = random(minMainSpeed, maxMainSpeed) / Const.NANOS_IN_SECOND;
                speedY = random(minSecondarySpeed, maxSecondarySpeed) / Const.NANOS_IN_SECOND;
                break;
            case BOTTOM:
                x = random(-0.65f, 0.65f);
                y = random(-0.3f, -0.4f);
                speedX = random(minSecondarySpeed, maxSecondarySpeed) / Const.NANOS_IN_SECOND;
                speedY = -random(minMainSpeed, maxMainSpeed) / Const.NANOS_IN_SECOND;
                break;
        }
        rotationSpeed = random(-180f, 180f) / Const.NANOS_IN_SECOND;
        alphaSpeed = 0.75f / Const.NANOS_IN_SECOND;
        setScale(random(0.4f, 0.9f));
        setAlpha(0);
        setPosition(x, y, z);
        maxSide = Math.max(getWidth(), getHeight());
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float deltaX = speedX * deltaNanos;
        float deltaY = speedY * deltaNanos;
        float deltaRotation = rotationSpeed * deltaNanos;
        translate(deltaX, deltaY, 0);
        rotateZ(deltaRotation);

        float alpha = color.a;
        if (alpha < 1) {
            float deltaAlpha = alphaSpeed * deltaNanos;
            setAlpha(Math.min(alpha + deltaAlpha, 1.0f));
        }

        float minY = -1 - maxSide;
        float maxY = 1 + maxSide;
        float minX = -1 - maxY;
        float maxX = 1 + maxY;
        float x = getX();
        float y = getY();
        if (x <= minX || x >= maxX || y <= minY || y >= maxY) {
            finished = true;
        }
    }

    public boolean isFinished() {
        return finished;
    }
}
