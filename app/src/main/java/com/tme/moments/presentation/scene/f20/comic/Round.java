package com.tme.moments.presentation.scene.f20.comic;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import java.util.concurrent.TimeUnit;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class Round extends Decal {

    private final float velocity;
    private final float speedNanos;
    private float rotationSpeedNanos;
    private float alphaSpeedNanos;
    private float currentSpeedNanos;

    private final float startX;
    private final float endX;
    private final float startY;
    private final float endY;

    private float startDelayNanos;
    private boolean finished;

    Round(TextureRegion region, float width, float height, float startX, float endX,
          float startY, float endY, float speedPerSecond, float velocityPerSecond) {
        this.startX = startX;
        this.endX = endX;
        this.startY = startY;
        this.endY = endY;
        setTextureRegion(region);
        setDimensions(width, height);
        setColor(1, 1, 1, 1);
        init();
        setStartDelay(random(1000, 2000));
        speedNanos = speedPerSecond / Const.NANOS_IN_SECOND;
        velocity = velocityPerSecond / Const.NANOS_IN_SECOND;
    }

    void setStartDelay(long millis) {
        startDelayNanos = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    void step(long deltaNanos) {
        if (finished) {
            return;
        }
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float alpha = color.a;
        if (alpha < 1) {
            float deltaAlpha = alphaSpeedNanos * deltaNanos;
            float newAlpha = Math.min(1, alpha + deltaAlpha);
            setAlpha(newAlpha);
        }
        float deltaRotation = rotationSpeedNanos * deltaNanos;
        rotateZ(deltaRotation);

        float delta = currentSpeedNanos * deltaNanos;
        currentSpeedNanos += velocity;
        translateX(delta);
        float x = getX();

        float xPathLength = Math.abs(endX - startX);
        float fraction = Math.abs(x - startX) / xPathLength;

        float yPathLength = endY - startY;
        float y = startY + fraction * yPathLength;
        setY(y);

        // goes out of screen
        if ((speedNanos > 0 && x > endX) || (speedNanos < 0 && x < endX)) {
            init();
            finished = true;
        }
    }

    private void init() {
        currentSpeedNanos = speedNanos;
        rotationSpeedNanos = (random() ? 450.0f : -450.0f) / Const.NANOS_IN_SECOND;
        alphaSpeedNanos = 0.75f / Const.NANOS_IN_SECOND;
        setAlpha(0);
        setPosition(startX, startY, -1.0f);
    }
}
