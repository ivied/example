package com.tme.moments.presentation.scene.f19.purple.fantasy;

import com.tme.moments.graphics.TextureRegion;
import com.tme.moments.graphics.g3d.Decal;
import com.tme.moments.presentation.Const;

import static com.tme.moments.math.MathUtils.random;

/**
 * @author zoopolitic.
 */
class FallingObject extends Decal {

    private float speedY;
    private float speedX;
    private float rotationSpeed;
    private long startDelayNanos;
    private float minY;
    private float minX;
    private float maxX;
    private boolean finished;
    private float minRotationSpeed;
    private float maxRotationSpeed;

    FallingObject(TextureRegion region, float width, float height, float x, float y,
                  float minRotationSpeed, float maxRotationSpeed) {
        this.minRotationSpeed = minRotationSpeed;
        this.maxRotationSpeed = maxRotationSpeed;
        setTextureRegion(region);
        setDimensions(width, height);
        init(x, y);
    }

    private void init(float x, float y) {
        float z = -1.0f;
        speedY = -random(0.2f, 0.55f) / Const.NANOS_IN_SECOND;
        speedX = random(-0.6f, 0.0f) / Const.NANOS_IN_SECOND;
        setColor(1, 1, 1, 1);
        minY = -1 - getHeight() / 2;
        minX = -1 - getWidth() / 2;
        maxX = 1 + getWidth() / 2;
        rotationSpeed = random(minRotationSpeed, maxRotationSpeed) / Const.NANOS_IN_SECOND; // degrees per second
        setPosition(x, y, z);
    }

    public boolean isFinished() {
        return finished;
    }

    void step(long deltaNanos) {
        if (startDelayNanos > 0) {
            startDelayNanos -= deltaNanos;
            return;
        }
        float deltaX = speedX * deltaNanos;
        float deltaY = speedY * deltaNanos;
        translate(deltaX, deltaY, 0);

        float deltaRotation = rotationSpeed * deltaNanos;
        rotateZ(deltaRotation);

        float x = getX();
        float y = getY();
        if (y <= minY || x <= minX || x >= maxX) {
            finished = true;
        }
    }
}
