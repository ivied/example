#include <jni.h>
#include <string>

extern "C"
JNIEXPORT int JNICALL
Java_com_tme_moments_presentation_Rippler_prepareNextFrame(JNIEnv *env,
                                                           jobject,
                                                           jintArray jHeightMap,
                                                           jint heightMapWidth,
                                                           jint heightMapHeight,
                                                           jint index,
                                                           jintArray jHeightMapOld,
                                                           jintArray jHeightMapForTexture) {
    jint *heightMap;
    jint *heightMapOld;
    jint *heightMapForTexture;

    heightMap = (*env).GetIntArrayElements(jHeightMap, false);
    heightMapOld = (*env).GetIntArrayElements(jHeightMapOld, false);
    heightMapForTexture = (*env).GetIntArrayElements(jHeightMapForTexture, false);

    for (int y = 0; y < heightMapHeight; y++) {
        for (int x = 0; x < heightMapWidth; x++) {
            int pixHeight;
            if (x == heightMapWidth - 1) {
                pixHeight = (heightMap[index - heightMapWidth]
                             + heightMap[heightMapWidth + index]
                             + heightMap[index - 1]) >> 1;
            } else if (x == 0) {
                pixHeight = (heightMap[index - heightMapWidth]
                             + heightMap[heightMapWidth + index]
                             + heightMap[index + 1]) >> 1;
            } else {
                pixHeight = (heightMap[index - heightMapWidth]
                             + heightMap[index + heightMapWidth]
                             + heightMap[index - 1]
                             + heightMap[index + 1]) >> 1;
            }
            pixHeight -= heightMapOld[index];
            pixHeight -= pixHeight >> 5;
            int pixHeightText = pixHeight + 10;
            if (pixHeightText < 0) {
                pixHeightText = 0;
            }
            int argb = 0xff000000 + pixHeightText;
            heightMapOld[index] = pixHeight;
            heightMapForTexture[index] = argb;
            index++;
        }
    }
    (*env).ReleaseIntArrayElements(jHeightMap, heightMap, 0);
    (*env).ReleaseIntArrayElements(jHeightMapOld, heightMapOld, 0);
    (*env).ReleaseIntArrayElements(jHeightMapForTexture, heightMapForTexture, 0);
    return 0;
}
